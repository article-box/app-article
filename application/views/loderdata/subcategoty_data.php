<?php include('time_3321313654kjhdsj2ds1231s1ds.php'); ?>
<h2 class="block-title mt3" data-title="For You">Categories</h2>
	<div class="row">
		<?php foreach($this->wp_connection->blog_category() as $category){ if($category->status==1){?>
			<div class="col-sm-4 col-md-4">
				<div class="category-block" >
					<h3 class="title-middle title-border width-auto category ">
						<?=$category->category_name?>		
					</h3>
					<?php $i=0; foreach($this->wp_connection->select_blog() as $blog){
						if($blog->status==1){
							if($category->category_id==$blog->category_id){
									if($i++ == 3)break;
					?>
					<div class="card mb-2">
						<p class="" id="card_header">
							<a href="<?=site_url('welcome/blog_details/'.$blog->url_slug);?>">
								<?=$blog->blog_title?> 
							</a>
						</p>

						<div class="card-body">
							<p><?=$blog->header_text;?></p>
							<div class="meta">
								<?php foreach($this->wp_connection->select_sub_categories() as $sub_categories){
									if($sub_categories->id==$blog->sub_category_id){
								?>
									<span class="author category">
										<?=$sub_categories->sub_categories_name?> 
									</span>
								<?php } } ?>
								<span class="date"> 
									<i class="fa fa-history p2"></i> 
									<?php echo time_elapsed_string($blog->time); ?>
								</span>
							</div>
						</div>
					</div>
					<?php } } }  ?>
					
					<a href="<?=site_url('welcome/category/'.$category->url_slug);?>" class="category-more">
						<span class="author category"> Continue to the Category </span>
						<img style="height: 64px; width: 64px;" src="<?=base_url();?>images/icons/wood2right.png">
					</a>
				</div>					
			</div>
		<?php } }?>
		</div>
	</div>