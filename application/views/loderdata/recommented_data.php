
			<div class="col-md-12">
				<h2 class="block-title mv5" data-title="for you">
					Recommended
					<a href="<?=site_url('welcome/mypost');?>" class="category-more text-right hidden-xs">Continue to the Latest <img src="<?=base_url();?>tana/images/arrow-right.png" alt="Arrow"></a>
				</h2>
				<div class="m-dimension-carousel news-block" data-col="5" data-row="1">
					<div class="swiper-container carousel-container">
					    <div class="swiper-wrapper">
					    	<?php $i=0; foreach($this->wp_connection->orderbyblog() as $blog ){ if($blog->status==1){ if($blog->star==1){ if($i++ == 6) break; ?>
					    		<div class="swiper-slide" style="padding:5px;">
					    			<div class="category-block articles">
					    				<div class="post hover-dark">
											<div class="image1 video-frame">
												<img style="border-radius: 10px 10px 0px 0px;"  src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>"/>
											</div>
											<div class="meta">
												<?php foreach($this->wp_connection->blog_category() as $category){ if($blog->category_id==$category->category_id){ ?>
													<span class="author category"><?=$category->category_name?></span>
													<span class="date"> <i class="fa fa-history p2"></i> <?php echo time_elapsed_string($blog->time); ?></span>
												<?php } }?>
											</div>
											<?php 
			                                    $blog_title = $blog->blog_title;
			                                    if (strlen($blog_title) > 50) {
			                                        $stringCut = substr($blog_title, 0, 50);
			                                        $endPoint = strrpos($stringCut, ' ');
			                                        $blog_title = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
			                                        $blog_title .= '<a title="SHOW MORE.." href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > ...</a>';
			                                    }
			                                ?>
											<h4 title="<?=$blog->blog_title?>"><a href="<?=site_url('welcome/blog_details/'.$blog->url_slug);?>"><?=$blog_title?></a></h4>
											<?php 
					                            $string = $blog->header_text;
					                            if (strlen($string) > 80) {
					                                // truncate string
					                                $stringCut = substr($string, 0, 80);
					                                $endPoint = strrpos($stringCut, ' ');

					                                //if the string doesn't contain any space then it will cut without word basis.
					                                $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
					                                $string .= '<a title="Show More..." href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > ...</a>';
					                            }
					                            echo '<p>'.$string.'</p>';
					                        ?>
										</div>
					    			</div>
					    		</div>
					    	<?php }} } ?>	    							
						</div>
						<div class="pagination-next-prev mt3">
							<a href="javascript:;" class="swiper-button-prev arrow-link" title="Prev"><img src="<?=base_url();?>tana/images/arrow-left.png" alt="Arrow"></a>
							<a href="javascript:;" class="swiper-button-next arrow-link" title="Next"><img src="<?=base_url();?>tana/images/arrow-right.png" alt="Arrow"></a>
						</div>
					</div>
				</div>
			</div>