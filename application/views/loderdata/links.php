<?php foreach($this->wp_connection->logo() as $logo); ?>
    <link rel="shortcut icon" type="image/x-icon" href="<?=base_url();?>images/logo/<?=$logo->logo_img?>"/>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <!-- <link rel="stylesheet" type="text/css" href="<?=base_url();?>tana/vendors/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>tana/vendors/bootstrap/css/bootstrap-theme.min.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Fontawesome -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>tana/vendors/font-awesome/css/font-awesome.min.css">

    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>tana/vendors/animate.css">

    <!-- Swiper -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>tana/vendors/swiper/css/swiper.min.css">

    <!-- Magnific-popup -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>tana/vendors/magnific-popup/magnific-popup.css">

    <!-- Base MasterSlider style sheet -->
    <link rel="stylesheet" href="<?=base_url();?>tana/vendors/masterslider/style/masterslider.css" />
    
    <!-- Master Slider Skin -->
    <link href="<?=base_url();?>tana/vendors/masterslider/skins/default/style.css" rel='stylesheet' type='text/css'>
    
        
    <!-- Stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>tana/style.css">
    <script src="<?=base_url();?>tana/js/3.5.1-jquery.min.js"></script>
    <div id="fb-root"></div>

    <style type="text/css">
        [data-title] {          
          position: relative;
        }

        [data-title]:hover::before {
          content: attr(data-title);
          position: absolute;
          z-index: 99;
          bottom: -30px;
          display: inline-block;
          padding: 3px 6px;
          margin-left: -7px;
          border-radius: 2px;
          background: #000;
          color: #fff;
          font-size: 12px;
          font-family: sans-serif;
          white-space: nowrap;
        }
        [data-title]:hover::after {
          content: '';
          position: absolute;
          bottom: -10px;
          left: 8px;
          display: inline-block;
          color: #fff;
          border: 8px solid transparent;    
          border-bottom: 8px solid #000;
        }
        .like-animaction{
            width: auto; height: auto; color:green; position: absolute; right: 15px;
        }
        #messages{
            animation-name: like;
            animation-duration: 1s;
        }
        @keyframes like {
          0%   {bottom:5px; font-size: 5px; right: 15px;}
          10%  {bottom:10px; font-size: 6px; right: 13px;}
          20%  {bottom:15px; font-size: 8px; right: 12px;}
          30%  {bottom:20px; font-size: 10px; right: 11px;}
          40% {bottom:25px; font-size: 12px;right: 7px;}
          50% {bottom:30px; font-size: 14px;right: 6px;}
          60% {bottom:35px; font-size: 16px;right: 6px;}
          70% {bottom:40px; font-size: 18px;right: 5px;}
          80% {bottom:45px; font-size: 20px; right: 3px;}
          90% {bottom:50px; font-size: 22px; right: 0px;}
          100% {bottom:55px; font-size: 24px;right: -3px;}

        }

        @-webkit-keyframes placeHolderShimmer {
          0% {
            background-position: -468px 0;
          }
          100% {
            background-position: 468px 0;
          }
        }

        @keyframes placeHolderShimmer {
          0% {
            background-position: -468px 0;
          }
          100% {
            background-position: 468px 0;
          }
        }

        .content-placeholder {
          display: inline-block;
          -webkit-animation-duration: 1s;
          animation-duration: 1s;
          -webkit-animation-fill-mode: forwards;
          animation-fill-mode: forwards;
          -webkit-animation-iteration-count: infinite;
          animation-iteration-count: infinite;
          -webkit-animation-name: placeHolderShimmer;
          animation-name: placeHolderShimmer;
          -webkit-animation-timing-function: linear;
          animation-timing-function: linear;
          background: #f6f7f8;
          background: -webkit-gradient(linear, left top, right top, color-stop(8%, #eeeeee), color-stop(18%, #dddddd), color-stop(33%, #eeeeee));
          background: -webkit-linear-gradient(left, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
          background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
          -webkit-background-size: 800px 104px;
          background-size: 800px 104px;
          height: inherit;
          position: relative;
        }

        .post_data
        {
          padding:24px;
          border:1px solid #f9f9f9;
          border-radius: 5px;
          margin-bottom: 24px;
          /*box-shadow: 10px 10px 5px #eeeeee;*/
        }
    </style>