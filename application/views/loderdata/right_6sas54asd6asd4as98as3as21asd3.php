<div class="parallax-column">
                            <h3 class="title-middle title-border text-center"> 
                                <img class="right_data_icon" src="<?=base_url();?>images/icons/mewscar.png"/> 
                                Right Now
                            </h3>
                            <div class="category-block articles trending row">
                                <?php $i=0; foreach($this->wp_connection->orderbyblog() as $blog){ if($i++ == 10) break; ?>
                                <div class="col-xs-6 col-sm-12">
                                    <div class="post hover-light">
                                        <span class="fancy-number"><?=$i;?></span>
                                        <a href="#">
                                            <div class="image" data-src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>">
                                                <img class="" style="border-radius: 10px 10px 2px 2px;" src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>" alt="Proportion"/>
                                            </div>
                                        </a>
                                        <?php 
                                            $blog_title = $blog->blog_title;
                                            if (strlen($blog_title) > 50) {
                                                $stringCut = substr($blog_title, 0, 50);
                                                $endPoint = strrpos($stringCut, ' ');
                                                $blog_title = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                                $blog_title .= '<a title="SHOW MORE.." href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > ...</a>';
                                            }
                                        ?>
                                        <h4 title="<?=$blog->blog_title?>"><a href="<?=site_url('welcome/blog_details/'.$blog->url_slug);?>"><?=$blog_title?></a></h4>
                                    </div>
                                </div>
                                <?php } ?>  
                            </div>
                        </div>