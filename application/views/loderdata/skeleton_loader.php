    <html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Skeleton page</title>
    <style>
       body{ 
           border: 2px solid #f6f7f8;
          padding: 0px 100px; 
        }

        .shine {
          background: #f6f7f8;
          background-image: linear-gradient(to right, #f6f7f8 0%, #edeef1 20%, #f6f7f8 40%, #f6f7f8 100%);
          background-repeat: no-repeat;
          background-size: 800px 104px; 
          display: inline-block;
          position: relative; 
          
          -webkit-animation-duration: 1s;
          -webkit-animation-fill-mode: forwards; 
          -webkit-animation-iteration-count: infinite;
          -webkit-animation-name: placeholderShimmer;
          -webkit-animation-timing-function: linear;
          }

        box {
          height: 104px;
          width: 1100px;
        }

        .div {
          display: inline-flex;
          flex-direction: column; 
          margin-left: 0px;
          margin-top: 15px;
          vertical-align: top; 
        }

        lines {
          height: 10px;
          margin-top: 10px;
          width: 1100px; 
        }

        photo {
          display: block!important;
          width: 1100px; 
          height: 100px; 
          margin-top: 15px;
        }

        @-webkit-keyframes placeholderShimmer {
          0% {
            background-position: -468px 0;
          }
          
          100% {
            background-position: 468px 0; 
          }
        }


    </style>
</head>
<body>
    <div >
        <box class="shine"></box>
        <div class="div">
            <lines class="shine"></lines>
            <lines class="shine"></lines>
            <lines class="shine"></lines>
        </div>
        <photo class="shine"></photo>
        <photo class="shine"></photo>
        <br>
        <box class="shine"></box>
        <div class="div">
            <lines class="shine"></lines>
            <lines class="shine"></lines>
            <lines class="shine"></lines>
        </div>
    </div>
    
</body>
    <!-- <script>
        document.addEventListener("DOMContentLoaded", () => {
            // Remove the setTimeout in production
            setTimeout(() => document.getElementById("skeleton").style.display = 'none', 3000);
        });
    </script> -->
</html>
    