<?php include('time_3321313654kjhdsj2ds1231s1ds.php'); ?>
<div class="row">
			<div class="col-sm-8 col-md-4 sticky-column">
				<div class="theiaStickySidebar">
					<h2 class="block-title mv5">
						<img class="" src="<?=base_url();?>images/icons/satellite.png"/>
						Opinions
					</h2>
					<div class="category-block articles">
						<?php $i=0; foreach($this->wp_connection->orderbyblog() as $blog){
								 if($blog->status==1){ if($blog->star==1){ if($i++ == 1) break; ?>
						<div class="post first hover-dark shadow-box opinion_border p-2">
							<a href="<?=site_url('welcome/blog_details/'.$blog->url_slug);?>">
								<div class="image p2 ">
									<img src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>" alt="Proportion">
								</div>
							</a>
							<div class="meta">
								<?php foreach($this->wp_connection->blog_category() as $category){ 
									if($blog->category_id==$category->category_id){ ?>
									<span class="author category"><?=$category->category_name?></span>
									<span class="date"> 
										<i class="fa fa-history p2"></i> 
										<?php echo time_elapsed_string($blog->time); ?>
									</span>
								<?php } }?>
							</div>
							<h4>
								<a href="<?=site_url('welcome/blog_details/'.$blog->url_slug);?>"><?=$blog->blog_title?>
								</a>
							</h4>
							<p><?=$blog->header_text?></p>
						</div>
						<?php }}} ?>
						
						<?php $i=0; foreach($this->wp_connection->orderbyblog() as $blog ){ if($blog->status==1){
						if($i++ == 7)break; ?>
						<div class="post hover-light clear-left trend_ ">
							<?php 
                                $string1 = $blog->blog_title;
                                if (strlen($string1) > 50) {
                                    // truncate string
                                    $stringCut = substr($string1, 0, 50);
                                    $endPoint = strrpos($stringCut, ' ');
                                    //if the string doesn't contain any space then it will cut without word basis.
                                    $string1 = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                    $string1 .= '<a title="Show More..." href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > ...</a>';
                                }
                            ?>
							<h4><a href="#"><?php echo $string1; ?></a></h4>
							<?php 
                                $string = $blog->header_text;
                                if (strlen($string) > 50) {
                                    // truncate string
                                    $stringCut = substr($string, 0, 50);
                                    $endPoint = strrpos($stringCut, ' ');
                                    //if the string doesn't contain any space then it will cut without word basis.
                                    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                    $string .= '<a data-title="Show More" href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > ...</a>';
                                }
                                echo '<p>'.$string.'</p>';
                            ?>
							
						</div>
						<?php } }?>
					</div>
				</div>
			</div>
			<!-- end .col-4 -->

				
			<div class="col-sm-4 col-md-2 sticky-column ">
				<div class="theiaStickySidebar">
					<div class="mv4"></div>
					<h3 class="title-middle title-border text-center">
						<img class="right_data_icon" src="<?=base_url();?>images/icons/firecamp.png"/>
						Trending
					</h3>
					<div class="category-block articles">
						<?php $i=0; foreach($this->wp_connection->select_blog() as $blog){ if($blog->status==1){  if($i++ == 5)break; ?>
						<div class="post opinion_tranding_border">
							<h4 title="<?=$blog->blog_title?>">
								<a href="#"><?=$blog->blog_title?></a>
							</h4>
							<div class="meta">
								<?php foreach($this->wp_connection->blog_category() as $category){ if($blog->category_id==$category->category_id){ ?>
									<span class="author category"><?=$category->category_name?></span>
									<span class="date"> <i class="fa fa-history p2"></i> <?php echo time_elapsed_string($blog->time); ?></span>
								<?php } }?>
							</div>
							<?php 
                                $string = $blog->header_text;
                                if (strlen($string) > 50) {
                                    // truncate string
                                    $stringCut = substr($string, 0, 50);
                                    $endPoint = strrpos($stringCut, ' ');
                                    //if the string doesn't contain any space then it will cut without word basis.
                                    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                    $string .= '<a title="Show More..." href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > ...</a>';
                                }
                                echo '<p>'.$string.'</p>';
                            ?>
						</div>
						<?php } } ?>
					</div>
				</div>
				</div>

			<!-- end .col-4 -->
		
			<div class="col-sm-4 col-md-2 sticky-column sm-clear-left">
				<div class="theiaStickySidebar">
					<div class="mv4"></div>
					<h3 class="title-middle title-border text-center">
						<img class="right_data_icon" src="<?=base_url();?>images/icons/bigantenna.png"/>
						Top News
					</h3>
					<div class="category-block articles">
						<?php $i=0; foreach($this->wp_connection->select_blog() as $blog){ if($blog->status==1){  if($i++ == 5)break; ?>
						<div class="post opinion_topnews_border">
							<h4 title="<?=$blog->blog_title?>">
								<a href="#"><?=$blog->blog_title?></a>
							</h4>
							<div class="meta">
								<?php foreach($this->wp_connection->blog_category() as $category){ if($blog->category_id==$category->category_id){ ?>
									<span class="author category"><?=$category->category_name?></span>
									<span class="date"> 
										<i class="fa fa-history p2"></i> 
										<?php echo time_elapsed_string($blog->time); ?>
									</span>
								<?php } }?>
							</div>
							<?php 
                                $string = $blog->header_text;
                                if (strlen($string) > 40) {
                                    $stringCut = substr($string, 0, 40);
                                    $endPoint = strrpos($stringCut, ' ');
                                    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                    $string .= '<a title="Show More..." href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > ...</a>';
                                }
                                echo '<p>'.$string.'</p>';
                            ?>
							
						</div>
						<?php } } ?>
					</div>
				</div>
				</div>
			<div class="col-sm-8 col-md-4 sticky-column">
				<div class="theiaStickySidebar">
					<div class="mv4"></div>
					<div class="category-block articles">
						<div class="row">
							<?php $i=0; foreach($this->wp_connection->orderbylike() as $blog ){ if($blog->status==1){ if($i++ == 10) break; ?>
								<div class="col-xs-6 col-sm-6 mostlike_card_border">
									<div class="post hover-light">
										<div class="image video-frame">
											<a href="<?=site_url('welcome/blog_details/'.$blog->url_slug);?>">
												<img src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>" alt="Proportion"/>
											</a>
										</div>
										<div class="meta">
												<span class="author "> 
													<i class="fa fa-heart" aria-hidden="true" style="color:red;"></i> 
													<span class="like_digits_style"> 
														<?=$blog->likes?> 
													</span>
												</span>
												<span class="date"> 
													<i class="fa fa-history p2"></i> 
													<?php echo time_elapsed_string($blog->time); ?>
												</span>
										</div>
										<?php 
		                                    $blog_title = $blog->blog_title;
		                                    if (strlen($blog_title) > 40) {
		                                        $stringCut = substr($blog_title, 0, 40);
		                                        $endPoint = strrpos($stringCut, ' ');
		                                        $blog_title = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
		                                        $blog_title .= '<a title="SHOW MORE.." href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > ...</a>';
		                                    }
		                                ?>
										<h4><a href="<?=site_url('welcome/blog_details/'.$blog->url_slug);?>"><?=$blog_title?></a></h4>
									</div>
								</div>
							<?php } } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<img class="center_not_found_img" src="<?=base_url();?>images/icons/combinedarrows2.png" />