<?php include('time_3321313654kjhdsj2ds1231s1ds.php'); ?>
<?php foreach($this->wp_connection->blog_category() as $category){ if($category->status==1){?>
			<div class="row ">
				<div class="col-md-12">
					<h2 class="block-title mv5" data-title="for you">
						<?=$category->category_name?>
						<a href="<?=site_url('welcome/category/'.$category->url_slug);?>" class="category-more text-right hidden-xs">
							<span class="author category"> Continue to the Category </span>
							<img style="height: 64px; width: 64px;" src="<?=base_url();?>images/icons/wood2right.png">
						</a>
					</h2>
					<div class="">
					    <div class="large-12 columns">
					      <div class="owl-carousel owl-theme col-sm-12">
						    	<?php foreach($this->wp_connection->select_blog() as $blog ){ if($blog->status==1){ if($category->category_id==$blog->category_id){ ?>
						    		<div>
						    			<div class="category-block articles">
						    				<div class="post hover-dark">
												<div class="image1" data-src="">
													<img style="border-radius: 10px 10px 0px 0px;" src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>"/>
												</div>
												<div class="meta">
													<?php foreach($this->wp_connection->select_sub_categories() as $sub_categories){ if($sub_categories->id==$blog->sub_category_id){
														?>
														<span class="author category"><?=$sub_categories->sub_categories_name?></span>
													<?php } } ?>
													<span class="date"> <i class="fa fa-history p2"></i> <?php echo time_elapsed_string($blog->time); ?></span>
												</div>
												<?php 
				                                    $blog_title = $blog->blog_title;
				                                    if (strlen($blog_title) > 50) {
				                                        $stringCut = substr($blog_title, 0, 50);
				                                        $endPoint = strrpos($stringCut, ' ');
				                                        $blog_title = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
				                                        $blog_title .= '<a title="SHOW MORE.." href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > ...</a>';
				                                    }
				                                ?>
												<h4 title="<?=$blog->blog_title?>"><a href="<?=site_url('welcome/blog_details/'.$blog->url_slug);?>"><?=$blog_title?></a></h4>
												<?php 
						                            $string = $blog->header_text;
						                            if (strlen($string) > 80) {
						                                // truncate string
						                                $stringCut = substr($string, 0, 80);
						                                $endPoint = strrpos($stringCut, ' ');

						                                //if the string doesn't contain any space then it will cut without word basis.
						                                $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
						                                $string .= '<a title="Show More..." href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > ...</a>';
						                            }
						                            echo '<p>'.$string.'</p>';
						                        ?>
											</div>
						    			</div>
						    		</div>
						    	<?php } } } ?>
						    	<div class="p-1">
					    			<div class="category-block articles">
					    				<a href="<?=site_url('welcome/category/'.$category->url_slug);?>">
						    				<div class="show_more_style">
						    					<img src="<?=base_url();?>images/icons/circles.png" style="height: 64px; width: 64px;">
						    					<h2>
						    						More... 
						    						<i class="fa fa-chevron-circle-down" style="color:  #f5b041;" ></i> 
						    					</h2>
						    				</div>
					    				</a>
					    			</div>
					    		</div>		    									
							</div>							
						</div>
					</div>
		 		</div>
		 		<div class="col-md-12">
					<div class="mv3 divider_border"></div>
				</div>
			</div>
		<?php }} ?>


<script>
$(document).ready(function(){
  $('.owl-carousel').owlCarousel({
    loop:true,
    dots:false,
    margin:5,
    nav:false,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:false,
            loop:false
        },
        600:{
            items:3,
            nav:false,
            loop:false
        },
        1000:{
            items:5,
            nav:false,
            loop:false
        }
    }
  }) 
});
</script>