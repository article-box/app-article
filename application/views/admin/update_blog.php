<?php include('header.php'); ?>
<a href="<?=site_url('admin/add_blog');?>"> 
    <div class="hide__">
        <button class="btn btn-primary" id="on_click_load" title="Add More Blogs" style="position: fixed; bottom: 30px; right: 50px; border-radius:100%; z-index:999; padding:20px 20px;" >
            <div id="click_to_hide"> <i class="fas fa-folder-plus"></i> </div>
            <div id="click_to_show"> <i class="fas fa-sync-alt"></i> </div>
        </button> 
    </div>
</a>

    <!--  BEGIN CONTENT AREA  -->
    <div id="content" class="main-content">
        <div class="layout-px-spacing">
            <div class="row layout-top-spacing">
                <div class="col-lg-12 col-12 layout-spacing">
                    <div class="statbox widget box box-shadow">
                        <div class="row">
                            <div class="col float-left">
                            <a href="<?=site_url('admin/blogs');?>"> 
                                <button class="btn btn-dark" title="Click To View All List">
                                    <i class="fas fa-arrow-circle-left"></i> Article List 
                                </button> 
                            </a>
                            </div>
                            <div class="float-left">
                                <button class="btn btn-dark" title="Click To View All Blogs List">
                                    <i class="fas fa-pen-alt"></i> Update Articles
                                </button>
                            </div>
                        </div>
                        <div class="widget-content mt-3">
                            <div class="blog_control mb-2" id="blog_control"></div>
                            <?=form_open_multipart('admin/edit_blog/'.$blog->blog_id , array('id' => 'myform_'));?>

                                <div class="row mb-4">
                                    <div class="col input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroup-sizing-sm">Title</span>
                                        </div>
                                        <input type="text" name="blog_title" value="<?=$blog->blog_title?>"  class="form-control" required>
                                    </div>
                                </div>
                                
                                <div class="row mb-4 form-group">
                                    <div class="col form-group">
                                        <label> <i class="fas fa-file-alt"></i> Header Text </label>
                                        <textarea name="header_text" class="form-control" maxlength="255" placeholder="maxlength 255" required style="height: 100px;"><?=$blog->header_text?></textarea>
                                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                    </div>
                                </div>
                                
                                <div class="row mb-8">
                                    <div class="col form-group">
                                        <label> <i class="fas fa-file-alt"></i> Text </label>
                                        <textarea name="blog_text"><?=$blog->blog_text?></textarea>
                                        <script>
                                                window.onload = function() {
                                                CKEDITOR.replace( 'blog_text');
                                            };
                                        </script>
                                    </div>
                                </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col text-center">
                                        <label> <i class="fas fa-file-image"></i> Image </label>
                                        <div>
                                            <a href="#" id="openInputFile" > <i class="fas fa-marker text-primary"></i> Edit</a>
                                        </div>
                                        <img src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>" alt="Article Image" height="200">
                                        <div id="preview"> </div>
                                        <input type="file" name="blog_img" id="inputFileImage">
                                    </div>
                                </div>
                                
                                <div class="row mb-4">
                                    <div class="col form-group">
                                        <label> <i class="fas fa-file-alt"></i> Meta Title </label>
                                        <input type="text" value="<?=$blog->meta_title?>" name="meta_title" class="form-control">
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col form-group">
                                        <label> <i class="fas fa-file-alt"></i> Meta Keywords </label>
                                        <textarea class="form-control" name="meta_keyword"><?=$blog->meta_keyword?></textarea>
                                    </div>
                                </div>

                                <div class="row mb-4">
                                    <div class="col form-group">
                                        <label> <i class="fas fa-file-alt"></i> Meta Description </label>
                                        <textarea class="form-control" name="meta_description"><?=$blog->meta_description?></textarea>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary mb-2 mr-2">Update <i class="fas fa-marker"></i> </button>
                            </form>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="footer-wrapper">
            <div class="footer-section f-section-2">
                <?php include("load_data/tagline.php"); ?>
            </div>
        </div>
        </div>
    </div>
    <!--  END CONTENT AREA  -->
</div>
<script> 
   $(document).ready(function(){
      $('#blog_control').load('<?=site_url('admin/blog_control');?>');
    });
</script>
<script >
    $('#click_to_show').hide();
    $('.hide__').on('click', '#on_click_load', function(){ 
        $('#click_to_hide').hide();
        $('#click_to_show').show();
    });
</script>
<script>
    //------------- On Click to open file------------------//
    $(document).on("click", "#openInputFile", function(e) {
        e.preventDefault();
        $("#inputFileImage").click();
    })

    //------------- image preview------------------//
    $(document).on("change", "#inputFileImage", function() {
        var url = window.URL.createObjectURL(this.files[0]);
        var img = ' <p> Preview </p> <img src="' + url +
            '" alt="Image" class="shadow border p-1"> <div class=" rounded-circle" id="removePreviewImage"> <i class="fas fa-times-circle"></i> </div>';
        $("#preview").html(img);
        $("#submitImgBtn").show();
    });
    //-------------Remove image preview------------------//
    $(document).on("click", "#removePreviewImage", function() {
        var path = $("#inputFileImage").val("");
        $("#preview").html(" ");
        $("#submitImgBtn").hide();
    });
</script>

    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/plugins/highlight/highlight.pack.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
</body>
</html>