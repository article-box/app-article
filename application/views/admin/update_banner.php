<form id="bannerUpdate">
    <input type="hidden" name="id" value="<?=$banner->banner_id?>">
    <img src="<?= base_url(); ?>images/banner/<?= $banner->banner_img ?>" class="card-img-top mb-2" alt="widget-card-2">
    <div class="custom-file-container" data-upload-id="myFirstImage">
        <label>Upload (Single Image) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
        <label class="custom-file-container__custom-file" >
            <input type="file" name="banner_image" class="custom-file-container__custom-file__custom-file-input" accept="image/*">
            <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
            <span class="custom-file-container__custom-file__custom-file-control"></span>
        </label>
        <input type="text" name="banner_title" value="<?=$banner->banner_title?>" class="form-control" placeholder="Image Title" style="margin-top: 5px;" require>
        <input type="text" name="banner_text" value="<?=$banner->banner_text?>" class="form-control" placeholder="Banner Link" style="margin-top: 5px;" >
        <div class="custom-file-container__image-preview"></div>
    </div>
    <input type="submit" class="btn btn-dark" name="upload">
</form>

<script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
<script src="<?=base_url();?>dashboard/plugins/file-upload/file-upload-with-preview.min.js"></script>

<script>
    //First upload
    var firstUpload = new FileUploadWithPreview('myFirstImage')
</script>