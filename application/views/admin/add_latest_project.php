<?php include('header.php');?>

<!--  BEGIN CONTENT PART  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                	<div id="flStackForm" class="col-lg-12 layout-spacing layout-top-spacing">
                        <div class="statbox widget box box-shadow">
                            <div class="widget-header">                                
                                <div class="row">
                                <a href="<?=site_url('admin/show_latest_project');?>"> <button class="btn btn-dark" title="Click To show Project List"> <svg  width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-corner-up-left"><polyline points="9 14 4 9 9 4"></polyline><path d="M20 20v-7a4 4 0 0 0-4-4H4"></path></svg> Show Projects</button> </a>
                                       
                                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                        <h4> Add Project</h4>
                                    </div>                                                                        
                                </div>
                            </div>
                            <?=form_open_multipart('admin/ins_latest_project');?>
                                    <div class="row mb-4">
                                        <div class="col input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroup-sizing-sm">Title</span>
                                            </div>
                                            <input type="text" name="title" class="form-control" autocomplete="off" autofocus required>
                                        </div>
                                    </div>
                                    
                                    <div class="row mb-12">
                                        <div class="col input-group">
                                            <textarea name="text" class="form-control" ></textarea>
                                        <script>
                                             window.onload = function() {
                                                CKEDITOR.replace('text');
                                            };
                                        </script>
                                        </div>
                                    </div>
                                <div class="widget-content ">
    	                            <div class="custom-file-container" data-upload-id="myFirstImage">
    								    <label>Upload (Single Image) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
    								    <label class="custom-file-container__custom-file" >
    								        <input type="file" name="project_img" class="custom-file-container__custom-file__custom-file-input" accept="image/*">
    								        <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
    								        <span class="custom-file-container__custom-file__custom-file-control"></span>
    								    </label>
    								    <div class="custom-file-container__image-preview"></div>
    								</div>
                                    <input type="submit" class="form-control btn btn-dark" name="upload">
    							</div>
                            </form>
							<script type="text/javascript">
								var firstUpload = new FileUploadWithPreview('myFirstImage');
							</script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-wrapper">
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg> Nishant</p>
                </div>
            </div>
        </div>
        <!--  END CONTENT PART  -->
    </div>
    <!-- END MAIN CONTAINER -->
 <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/plugins/highlight/highlight.pack.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/file-upload/file-upload-with-preview.min.js"></script>

    <script>
        //First upload
        var firstUpload = new FileUploadWithPreview('myFirstImage')
        //Second upload
        var secondUpload = new FileUploadWithPreview('mySecondImage')
    </script>
    <!-- END PAGE LEVEL PLUGINS -->
</body>
</html>