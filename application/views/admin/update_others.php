<?php include('header.php'); ?>
        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div class="col-lg-10 col-12 layout-spacing">
                            <div class="statbox widget box box-shadow">
                                <div class=" ">
                                    <div class="row mb-4">
                                        <div class="col-6">
                                             <a href="<?=site_url('admin/gallery');?>"> <button class="btn btn-dark" title="Click To View Logo "> <svg  width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-corner-up-left"><polyline points="9 14 4 9 9 4"></polyline><path d="M20 20v-7a4 4 0 0 0-4-4H4"></path></svg> View Gallery</button> </a>
                                        </div>
                                        <div class="col-6">
                                            <img title="Logo Image" style="height: 50px; width: auto; background:white; padding:10px;" src="<?=base_url();?>images/others/<?=$imgs->img?>">
                                        </div>
                                    </div>
                                   <?=form_open_multipart('admin/edit_others/'.$imgs->others_id);?>
                                        <!--<div class="widget-content widget-content-area">-->
            	                            <div class="custom-file-container" data-upload-id="myFirstImage">
            								    <label>Update (Other Image) <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">x</a></label>
            								    <label class="custom-file-container__custom-file" >
            								        <input type="file" name="imgs" class="custom-file-container__custom-file__custom-file-input" accept="image/*">
            								        <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
            								        <span class="custom-file-container__custom-file__custom-file-control"></span>
                                                    <input type="text" name="name" value="<?=$imgs->name?>" class="form-control mt-2" placeholder="Image Name" required>
            								    </label>
            								    <div class="custom-file-container__image-preview"></div>
            								</div>
                                            <input type="submit" class="form-control btn btn-dark" title="Click To Update Logo" name="upload">
            							<!--</div>-->
                                    </form>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="footer-wrapper">
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg  width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg> Nishant</p>
                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->
    </div>




    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/plugins/highlight/highlight.pack.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
    
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/file-upload/file-upload-with-preview.min.js"></script>

    <script>
        //First upload
        var firstUpload = new FileUploadWithPreview('myFirstImage')
        //Second upload
        var secondUpload = new FileUploadWithPreview('mySecondImage')
    </script>
    <!-- END PAGE LEVEL PLUGINS -->
</body>
</html>