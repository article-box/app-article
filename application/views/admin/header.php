<?php
if($this->session->userdata("checksesn")!="asdf8s7df8dsf"){
redirect("admin/login");
}
?>
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Admin | Dashboard </title>
    <link rel="icon" type="image/x-icon" href="<?=base_url();?>dashboard/assets/img/favicon.ico"/>
    <link href="<?=base_url();?>dashboard/assets/css/loader.css" rel="stylesheet" type="text/css" />
    <script src="<?=base_url();?>dashboard/assets/js/loader.js"></script>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="<?=base_url();?>dashboard/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>dashboard/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="<?=base_url();?>dashboard/plugins/apex/apexcharts.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url();?>dashboard/assets/css/dashboard/dash_1.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>dashboard/plugins/table/datatable/datatables.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>dashboard/plugins/table/datatable/custom_dt_html5.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>dashboard/plugins/table/datatable/dt-global_style.css">
    <!-- END PAGE LEVEL STYLES -->

     <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?=base_url();?>dashboard/plugins/drag-and-drop/dragula/dragula.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>dashboard/plugins/drag-and-drop/dragula/example.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->    
    
    <!-- For File Uplode -->
    <link href="<?=base_url();?>dashboard/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>dashboard/plugins/file-upload/file-upload-with-preview.min.css" rel="stylesheet" type="text/css" />
    <!-- End FIle Uplode -->
    
    <!--  BEGIN CUSTOM STYLE FILE For Account Setting  -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>dashboard/plugins/dropify/dropify.min.css">
    <link href="<?=base_url();?>dashboard/assets/css/users/account-setting.css" rel="stylesheet" type="text/css" />
    <!--  END CUSTOM STYLE FILE  -->
    
    <!-- For Switch -->
    <!-- <link href="<?=base_url();?>dashboard/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" /> -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>dashboard/assets/css/forms/switches.css">
    <!-- End Switch -->

    <!-- For Sweet Alert -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?=base_url();?>dashboard/plugins/animate/animate.css" rel="stylesheet" type="text/css" />
    <script src="<?=base_url();?>dashboard/plugins/sweetalerts/promise-polyfill.js"></script>
    <link href="<?=base_url();?>dashboard/plugins/sweetalerts/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>dashboard/plugins/sweetalerts/sweetalert.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>dashboard/assets/css/components/custom-sweetalert.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- End Sweet alert -->

    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="<?=base_url();?>dashboard/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>dashboard/assets/css/components/tabs-accordian/custom-tabs.css" rel="stylesheet" type="text/css" />
    <!--  END CUSTOM STYLE FILE  -->

     <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>dashboard/assets/css/forms/theme-checkbox-radio.css">
    <link href="<?=base_url();?>dashboard/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>dashboard/assets/css/components/custom-media_object.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->

    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="<?=base_url();?>dashboard/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>dashboard/assets/css/components/custom-modal.css" rel="stylesheet" type="text/css" />
    <!--  END CUSTOM STYLE FILE  -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="<?=base_url();?>dashboard/assets/css/apps/notes.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url();?>dashboard/assets/css/forms/theme-checkbox-radio.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->

    <!-- For Cards -->
    <link href="<?=base_url();?>dashboard/assets/css/components/cards/card.css" rel="stylesheet" type="text/css" />
    <!-- End Cards -->
    <script src="<?=base_url();?>dashboard/ckeditor/ckeditor.js"></script>
    <!-- <script src="https://cdn.ckeditor.com/ckeditor5/31.0.0/balloon/ckeditor.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Additional style  -->
    <link href="<?=base_url();?>dashboard/assets/css/style.css" rel="stylesheet" type="text/css" />
    <!-- FontAwosham -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

</head>
<body>
    <!-- <?php 
        if(!$sock = @fsockopen('www.google.com', 80))
        {
            echo '<div style="height:auto; position:fixed; top:40%; left:48%; z-index:9999999999; width:100px; background: rgba(17, 103, 189,0.4); border-radius:5px;" class="shadow-sm"> <div class="align-self-center p-2"> <h1 style="text-align: center;  "> 
                    <svg height="50" width="50" viewBox="0 0 640 512"><path fill="currentColor" d="M634.91 154.88C457.74-8.99 182.19-8.93 5.09 154.88c-6.66 6.16-6.79 16.59-.35 22.98l34.24 33.97c6.14 6.1 16.02 6.23 22.4.38 145.92-133.68 371.3-133.71 517.25 0 6.38 5.85 16.26 5.71 22.4-.38l34.24-33.97c6.43-6.39 6.3-16.82-.36-22.98zM320 352c-35.35 0-64 28.65-64 64s28.65 64 64 64 64-28.65 64-64-28.65-64-64-64zm202.67-83.59c-115.26-101.93-290.21-101.82-405.34 0-6.9 6.1-7.12 16.69-.57 23.15l34.44 33.99c6 5.92 15.66 6.32 22.05.8 83.95-72.57 209.74-72.41 293.49 0 6.39 5.52 16.05 5.13 22.05-.8l34.44-33.99c6.56-6.46 6.33-17.06-.56-23.15z"></path></svg>
                </h1> <p style="font-size:10px; text-align:center;">No Network </p> </div>
            </div>';
        }
        else
        {
        echo ' ';
        }
        ?> -->
    <!-- BEGIN LOADER -->
    <div id="load_screen" style="background: #0e1726;"> <div class="loader"> <div class="loader-content">
        <div class="spinner-grow align-self-center"></div>
            <p class="align-self-center">Updating...</p>
        </div></div>
    </div>
    <!--  END LOADER -->
    <!--  BEGIN NAVBAR  -->
    <div class="header-container fixed-top">
        <header class="header navbar navbar-expand-sm">
            <ul class="navbar-item theme-brand flex-row  text-center">
                <li class="nav-item theme-logo">
                    <i class="fas fa-tachometer-alt"></i>
                </li>
                
                <li class="nav-item theme-text">
                    <a href="<?=site_url('admin/index');?>" class="nav-link">
                      <?php foreach($this->db_connection->select_signup() as $nam_){ if($nam_->email== $this->session->userdata('sesn')){ ?>
                              <p> <?=$nam_->name?> </p>  
                        <?php } }?>   
                    </a>
                </li>
            </ul>
            <ul class="navbar-item flex-row ml-md-0 ml-auto">
                <li class="nav-item align-self-center search-animated">
                    <svg  width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search toggle-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>
                    <form class="form-inline search-full form-inline search" role="search">
                        <div class="search-bar">
                            <input type="text" class="form-control search-form-control  ml-lg-auto" id="search_text1" placeholder="Search...">
                        </div>
                    </form>
                    <div id="result1" style="height: auto; width: 100%; border-radius: 8px; box-shadow: 0 4px 6px 0 rgba(32,33,36,0.28); background:none repeat scroll 0 0 #191e3a; position: absolute;">
                    </div>
                </li>
            </ul>
            <script>
                $(document).ready(function(){
                    // load_data();
                    function load_data(query)
                    {
                        $.ajax({
                            url:"<?php echo base_url(); ?>admin/fetch1",
                            method:"POST",
                            data:{query:query},
                            success:function(data){
                                $('#result1').html(data);
                            }
                        })
                    }
                    $('#search_text1').keyup(function(){
                        var search = $(this).val();
                        if(search != '')
                        {
                            load_data(search);
                            $('#result1').show();
                        }
                        else
                        {
                            load_data();
                            $('#result1').hide();

                        }
                    });
                });
            </script>
            <ul class="navbar-item flex-row ml-md-10 ml-auto hidden_mobile" id="TIME" >
                <p class="hidden_mobile" style="font-size:20px; color: #17D4FE; letter-spacing: 10px;" > 
                    <?php
                        date_default_timezone_set("Asia/Kolkata");
                        $hour = date('H', time());

                        if( $hour > 6 && $hour <= 11) {
                          echo "<p class='hidden_mobile'> <i class='fas fa-sun'></i> Good Morning </p>";
                        }
                        else if($hour > 11 && $hour <= 16) {
                          echo "<p class='hidden_mobile'> <i class='fas fa-cloud-sun'></i> Good Afternoon </p>";
                        }
                        else if($hour > 16 && $hour <= 23) {
                          echo "<p class='hidden_mobile'> <i class='fas fa-cloud-moon'></i> Good Evening </p>";
                        }
                        else {
                          echo "<p class='hidden_mobile'> <i class='fas fa-moon'></i> Why aren't you a sleep? </p>";
                        }
                    ?>
                </p>
            <?php
                // date_default_timezone_set("Asia/Kolkata");
                // $time =  Date('d-m-Y');
                // echo ($time);
            ?>
            </ul>
            <ul class="navbar-item flex-row ml-md-auto">
                <li class="nav-item dropdown notification-dropdown">
                    <a href="javascript:void(0);" class="nav-link dropdown-toggle" id="notificationDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bell"><path d="M18 8A6 6 0 0 0 6 8c0 7-3 9-3 9h18s-3-2-3-9"></path><path d="M13.73 21a2 2 0 0 1-3.46 0"></path></svg>
                        <?php if($this->db_connection->notification()==0){ ?>
                           <span class="badge badge-success"></span> 
                        </span> 
                        <?php }else{ ?>
                        <span class="badge badge-success" style="background-color: red;"></span>
                        </span>
                        <?php } ?>
                    </a>
                    <div class="dropdown-menu position-absolute" aria-labelledby="notificationDropdown">
                        <div class="notification-scroll disable-scrollbars" style="height: 400px; overflow-y: scroll; ">
                            <?php foreach($this->db_connection->comment_n() as $notefic){ ?>
                            <div class="dropdown-item">
                                <div class="media">
                                    <div class="media-body">
                                        <div class="notification-para">
                                            <i class="fas fa-comment-dots"></i>
                                            <span class="user-name"><?=$notefic->name?></span>
                                             Comment on Blog. 
                                            <p style="font-size: 10px;">
                                                <?=$notefic->date_time?>
                                            </p> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown user-profile-dropdown">
                    <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="userProfileDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <img src="<?=base_url();?>images/profile/profile.png" alt="avatar">
                    </a>
                    <div class="dropdown-menu position-absolute" aria-labelledby="userProfileDropdown">
                        <div class="">
                            <div class="dropdown-item">
                                <a href="<?=site_url('admin/logout');?>">
                                   <i class="fas fa-door-open"></i>
                                    Sign Out
                                </a>
                            </div>
                            <div class="dropdown-item">
                                <a href="<?=site_url('welcome/index');?>"> 
                                <i class="fab fa-chrome"></i>
                                Visit Site </a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <div class="sub-header-container">
        <header class="header navbar navbar-expand-sm">
            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>
            <ul class="navbar-nav flex-row">
                <li>
                    <div class="page-header">
                        <nav class="breadcrumb-one" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?=site_url('admin/index');?>">Home</a></li>
                            </ol>
                        </nav>
                        <nav class="breadcrumb-one" aria-label="breadcrumb" style="margin-left:100px; color: #17D4FE;">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item" id="digital-clock" style="letter-spacing: 7px;">  </li> ⌚
                            </ol>
                        </nav>
                        <script>
                            function getDateTime() {
                                var now     = new Date(); 
                                var hour    = now.getHours();
                                var minute  = now.getMinutes();
                                var second  = now.getSeconds();
                                var ampm = hour >= 12 ? 'PM' : 'AM';
                                hour = hour % 12;
                                hour = hour ? hour : 12; 
                                   
                                if(hour.toString().length == 1) {
                                    hour = '0'+hour;
                                }
                                if(minute.toString().length == 1) {
                                    minute = '0'+minute;
                                }
                                if(second.toString().length == 1) {
                                    second = '0'+second;
                                }   
                                var dateTime = hour+':'+minute+':'+second+':'+ampm;   
                                return dateTime;
                            }
                            setInterval(function(){
                                currentTime = getDateTime();
                                document.getElementById("digital-clock").innerHTML = currentTime;
                            }, 1000);
                        </script>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <div class="main-container" id="container">
        <div class="overlay"></div>
        <div class="search-overlay"></div>
        <div class="sidebar-wrapper sidebar-theme">
            <nav id="sidebar">
                <div class="shadow-bottom"></div>
                <ul class="list-unstyled menu-categories" id="accordionExample">

                    <?php 
                        $url = $this->uri->segment(2);//it will print blog
                        if($url == "details_info"){
                            $expanded = "true";
                            $submenu = "show";
                        }else{
                            $expanded = "false";
                            $submenu = "";
                        }
                    ?>

                    <li class="menu">
                        <a href="#general" data-toggle="collapse" aria-expanded="<?=$expanded?>" class="dropdown-toggle">
                            <div class="">
                                <i class="fas fa-cog"></i>
                                <span>General Details</span>
                            </div>
                            <div>
                                <svg  width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled <?=$submenu?>" id="general" data-parent="#accordionExample">
                            <li class="<?php if($url == "details_info"){ echo "active"; } ?>">
                                <a href="<?=site_url('admin/details_info');?>">MetaData </a>
                            </li>
                            <li class="<?php if($url == "details_info"){ echo "active"; } ?>">
                                <a href="<?=site_url('admin/details_info#terms&conditions');?>">Terms & Conditions </a>
                            </li>
                            <li class="<?php if($url == "details_info"){ echo "active"; } ?>">
                                <a href="<?=site_url('admin/details_info#privacypolicy');?>">Privacy & Policy </a>
                            </li>
                        </ul>
                    </li>

                    <?php 
                        $url = $this->uri->segment(2);//it will print blog
                        if($url == "details" || $url == "banner" || $url == "show_about"){
                            $expanded = "true";
                            $submenu = "show";
                        }else{
                            $expanded = "false";
                            $submenu = "";
                        }
                    ?>

                    <li class="menu">
                        <a href="#home" data-toggle="collapse" aria-expanded="<?=$expanded?>" class="dropdown-toggle">
                            <div class="">
                                <i class="fas fa-info-circle"></i>
                                <span>Home Screen</span>
                            </div>
                            <div>
                                <svg  width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled <?=$submenu?>" id="home" data-parent="#accordionExample">
                            <li class="<?php if($url == "details"){ echo "active"; } ?>">
                                <a href="<?=site_url('admin/details');?>">Author Details </a>
                            </li>
                             <li class="<?php if($url == "banner"){ echo "active"; } ?>">
                                <a href="<?=site_url('admin/banner');?>">Banner</a>
                            </li>                            
                            <li class="<?php if($url == "show_about"){ echo "active"; } ?>">
                                <a href="<?=site_url('admin/show_about');?>">About-Us</a>
                            </li>
                        </ul>
                    </li>                  

                    <?php 
                        $url = $this->uri->segment(2);//it will print blog
                        if($url == "articles"){
                            $expanded = "true";
                            $submenu = "show";
                        }else{
                            $expanded = "false";
                            $submenu = "";
                        }
                    ?>
                    <li class="menu">
                        <a href="<?=site_url('admin/articles');?>" aria-expanded="<?=$expanded?>" class="dropdown-toggle">
                            <div class="">
                               <i class="fas fa-pen-square"></i>
                                <span>Post Article</span>
                            </div>
                        </a>
                    </li>

                    <li class="menu">
                        <a href="#category1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                                <i class="fas fa-list-alt"></i>
                                <span> Category </span>
                            </div>
                            <div>
                                <svg  width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                            </div>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="category1" data-parent="#accordionExample">
                            <li class="active">
                                <a href="<?=site_url('admin/show_categories');?>">📄 Category </a>
                            </li>
                            <li class="active">
                                <a href="<?=site_url('admin/show_sub_categories');?>">📄 Sub Category </a>
                            </li>
                        </ul>
                    </li>

                    <?php 
                        $url = $this->uri->segment(2);//it will print blog
                        if($url == "comments"){
                            $expanded = "true";
                        }else{
                            $expanded = "false";
                        }
                    ?>

                    <li class="menu">
                        <a href="<?=site_url('admin/comments');?>" aria-expanded="<?=$expanded?>" class="dropdown-toggle">
                            <div class="">
                               <i class="fas fa-comments"></i>
                                <span>Comment</span>
                                <span>
                                <?php if($this->db_connection->notification()==0){ ?>
                                    <i class="fas fa-circle"></i>
                                <?php }else{ ?>
                                    <i class="fas fa-circle" style="color:red;">
                                        <?php echo $this->db_connection->notification() ; ?>
                                    </i>
                                <?php } ?>
                                </span>
                            </div>
                        </a>
                    </li>

                    <li class="menu">
                        <a href="<?=site_url('admin/inquiry');?>" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                               <i class="fas fa-envelope-open-text"></i>
                                <span>Inquiry List</span>
                            </div>
                        </a>
                    </li>

                    <li class="menu">
                        <a href="<?=site_url('admin/inquiry');?>" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                               <i class="fas fa-envelope-open-text"></i>
                                <span>Inquiry List</span>
                            </div>
                        </a>
                    </li>

                    <li class="menu">
                        <a href="<?=site_url('admin/gallery');?>" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                               <i class="fas fa-images"></i>
                                <span>Gallery</span>
                            </div>
                        </a>
                    </li>

                    <li class="menu">
                        <a href="<?=site_url('admin/notes');?>" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                               <i class="fas fa-clipboard"></i>
                                <span>Notes</span>
                            </div>
                        </a>
                    </li>

                    <?php foreach($this->db_connection->select_signup() as $nam_){ if($nam_->email== $this->session->userdata('sesn')){ ?>
                    <?php if($nam_->signup_id=='1'){ ?>
                    <li class="menu">
                        <a href="<?=site_url('admin/show_admin');?>" aria-expanded="false" class="dropdown-toggle">
                            <div class="">
                               <i class="fas fa-users"></i>
                                <span>Admin User</span>
                            </div>
                        </a>
                    </li>
                    <?php } else{ ?>
                    <?php } ?>
                    <?php } }?> 
                </ul>
            </nav>

        </div>
        <!--  END SIDEBAR  -->