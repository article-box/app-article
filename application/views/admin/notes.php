<?php include('header.php'); ?>        
        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row app-notes layout-top-spacing" id="cancel-row">
                    <div class="col-lg-12">
                        <div class="app-hamburger-container">
                            <div class="hamburger"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu chat-menu d-xl-none"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></div>
                        </div>
                        <div style="  height: auto; background: #1b2e4b; border-radius: 10px; width: 100%;padding: 10px;" id="message"></div>
                        <div class="app-container">
                            <div class="app-note-container">
                                <div class="app-note-overlay"></div>
                                <div class="tab-title">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12 text-center">
                                            <a id="btn-add-notes" class="btn btn-outline-primary" href="javascript:void(0);">
                                                <svg viewBox="0 0 512 512"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm144 276c0 6.6-5.4 12-12 12h-92v92c0 6.6-5.4 12-12 12h-56c-6.6 0-12-5.4-12-12v-92h-92c-6.6 0-12-5.4-12-12v-56c0-6.6 5.4-12 12-12h92v-92c0-6.6 5.4-12 12-12h56c6.6 0 12 5.4 12 12v92h92c6.6 0 12 5.4 12 12v56z"></path></svg>
                                            </a>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-12 mt-5">
                                            <ul class="nav nav-pills d-block" id="pills-tab3" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link list-actions active" id="all-notes"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg> All Notes</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link list-actions" id="note-fav"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg> Favourites</a>
                                                </li>
                                            </ul>

                                            <hr/>

                                            <p class="group-section"><svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-tag"><path d="M20.59 13.41l-7.17 7.17a2 2 0 0 1-2.83 0L2 12V2h10l8.59 8.59a2 2 0 0 1 0 2.82z"></path><line x1="7" y1="7" x2="7" y2="7"></line></svg> Tags</p>

                                            <ul class="nav nav-pills d-block group-list" id="pills-tab" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link list-actions g-dot-primary" id="note-personal">Personal</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link list-actions g-dot-warning" id="note-work">Work</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link list-actions g-dot-success" id="note-social">Social</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link list-actions g-dot-danger" id="note-important">Important</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div id="ct" class="note-container note-grid">
                                    <?php foreach($this->db_connection->select_notes() as $notes){ ?>
                                        <div class="note-item all-notes notes_cards notes_card<?=$notes->notes_id?> <?php if($notes->star==1){ echo 'note-fav '; }; echo $notes->tags; ?>">
                                            <div class="note-inner-content">
                                                <div class="note-content">
                                                    <p class="note-title" data-noteTitle="Meeting with Kelly"><?=$notes->title?></p>
                                                    <div class="note-description-content">
                                                        <p class="note-description" data-noteDescription="Curabitur facilisis vel elit sed dapibus sodales purus rhoncus."><?=$notes->description?></p>
                                                    </div>
                                                </div>
                                                <div class="note-action">
                                                    <?=form_open_multipart('admin/star_notes/'.$notes->notes_id,array('id' => 'contactForm_notes'.$notes->notes_id.''));
                                                    if($notes->star==1){ ?>
                                                        <input type="hidden" name="star" value="2">
                                                        <button class="btn btn-success hide p-0" style="border: none; background-color: transparent;">
                                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="#bfc9d4" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                        </button>
                                                        <?php }else{?>
                                                        <input type="hidden" name="star" value="1">
                                                        <button class="btn btn-success hide1 p-0" style="border: none; background-color: transparent;">
                                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg>
                                                        </button>
                                                        <?php } ?>
                                                    </form>
                                                    <script>
                                                        $(function() {
                                                            $("#message").hide();
                                                            $("#contactForm_notes<?=$notes->notes_id?>").on('submit', function(e) {
                                                                e.preventDefault();
                                                                var contactForm = $(this);
                                                                $.ajax({
                                                                    url: contactForm.attr('action'),
                                                                    type: 'post',
                                                                    data: contactForm.serialize(),
                                                                    success: function(response){
                                                                        console.log(response);
                                                                        // location.reload();
                                                                        if(response.status == 'success') {
                                                                            
                                                                            setTimeout(function(){ $("#contactForm_notes<?=$notes->notes_id?>")[0].reset(); }, 1000);
                                                                             $("#message").html(response.message);
                                                                            setTimeout(function(){ location.reload(); }, 100);

                                                                        }
                                                                        // $("#message").html(response.message);
                                                                    }
                                                                });
                                                            });
                                                        });
                                                        </script>
                                                </div>
                                                <a  href="javascript:;" title="Delete" class="item-delete1" data="<?=$notes->notes_id?>">
                                                        <svg width="20" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                                    </a>
                                                <div class="note-footer">
                                                    <div class="tags-selector btn-group">
                                                        <a class="nav-link dropdown-toggle d-icon label-group" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">
                                                            <div class="tags">
                                                                <div class="g-dot-personal"></div>
                                                                <div class="g-dot-work"></div>
                                                                <div class="g-dot-social"></div>
                                                                <div class="g-dot-important"></div>
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                                                            </div>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right d-icon-menu">
                                                            <a class="note-personal label-group-item label-personal dropdown-item position-relative g-dot-personal" href="javascript:void(0);"> Personal</a>
                                                            <a class="note-work label-group-item label-work dropdown-item position-relative g-dot-work" href="javascript:void(0);"> Work</a>
                                                            <a class="note-social label-group-item label-social dropdown-item position-relative g-dot-social" href="javascript:void(0);"> Social</a>
                                                            <a class="note-important label-group-item label-important dropdown-item position-relative g-dot-important" href="javascript:void(0);"> Important</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="meta-time p-1"><?=$notes->date?></p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <!-- Modal -->
                        <div class="modal fade" id="notesMailModal" tabindex="-1" role="dialog" aria-labelledby="notesMailModalTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x close" data-dismiss="modal"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>
                                        <?php $today = date("j-F-Y, g:i a");?>
                                        <?=form_open_multipart('admin/add_notes',array('id'=> 'contactForm'))?>
                                        <div class="notes-box">
                                            <div id="message1" class="mb-2 alert alert-success" style="height: auto; width: 100%;"></div>
                                            <div class="notes-content">
                                                    <div class="row">
                                                        <div class="col-md-12 mb-5">
                                                            <div class="d-flex note-title">
                                                                <input type="text" id="n-title" name="title" class="form-control" maxlength="30" placeholder="Title" required>
                                                                <input type="hidden" name="date" value="<?php echo $today; ?>">
                                                            </div>
                                                            <span class="validation-text"></span>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="d-flex note-description">
                                                                <textarea id="n-description" class="form-control" name="description" maxlength="255" placeholder="Description" rows="3" required></textarea>
                                                            </div>
                                                            <span class="validation-text"></span>
                                                        </div>
                                                        <div class="col-md-12 mt-3">
                                                            <div class="d-flex note-description">
                                                                <select class="form-control" name="tags">
                                                                    <option value=""> Select Tags</option>
                                                                    <option value="note-personal" style="background-color: rgba(0, 150, 136, 0.28);">Personal</option>
                                                                    <option value="note-work" style="background-color: rgba(226, 160, 63, 0.42);">Work</option>
                                                                    <option value="note-social" style="background-color: rgba(92, 26, 195, 0.26);">Social</option>
                                                                    <option value="note-important" style="background-color: rgba(231, 80, 90, 0.38);">Important</option>
                                                                    
                                                                </select>
                                                            </div>
                                                            <span class="validation-text"></span>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button id="btn-n-save" class="float-left btn">Save</button>
                                        <button class="btn" data-dismiss="modal"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg> Discard</button>
                                        <button type="submit" class="btn btn-primary"> 
                                            <svg viewBox="0 0 448 512"><path fill="currentColor" d="M433.941 129.941l-83.882-83.882A48 48 0 0 0 316.118 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V163.882a48 48 0 0 0-14.059-33.941zM272 80v80H144V80h128zm122 352H54a6 6 0 0 1-6-6V86a6 6 0 0 1 6-6h42v104c0 13.255 10.745 24 24 24h176c13.255 0 24-10.745 24-24V83.882l78.243 78.243a6 6 0 0 1 1.757 4.243V426a6 6 0 0 1-6 6zM224 232c-48.523 0-88 39.477-88 88s39.477 88 88 88 88-39.477 88-88-39.477-88-88-88zm0 128c-22.056 0-40-17.944-40-40s17.944-40 40-40 40 17.944 40 40-17.944 40-40 40z"></path></svg> Save
                                        </button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="footer-wrapper">
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg> Nishant</p>
                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->        
    </div>
    <!-- END MAIN CONTAINER -->
    
<script>
$(function() {
    $("#message1").hide();
    $("#contactForm").on('submit', function(e) {
        e.preventDefault();
        var contactForm = $(this);
        $.ajax({
            url: contactForm.attr('action'),
            type: 'post',
            data: contactForm.serialize(),
            success: function(response){
                console.log(response);
                // location.reload();
                if(response.status == 'success') {
                    // $("#contactForm").hide();
                    setTimeout(function(){ $("#message1").show(); }, 0);
                    // setTimeout(function(){ $(".swal2-container").hide(); }, 1500);
                    // swal({
                    //   title: response.message,
                    //   text: 'done',
                    //   type: 'success',
                    //   padding: '2em'
                    // })
                    setTimeout(function(){ $("#message1").hide(); }, 5000);
                    setTimeout(function(){ $("#contactForm")[0].reset(); }, 100);
                    // setTimeout(function(){ $(".notes_card")[0].reload(); }, 100);
                     $("#message1").html(response.message);
                    setTimeout(function(){ location.reload(); }, 400);

                }
                // $("#message").html(response.message);
            }
        });
    });
});
</script>
<script>
    $("#message").hide();
    $('.notes_cards').on('click', '.item-delete1', function(){
         var el = this;
        var id = $(this).attr('data');
            if(confirm('Are you sure for Delete')){
            $.ajax({
                type: 'ajax',
                method: 'get',
                async: false,
                url: '<?php echo base_url() ?>admin/delete_notes',
                data:{id:id},
                dataType: 'json',
                success: function(response){
                    if(response.status == 'success'){
                         $(el).closest('div').parent('div').css('background','none');
                         // $(el).parent('div').hide();
                                $(el).closest('div').parent('div').fadeOut(800,function(){
                           // $(el).html('<p style="color:red;">Deleted</p>');
                        });
                    }else{
                        alert('Error');
                    }
                },
                error: function(){
                    alert('Error deleting');
                }
            });
        }
    });
</script>

    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/ie11fix/fn.fix-padStart.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/apps/notes.js"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
</body>
</html>