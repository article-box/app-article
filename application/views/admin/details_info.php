<?php include('header.php'); ?>
        <div id="content" class="main-content">
            <div id="alertMsg"></div>
            <div class="layout-px-spacing">                
                <div class="account-settings-container layout-top-spacing">
                  <!-- <?=form_open_multipart('admin/edit_details_info/'.$details->details,array('id' => 'contactForm')); ?> -->
                    <div class="row">

                        <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" id="meta">
                            <div id="about" class="section about">
                                <div class="info">
                                    <h5 class="">Logo</h5>
                                    <div class="row">
                                        <div class="col-md-11 mx-auto">
                                            <div id="logo"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <form id="detailsForm">
                            <input type="hidden" name="id" value="<?=$details->details?>">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <div id="general-info" class="section general-info">
                                    <div class="info">
                                        <h6 class="">General Information <button type="submit" class="btn btn-primary"> Save </button> </h6>
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-xl-12 col-lg-12 col-md-8 mt-md-0 mt-4">
                                                        <div class="form">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="fullName">Full Name</label>
                                                                        <input type="text" name="name" class="form-control mb-4" id="fullName" placeholder="Company Name" value="<?=$details->name?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="fullName">Mail Id</label>
                                                                        <input type="email" name="gmail" class="form-control mb-4" id="fullName" placeholder="gmail" value="<?=$details->gmail?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="form-group">
                                                                    <label for="phone">About You (Short Text*)</label>
                                                                    <textarea  class="form-control mb-4" name="about_you" placeholder="Paset Embad Code "> <?=$details->about_you?> </textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="profession">PDF Link</label>
                                                                <input type="text" name="pdf" class="form-control mb-4" id="profession" placeholder="Pdf Link (Google Drive)" value="<?=$details->pdf?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <div id="contact" class="section contact">
                                    <div class="info">
                                        <h5 class="">Contact</h5>
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">Country</label>
                                                            <input type="text" name="country" class="form-control mb-4" id="profession" placeholder="Country"  value="<?=$details->country?>"  autocomplete="off" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address">Address</label>
                                                            <input type="text" class="form-control mb-4" id="address" placeholder="Address" name="address" value="<?=$details->address?>" >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="location">Location (City)</label>
                                                            <input type="text" name="city" class="form-control mb-4" id="location" placeholder="Location (City)" value="<?=$details->city?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="phone">Phone</label>
                                                            <input type="text" class="form-control mb-4" id="phone" name="phone" placeholder="Write your phone number here" value="<?=$details->phone?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="phone">Google Map (Embad a map Code)</label>
                                                            <textarea  class="form-control mb-4" name="map" placeholder="Paset Embad Code "> <?=$details->map?> </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <div id="social" class="section social">
                                    <div class="info">
                                        <h5 class="">Social</h5>
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="input-group social-linkedin mb-3">
                                                            <div class="input-group-prepend mr-3">
                                                                <span class="input-group-text" id="linkedin"> 
                                                                    <i class="fab fa-linkedin"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" name="linkedin" class="form-control" placeholder="linkedin Username" aria-label="Username" aria-describedby="linkedin" value="<?=$details->linkedin?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="input-group social-tweet mb-3">
                                                            <div class="input-group-prepend mr-3">
                                                                <span class="input-group-text" id="tweet"> 
                                                                    <i class="fab fa-twitter"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" name="twitter" class="form-control" placeholder="Twitter Username" aria-label="Username" aria-describedby="tweet" value="<?=$details->twitter?>">
                                                        </div>
                                                    </div>                                                        
                                                </div>
                                            </div>

                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="input-group social-fb mb-3">
                                                            <div class="input-group-prepend mr-3">
                                                                <span class="input-group-text" id="fb"> 
                                                                    <i class="fab fa-facebook"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" name="fb" class="form-control" placeholder="Facebook Username" aria-label="Username" aria-describedby="fb" value="<?=$details->fb?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="input-group social-instagram mb-3">
                                                            <div class="input-group-prepend mr-3">
                                                                <span class="input-group-text" > 
                                                                    <i class="fab fa-whatsapp"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" name="whatsapp" class="form-control" placeholder="Whatsapp Link" aria-label="Username" aria-describedby="github" value="<?=$details->whatsapp?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="input-group social-instagram mb-3">
                                                            <div class="input-group-prepend mr-3">
                                                                <span class="input-group-text" >
                                                                    <i class="fab fa-instagram"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" name="instagram" class="form-control" placeholder="Instagram Link" aria-label="Username" aria-describedby="github" value="<?=$details->instagram?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <div id="about" class="section about">
                                    <div class="info">
                                        <!-- <h5 class="">About</h5> -->
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="form-group">
                                                    <label for="aboutBio">Bio</label>
                                                    <textarea name="text" class="form-control" id="aboutBio" placeholder="Tell something interesting about yourself" rows="10"><?=$details->text?></textarea>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        
                        <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" id="meta">
                            <div id="about" class="section about">
                                <div class="info">
                                    <h5 class="">Meta Data</h5>
                                    <div class="row">
                                        <div class="col-md-11 mx-auto">
                                            <div id="metaData"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" id="terms&conditions">
                            <div id="about" class="section about">
                                <div class="info">
                                    <h5 class="">Terms & Conditions</h5>
                                    <div class="row">
                                        <div class="col-md-11 mx-auto">
                                            <div id="terms_and_conditions"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing" id="privacypolicy">
                            <div id="about" class="section about">
                                <div class="info">
                                    <h5 class="">Privacy Policy</h5>
                                    <div class="row">
                                        <div class="col-md-11 mx-auto">
                                            <div id="privacy_policy"> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include("load_data/alert.php"); ?>
<script>
    $(document).on("submit", "#detailsForm", function(e){
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url:"<?=site_url('admin/edit_details_info');?>",
            type:"POST",
            data: form_data,
            contentType: false,
            processData: false,
            success:function(data){
                alertMassage(data);
            }
        })
    })

    function loadMetaData(){
        $.ajax({
            url: "<?=site_url('admin/meta_data');?>",
            type: "POST",
            beforeSend: function() {
                preloder();
            },
            success: function(response) {
                $("#metaData").html(response);
            },
            complete: function() {
                $("#loading").html("");
            }
        })
    }loadMetaData();

    $(document).on("submit", "#metaForm", function(e){
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url:"<?=site_url('admin/edit_meta');?>",
            type:"POST",
            data: form_data,
            contentType: false,
            processData: false,
            success:function(data){
                alertMassage(data);
                loadMetaData();
            }
        })
    })


    function loadLogo(){
        $.ajax({
            url: "<?=site_url('admin/update_logo');?>",
            type: "POST",
            beforeSend: function() {
                preloder();
            },
            success: function(response) {
                $("#logo").html(response);
            },
            complete: function() {
                $("#loading").html("");
            }
        })
    }loadLogo();

    $(document).on("submit", "#logoForm", function(e) {
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url: "<?= site_url("admin/updatelogo") ?>",
            type: "POST",
            data: form_data,
            contentType: false,
            processData: false,
            success: function(data) {
                alertMassage(data);
                loadLogo();
            }
        })
    })

    $(document).on("click", "#openInputFile", function(e) {
        e.preventDefault();
        $("#inputFileImage").click();
    })

    //------------- image preview------------------//
    $(document).on("change", "#inputFileImage", function() {
        var url = window.URL.createObjectURL(this.files[0]);
        var img = ' <p> Preview </p> <img height="200" src="' + url +
            '" alt="Image" class="shadow border p-1"> <div class=" rounded-circle" id="removePreviewImage"> <i class="fas fa-times-circle"></i> </div>';
        $("#preview").html(img);
        $("#submitImgBtn").show(); 
    });
    //-------------Remove image preview------------------//
    $(document).on("click", "#removePreviewImage", function() {
        var path = $("#inputFileImage").val("");
        $("#preview").html(" ");
        $("#submitImgBtn").hide();
    });

    function privacy_policy(){
        $.ajax({
            url: "<?=site_url('admin/update_privacy_policy');?>",
            type: "POST",
            beforeSend: function() {
                preloder();
            },
            success: function(response) {
                $("#privacy_policy").html(response);
                CKEDITOR.replace('ptext');
            },
            complete: function() {
                $("#loading").html("");
            }
        })
    }privacy_policy();

    $(document).on("submit", "#privacyForm", function(e) {
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url: "<?= site_url("admin/edit_pap") ?>",
            type: "POST",
            data: form_data,
            contentType: false,
            processData: false,
            success: function(data) {
                alertMassage(data);
                privacy_policy();
            }
        })
    })

    $(document).on("submit", "#termsForm", function(e) {
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url: "<?= site_url("admin/edit_tac") ?>",
            type: "POST",
            data: form_data,
            contentType: false,
            processData: false,
            success: function(data) {
                alertMassage(data);
                terms_and_conditions();
            }
        })
    })

    function terms_and_conditions(){
        $.ajax({
            url: "<?=site_url('admin/update_terms_and_conditions');?>",
            type: "POST",
            beforeSend: function() {
                preloder();
            },
            success: function(response) {
                $("#terms_and_conditions").html(response);
                CKEDITOR.replace('ttext');
            },
            complete: function() {
                $("#loading").html("");
            }
        })
    }terms_and_conditions();
</script>

    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
</body>
</html>