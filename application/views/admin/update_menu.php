<?php include('header.php'); ?>
        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div class="col-lg-10 col-12 layout-spacing">
                            <div class="statbox widget box box-shadow">
                                <div class="widget-header">                                
                                    <div class="row">
                                        <a href="<?=site_url('admin/show_menu');?>"> <button class="btn btn-dark" title="Click To View All Menu List"> <svg  width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-corner-up-left"><polyline points="9 14 4 9 9 4"></polyline><path d="M20 20v-7a4 4 0 0 0-4-4H4"></path></svg> View Menu List</button> </a>
                                        <div style="  height: auto; background: #1b2e4b; border-radius: 10px; width: 100%;padding: 10px;" id="message"></div>
                                        <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                            <h4>Update Menu</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                   <?=form_open_multipart('admin/edit_menu/'.$m->menu_id,array('id' => 'contactForm'));?>
                                        <div class="row mb-4">
                                            <div class="col">
                                                <input type="text" value="<?=$m->menu_name ?>" name="menu_name" class="form-control" placeholder="Menu Name">
                                            </div>
                                            <div class="col">
                                                <input type="text" value="<?=$m->menu_url ?>" name="menu_url" class="form-control" placeholder="Menu url">
                                            </div>
                                            <div class="col">
                                                <input type="submit" name="time" class="btn btn-primary">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <script>
                $(function() {
                    $("#message").hide();
                    $("#contactForm").on('submit', function(e) {
                        e.preventDefault();
                        var contactForm = $(this);
                        $.ajax({
                            url: contactForm.attr('action'),
                            type: 'post',
                            data: contactForm.serialize(),
                            success: function(response){
                                console.log(response);
                                // location.reload();
                                if(response.status == 'success') {
                                    // $("#contactForm").hide();
                                    setTimeout(function(){ $("#message").show(); }, 2000);
                                    setTimeout(function(){ $(".swal2-container").hide(); }, 2000);
                                    swal({
                                      title: response.message,
                                      text: 'done',
                                      type: 'success',
                                      padding: '2em'
                                    })
                                    setTimeout(function(){ $("#message").hide(); }, 5000);
                                    // setTimeout(function(){ $("#contactForm")[0].reset(); }, 2000);
                                     $("#message").html(response.message);
                                    // setTimeout(function(){ location.reload(); }, 5000);

                                }
                                // $("#message").html(response.message);
                            }
                        });
                    });
                });
            </script>
            <div class="footer-wrapper">
                
                <div class="footer-section f-section-2">
                    <p class="">Coded with Nishant<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg></p>
                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->

    </div>
    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/plugins/highlight/highlight.pack.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/sweetalerts/sweetalert2.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/sweetalerts/custom-sweetalert.js"></script>
</body>
</html>