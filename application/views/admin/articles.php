<?php include('header.php') ?>
        <!--  BEGIN CONTENT AREA  -->
        <!-- <?php echo $this->session->flashdata('suc_msg'); ?> -->
        <a href="<?=site_url('admin/add_blog');?>"> 
            <div class="hide__">
                <button class="btn btn-primary" id="on_click_load" title="Add More Blogs" style="position: fixed; bottom: 30px; right: 50px; border-radius:100%; z-index:999; padding:20px 20px;" >
                    <div id="click_to_hide">
                        <i class="fas fa-folder-plus" style="font-size: 24px;"></i>
                    </div>
                    <div id="click_to_show">
                        <i class="fas fa-spinner" id="loader_css"></i>
                    </div>
                </button> 
            </div>
        </a>
         <div id="content" class="main-content">
            <div id="alertMsg"></div>
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <div class="widget-content widget-content-area br-6">

                            <div class="d-flex justify-content-end">
                                <input type="search" id="searchData" class="form-control d-flex mr-1 ml-1" placeholder="Search ...">

                                <div class="input-group d-flex mr-1">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">To</span>
                                    </div>
                                    <input type="date" name="to" id="to" class="form-control" required>
                                </div>

                                <div class=" input-group d-flex mr-1">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">From</span>
                                    </div>
                                    <input type="date" name="from" id="from" class="form-control" required>
                                </div>

                                <button class=" btn btn-dark" id="apply"> Apply </button>
                            </div>

                            <div class="d-flex justify-content-end">
                                <select class="form-control d-flex m-1 col-sm-1" id="limit" aria-label="Default select example">
                                  <option selected value="5">5</option>
                                  <option value="10">10</option>
                                  <option value="25">25</option>
                                  <option value="50">50</option>
                                  <option value="70">75</option>
                                  <option value="100">100</option>
                                </select>

                                <select class="form-control d-flex m-1" id="FilterBy" aria-label="Default select example">
                                  <option selected value="">Filter By</option>
                                  <option value="pin">Pin</option>
                                  <option value="tranding">Tranding</option>
                                  <option value="star">Star</option>
                                </select>

                                <select class="form-control d-flex m-1" id="uplodes" aria-label="Default select example">
                                  <option selected value="">Uplodes</option>
                                  <option value="desc">Newest</option>
                                  <option value="asc">Oldest</option>
                                </select>

                                <select class="form-control d-flex m-1" id="status" aria-label="Default select example">
                                  <option selected value="">Status</option>
                                  <option value="1">Active</option>
                                  <option value="null">Draft</option>
                                </select>
                            </div>
                            <div id="tableData" class="pre-loading"></div>
                        </div>
                    </div>
                </div>
            </div>    
            <div class="footer-wrapper">
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg> Nishant </p>
                </div>
            </div>
        </div>
    </div>
<?php include("load_data/alert.php"); ?>

<script>

    $(document).on("keyup", "#searchData", function(e) {
        e.preventDefault();
        $("#apply").click();
    })

    $(document).on("change", "#FilterBy, #uplodes, #status, #limit", function(e) {
        e.preventDefault();
        $("#apply").click();
    })

    $(document).on("click", "#apply", function(e) {
        e.preventDefault();
        loadBlogData(page_url = false);
    })
    
    /*--first time load--*/
    loadBlogData(page_url = false);

    /*-- Page click --*/
    $(document).on('click', ".pagination li a", function(event) {
        var page_url = $(this).attr('href');
        // alert(page_url);
        loadBlogData(page_url);
        event.preventDefault();
    });

    function loadBlogData(page_url = false) {
        var base_url = '<?php echo site_url('admin/blogdata/') ?>';

        var searchData = $("#searchData").val();
        var status = $("#status").val();
        var to = $("#to").val();
        var from = $("#from").val();
        var FilterBy = $("#FilterBy").val();
        var uplodes = $("#uplodes").val();
        var limit = $("#limit").val();

        if (page_url == false) {
            var page_url = base_url;
        }

        $.ajax({
            type: "POST",
            url: page_url,
            data: {
                searchData: searchData,
                status:status,
                to: to,
                from: from,
                FilterBy:FilterBy,
                uplodes:uplodes,
                limit:limit
            },
            beforeSend: function() {
                preloder();
            },
            success: function(response) {
                $("#tableData").html(response);
                $("ul.pagination > li > a").addClass("page-link");
            },
            complete: function() {
                $("#loading").html("");
            }
        });
    }


    $(document).on("change", "#statusUpdate", function(){
        var statusData = $(this).val();
        if ($(this).is(':checked')) {
            var blogValue = 1;
        }else{
            var blogValue = 2;
        }

        // alert(blogValue);
        $.ajax({
            url:"<?=site_url('admin/updateStatus');?>",
            type:"POST",
            data:{statusData:statusData, blogValue:blogValue},
            success:function(data){
              alertMassage(data);
              // articleData(1);
            }
        });
    })
</script>

<script>
    $(document).on("change", "#trandUpdate", function(){
        var articleId = $(this).val();
        if ($(this).is(':checked')) {
            var trandStatus = 1;
        }else{
            var trandStatus = 2;
        }
        $.ajax({
            url:"<?=site_url('admin/updateTrending');?>",
            type:"POST",
            data:{articleId:articleId, trandStatus:trandStatus},
            success:function(data){
              alertMassage(data);
            }
        });
    })
</script>

<script>
    $(document).on("change", "#pinUpdate", function(){
        var articleIdPin = $(this).val();
        if ($(this).is(':checked')) {
            var pinStatus = 1;
        }else{
            var pinStatus = 2;
        }
        $.ajax({
            url:"<?=site_url('admin/updatePin');?>",
            type:"POST",
            data:{articleIdPin:articleIdPin, pinStatus:pinStatus},
            success:function(data){
              // $("#message").html(data);
              alertMassage(data);
            }
        });
    })
</script>

<script>
    $(document).on("change", "#starUpdate", function(){
        var articleStarId = $(this).val();
        if ($(this).is(':checked')) {
            var starStatus = 1;
        }else{
            var starStatus = 2;
        }
        $.ajax({
            url:"<?=site_url('admin/updateStar');?>",
            type:"POST",
            data:{articleStarId:articleStarId, starStatus:starStatus},
            success:function(data){
              // $("#message").html(data);
              alertMassage(data);
            }
        });
    })
</script>

<script>
    $(document).on("click", "#deleteArticle", function(){
        var deleteId = $(this).data("id");
        // var confirm = confirm('Are you sure delete this article !');
        if (confirm("Do You Really want to delete this record ?")) {
            $.ajax({
                url:"<?=site_url('admin/deleteArticle');?>",
                type:"POST",
                data:{deleteId:deleteId},
                success:function(data){
                  // $("#message").html(data);
                  alertMassage(data);
                  $("#deleteArticle").fadeOut().remove();
                }
            });
        }
    })
</script>
<script>
    $('#click_to_show').hide();
    $('.hide__').on('click', '#on_click_load', function(){ 
        $('#click_to_hide').hide();
        $('#click_to_show').show();
    });
</script>


    
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
</body>
</html>