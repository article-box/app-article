<div class="col-lg-12 col-12 layout-spacing" style="margin:auto; ">
    <div class="statbox widget box box-shadow">
        <div class="widget-header d-flex flex-column">

            <?php foreach ($this->db_connection->select_banner() as $banner) {
            ?>
            <div class="card component-card d-flex col-12">
                <img src="<?= base_url(); ?>images/banner/<?= $banner->banner_img ?>" class="card-img-top"
                    alt="widget-card-2" height="200px">
                <div class="card-body">
                    <h5 class="card-title"> <b> Image Title :-</b> <?= $banner->banner_title ?></h5>
                    <p class="card-text"><b> Banner Text :-</b> <?= $banner->banner_text ?></p>
                    <div class="d-flex justify-content-between">
                        <div class="d-flex">
                            <a data-id="<?= $banner->banner_id ?>" id="updateBanner" class="btn bg-transparent" data-toggle="modal" data-target="#editBanner">
                                <i class="fas fa-pen text-primary"></i>
                            </a>

                            <!-- <a id="deleteBanner" data-id="<?= $banner->banner_id ?>" class="btn bg-transparent">
                                <i class="fas fa-trash-alt text-danger"></i>
                            </a> -->
                        </div>
                        <div class="d-flex">
                            <div class="n-chk">
                                <label class="new-control new-checkbox new-checkbox-rounded checkbox-outline-primary">
                                    <?php if ($banner->status == 1) { ?>
                                    <input type="checkbox" value="<?= $banner->banner_id ?>" id="bannerStatus"
                                        class="new-control-input" checked>
                                    <span class="pl-2"> Active </span>
                                    <?php } else { ?>
                                    <input type="checkbox" value="<?= $banner->banner_id ?>" id="bannerStatus"
                                        class="new-control-input">
                                    <span class="pl-2"> Draft </span>
                                    <?php } ?>
                                    <span class="new-control-indicator"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>