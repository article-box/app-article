<?php include('header.php'); ?>
<div id="content" class="main-content">
    <div class="">
        <!-- <h1>
            <?php print_r($this->db_connection->notification()) ; ?>
        </h1> -->
        <div class="col-sm-12">
            <div class="row">
                <div id="" class="col-lg-12 col-sm-12 ">
                    <div class="statbox widget box box-shadow">
                        <div class="widget-content justify-tab">
                            <ul class="nav nav-tabs  mb-3 mt-3 nav-fill" id="justifyTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="justify-home-tab" data-toggle="tab" href="#justify-home" role="tab" aria-controls="justify-home" aria-selected="true">
                                    <svg viewBox="0 0 512 512"><path fill="currentColor" d="M256 32C114.6 32 0 125.1 0 240c0 49.6 21.4 95 57 130.7C44.5 421.1 2.7 466 2.2 466.5c-2.2 2.3-2.8 5.7-1.5 8.7S4.8 480 8 480c66.3 0 116-31.8 140.6-51.4 32.7 12.3 69 19.4 107.4 19.4 141.4 0 256-93.1 256-208S397.4 32 256 32zM128 272c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32zm128 0c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32zm128 0c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32z"></path></svg>
                                    <span style="color: red;"><?php print_r($this->db_connection->notification()) ; ?> New</span> Comment</a>
                                </li>
                            </ul>

                            <div class="tab-content" id="justifyTabContent">
                                <div class="tab-pane fade show active" id="justify-home" role="tabpanel" aria-labelledby="justify-home-tab ">
                                      <div style="  height: auto; background: #1b2e4b; border-radius: 10px; width: 100%;padding: 10px;" id="message"> </div>
                                    <?php foreach($this->db_connection->comment() as $comment){ ?>
                                        <div class="widget-content widget-content-area notation-text-icon col-sm-6 mb-2 ml-5 " id="<?=$comment->comment_id?>">
                                            <div id="hide<?=$comment->comment_id?>">
                                                <?php  if($comment->notification==1){ ?>
                                                <div class="bg-success" style="opacity: 0.8; height: auto; margin-left: -20px; margin-top: -10px; border-radius: 8px 0px 0px 0px; width: auto; background-color: red; position: absolute;">
                                                     <span class="p-1">Read</span>
                                                </div>
                                                <?php }else{ ?>
                                                    <div class="bg-danger" style="opacity: 0.8; height: auto; margin-left: -20px; margin-top: -10px; border-radius: 8px 0px 0px 0px; width: auto; background-color: red; position: absolute;">
                                                     <span class="p-1">New</span> 
                                                </div>
                                                <?php } ?>
                                            </div>
                                            <div id="message<?=$comment->comment_id?>">
                                                <div class="bg-success" style="opacity: 0.8; height: auto; margin-left: -20px; margin-top: -10px; border-radius: 8px 0px 0px 0px; width: auto; background-color: red; position: absolute;">
                                                     <span class="p-1">Read</span> 
                                                </div>
                                            </div>
                                            <div class="media">
                                                <img class=" rounded" src="<?=base_url();?>images/profile/profile.png" alt="pic1">
                                                <div class="media-body">
                                                    <div class="action-dropdown custom-dropdown-icon float-right">
                                                        <div class="dropdown">
                                                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink<?=$comment->comment_id?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-vertical"><circle cx="12" cy="12" r="1"></circle><circle cx="12" cy="5" r="1"></circle><circle cx="12" cy="19" r="1"></circle></svg>
                                                            </a>

                                                            <div class="dropdown-menu" aria-labelledby="<?=$comment->comment_id?>">
                                                                <div id="msg<?=$comment->comment_id?>">
                                                                    <span style=" color: white;">
                                                                        <svg width="20" height="20" viewBox="0 0 512 512" class="mr-1" style="padding: 2px;"><path fill="#3FD60F" d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z"></path></svg>Read
                                                                    </span>
                                                                </div>
                                                                <div id="hide1<?=$comment->comment_id?>">
                                                                    <?php if($comment->notification==1){ ?>
                                                                        <span style=" color: white;">
                                                                            <svg width="20" height="20" viewBox="0 0 512 512" class="mr-1" style="padding: 2px;"><path fill="#3FD60F" d="M504 256c0 136.967-111.033 248-248 248S8 392.967 8 256 119.033 8 256 8s248 111.033 248 248zM227.314 387.314l184-184c6.248-6.248 6.248-16.379 0-22.627l-22.627-22.627c-6.248-6.249-16.379-6.249-22.628 0L216 308.118l-70.059-70.059c-6.248-6.248-16.379-6.248-22.628 0l-22.627 22.627c-6.248 6.248-6.248 16.379 0 22.627l104 104c6.249 6.249 16.379 6.249 22.628.001z"></path></svg>Read
                                                                        </span>
                                                                    <?php }else{ ?>
                                                                        <?=form_open_multipart('admin/notification/'.$comment->comment_id,array('id' => 'contactForm_notification'.$comment->comment_id.''));?>
                                                                            <input type="hidden" name="notification" value="1">
                                                                            <button type="submit" class="edit dropdown-item " style="color: white;">
                                                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#3FD60F" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg>
                                                                             View </button>
                                                                        </form>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <script>
                                                        $(function() {
                                                            $('#message<?=$comment->comment_id?>').hide();
                                                            $('#msg<?=$comment->comment_id?>').hide();
                                                            $("#contactForm_notification<?=$comment->comment_id?>").on('submit', function(e) {
                                                                e.preventDefault();
                                                                var contactForm = $(this);
                                                                $.ajax({
                                                                    url: contactForm.attr('action'),
                                                                    type: 'post',
                                                                    data: contactForm.serialize(),
                                                                    success: function(response){
                                                                        console.log(response);
                                                                        // location.reload();
                                                                        if(response.status == 'success') {
                                                                            $("#message<?=$comment->comment_id?>").show();
                                                                            $("#msg<?=$comment->comment_id?>").show();
                                                                            $('#hide1<?=$comment->comment_id?>').hide();
                                                                            $('#hide<?=$comment->comment_id?>').hide();
                                                                            // setTimeout(function(){ $("#message").show(); }, 2000);
                                                                            // setTimeout(function(){ $(".swal2-container").hide(); }, 1500);
                                                                            // swal({
                                                                            //   title: response.message,
                                                                            //   text: 'done',
                                                                            //   type: 'success',
                                                                            //   padding: '2em'
                                                                            // })
                                                                            // setTimeout(function(){ $("#message").hide(); }, 5000);
                                                                            setTimeout(function(){ $("#contactForm")[0].reset(); }, 1000);
                                                                             $("#message").html(response.message);
                                                                            // setTimeout(function(){ location.reload(); }, 1400);

                                                                        }
                                                                        // $("#message").html(response.message);
                                                                    }
                                                                });
                                                            });
                                                        });
                                                    </script>
                                                    <h4 class="media-heading"><?=$comment->name?></h4>
                                                    <p class="media-text"> 
                                                        <svg viewBox="0 0 512 512" style="width: 15px;"><path fill="currentColor" d="M256 32C114.6 32 0 125.1 0 240c0 49.6 21.4 95 57 130.7C44.5 421.1 2.7 466 2.2 466.5c-2.2 2.3-2.8 5.7-1.5 8.7S4.8 480 8 480c66.3 0 116-31.8 140.6-51.4 32.7 12.3 69 19.4 107.4 19.4 141.4 0 256-93.1 256-208S397.4 32 256 32zM128 272c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32zm128 0c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32zm128 0c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32z"></path></svg>
                                                        <?=$comment->comment?> 
                                                    </p>
                                                        <?php foreach($this->db_connection->reply() as $reply){ if($reply->comment_id==$comment->comment_id){ ?>
                                                            <p class="reply_text">
                                                                <svg viewBox="0 0 512 512" style="width: 10px;"><path fill="currentColor" d="M476 3.2L12.5 270.6c-18.1 10.4-15.8 35.6 2.2 43.2L121 358.4l287.3-253.2c5.5-4.9 13.3 2.6 8.6 8.3L176 407v80.5c0 23.6 28.5 32.9 42.5 15.8L282 426l124.6 52.2c14.2 6 30.4-2.9 33-18.2l72-432C515 7.8 493.3-6.8 476 3.2z"></path></svg>
                                                                <span class="p-1"><?=$reply->text?>
                                                                    <a  href="javascript:;" title="Delete" class="item-delete1" data="<?=$reply->reply_id?>">
                                                                     <svg viewBox="0 0 448 512" style="width: 10px;"><path fill="currentColor" d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path></svg>
                                                                    </a>
                                                                </span>
                                                            </p>
                                                        <?php }} ?>
                                                    
                                                    <div class="media-notation mt-3">
                                                        <?=form_open_multipart('admin/insert_reply',array('id' => 'contactForm_reply'.$comment->comment_id.''));?>
                                                        <div class="input-group input-group-sm mb-4">
                                                            <input type="hidden" name="comment_id" value="<?=$comment->comment_id?>">
                                                              <input type="text" name="reply_text" class="form-control" placeholder="Reply Comment" aria-describedby="button-addon2" aria-label="Small" aria-describedby="inputGroup-sizing-sm" required>
                                                              <div class="input-group-append">
                                                                <button class="btn btn-outline-primary" type="submit" id="button-addon2">
                                                                    <svg viewBox="0 0 512 512"><path fill="currentColor" d="M476 3.2L12.5 270.6c-18.1 10.4-15.8 35.6 2.2 43.2L121 358.4l287.3-253.2c5.5-4.9 13.3 2.6 8.6 8.3L176 407v80.5c0 23.6 28.5 32.9 42.5 15.8L282 426l124.6 52.2c14.2 6 30.4-2.9 33-18.2l72-432C515 7.8 493.3-6.8 476 3.2z"></path></svg>
                                                                </button>
                                                              </div>
                                                        </div>
                                                        </form>
                                                    </div>
                                                    <div class="">
                                                        <?php foreach($this->db_connection->select_blog() as $blog){ if($blog->blog_id==$comment->blog_id){ ?>
                                                               <span>
                                                                <a href="<?=site_url('admin/show_blogs/'.$blog->blog_id.'#'.$comment->comment_id);?>">
                                                                <svg viewBox="0 0 576 512" style="color: blue; width: 15px;"><path fill="currentColor" d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z"></path></svg>
                                                                </a>
                                                                <?=$blog->blog_title?></span>
                                                        <?php } }?>
                                                    </div>
                                                    <div class="font-italic float-right p-2">
                                                        <span> 
                                                            <svg viewBox="0 0 512 512" style="height: 15px;"><path fill="currentColor" d="M504 255.531c.253 136.64-111.18 248.372-247.82 248.468-59.015.042-113.223-20.53-155.822-54.911-11.077-8.94-11.905-25.541-1.839-35.607l11.267-11.267c8.609-8.609 22.353-9.551 31.891-1.984C173.062 425.135 212.781 440 256 440c101.705 0 184-82.311 184-184 0-101.705-82.311-184-184-184-48.814 0-93.149 18.969-126.068 49.932l50.754 50.754c10.08 10.08 2.941 27.314-11.313 27.314H24c-8.837 0-16-7.163-16-16V38.627c0-14.254 17.234-21.393 27.314-11.314l49.372 49.372C129.209 34.136 189.552 8 256 8c136.81 0 247.747 110.78 248 247.531zm-180.912 78.784l9.823-12.63c8.138-10.463 6.253-25.542-4.21-33.679L288 256.349V152c0-13.255-10.745-24-24-24h-16c-13.255 0-24 10.745-24 24v135.651l65.409 50.874c10.463 8.137 25.541 6.253 33.679-4.21z"></path></svg>
                                                        <?=$comment->date_time?></span>
                                                    </div>
                                                    <div class="media-notation float-right">
                                                        <a href="javascript:;" title="Delete" class="item-delete" data="<?=$comment->comment_id?>">
                                                            <svg viewBox="0 0 448 512" style="color: red;"><path fill="currentColor" d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path></svg>
                                                        </a>
                                                     </div>
                                                     <div class="float-left">
                                                        <?=form_open_multipart('admin/varified/'.$comment->comment_id,array('id' => 'contactForm'.$comment->comment_id.''));?>
                                                            <?php if($comment->varified==1){ ?>
                                                                <input type="hidden" name="status" value="2">
                                                                <button type="submit" data-toggle="tooltip" data-placement="top" title="varified" class="p-2" style="border: none; background-color: transparent;">
                                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-check-square-fill" fill="currentColor" style="color: #31D011;">
                                                                      <path fill-rule="evenodd" d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm10.03 4.97a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                                                                    </svg><span style="color: white; padding: 5px;">Varified</span>
                                                                </button>
                                                            <?php } else{ ?>
                                                                <input type="hidden" name="status" value="1" >
                                                                <button title="Unvarified" class="p-2" style="border: none; background-color: transparent;">
                                                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-check-square-fill" fill="currentColor" style="color: red;">
                                                                      <path fill-rule="evenodd" d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm10.03 4.97a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                                                                    </svg>
                                                                </button>
                                                            <?php } ?>
                                                            <span class="slider round"></span>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <script>
                                            $(function() {
                                                $("#message").hide();
                                                $("#contactForm<?=$comment->comment_id?>").on('submit', function(e) {
                                                    e.preventDefault();
                                                    var contactForm = $(this);
                                                    $.ajax({
                                                        url: contactForm.attr('action'),
                                                        type: 'post',
                                                        data: contactForm.serialize(),
                                                        success: function(response){
                                                            console.log(response);
                                                            // location.reload();
                                                            if(response.status == 'success') {
                                                                // $("#contactForm").hide();
                                                                setTimeout(function(){ $("#message").show(); }, 2000);
                                                                setTimeout(function(){ $(".swal2-container").hide(); }, 1500);
                                                                swal({
                                                                  title: response.message,
                                                                  text: 'done',
                                                                  type: 'success',
                                                                  padding: '2em'
                                                                })
                                                                setTimeout(function(){ $("#message").hide(); }, 5000);
                                                                setTimeout(function(){ $("#contactForm")[0].reset(); }, 1000);
                                                                 $("#message").html(response.message);
                                                                setTimeout(function(){ location.reload(); }, 1400);

                                                            }
                                                            // $("#message").html(response.message);
                                                        }
                                                    });
                                                });
                                            });
                                            </script>
                                            <script>
                                            $(function() {
                                                $("#message").hide();
                                                $("#contactForm_reply<?=$comment->comment_id?>").on('submit', function(e) {
                                                    e.preventDefault();
                                                    var contactForm = $(this);
                                                    $.ajax({
                                                        url: contactForm.attr('action'),
                                                        type: 'post',
                                                        data: contactForm.serialize(),
                                                        success: function(response){
                                                            console.log(response);
                                                            // location.reload();
                                                            if(response.status == 'success') {
                                                                // $("#contactForm").hide();
                                                                setTimeout(function(){ $("#message").show(); }, 2000);
                                                                setTimeout(function(){ $(".swal2-container").hide(); }, 1500);
                                                                swal({
                                                                  title: response.message,
                                                                  text: 'done',
                                                                  type: 'success',
                                                                  padding: '2em'
                                                                })
                                                                setTimeout(function(){ $("#message").hide(); }, 5000);
                                                                setTimeout(function(){ $("#contactForm_reply")[0].reset(); }, 100);
                                                                 $("#message").html(response.message);
                                                                setTimeout(function(){ location.reload(); }, 1400);

                                                            }
                                                            // $("#message").html(response.message);
                                                        }
                                                    });
                                                });
                                            });
                                            </script>
                                    <?php }  ?>         
                                      <!-- </div> -->

                                </div>
                               <!--  <div class="tab-pane fade" id="justify-profile" role="tabpanel" aria-labelledby="justify-profile-tab">
                                    <div class="justify-content-center">
                                        <p class="mb-4 justify-content-center">
                                          <img style="height: auto; width: 600px"  src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>"/>
                                        </p>
                                      </div>
                                </div>
                                <div class="tab-pane fade" id="justify-contact" role="tabpanel" aria-labelledby="justify-contact-tab">
                                    
                                </div>
                                <div class="tab-pane fade" id="justify-action" role="tabpanel" aria-labelledby="justify-contact-tab" >
                                    
                                    <li>
                                        <a href="<?=site_url('admin/blogs');?>">
                                        <button class="btn btn-outline-success m-3">View Table</button></a>
                                    </li>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("#message").hide();
    $('.reply_text').on('click', '.item-delete1', function(){
         var el = this;
        var id = $(this).attr('data');
        // $('#deleteModal').modal('show');
        //prevent previous handler - unbind()
        // $('.item-delete').unbind().click(function(){
            if(confirm('Are you sure for Delete')){
            $.ajax({
                type: 'ajax',
                method: 'get',
                async: false,
                url: '<?php echo base_url() ?>admin/delete_reply',
                data:{id:id},
                dataType: 'json',
                success: function(response){
                    if(response.status == 'success'){
                        // $('#deleteModal').modal('hide');
                        $('#message').html('Deleted successfully').fadeIn().delay(2000).fadeOut('slow');
                        // setTimeout(function(){ $(".swal2-container").hide(); }, 2000);
                        // swal({
                        //   title: response.message,
                        //   text: 'done',
                        //   type: 'success',
                        //   padding: '2em'
                        // })
                         $(el).closest('p').css('background','tomato');
                                $(el).closest('p').fadeOut(800,function(){
                           // $(this).next().html('<p style="color:red;">Deleted</p>');
                        });
                    }else{
                        alert('Error');
                    }
                },
                error: function(){
                    alert('Error deleting');
                }
            });
        }
        // });
    });
</script>
<script>
    $("#message").hide();
    $('.media-notation').on('click', '.item-delete', function(){
         var el = this;
        var id = $(this).attr('data');
        // $('#deleteModal').modal('show');
        //prevent previous handler - unbind()
        // $('.item-delete').unbind().click(function(){
            if(confirm('Are you sure for Delete')){
            $.ajax({
                type: 'ajax',
                method: 'get',
                async: false,
                url: '<?php echo base_url() ?>admin/delete_comment',
                data:{id:id},
                dataType: 'json',
                success: function(response){
                    if(response.status == 'success'){
                        // $('#deleteModal').modal('hide');
                        $('#message').html('Deleted successfully').fadeIn().delay(2000).fadeOut('slow');
                        setTimeout(function(){ $(".swal2-container").hide(); }, 2000);
                        swal({
                          title: response.message,
                          text: 'done',
                          type: 'success',
                          padding: '2em'
                        })
                         $(el).closest('div').css('background','tomato');
                                $(el).closest('div').fadeOut(800,function(){
                           $(this).next().html('<p style="color:red;">Deleted</p>');
                        });
                    }else{
                        alert('Error');
                    }
                },
                error: function(){
                    alert('Error deleting');
                }
            });
        }
        // });
    });
</script>



<!-- BEGIN GLOBAL MANDATORY STYLES -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/blockui/jquery.blockUI.min.js"></script>
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/plugins/highlight/highlight.pack.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY STYLES -->
    <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
            <script src="<?=base_url();?>dashboard/plugins/sweetalerts/sweetalert2.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/sweetalerts/custom-sweetalert.js"></script>
</body>
</html>