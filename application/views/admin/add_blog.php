<?php include('header.php'); ?>
        <!--  BEGIN CONTENT AREA  -->
        <?php echo $this->session->flashdata('suc_msg'); ?>
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div class="col-lg-12 col-12 layout-spacing">
                            <div class="statbox widget box box-shadow">
                                <div class="widget-header"> 
                                <?php echo $this->session->flashdata('suc_data'); ?>
                                    <div class="row">
                                      <div class="col float-left"> 
                                        <a href="<?=site_url('admin/blogs');?>"> <button class="btn btn-dark" title="Click To show Blogs List"> <svg  width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-corner-up-left"><polyline points="9 14 4 9 9 4"></polyline><path d="M20 20v-7a4 4 0 0 0-4-4H4"></path></svg> Articles Table</button> </a>
                                      </div>
                                      <div class=" float-left">
                                          <button class="btn btn-dark" title="Click To View All Blogs List">
                                            <svg viewBox="0 0 512 512"><path fill="currentColor" d="M464,128H272L208,64H48A48,48,0,0,0,0,112V400a48,48,0,0,0,48,48H464a48,48,0,0,0,48-48V176A48,48,0,0,0,464,128ZM359.5,296a16,16,0,0,1-16,16h-64v64a16,16,0,0,1-16,16h-16a16,16,0,0,1-16-16V312h-64a16,16,0,0,1-16-16V280a16,16,0,0,1,16-16h64V200a16,16,0,0,1,16-16h16a16,16,0,0,1,16,16v64h64a16,16,0,0,1,16,16Z"></path></svg>
                                             <span style="padding: 5px;"> Add New Articles </span>
                                         </button>
                                      </div>
                                    </div>
                                </div>
                                <div class="">
                                  <?php 
                                    date_default_timezone_set("Asia/Calcutta"); 
                                    $today = date("j-M-Y");
                                  ?>
                                   <?=form_open_multipart('admin/insert_blog', array('id' => 'myform_'));?>
                                        <div class="row mb-4">
                                            <div class="col-6">
                                              <label class="col-form-label">* Category</label>
                                                <select name="category_id" id="category" class="form-control input-lg">
                                                    <option value="">Select Category </option>
                                                    <?php
                                                    foreach($category as $rows)
                                                        {
                                                         echo '<option value="'.$rows->category_id.'">'.$rows->category_name.'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-6">
                                              <label class="col-form-label">* Sub category</label>
                                                <select name="sub_category" id="sub" class="form-control" disabled>
                                                    <option value="" disabled>Plz Select Category To enable sub category </option>
                                               </select>
                                            </div>
                                        </div>
                                       <script>
                                            $(document).ready(function(){
                                             $('#category').change(function(){
                                              var category_id = $('#category').val();
                                              
                                              if(category_id != '')
                                              {
                                               $.ajax({
                                                url:"<?php echo base_url(); ?>admin/fetch_state",
                                                method:"POST",
                                                // this is a name of sub_category_id
                                                data:{category_id:category_id},
                                                success:function(data)
                                                {
                                                 $('#sub').html(data);
                                                 $("#sub").removeAttr("disabled");
                                                 // $('#city').html('<option value="">Select City</option>');
                                                }
                                               });
                                              }
                                              else
                                              {
                                               $('#sub').html('<option value="">Select Sub Category</option>');
                                               // $('#city').html('<option value="">Select City</option>');
                                              }
                                             });                                             
                                            });
                                        </script>
                                        <div class="row mb-4">
                                          <div class="col input-group">
                                              <div class="input-group-prepend">
                                                  <span class="input-group-text" id="inputGroup-sizing-sm">Blog Name / Blog Title</span>
                                              </div>
                                              <input type="text" name="blog_title" class="form-control" required>
                                          </div>
                                        </div>
                                        <input type="hidden" name="time" value="<?php echo $today ?>">
                                        <div class="row mb-4">
                                            <div class="col input-group" style="">
                                                
                                                <textarea name="header_text" class="form-control" maxlength="255" placeholder="maxlength 255 text (Short Text)" required style="width: 200px; height: 200px; " ></textarea>
                                            </div>
                                        </div>
                                        <div class="row mb-12" id="area"  style="margin-bottom: 20px;">
                                            <div class="col input-group">
                                                <textarea name="blog_text"></textarea>
                                                <script>
                                                     window.onload = function() {
                                                       CKEDITOR.replace("blog_text", { 
                                                          on : {
                                                            // maximize the editor on startup
                                                            'instanceReady' : function( evt ) {
                                                              evt.editor.resize("100%", $("#area").height());
                                                            }
                                                          }
                                                        });
                                                    };
                                                </script>
                                            </div>
                                        </div>
                                        </div>

                                        <div class="row mb-4">
                                            <div class="col input-group mb-3">
                                              <div class="input-group-prepend">
                                                <span class="input-group-text">Upload Images</span>
                                              </div>
                                              <div class="custom-file">
                                                <input type="file" name="blog_img" class="custom-file-input" id="inputGroupFile01">
                                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                              </div>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                          <div class="col input-group">
                                              <div class="input-group-prepend">
                                                  <span class="input-group-text" id="inputGroup-sizing-sm">Meta Title</span>
                                              </div>
                                              <input type="text" name="meta_title" class="form-control">
                                          </div>
                                        </div>
                                        <div class="row mb-4">
                                          <div class="col input-group">
                                              <div class="input-group-prepend">
                                                  <span class="input-group-text" id="inputGroup-sizing-sm">Meta Keywords</span>
                                              </div>
                                              <textarea class="form-control" name="meta_keyword"></textarea>
                                          </div>
                                        </div>

                                        <div class="row mb-4">
                                          <div class="col input-group">
                                              <div class="input-group-prepend">
                                                  <span class="input-group-text" id="inputGroup-sizing-sm">Meta Description</span>
                                              </div>
                                              <textarea class="form-control" name="meta_description"></textarea>
                                          </div>
                                        </div>
                                       <button type="submit" class="btn btn-primary mb-2 mr-2" id="hide_btn_">
                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="cloud-upload-alt" class="svg-inline--fa fa-cloud-upload-alt fa-w-20" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M537.6 226.6c4.1-10.7 6.4-22.4 6.4-34.6 0-53-43-96-96-96-19.7 0-38.1 6-53.3 16.2C367 64.2 315.3 32 256 32c-88.4 0-160 71.6-160 160 0 2.7.1 5.4.2 8.1C40.2 219.8 0 273.2 0 336c0 79.5 64.5 144 144 144h368c70.7 0 128-57.3 128-128 0-61.9-44-113.6-102.4-125.4zM393.4 288H328v112c0 8.8-7.2 16-16 16h-48c-8.8 0-16-7.2-16-16V288h-65.4c-14.3 0-21.4-17.2-11.3-27.3l105.4-105.4c6.2-6.2 16.4-6.2 22.6 0l105.4 105.4c10.1 10.1 2.9 27.3-11.3 27.3z"></path></svg>
                                         Save
                                       </button>
                                       <button type="button" class="btn btn-primary mb-2 mr-2" id="show_btn_">
                                            <svg id="loader_css" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="sync-alt" class="svg-inline--fa fa-sync-alt fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M370.72 133.28C339.458 104.008 298.888 87.962 255.848 88c-77.458.068-144.328 53.178-162.791 126.85-1.344 5.363-6.122 9.15-11.651 9.15H24.103c-7.498 0-13.194-6.807-11.807-14.176C33.933 94.924 134.813 8 256 8c66.448 0 126.791 26.136 171.315 68.685L463.03 40.97C478.149 25.851 504 36.559 504 57.941V192c0 13.255-10.745 24-24 24H345.941c-21.382 0-32.09-25.851-16.971-40.971l41.75-41.749zM32 296h134.059c21.382 0 32.09 25.851 16.971 40.971l-41.75 41.75c31.262 29.273 71.835 45.319 114.876 45.28 77.418-.07 144.315-53.144 162.787-126.849 1.344-5.363 6.122-9.15 11.651-9.15h57.304c7.498 0 13.194 6.807 11.807 14.176C478.067 417.076 377.187 504 256 504c-66.448 0-126.791-26.136-171.315-68.685L48.97 471.03C33.851 486.149 8 475.441 8 454.059V320c0-13.255 10.745-24 24-24z"></path></svg>
                                         Loading..
                                       </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                </div>

                <script>
                  $(document).ready(function(){
                    $("#show_btn_").hide();
                    $("#myform_").on("submit", function(){
                      $("#hide_btn_").hide();
                      $("#show_btn_").show();
                    });//submit
                  })
                </script>
                
                <div class="footer-wrapper">
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg> Nishant</p>
                </div>
            </div>
            </div>
            
        </div>
        <!--  END CONTENT AREA  -->

    </div>




    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/plugins/highlight/highlight.pack.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
</body>
</html>