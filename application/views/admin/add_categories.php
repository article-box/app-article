<?php include('header.php'); ?>
        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div class="col-lg-12 col-12 layout-spacing">
                            <div class="statbox widget box box-shadow">
                                <div class="widget-header">                                 
                                    <div class="row">
                                        <div class="col float-left mb-3">
                                            <a href="<?=site_url('admin/show_categories');?>"> <button class="btn btn-dark" title="Click To Show Categories List"> <svg  width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-corner-up-left"><polyline points="9 14 4 9 9 4"></polyline><path d="M20 20v-7a4 4 0 0 0-4-4H4"></path></svg> Show Categories</button> </a>        
                                            <div style="  height: auto; background: #1b2e4b; border-radius: 10px;margin: 10px; width: 100%;padding: 10px;" id="message">
                                            </div>
                                        </div>
                                        <div class="float-left">
                                            <button class="btn btn-dark" title="Click To View All Blogs List">
                                                <svg viewBox="0 0 512 512"><path fill="currentColor" d="M464,128H272L208,64H48A48,48,0,0,0,0,112V400a48,48,0,0,0,48,48H464a48,48,0,0,0,48-48V176A48,48,0,0,0,464,128ZM359.5,296a16,16,0,0,1-16,16h-64v64a16,16,0,0,1-16,16h-16a16,16,0,0,1-16-16V312h-64a16,16,0,0,1-16-16V280a16,16,0,0,1,16-16h64V200a16,16,0,0,1,16-16h16a16,16,0,0,1,16,16v64h64a16,16,0,0,1,16,16Z"></path></svg>
                                                <span style="padding: 5px;"> Add Blog Categories </span>
                                            </button>
                                            <!-- <h4>Add Blog Categories</h4> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                   <?=form_open_multipart('admin/insert_categories',array('id' => 'contactForm'));?>
                                        <!-- <div class="row mb-4">
                                            <div class="col-6">
                                                <input type="text" name="categories_name" class="form-control" placeholder="Categories Name">
                                            </div>
                                        </div> -->
                                        <div class="row mb-4">
                                          <div class="col input-group">
                                              <div class="input-group-prepend">
                                                  <span class="input-group-text" id="inputGroup-sizing-sm">Categories Name</span>
                                              </div>
                                              <input type="text" name="categories_name" class="form-control" required>
                                          </div>
                                          <div class="col input-group">
                                              <div class="input-group-prepend">
                                                  <span class="input-group-text" id="inputGroup-sizing-sm">Meta Title</span>
                                              </div>
                                              <input type="text" name="meta_title" class="form-control" required>
                                          </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div class="col input-group" style="">
                                                <textarea name="meta_description" class="form-control" maxlength="255" placeholder="Meta Description" required style="width: 200px; height: 150px; " required></textarea>
                                            </div>
                                        </div>
                                        <div class="row mb-4">
                                            <div class="col input-group" style="">
                                                <textarea name="meta_keyword" class="form-control" maxlength="255" placeholder="Meta Keyword" required></textarea>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <button type="submit" class="btn btn-primary mb-2 mr-2">
                                                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="cloud-upload-alt" class="svg-inline--fa fa-cloud-upload-alt fa-w-20" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M537.6 226.6c4.1-10.7 6.4-22.4 6.4-34.6 0-53-43-96-96-96-19.7 0-38.1 6-53.3 16.2C367 64.2 315.3 32 256 32c-88.4 0-160 71.6-160 160 0 2.7.1 5.4.2 8.1C40.2 219.8 0 273.2 0 336c0 79.5 64.5 144 144 144h368c70.7 0 128-57.3 128-128 0-61.9-44-113.6-102.4-125.4zM393.4 288H328v112c0 8.8-7.2 16-16 16h-48c-8.8 0-16-7.2-16-16V288h-65.4c-14.3 0-21.4-17.2-11.3-27.3l105.4-105.4c6.2-6.2 16.4-6.2 22.6 0l105.4 105.4c10.1 10.1 2.9 27.3-11.3 27.3z"></path></svg>
                                                Save
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <script>
                $(function() {
                    $("#message").hide();
                    $("#contactForm").on('submit', function(e) {
                        e.preventDefault();
                        var contactForm = $(this);
                        $.ajax({
                            url: contactForm.attr('action'),
                            type: 'post',
                            data: contactForm.serialize(),
                            success: function(response){
                                console.log(response);
                                // location.reload();
                                if(response.status == 'success') {
                                    // $("#contactForm").hide();
                                    setTimeout(function(){ $("#message").show(); }, 2000);
                                    setTimeout(function(){ $(".swal2-container").hide(); }, 2000);
                                    swal({
                                      title: response.message,
                                      text: 'done',
                                      type: 'success',
                                      padding: '2em'
                                    })
                                    setTimeout(function(){ $("#message").hide(); }, 5000);
                                    setTimeout(function(){ $("#contactForm")[0].reset(); }, 2000);
                                     $("#message").html(response.message);
                                    // setTimeout(function(){ location.reload(); }, 2000);

                                }
                                // $("#message").html(response.message);
                            }
                        });
                    });
                });
            </script>
            <div class="footer-wrapper">
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg> Nishant</p>
                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->
    </div>

    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/plugins/highlight/highlight.pack.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
    <!-- <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script> -->
    <script src="<?=base_url();?>dashboard/plugins/sweetalerts/sweetalert2.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/sweetalerts/custom-sweetalert.js"></script>
</body>
</html>