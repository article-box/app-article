<?php include('header.php') ?>

        <!--  BEGIN CONTENT AREA  -->
        <?php echo $this->session->flashdata('suc_msg'); ?>
        <a href="<?=site_url('admin/add_blog');?>"> 
            <div class="hide__">
                <button class="btn btn-primary" id="on_click_load" title="Add More Blogs" style="position: fixed; bottom: 30px; right: 50px; border-radius:100%; z-index:999; padding:20px 20px;" >
                    <div id="click_to_hide">
                        <svg viewBox="0 0 512 512"><path fill="currentColor" d="M464,128H272L208,64H48A48,48,0,0,0,0,112V400a48,48,0,0,0,48,48H464a48,48,0,0,0,48-48V176A48,48,0,0,0,464,128ZM359.5,296a16,16,0,0,1-16,16h-64v64a16,16,0,0,1-16,16h-16a16,16,0,0,1-16-16V312h-64a16,16,0,0,1-16-16V280a16,16,0,0,1,16-16h64V200a16,16,0,0,1,16-16h16a16,16,0,0,1,16,16v64h64a16,16,0,0,1,16,16Z"></path></svg>
                    </div>

                    <div id="click_to_show">
                        <svg id="loader_css" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="sync-alt" class="svg-inline--fa fa-sync-alt fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M370.72 133.28C339.458 104.008 298.888 87.962 255.848 88c-77.458.068-144.328 53.178-162.791 126.85-1.344 5.363-6.122 9.15-11.651 9.15H24.103c-7.498 0-13.194-6.807-11.807-14.176C33.933 94.924 134.813 8 256 8c66.448 0 126.791 26.136 171.315 68.685L463.03 40.97C478.149 25.851 504 36.559 504 57.941V192c0 13.255-10.745 24-24 24H345.941c-21.382 0-32.09-25.851-16.971-40.971l41.75-41.749zM32 296h134.059c21.382 0 32.09 25.851 16.971 40.971l-41.75 41.75c31.262 29.273 71.835 45.319 114.876 45.28 77.418-.07 144.315-53.144 162.787-126.849 1.344-5.363 6.122-9.15 11.651-9.15h57.304c7.498 0 13.194 6.807 11.807 14.176C478.067 417.076 377.187 504 256 504c-66.448 0-126.791-26.136-171.315-68.685L48.97 471.03C33.851 486.149 8 475.441 8 454.059V320c0-13.255 10.745-24 24-24z"></path></svg>
                    </div>
                </button> 
            </div>
        </a>
        <script >
            $('#click_to_show').hide();
            $('.hide__').on('click', '#on_click_load', function(){ 
                $('#click_to_hide').hide();
                $('#click_to_show').show();
            });
        </script>
         <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <div class="widget-content widget-content-area br-6">
                            <div class="table-responsive mb-4 mt-4">
                                <table id="html5-extension" class="table table-hover non-hover" style="width:100%">
                                    <thead>
                                        <div class="message1" id="message"></div>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Title</th>
                                            <!-- <th>Author</th> -->
                                            <th>Catogery</th>
                                            <th>Image</th>
                                            <th>Status</th>
                                            <th title="trending">T</th>
                                            <th title="Pin">P</th>
                                            <th title="Star">S</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="showdata">
                                        <?php $i=0; foreach($this->db_connection->orderbyblog() as $blog){ $i++; ?>
                                        <tr>
                                            <td><?=$i;?></td>
                                            <td><?=$blog->blog_title?></td>
                                            
                                            <?php
                                                foreach($this->db_connection->blog_category() as $blog_category)
                                                if($blog_category->category_id==$blog->category_id){
                                            ?>
                                            <td><?=$blog_category->category_name?></td>
                                            <?php }  ?>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="usr-img-frame mr-2 "style="width: 100px; height: 50px;">
                                                        <img alt="avatar" style="width: 100px; height: 50px;" class="img-fluid " src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <?=form_open_multipart('admin/blog_status/'.$blog->blog_id);?>
                                                    <label class="switch s-primary mr-2">
                                                        <?php if($blog->status==1){ ?>
                                                            <input type="checkbox" name="status" onclick="this.form.submit();" value="2" checked>
                                                        <?php } else{ ?>
                                                            <input type="checkbox" name="status" onclick="this.form.submit();" value="1" >
                                                        <?php } ?>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </form>
                                            </td>
                                            <td>
                                                <?=form_open_multipart('admin/trending/'.$blog->blog_id);?>
                                                        <?php if($blog->trending==1){ ?>
                                                                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="fire" class="svg-inline--fa fa-fire fa-w-12" role="img" viewBox="0 0 384 512" style="height:  50px; width: 50px;"><path fill="currentColor" d="M216 23.86c0-23.8-30.65-32.77-44.15-13.04C48 191.85 224 200 224 288c0 35.63-29.11 64.46-64.85 63.99-35.17-.45-63.15-29.77-63.15-64.94v-85.51c0-21.7-26.47-32.23-41.43-16.5C27.8 213.16 0 261.33 0 320c0 105.87 86.13 192 192 192s192-86.13 192-192c0-170.29-168-193-168-296.14z" style="color:tomato;"></path></svg>
                                                            <input class="form-check-input" type="checkbox" name="status" onclick="this.form.submit();" value="2" checked>
                                                        <?php } else{ ?>

                                                                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="fire" class="svg-inline--fa fa-fire fa-w-12" role="img" viewBox="0 0 384 512" style="height:  50px; width: 50px;"><path fill="currentColor" d="M216 23.86c0-23.8-30.65-32.77-44.15-13.04C48 191.85 224 200 224 288c0 35.63-29.11 64.46-64.85 63.99-35.17-.45-63.15-29.77-63.15-64.94v-85.51c0-21.7-26.47-32.23-41.43-16.5C27.8 213.16 0 261.33 0 320c0 105.87 86.13 192 192 192s192-86.13 192-192c0-170.29-168-193-168-296.14z"></path></svg>
                                                            <input class="form-check-input" type="checkbox" name="status" onclick="this.form.submit();" value="1" >
                                                        <?php } ?>
                                                        <span class="slider round"></span>
                                                </form>
                                            </td>
                                            <td>
                                                <?=form_open_multipart('admin/pin/'.$blog->blog_id);?>
                                                    <?php if($blog->pin==1){ ?>
                                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="thumbtack" class="svg-inline--fa fa-thumbtack fa-w-12" role="img" viewBox="0 0 384 512" style="height:  50px; width: 50px;"><path fill="currentColor" d="M298.028 214.267L285.793 96H328c13.255 0 24-10.745 24-24V24c0-13.255-10.745-24-24-24H56C42.745 0 32 10.745 32 24v48c0 13.255 10.745 24 24 24h42.207L85.972 214.267C37.465 236.82 0 277.261 0 328c0 13.255 10.745 24 24 24h136v104.007c0 1.242.289 2.467.845 3.578l24 48c2.941 5.882 11.364 5.893 14.311 0l24-48a8.008 8.008 0 0 0 .845-3.578V352h136c13.255 0 24-10.745 24-24-.001-51.183-37.983-91.42-85.973-113.733z" style="color: blue; cursor: pointer;"></path></svg>
                                                        <input class="form-check-input" type="checkbox" name="pin" onclick="this.form.submit();" value="2" checked>
                                                    <?php } else{ ?>
                                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="thumbtack" class="svg-inline--fa fa-thumbtack fa-w-12" role="img" viewBox="0 0 384 512" style="height:  50px; width: 50px;"><path fill="currentColor" d="M298.028 214.267L285.793 96H328c13.255 0 24-10.745 24-24V24c0-13.255-10.745-24-24-24H56C42.745 0 32 10.745 32 24v48c0 13.255 10.745 24 24 24h42.207L85.972 214.267C37.465 236.82 0 277.261 0 328c0 13.255 10.745 24 24 24h136v104.007c0 1.242.289 2.467.845 3.578l24 48c2.941 5.882 11.364 5.893 14.311 0l24-48a8.008 8.008 0 0 0 .845-3.578V352h136c13.255 0 24-10.745 24-24-.001-51.183-37.983-91.42-85.973-113.733z"></path></svg>
                                                        <input class="form-check-input" type="checkbox" name="pin" onclick="this.form.submit();" value="1" >
                                                    <?php } ?>
                                                    <span class="slider round"></span>
                                                </form>
                                            </td>
                                            <td>
                                                <?=form_open_multipart('admin/star/'.$blog->blog_id);?>
                                                    <?php if($blog->star==1){ ?>

                                                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="star" class="svg-inline--fa fa-star fa-w-18" role="img" viewBox="0 0 576 512"  style="height:  50px; width: 50px; color: blue;"><path fill="currentColor" d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path></svg>
                                                        <input class="form-check-input" type="checkbox" name="star" onclick="this.form.submit();" value="2" checked>
                                                    <?php } else{ ?>

                                                            <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="star" class="svg-inline--fa fa-star fa-w-18" role="img" viewBox="0 0 576 512" style="height:  50px; width: 50px;"><path fill="currentColor" d="M528.1 171.5L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6zM388.6 312.3l23.7 138.4L288 385.4l-124.3 65.3 23.7-138.4-100.6-98 139-20.2 62.2-126 62.2 126 139 20.2-100.6 98z"></path></svg>
                                                        <input class="form-check-input" style="color: red;" type="checkbox" name="star" onclick="this.form.submit();" value="1" >
                                                    <?php } ?>
                                                    <span class="slider round"></span>
                                                </form>
                                            </td>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="<?=site_url('admin/show_blogs/'.$blog->blog_id);?>"> <button type="button" class="btn btn-dark btn-sm">Open</button> </a>
                                                    <button type="button" class="btn btn-dark btn-sm dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
                                                      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuReference1">
                                                      <a class="dropdown-item" href="<?=site_url('admin/update_blog/'.$blog->blog_id);?>">
                                                        <svg viewBox="0 0 576 512" ><path fill="currentColor" d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z"></path></svg>
                                                        Update
                                                      </a>
                                                      <a class="dropdown-item item-delete" href="javascript:;" data="<?=$blog->blog_id?>" >
                                                        
                                                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="trash-alt" class="svg-inline--fa fa-trash-alt fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z"></path></svg>
                                                        Delete
                                                      </a>
                                                    </div>
                                                  </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $("#message").hide();
                $('#showdata').on('click', '.item-delete', function(){
                    var el = this;
                    var id = $(this).attr('data');
                        if(confirm('Are you sure for Delete')){
                        $.ajax({
                            type: 'ajax',
                            method: 'get',
                            async: false,
                            url: '<?php echo base_url() ?>admin/deleteblog',
                            data:{id:id},
                            dataType: 'json',
                            success: function(response){
                                if(response.status == 'success'){
                                    $('#message').html('<span class="icon">❌️</span> Deleted successfully').fadeIn().delay(3000).fadeOut('slow');
                                    setTimeout(function(){ $(".swal2-container").hide(); }, 1000);
                                    swal({
                                      title: response.message,
                                      text: 'done',
                                      type: 'success',
                                      padding: '2em'
                                    })
                                    $(el).closest('tr').css('background','tomato');
                                            $(el).closest('tr').fadeOut(800,function(){
                                       $(this).remove();
                                    });
                                }else{
                                    alert('Error');
                                }
                            },
                            error: function(){
                                alert('Error deleting');
                            }
                        });
                    }
                });
            </script>
    
            <div class="footer-wrapper">
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg>   Nishant </p>
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTAINER -->
    
    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL CUSTOM SCRIPTS -->
    <script src="<?=base_url();?>dashboard/plugins/table/datatable/datatables.js"></script>
    <!-- NOTE TO Use Copy CSV Excel PDF Print Options You Must Include These Files  -->
    <script src="<?=base_url();?>dashboard/plugins/table/datatable/button-ext/dataTables.buttons.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/table/datatable/button-ext/jszip.min.js"></script>    
    <script src="<?=base_url();?>dashboard/plugins/table/datatable/button-ext/buttons.html5.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/table/datatable/button-ext/buttons.print.min.js"></script>
        <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/sweetalerts/sweetalert2.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/sweetalerts/custom-sweetalert.js"></script>
    <script>
        $('#html5-extension').DataTable( {
            dom: '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
            buttons: {
                buttons: [
                    { extend: 'copy', className: 'btn' },
                    { extend: 'csv', className: 'btn' },
                    { extend: 'excel', className: 'btn' },
                    { extend: 'print', className: 'btn' }
                ]
            },
            "oLanguage": {
                "oPaginate": { "sPrevious": '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
               "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 10 
        } );
    </script>
</body>
</html>