<?php include('header.php'); ?>
<!--  BEGIN CONTENT AREA  -->
<?php echo $this->session->flashdata('suc_msg'); ?>
<a href="<?=site_url('admin/add_service');?>"> <button class="btn btn-primary" title="Add More sub categories" style="position: fixed; bottom: 30px; right: 50px; border-radius:100%; z-index:999;padding:20px 20px;" >
    <svg viewBox="0 0 512 512"><path fill="currentColor" d="M464,128H272L208,64H48A48,48,0,0,0,0,112V400a48,48,0,0,0,48,48H464a48,48,0,0,0,48-48V176A48,48,0,0,0,464,128ZM359.5,296a16,16,0,0,1-16,16h-64v64a16,16,0,0,1-16,16h-16a16,16,0,0,1-16-16V312h-64a16,16,0,0,1-16-16V280a16,16,0,0,1,16-16h64V200a16,16,0,0,1,16-16h16a16,16,0,0,1,16,16v64h64a16,16,0,0,1,16,16Z"></path></svg>
</button> </a>
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <div class="widget-content widget-content-area br-6">
                            <div class="table-responsive mb-4 mt-4">
                                <div style="  height: auto; background: white; border-radius: 10px; width: auto;padding: 10px;" id="message"></div>
                                <table id="zero-config" class="table" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Cateroty</th>
                                            <th class="btn-primary"> Sub Categories Name</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                            <!--<th class="no-content"></th>-->
                                        </tr>
                                    </thead>
                                    <tbody id="showdata">
                                    <?php $i=0; foreach($this->db_connection->select_sub() as $sub){ $i++; ?>
                                        <tr>
                                            <td><?=$i;?></td>
                                            <td>
                                                <?php foreach($this->db_connection->blog_category() as $category){ if($sub->category_id==$category->category_id){ ?>
                                                    <span class="author category"><?=$category->category_name?></span>
                                                <?php } }?>
                                            </td>
                                            <td><?=$sub->sub_categories_name?></td>
                                            <td>
                                                <?=form_open_multipart('admin/subcategories_status/'.$sub->id,array('id' => 'contactForm'));?>
                                                    <label class="switch s-primary mr-2">
                                                        <?php if($sub->status==1){ ?>
                                                            <input class="change-status" type="checkbox" name="status" onclick="this.form.submit();" value="2" checked>
                                                        <?php } else{ ?>
                                                            <input class="change-status" type="checkbox" name="status" onclick="this.form.submit();" value="1" >
                                                        <?php } ?>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </form>
                                            </td>
                                            <td><a href="javascript:;" class="btn btn-outline-danger item-delete" data="<?=$sub->id?>">Delete</a>
                                            <a href="<?=site_url('admin/update_sub_categories/'.$sub->id);?>" class="btn btn-outline-primary" title="Edit">
                                                <svg viewBox="0 0 576 512"><path fill="currentColor" d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z"></path></svg>
                                            </a></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    $("#message").hide();
                    $('#showdata').on('click', '.item-delete', function(){
                         var el = this;
                        var id = $(this).attr('data');
                        // $('#deleteModal').modal('show');
                        //prevent previous handler - unbind()
                        // $('.item-delete').unbind().click(function(){
                            if(confirm('Are you sure for Delete')){
                            $.ajax({
                                type: 'ajax',
                                method: 'get',
                                async: false,
                                url: '<?php echo base_url() ?>admin/delete_subcategory',
                                data:{id:id},
                                dataType: 'json',
                                success: function(response){
                                    if(response.status == 'success'){
                                        // $('#deleteModal').modal('hide');
                                        $('#message').html('Deleted successfully').fadeIn().delay(1000).fadeOut('slow');
                                        setTimeout(function(){ $(".swal2-container").hide(); }, 2000);
                                        swal({
                                          title: response.message,
                                          text: 'done',
                                          type: 'success',
                                          padding: '2em'
                                        })
                                        $(el).closest('tr').css('background','tomato');
                                                $(el).closest('tr').fadeOut(800,function(){
                                           $(this).remove();
                                        });
                                    }else{
                                        alert('Error');
                                    }
                                },
                                error: function(){
                                    alert('Error deleting');
                                }
                            });
                        }
                        // });
                    });
                </script>
            <script>
                $(function() {
                    $("#message").hide();
                    $(".btn-danger").on('click', function(e) {
                        e.preventDefault();
                        var contactForm = $(this);
                        $.ajax({
                            url: contactForm.attr('action'),
                            type: 'post',
                            data: contactForm.serialize(),
                            success: function(response){
                                console.log(response);
                                // location.reload();
                                if(response.status == 'success') {
                                    // $("#contactForm").hide();
                                    setTimeout(function(){ $("#message").show(); }, 2000);
                                    setTimeout(function(){ $(".swal2-container").hide(); }, 2000);
                                    swal({
                                      title: response.message,
                                      text: 'done',
                                      type: 'success',
                                      padding: '2em'
                                    })
                                    setTimeout(function(){ $("#message").hide(); }, 5000);
                                    setTimeout(function(){ $("#contactForm")[0].reset(); }, 2000);
                                     $("#message").html(response.message);
                                    // setTimeout(function(){ location.reload(); }, 2000);

                                }
                                // $("#message").html(response.message);
                            }
                        });
                    });
                });
            </script>
            </div>
            <div class="footer-wrapper">
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg> Nishant</p>
                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->
    </div>
    <!-- END MAIN CONTAINER -->




    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/plugins/highlight/highlight.pack.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
     <script src="<?=base_url();?>dashboard/plugins/sweetalerts/sweetalert2.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/sweetalerts/custom-sweetalert.js"></script>

     <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?=base_url();?>dashboard/plugins/table/datatable/datatables.js"></script>
    <script>
        $('#zero-config').DataTable({
            "oLanguage": {
                "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
               "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7 
        });
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->
</body>
</html>