<?php include('header.php') ?>

        <!--  BEGIN CONTENT AREA  -->
        <?php echo $this->session->flashdata('suc_msg'); ?>
       <a href="<?=site_url('admin/login_history');?>"> <button class="btn btn-primary" title="Reload" style="position: fixed; bottom: 30px; right: 50px; border-radius:100%; z-index:999; padding:20px 20px;" >
           <i class="fas fa-history" style="font-size: 20px;"></i>
       </button> </a>
         <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <div class="widget-content widget-content-area br-6">
                            <div class="d-flex justify-content-end">
        
                                <select class="form-control d-flex m-1 col-sm-1" id="limit" aria-label="Default select example">
                                  <option selected value="10">10</option>
                                  <option value="25">25</option>
                                  <option value="50">50</option>
                                  <option value="70">75</option>
                                  <option value="100">100</option>
                                </select>

                                <select class="form-control d-flex m-1 col-sm-2" id="uplodes" aria-label="Default select example">
                                  <option selected value="">Uplodes</option>
                                  <option value="desc">Newest</option>
                                  <option value="asc">Oldest</option>
                                </select>

                                <input type="date" id="toDate" class="form-control m-1" placeholder="Search ...">

                                <input type="date" id="fromDate" class="form-control m-1" placeholder="Search ...">

                                <input type="search" id="searchData" class="form-control m-1" placeholder="Search ...">

                                <button class="btn btn-primary m-1" id="apply"> Apply </button>
                            </div>
                            <div id="loginData"></div>
                        </div>
                    </div>
                </div>
            </div>    
            <div class="footer-wrapper">
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg>   Nishant </p>
                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->
    </div>
    <!-- END MAIN CONTAINER -->

    <script>
        function loadHistory(){
            $.ajax({
                url:"<?=site_url('admin/loginHistory')?>",
                type:"POST",
                success:function(data){
                    $("#loginData").html(data);
                }
            })
        }
        loadHistory();

         $(document).on("click", "#loginPageSegment", function(){
            var data = $(this).data("id");
            var searchData = $("#searchData").val();
            var limit = $("#limit").val();
            var uplodes = $("#uplodes").val();
            $.ajax({
                url:"<?=site_url('admin/loginHistory');?>",
                type:"POST",
                data:{
                    page:data,
                    limit:limit,
                    searchData:searchData,
                    uplodes:uplodes,
                },
                success:function(data){
                    $('#loginData').html(data);
                }
            });
          })

         $(document).on("click", "#apply", function(){
            var data = $("#loginPageSegment").data("id");
            var searchData = $("#searchData").val();
            var limit = $("#limit").val();
            var uplodes = $("#uplodes").val();
            $.ajax({
                url:"<?=site_url('admin/loginHistory');?>",
                type:"POST",
                data:{
                    page:data,
                    searchData:searchData,
                    limit:limit,
                    uplodes:uplodes,
                },
                success:function(data){
                    $('#loginData').html(data);
                }
            });
          })
    </script>
    
    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <!-- BEGIN PAGE LEVEL CUSTOM SCRIPTS -->
    <script src="<?=base_url();?>dashboard/plugins/table/datatable/datatables.js"></script>
    <!-- NOTE TO Use Copy CSV Excel PDF Print Options You Must Include These Files  -->
    <script src="<?=base_url();?>dashboard/plugins/table/datatable/button-ext/dataTables.buttons.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/table/datatable/button-ext/jszip.min.js"></script>    
    <script src="<?=base_url();?>dashboard/plugins/table/datatable/button-ext/buttons.html5.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/table/datatable/button-ext/buttons.print.min.js"></script>
        <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/sweetalerts/sweetalert2.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/sweetalerts/custom-sweetalert.js"></script>
    <script>
        $('#html5-extension').DataTable( {
            dom: '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >',
            buttons: {
                buttons: [
                    { extend: 'copy', className: 'btn' },
                    { extend: 'csv', className: 'btn' },
                    { extend: 'excel', className: 'btn' },
                    { extend: 'print', className: 'btn' }
                ]
            },
            "oLanguage": {
                "oPaginate": { "sPrevious": '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
               "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 10 
        } );
    </script>
    <!-- END PAGE LEVEL CUSTOM SCRIPTS -->
</body>
</html>