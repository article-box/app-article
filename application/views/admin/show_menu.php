<?php include('header.php'); ?>
<!--  BEGIN CONTENT AREA  -->
<?php echo $this->session->flashdata('suc_msg'); ?>
<a href="<?=site_url('admin/add_menu');?>"> <button class="btn btn-dark" title="Add More Menu" style="position: fixed; bottom: 30px; right: 50px; border-radius:100%; z-index:999; padding:16px 20px;" > ➕ </button> </a>
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">                
                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <div class="widget-content widget-content-area br-6">
                            <div class="table-responsive mb-4 mt-4">
                                <!-- zero-config -->
                                <div style="  height: auto; background: #1b2e4b; border-radius: 10px; width: 100%;padding: 10px;" id="message"></div>
                                <table id="zero-config" class="table table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Name</th>
                                            <th>Url</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                            <!--<th class="no-content"></th>-->
                                        </tr>
                                    </thead>
                                    <tbody id="showdata">
                                    <?php $i=0; foreach($this->db_connection->select_menu() as $menu){ $i++; ?>
                                        <tr>
                                            <td><?=$i;?></td>
                                            <td><?=$menu->menu_name?></td>
                                            <td><?=$menu->menu_url?></td>
                                            <td>
                                                <?=form_open_multipart('admin/menu_status/'.$menu->menu_id);?>
                                                    <label class="switch s-primary mr-2">
                                                        <?php if($menu->status==1){ ?>
                                                            <input type="checkbox" name="status" title="Activate" onclick="this.form.submit();" value="2" checked>
                                                        <?php } else{ ?>
                                                            <input type="checkbox" name="status" title="Deactivate" onclick="this.form.submit();" value="1" >
                                                        <?php } ?>
                                                        <span class="slider round"></span>
                                                    </label>
                                                </form>
                                            </td>
                                            <td> <a href="<?=site_url('admin/update_menu/'.$menu->menu_id);?>"> <button class="btn btn-dark" title="Update">Update </button> </a>

                                            <a  href="javascript:;" class="btn btn-danger item-delete" data="<?=$menu->menu_id?>" > Delete </a> </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>          
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                $("#message").hide();
                $('#showdata').on('click', '.item-delete', function(){
                     var el = this;
                    var id = $(this).attr('data');
                    // $('#deleteModal').modal('show');
                    //prevent previous handler - unbind()
                    // $('.item-delete').unbind().click(function(){
                        if(confirm('Are you sure for Delete')){
                        $.ajax({
                            type: 'ajax',
                            method: 'get',
                            async: false,
                            url: '<?php echo base_url() ?>admin/deletemenu',
                            data:{id:id},
                            dataType: 'json',
                            success: function(response){
                                if(response.status == 'success'){
                                    // $('#deleteModal').modal('hide');
                                    $('#message').html('Deleted successfully').fadeIn().delay(1000).fadeOut('slow');
                                    setTimeout(function(){ $(".swal2-container").hide(); }, 2000);
                                    swal({
                                      title: response.message,
                                      text: 'done',
                                      type: 'success',
                                      padding: '2em'
                                    })
                                    $(el).closest('tr').css('background','tomato');
                                            $(el).closest('tr').fadeOut(800,function(){
                                       $(this).remove();
                                    });
                                }else{
                                    alert('Error');
                                }
                            },
                            error: function(){
                                alert('Error deleting');
                            }
                        });
                    }
                    // });
                });
            </script>
            <div class="footer-wrapper">
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg> Nishant</p>
                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->
    </div>
    <!-- END MAIN CONTAINER -->




    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/plugins/highlight/highlight.pack.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
        <script src="<?=base_url();?>dashboard/plugins/sweetalerts/sweetalert2.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/sweetalerts/custom-sweetalert.js"></script>

     <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?=base_url();?>dashboard/plugins/table/datatable/datatables.js"></script>
    <script>
        $('#zero-config').DataTable({
            "oLanguage": {
                "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
               "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7 
        });
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->
</body>
</html>