<?php include('header.php') ?>
        <div id="content" class="main-content">
            <div id="alertMsg"></div>
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <div id="bannerData"></div>
                    </div>
                </div>
            </div>    
            <div class="footer-wrapper">
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg> Nishant </p>
                </div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="editBanner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit Banner</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="updateForm"></div>
      </div>
    </div>
  </div>
</div>


<?php include("load_data/alert.php"); ?>
<script>
    function loadBannerData(){
        $.ajax({
            url: "<?=site_url('admin/show_banner');?>",
            type: "POST",
            beforeSend: function() {
                preloder();
            },
            success: function(response) {
                $("#bannerData").html(response);
            },
            complete: function() {
                $("#loading").html("");
            }
        })
    }loadBannerData();

    $(document).on("click", "#updateBanner", function(){
        var id = $(this).data("id");
        $.ajax({
            url:"<?=site_url('admin/update_banner');?>",
            type:"POST",
            data:{id:id},
            success:function(data){
                $("#updateForm").html(data);
            }
        })
    })


    $(document).on("submit", "#bannerUpdate", function(e){
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url:"<?=site_url('admin/edit_banner');?>",
            type:"POST",
            data: form_data,
            contentType: false,
            processData: false,
            success:function(data){
                alertMassage(data);
                loadBannerData();
            }
        })
    })

    $(document).on("change", "#bannerStatus", function(){
        var id = $(this).val();
        if ($(this).is(':checked')) {
            var status = 1;
        }else{
            var status = 2;
        }
        $.ajax({
            url:"<?=site_url('admin/banner_status');?>",
            type:"POST",
            data:{id:id, status:status},
            success:function(data){
              alertMassage(data);
              loadBannerData();
            }
        });
    })

    $(document).on("click", "#deleteBanner", function(){
        var id = $(this).data("id");
        if (confirm("Do You Really want to delete this record ?")) {
            $.ajax({
                url:"<?=site_url('admin/delete_banner');?>",
                type:"POST",
                data:{id:id},
                success:function(data){
                  alertMassage(data);
                  loadBannerData();
                }
            });
        }
    })

</script>
    
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
</body>
</html>