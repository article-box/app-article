<?php include('header.php'); ?>
<!--  BEGIN CONTENT AREA  -->
<?php echo $this->session->flashdata('suc_msg'); ?>
<a id="btn-add-notes" href="javascript:void(0);"> <button class="btn btn-primary" title="Add More Categories" style="position: fixed; bottom: 30px; right: 50px; border-radius:100%; z-index:999; padding:20px 20px;" >
    <svg viewBox="0 0 512 512"><path fill="currentColor" d="M464,128H272L208,64H48A48,48,0,0,0,0,112V400a48,48,0,0,0,48,48H464a48,48,0,0,0,48-48V176A48,48,0,0,0,464,128ZM359.5,296a16,16,0,0,1-16,16h-64v64a16,16,0,0,1-16,16h-16a16,16,0,0,1-16-16V312h-64a16,16,0,0,1-16-16V280a16,16,0,0,1,16-16h64V200a16,16,0,0,1,16-16h16a16,16,0,0,1,16,16v64h64a16,16,0,0,1,16,16Z"></path></svg>
</button> </a>
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <div class="widget-content widget-content-area br-6">
                            <div class="table-responsive mb-4 mt-4">
                                <div style="height: auto; background: #060818; color: white; border-radius: 10px; width: auto;padding: 10px;" id="message"></div>
                                <div class="justify-content-between d-flex p-3">
                                    <div class="d-flex"> 
                                        
                                    </div>
                                    <div class="d-flex">
                                        <a data-toggle="modal" data-target="#insertFormModel">
                                            <i class="fa fa-plus text-primary btn bg-transparent"></i> 
                                        </a> 
                                    </div>
                                </div>
                                <div class="cat_table" id="cat_table"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-wrapper">
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg> Nishant</p>
                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->
    </div>
    <!-- END MAIN CONTAINER -->


    <!-- Modal 1 for insert category -->
    <div class="modal fade" id="insertFormModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <i class="fas fa-th-list"></i> Add More Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <i class="fas fa-times"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <?=form_open_multipart('admin/insert_categories',array('id' => 'contactForm_'));?>
                        <div class="notes-box">
                            <div id="message1" class=""  style=" background-color: #060818; color: white; height: auto; width: 100%;"></div>
                            <div class="notes-content">
                                <div class="row">
                                    <div class="col-md-12 mb-2">
                                        <div class="d-flex note-title">
                                            <input type="text" id="n-title" name="categories_name" class="form-control" maxlength="30" placeholder="Categories Name" required>
                                        </div>
                                        <span class="validation-text"></span>
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <div class="d-flex note-title">
                                            <input type="text" id="n-title" name="meta_title" class="form-control" maxlength="30" placeholder="Meta Title" required>
                                        </div>
                                        <span class="validation-text"></span>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="d-flex note-description">
                                            <textarea id="n-description" class="form-control" name="meta_description" maxlength="255" placeholder=" Meta Description" rows="3" required></textarea>
                                        </div>
                                        <span class="validation-text"></span>
                                    </div>
                                    <div class="col-md-12 mt-3">
                                         <div class="d-flex note-description">
                                            <textarea id="n-description" class="form-control" name="meta_keyword" maxlength="255" placeholder=" Meta Keyword" rows="2" required></textarea>
                                        </div>
                                        <span class="validation-text"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal 2 for update category -->
    <div class="modal fade" id="updateFormModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <i class="fas fa-th-list"></i>  Update Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <i class="fas fa-times"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="updateFormData"> <h1>test</h1> </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal 3 for View Sub Category -->
    <div class="modal fade" id="viewSubCatModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <i class="fas fa-th-list"></i>  View Sub Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <i class="fas fa-times"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="subCategotyData"> </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                    <a data-toggle="modal" data-target="#insertSubCatModel" id="insertSubCategories" href="#">
                        <button type="button" class="btn btn-primary"> <i class="fa fa-plus"></i> Add</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal 4 for Edit Sub Category -->
    <div class="modal fade" id="editSubCatModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <i class="fas fa-th-list"></i>  Edit Sub Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <i class="fas fa-times"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="subCategotyUpdateForm"> <h1>test</h1> </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal 5 for Insert Sub Category -->
    <div class="modal fade" id="insertSubCatModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <i class="fas fa-th-list"></i>  Insert Sub Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <i class="fas fa-times"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="subCategotyInsertForm"> <h1>test</h1> </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal"><i class="flaticon-cancel-12"></i> Discard</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <?php include("load_data/alert.php"); ?>
    <script type="text/javascript">
        $(document).ready(function(){
          $('#cat_table').load('<?=site_url('admin/cat_table');?>');
        });

        $(document).on("click", "#editCategories", function() {
            var id = $(this).data("id");
            $.ajax({
                url: "<?= site_url("admin/update_categories") ?>",
                type: "POST",
                data: {
                    id: id
                },
                beforeSend: function() {
                    preloder();
                },
                success: function(data) {
                    $("#updateFormData").html(data);
                },
                complete: function() {
                    $("#loading").html("");
                }
            })
        })

        $(document).on("click", "#viewSubCategories", function() {
            var id = $(this).data("id");
            $.ajax({
                url: "<?= site_url("admin/subCategoriesList") ?>",
                type: "POST",
                data: {
                    id: id
                },
                beforeSend: function() {
                    preloder();
                },
                success: function(data) {
                    $("#subCategotyData").html(data);
                },
                complete: function() {
                    $("#loading").html("");
                }
            })
        })

        $(document).on("click", "#editSubCategories", function() {
            var id = $(this).data("id");
            $.ajax({
                url: "<?= site_url("admin/update_sub_categories") ?>",
                type: "POST",
                data: {
                    id: id
                },
                beforeSend: function() {
                    preloder();
                },
                success: function(data) {
                    $("#subCategotyUpdateForm").html(data);
                },
                complete: function() {
                    $("#loading").html("");
                }
            })
        })

        $(document).on("click", "#insertSubCategories", function() {
            var id = $(this).data("id");
            $.ajax({
                url: "<?= site_url("admin/add_service") ?>",
                type: "POST",
                data: {
                    id: id
                },
                beforeSend: function() {
                    preloder();
                },
                success: function(data) {
                    $("#subCategotyInsertForm").html(data);
                },
                complete: function() {
                    $("#loading").html("");
                }
            })
        })

    </script>

    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/plugins/highlight/highlight.pack.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
</body>
</html>