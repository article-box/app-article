<div class="">
   <?=form_open_multipart('admin/insert_category',array('id' => 'contactForm'));?>
        <div class="row mb-4">
            <div class="col">
               <select class="form-control" name="category_id">
                <?php foreach($this->db_connection->blog_category() as $menu){ ?>
                   <option value="<?=$menu->category_id?>"><?=$menu->category_name?></option>
               <?php }?>
               </select>
            </div>
            <div class="col">
                <input type="text" name="sub_category_name" class="form-control" placeholder="Sub Category Name" required>
            </div>                                       
        </div>
        <div class="row mb-4">
              <div class="col input-group">
                  <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroup-sizing-sm">Meta Title</span>
                  </div>
                  <input type="text" name="meta_title" class="form-control" required>
              </div>
            </div>
            <div class="row mb-4">
                <div class="col input-group" style="">
                    <textarea name="meta_description" class="form-control" maxlength="255" placeholder="Meta Description" required style="width: 200px; height: 150px; " required></textarea>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col input-group" style="">
                    <textarea name="meta_keyword" class="form-control" maxlength="255" placeholder="Meta Keyword" required></textarea>
                </div>
            </div>
            <div class="col">
                <button type="submit" class="btn btn-primary mb-2 mr-2">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="cloud-upload-alt" class="svg-inline--fa fa-cloud-upload-alt fa-w-20" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M537.6 226.6c4.1-10.7 6.4-22.4 6.4-34.6 0-53-43-96-96-96-19.7 0-38.1 6-53.3 16.2C367 64.2 315.3 32 256 32c-88.4 0-160 71.6-160 160 0 2.7.1 5.4.2 8.1C40.2 219.8 0 273.2 0 336c0 79.5 64.5 144 144 144h368c70.7 0 128-57.3 128-128 0-61.9-44-113.6-102.4-125.4zM393.4 288H328v112c0 8.8-7.2 16-16 16h-48c-8.8 0-16-7.2-16-16V288h-65.4c-14.3 0-21.4-17.2-11.3-27.3l105.4-105.4c6.2-6.2 16.4-6.2 22.6 0l105.4 105.4c10.1 10.1 2.9 27.3-11.3 27.3z"></path></svg>
                    Save
                </button>
            </div>
    </form>
</div>