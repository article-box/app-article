<?php include('header.php'); ?>
        <!--  BEGIN CONTENT AREA  -->
<?php echo $this->session->flashdata('suc_msg'); ?>
<a href="<?=site_url('admin/add_about');?>" data-toggle="modal" data-target="#addabout" id="insertForm"> <button class="btn btn-primary" title="Add More About" style="position: fixed; bottom: 30px; right: 50px; border-radius:100%; z-index:999; padding:20px 20px;"> 
    <svg viewBox="0 0 512 512"><path fill="currentColor" d="M464,128H272L208,64H48A48,48,0,0,0,0,112V400a48,48,0,0,0,48,48H464a48,48,0,0,0,48-48V176A48,48,0,0,0,464,128ZM359.5,296a16,16,0,0,1-16,16h-64v64a16,16,0,0,1-16,16h-16a16,16,0,0,1-16-16V312h-64a16,16,0,0,1-16-16V280a16,16,0,0,1,16-16h64V200a16,16,0,0,1,16-16h16a16,16,0,0,1,16,16v64h64a16,16,0,0,1,16,16Z"></path></svg>
</button> </a>
    
        <div id="content" class="main-content">
            <div id="alertMsg"></div>
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing">
                    <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                        <div id="aboutUsData"></div>
                    </div>
                </div>
            </div>    
            <div class="footer-wrapper">
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg> Nishant </p>
                </div>
            </div>
        </div>
    </div>

<!--insert Modal -->
<div class="modal fade" id="addabout" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Insert About</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="insertAboutForm"></div>
      </div>
    </div>
  </div>
</div>

<!--Update Modal -->
<div class="modal fade" id="updateModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Update About</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="updateAboutForm"></div>
      </div>
    </div>
  </div>
</div>

<?php include("load_data/alert.php"); ?>
<script>

    function loadAbout(){
        $.ajax({
            url:"<?=site_url('admin/aboutusdata');?>",
            type:"POST",
            success:function(data){
                $("#aboutUsData").html(data);
            }
        })
    }loadAbout();

    $(document).on("click", "#insertForm", function(){
         $.ajax({
            url:"<?=site_url('admin/add_about');?>",
            type:"POST",
            success:function(data){
                $("#insertAboutForm").html(data);
                CKEDITOR.replace('text');
            }
        })
    })

    $(document).on("submit", "#insert_AboutForm", function(e){
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url:"<?=site_url('admin/ins_about');?>",
            type:"POST",
            data: form_data,
            contentType: false,
            processData: false,
            success:function(data){
                $("#insert_AboutForm")[0].reset();
                loadAbout();
                alertMassage(data);
            }
        })
    })

    $(document).on("submit", "#update_AboutForm", function(e){
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url:"<?=site_url('admin/edit_about');?>",
            type:"POST",
            data: form_data,
            contentType: false,
            processData: false,
            success:function(data){
                loadAbout();
                alertMassage(data);
            }
        })
    })

    $(document).on("click", "#updateAbout", function(){
        var id = $(this).data("id");
         $.ajax({
            url:"<?=site_url('admin/update_about');?>",
            type:"POST",
            data:{id:id},
            success:function(data){
                $("#updateAboutForm").html(data);
                CKEDITOR.replace('utext');
            }
        })
    })

    $(document).on("click", "#deleteAbout", function(){
        var id = $(this).data("id");
        if (confirm("Do You Really want to delete this record ?")) {
            $.ajax({
                url:"<?=site_url('admin/delete_about');?>",
                type:"POST",
                data:{id:id},
                success:function(data){
                    loadAbout();
                    alertMassage(data);
                }
            });
        }
    })

    $(document).on("change", "#aboutStatus", function(){
        var id = $(this).data("id");
        if ($(this).is(':checked')) {
            var status = 1;
        }else{
            var status = 2;
        }
        $.ajax({
            url:"<?=site_url('admin/about_status');?>",
            type:"POST",
            data:{id:id, status:status},
            success:function(data){
              alertMassage(data);
              loadAbout();
            }
        });
    })

</script>

    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/blockui/jquery.blockUI.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/plugins/highlight/highlight.pack.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <!--  BEGIN CUSTOM SCRIPTS FILE  -->
    <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
</body>
</html>