<?php include('header.php'); ?>

<div id="content" class="main-content">
	<div id="alertMsg"></div>
	<div class="layout-px-spacing">
    	<div class="row layout-top-spacing">
        	<div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
        		<div class="col-sm-10 mb-2 widget-content widget-content-area br-6">
	                <div class="d-flex justify-content-end mb-2">
	                	<select class="form-control d-flex mr-1 col-sm-1" id="limit" aria-label="Default select example">
                          <option selected value="5">5</option>
                          <option value="10">10</option>
                          <option value="25">25</option>
                          <option value="50">50</option>
                          <option value="70">75</option>
                          <option value="100">100</option>
                        </select>

                        <select class="form-control d-flex col-sm-2 mr-1" id="status" aria-label="Default select example">
                          <option selected value="">Status</option>
                          <option value="1">Active</option>
                          <option value="null">Draft</option>
                        </select>
	                    
	                    <div class="input-group d-flex mr-1">
	                        <div class="input-group-prepend">
	                            <span class="input-group-text" id="inputGroup-sizing-sm">To</span>
	                        </div>
	                        <input type="date" name="to" id="to" class="form-control" required>
	                    </div>

	                    <div class=" input-group d-flex mr-1">
	                        <div class="input-group-prepend">
	                            <span class="input-group-text" id="inputGroup-sizing-sm">From</span>
	                        </div>
	                        <input type="date" name="from" id="from" class="form-control" required>
	                    </div>

	                    <button class=" btn btn-dark" id="apply"> Apply </button>
	                </div>
	                <div class="d-flex justify-content-end">
	                	<select class="form-control d-flex mr-1 col-sm-2" id="uplodes" aria-label="Default select example">
                          <option selected value="">Uplodes</option>
                          <option value="desc">Newest</option>
                          <option value="asc">Oldest</option>
                        </select>
	                    <input type="search" id="searchData" class="form-control d-flex mr-1" placeholder="Search ...">
	                </div>
        		</div>
            	<div id="commentData" class="pre-loading"></div>
            </div>
        </div>
    </div>
</div>

<!-- Reply Comment Modal -->
<div class="modal fade" id="replyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Reply Comment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="insertReply">
            <div class="input-group input-group-sm mb-4">
                <input type="text" name="reply_text" id="reply_text" class="form-control" placeholder="Reply Comment" required>
             	
                <div class="input-group-append">
                	<button class="btn btn-outline-primary" type="submit" id="button-addon2">
                    	<i class="fas fa-paper-plane"></i>
    	            </button>
             	</div>
            </div>
            <div id="replyData" class="col-xl-12"></div>
        </form>
      </div>
    </div>
  </div>
</div>


<?php include("load_data/alert.php"); ?>
<script>

	$(document).on("keyup", "#searchData", function(e) {
        e.preventDefault();
        $("#apply").click();
    })

    $(document).on("change", "#uplodes, #status, #limit", function(e) {
        e.preventDefault();
        $("#apply").click();
    })

    $(document).on("click", "#apply", function(e) {
        e.preventDefault();
        loadCommentData(page_url = false);
    })

	/*--first time load--*/
    loadCommentData(page_url = false);

    /*-- Page click --*/
    $(document).on('click', ".pagination li a", function(event) {
        var page_url = $(this).attr('href');
        // alert(page_url);
        loadCommentData(page_url);
        event.preventDefault();
    });

    function loadCommentData(page_url = false) {
        var base_url = '<?php echo site_url('admin/commentdata/') ?>';

        var searchData = $("#searchData").val();
        var status = $("#status").val();
        var to = $("#to").val();
        var from = $("#from").val();
        var uplodes = $("#uplodes").val();
        var limit = $("#limit").val();

        if (page_url == false) {
            var page_url = base_url;
        }

        $.ajax({
            type: "POST",
            url: page_url,
            data: {
                searchData: searchData,
                status:status,
                to: to,
                from: from,
                uplodes:uplodes,
                limit:limit
            },
            beforeSend: function() {
                preloder();
            },
            success: function(response) {
                // console.log(response);
                $("#commentData").html(response);
                $("ul.pagination > li > a").addClass("page-link");
            },
            complete: function() {
                $("#loading").html("");
            }
        });
    }

    $(document).on("change", "#varified", function(){
        var id = $(this).val();
        if ($(this).is(':checked')) {
            var status = 1;
        }else{
            var status = 2;
        }
        $.ajax({
            url:"<?=site_url('admin/varified');?>",
            type:"POST",
            data:{id:id, status:status},
            success:function(data){
              alertMassage(data);
              // $("#apply").click();
            }
        });
    })

    $(document).on("click", "#deleteComment", function(){
        var id = $(this).data("id");
        if (confirm("Do You Really want to delete this record ?")) {
            $.ajax({
                url:"<?=site_url('admin/delete_comment');?>",
                type:"POST",
                data:{id:id},
                success:function(data){
                  alertMassage(data);
                  $("#apply").click();
                }
            });
        }
    })

    $(document).on("click", "#deleteReply", function(){
        var comment_id = $("#comment_id").val();
        var id = $(this).data("id");
        if (confirm("Do You Really want to delete this record ?")) {
            $.ajax({
                url:"<?=site_url('admin/delete_reply');?>",
                type:"POST",
                data:{id:id,comment_id:comment_id},
                success:function(data){
                  if(data==''){
                        alertMassage("Error !");
                    }else{
                        loadReplyData(data);
                        alertMassage("Delete Comment Successfull !");
                    }                  
                }
            });
        }
    })

    $(document).on("click", "#reply", function(){
    	var id = $(this).data("id");
        loadReplyData(id);
    })

    function loadReplyData(id){
        $.ajax({
            url:"<?=site_url('admin/comment_replyData');?>",
            type:"POST",
            data:{id:id},
            success:function(data){
                $("#replyData").html(data);
            }
        })
    }

    $(document).on("submit", "#insertReply", function(e){
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url:"<?=site_url('admin/insert_reply');?>",
            type:"POST",
            data: form_data,
            contentType: false,
            processData: false,
            success:function(data){
                $("#insertReply")[0].reset();
                if(data==''){
                    alertMassage("Error !");
                }else{
                    loadReplyData(data);
                    alertMassage("Reply Comment Successfull !");
                }
            }
        })
    })
</script>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/blockui/jquery.blockUI.min.js"></script>
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/plugins/highlight/highlight.pack.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY STYLES -->
    <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
            <script src="<?=base_url();?>dashboard/plugins/sweetalerts/sweetalert2.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/sweetalerts/custom-sweetalert.js"></script>
</body>
</html>