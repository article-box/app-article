<div class="table-responsive mb-4 mt-4">
  <table id="html5-extension" class="table table-hover non-hover bg-transparent">
      <thead>
          <tr>
              <th>S.no</th>
              <th>Time</th>
              <th>Name</th>
              <th>Email</th>
              <th>Date</th>
              <th>IP</th>
          </tr>
      </thead>
      <tbody id="showdata">
        <?php foreach($loginHistory as $history){ ?>
        <tr>
            <td><?=$history->id?></td>
            <td><?=$history->time?></td>
            <?php foreach($selectSignUp as $nam_){ if($nam_->email== $history->email){ ?>
                  <td> <?=$nam_->name?> </td>  
            <?php } }?>
            <td><?=$history->email?></td>
            <td><?=$history->date?></td>
            <td><?=$history->ip?></td>
        </tr>
        <?php } ?>
      </tbody>
      <div id="paginationLink"></div>
  </table>
</div>