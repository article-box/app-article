<script>
//----------- alert mesage functions  ----------//
function alertMassage(message) {
    var msg =
        '<div class="alert alert-success bg-dark position-fixed" role="alert" style="z-index: 99999999; right:50px;"> <i class="fas fa-check-circle" style="color:#0FF16F;"></i>  ' +
        message + ' <i class="fas fa-times-circle ml-4 btn btn-danger p-1" id="closeBtn"></i></div>';
    $("#alertMsg").html(msg).fadeIn();
}
//----------- click to close alert message ----------//
$(document).on("click", "#closeBtn", function() {
    $("#alertMsg").html("");
})

function preloder() {
    var msg ='<div class="text-center m-4"><i class="fas fa-spinner text-primary" id="loader_css" style="font-size:30px;"></i></div>';
    $(".pre-loading").html(msg).fadeIn();
}

function loading(id) {
    var msg ='<div class="text-center m-4"><i class="fas fa-spinner text-primary" id="loader_css" style="font-size:30px;"></i></div>';
    $("#"+id).html(msg).fadeIn();
}
</script>