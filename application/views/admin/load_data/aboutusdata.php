<?php foreach ($data as $about) { ?>
<div class="card component-card_2 col-sm-10 mb-2" style="width:100%; margin:auto;">
    <div class="card-body">
        <h5 class="card-title"><?= $about->title ?> <i class="far fa-address-book"></i></h5>
        <div class="card-text text-wrap">
            <?=$about->text ?>
        </div>

        <div class="d-flex justify-content-between">
            <div class="d-flex">

                <a href="<?= site_url('admin/update_about/' . $about->about_id); ?>" class="btn bg-transparent" data-id="<?=$about->about_id?>" data-toggle="modal" data-target="#updateModel" id="updateAbout">
                    <i class="fas fa-edit text-primary"></i>
                </a>

                <a data-id="<?= $about->about_id ?>" class="btn bg-transparent" id="deleteAbout">
                    <i class="fas fa-trash-alt text-danger"></i>
                </a>

            </div>
            <div class="d-flex">
                <div class="n-chk">
                    <label class="new-control new-checkbox new-checkbox-rounded checkbox-outline-primary">
                        <?php if ($about->status == 1) { ?>
                        <input type="checkbox" class="new-control-input" id="aboutStatus"
                            data-id="<?= $about->about_id ?>" name="status" checked>
                        <span class="pl-2"> Active </span>
                        <?php } else { ?>
                        <input type="checkbox" class="new-control-input" id="aboutStatus"
                            data-id="<?= $about->about_id ?>" name="status">
                        <span class="pl-2"> Draft </span>
                        <?php } ?>
                        <span class="new-control-indicator"></span>
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>