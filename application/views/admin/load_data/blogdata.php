

    <div class="table-responsive mb-4 mt-4">
        <table id="html5-extension" class="table bg-transparent non-hover" style="width:100%">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>Title</th>
                    <th>Status</th>
                    <th title="trending">T</th>
                    <th title="Pin">P</th>
                    <th title="Star">S</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="showdata" class="articleData">
                <?php $i = 0; foreach ($data as $blog) { $i++; ?>
                <tr>
                    <td><?= $blog->blog_id ?></td>
                    <td><?= $blog->blog_title ?></td>
                    <td>
                        <div class="n-chk">
                            <label class="new-control new-checkbox new-checkbox-rounded checkbox-outline-primary">
                                <?php if ($blog->status == 1) { ?>
                                <input type="checkbox" value="<?= $blog->blog_id ?>" id="blogStatus"
                                    class="new-control-input" checked>
                                <span class="pl-2"> Active </span>
                                <?php } else { ?>
                                <input type="checkbox" value="<?= $blog->blog_id ?>" id="blogStatus"
                                    class="new-control-input">
                                <span class="pl-2"> Draft </span>
                                <?php } ?>
                                <span class="new-control-indicator"></span>
                            </label>
                        </div>
                    </td>

                    <td>
                        <div class="n-chk">
                            <label class="new-control new-checkbox new-checkbox-rounded checkbox-outline-primary">
                                <?php if ($blog->trending == 1) { ?>
                                <input type="checkbox" value="<?= $blog->blog_id ?>" id="trandUpdate"
                                    class="new-control-input" checked>
                                <span class="pl-2"> 
                                    <i class="fas fa-fire" style="color:#F1C40F;"></i>
                                </span>
                                <?php } else { ?>
                                <input type="checkbox" value="<?= $blog->blog_id ?>" id="trandUpdate"
                                    class="new-control-input">
                                <span class="pl-2"> 
                                    <i class="fas fa-fire"></i>
                                </span>
                                <?php } ?>
                                <span class="new-control-indicator"></span>
                            </label>
                        </div>
                    </td>

                    <td>
                        <div class="n-chk">
                            <label class="new-control new-checkbox new-checkbox-rounded checkbox-outline-primary">
                                <?php if ($blog->pin == 1) { ?>
                                <input type="checkbox" value="<?= $blog->blog_id ?>" id="pinUpdate"
                                    class="new-control-input" checked>
                                <span class="pl-2"> 
                                    <i class="fas fa-thumbtack" style="color:blue;"></i>
                                </span>
                                <?php } else { ?>
                                <input type="checkbox" value="<?= $blog->blog_id ?>" id="pinUpdate"
                                    class="new-control-input">
                                <span class="pl-2"> 
                                    <i class="fas fa-thumbtack"></i>
                                </span>
                                <?php } ?>
                                <span class="new-control-indicator"></span>
                            </label>
                        </div>
                    </td>

                    <td>
                        <div class="n-chk">
                            <label class="new-control new-checkbox new-checkbox-rounded checkbox-outline-primary">
                                <?php if ($blog->star == 1) { ?>
                                <input type="checkbox" value="<?= $blog->blog_id ?>" id="starUpdate"
                                    class="new-control-input" checked>
                                <span class="pl-2"> 
                                    <i class="fas fa-star" style="color:blue;"></i>
                                </span>
                                <?php } else { ?>
                                <input type="checkbox" value="<?= $blog->blog_id ?>" id="starUpdate"
                                    class="new-control-input">
                                <span class="pl-2"> 
                                    <i class="fas fa-star"></i>
                                </span>
                                <?php } ?>
                                <span class="new-control-indicator"></span>
                            </label>
                        </div>
                    </td>

                    <td>
                        <a href="<?=site_url('admin/update_blog/'.$blog->blog_id)?>" >
                            <i class="far fa-eye"></i>
                        </a>
                        
                        <a href="<?=site_url('admin/update_blog/'.$blog->blog_id)?>" >
                            <i class="fas fa-edit"></i>
                        </a>
                        <i class="fas fa-trash-alt" id="deleteArticle" data-id="<?=$blog->blog_id ?>" style="color:red; cursor: pointer;"></i>
                    </td>

                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="text-center">
        <?php echo $pagelinks; ?>
    </div>