


<table id="zero-config" class="table" style="background-color: transparent; width:100%">
    <thead>
        <tr>
            <th>S.No</th>
            <th>Name</th>
            <th>Status</th>
            <th>Action</th>
            <th>Sub Cat</th>
            <th class="no-content"></th>
        </tr>
    </thead>
    <tbody id="showdata">
    <?php $i=0; foreach($this->db_connection->select_categories() as $categories){ $i++; ?>
        <tr>
            <td><?=$i;?></td>
            <td><?=$categories->category_name?></td>
            <td>
                <?=form_open_multipart('admin/categories_status/'.$categories->category_id,array('id' => 'contactForm'));?>
                    <label class="switch s-primary mr-2">
                        <?php if($categories->status==1){ ?>
                            <input class="change-status" type="checkbox" name="status" onclick="this.form.submit();" value="2" checked>
                        <?php } else{ ?>
                            <input class="change-status" type="checkbox" name="status" onclick="this.form.submit();" value="1" >
                        <?php } ?>
                        <span class="slider round"></span>
                    </label>
                </form>
            </td>
            <td>
                <a href="javascript:;" class="btn bg-transparent text-danger p-1 item-delete" data="<?=$categories->category_id?>"><i class="fa fa-trash-alt"></i> Delete
                </a>
                <!-- <a href="<?=site_url('admin/update_categories/'.$categories->category_id);?>" title="Edit" class="btn bg-transparent p-1 text-primary">
                    <i class="fa fa-pen"></i> Edit
                </a> -->
                <a data-toggle="modal" data-target="#updateFormModel" title="Edit" href="#" id="editCategories" data-id="<?=$categories->category_id?>">
                    <i class="fa fa-pen text-primary"></i> Edit
                </a>
            </td>
            <td>
                <a data-toggle="modal" data-target="#viewSubCatModel" data-id="<?=$categories->category_id?>" href="#" id="viewSubCategories">
                    <!-- <button class="btn btn-primary p-1" ></button> -->
                    Sub
                    <i class="fa fa-eye"> 
                        <?php echo $this->db_connection->sub_categories_list($categories->category_id,$count=true);?> 
                    </i> |
                </a>
                <a data-toggle="modal" data-target="#insertSubCatModel" data-id="<?=$categories->category_id?>" href="#" id="insertSubCategories">
                    <i class="fa fa-plus-square text-primary"></i>
                </a>
            </td>
              
        </tr>
    <?php } ?>
    </tbody>
</table>
<script>
    $("#message").hide();
    $('#showdata').on('click', '.item-delete', function(){
         var el = this;
        var id = $(this).attr('data');
        // $('#deleteModal').modal('show');
        //prevent previous handler - unbind()
        // $('.item-delete').unbind().click(function(){
            if(confirm('Are you sure for Delete')){
            $.ajax({
                type: 'ajax',
                method: 'get',
                async: false,
                url: '<?php echo base_url() ?>admin/delete_category',
                data:{id:id},
                dataType: 'json',
                success: function(response){
                    if(response.status == 'success'){
                        // $('#deleteModal').modal('hide');
                        $('#message').html('Deleted successfully').fadeIn().delay(1000).fadeOut('slow');
                        setTimeout(function(){ $(".swal2-container").hide(); }, 2000);
                        swal({
                          title: response.message,
                          text: 'done',
                          type: 'success',
                          padding: '2em'
                        })
                        $(el).closest('tr').css('background','tomato');
                                $(el).closest('tr').fadeOut(800,function(){
                           $(this).remove();
                        });
                    }else{
                        alert('Error');
                    }
                },
                error: function(){
                    alert('Error deleting');
                }
            });
        }
        // });
    });
</script>