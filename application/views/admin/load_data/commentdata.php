<?php foreach($data as $comment){ ?>
<div class="card mb-2 col-sm-10">
    <div class="card-body">
        <p class="card-text p-2 on_hover">
        	<i class="fas fa-comment-dots"></i> <?=$comment->comment?>
        </p>
        <div class="user-info">
            <div class="media-body">
                <div class="d-flex justify-content-between">
                	<div class="d-flex">
                		<p class="card-user_name text-muted p-1">
                            <i class="fas fa-user-circle text-warning"></i><strong> <?=$comment->name?></strong>
                            |
                            <?=$comment->date_time?>
                        </p>
                	</div>
                	<div class="d-flex">

                        <div class="n-chk p-1">
                            <label class="new-control new-checkbox new-checkbox-rounded checkbox-outline-success">
                                <?php if ($comment->varified==1) { ?>
                                <input type="checkbox" value="<?=$comment->comment_id?>" id="varified"
                                    class="new-control-input" checked>
                                <span class="pl-2"> Varified </span>
                                <?php } else { ?>
                                <input type="checkbox" value="<?=$comment->comment_id?>" id="varified" class="new-control-input">
                                <span class="pl-2"> Unvarified </span>
                                <?php } ?>
                                <span class="new-control-indicator"></span>
                            </label>
                        </div>

                		<i class="fas fa-reply btn bg-transparent p-1 text-primary" data-toggle="modal" data-target="#replyModal" id="reply" data-id="<?=$comment->comment_id?>"></i>

                		<i class="fas fa-trash-alt btn bg-transparent p-1 text-danger" id="deleteComment" data-id="<?=$comment->comment_id?>"></i>
                	</div>
                </div>
                <a href="<?=site_url("admin/update_blog/".$comment->blog_id)?>"> 
                    <p class="on_hover text-muted"><i class="fas fa-newspaper"></i>  <?=$comment->blog_title?> </p>
                </a>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div class="text-center">
    <?php echo $pagelinks; ?>
</div>