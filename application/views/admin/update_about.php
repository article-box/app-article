<form id="update_AboutForm">
<input type="hidden" name="id" value="<?=$about->about_id?>">
<div class="row mb-4">
    <div class="col input-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="inputGroup-sizing-sm">About Title</span>
        </div>
        <input type="text" name="title" value="<?=$about->title?>" class="form-control" autocomplete="off" required autofocus>
    </div>
</div>
<div class="row mb-12">
    <div class="col input-group">
        <textarea name="utext" class="form-control" ><?=$about->text?></textarea>
    </div>
</div>
<div class="row mb-12">
    <div class="col input-group" style="padding:20px;">
        <button type="submit" class="btn btn-primary mb-2 mr-2">
            Update
        </button>
    </div>
</div>
</form>