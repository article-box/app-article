<?php include('header.php'); ?>
        <div id="content" class="main-content">
            <div id="alertMsg"></div>
            <div class="layout-px-spacing">                
                <div class="account-settings-container layout-top-spacing">
                    <div class="row">
                        <form id="authorForm">
                            <input type="hidden" name="id" value="<?=$details->details?>">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <div id="general-info" class="section general-info">
                                    <div class="info">
                                        <h6 class="">General Information 
                                            <button type="submit" class="btn btn-primary"> Save </button> 
                                        </h6>
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-xl-2 col-lg-12 col-md-4">
                                                        <div class="upload mt-4 pr-md-4">
                                                            <input type="file" name="profile_img" id="input-file-max-fs" class="dropify" data-default-file="<?=base_url();?>images/profile/<?=$details->profile_img;?>" data-max-file-size="2M" accept="image/*" />
                                                            <p class="mt-2">
                                                              <i class="flaticon-cloud-upload mr-1"></i>
                                                               Upload Picture
                                                             </p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-10 col-lg-12 col-md-8 mt-md-0 mt-4">
                                                        <div class="form">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="fullName">Full Name</label>
                                                                        <input type="text" name="name" class="form-control mb-4" id="fullName" placeholder="Company Name" value="<?=$details->name?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="fullName">Mail Id</label>
                                                                        <input type="email" name="gmail" class="form-control mb-4" id="fullName" placeholder="gmail" value="<?=$details->gmail?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="phone">About You (Short Text*)</label>
                                                                <textarea  class="form-control mb-4" name="about_you" placeholder="Paset Embad Code "> <?=$details->about_you?> </textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="profession">PDF Link</label>
                                                                <input type="text" name="pdf" class="form-control mb-4" id="profession" placeholder="Pdf Link (Google Drive)" value="<?=$details->pdf?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <div id="contact" class="section contact">
                                    <div class="info">
                                        <h5 class="">Contact</h5>
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="country">Country</label>
                                                            <input type="text" name="country" class="form-control mb-4" id="profession" placeholder="Country"  value="<?=$details->country?>"  autocomplete="off" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="address">Address</label>
                                                            <input type="text" class="form-control mb-4" id="address" placeholder="Address" name="address" value="<?=$details->address?>" >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="location">Location (City)</label>
                                                            <input type="text" name="city" class="form-control mb-4" id="location" placeholder="Location (City)" value="<?=$details->city?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="phone">Phone</label>
                                                            <input type="text" class="form-control mb-4" id="phone" name="phone" placeholder="Write your phone number here" value="<?=$details->phone?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="phone">Google Map (Embad a map Code)</label>
                                                            <textarea  class="form-control mb-4" name="map" placeholder="Paset Embad Code " style="height: 150px;"> <?=$details->map?> </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <div id="social" class="section social">
                                    <div class="info">
                                        <h5 class="">Social</h5>
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="input-group social-linkedin mb-3">
                                                            <div class="input-group-prepend mr-3">
                                                                <span class="input-group-text" id="linkedin"> 
                                                                </span>
                                                            </div>
                                                            <input type="text" name="linkedin" class="form-control" placeholder="linkedin Username" aria-label="Username" aria-describedby="linkedin" value="<?=$details->linkedin?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="input-group social-tweet mb-3">
                                                            <div class="input-group-prepend mr-3">
                                                                <span class="input-group-text" id="tweet"> 
                                                                </span>
                                                            </div>
                                                            <input type="text" name="twitter" class="form-control" placeholder="Twitter Username" aria-label="Username" aria-describedby="tweet" value="<?=$details->twitter?>">
                                                        </div>
                                                    </div>                                                        
                                                </div>
                                            </div>

                                            <div class="col-md-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="input-group social-fb mb-3">
                                                            <div class="input-group-prepend mr-3">
                                                                <span class="input-group-text" id="fb">  
                                                                </span>
                                                            </div>
                                                            <input type="text" name="fb" class="form-control" placeholder="Facebook Username" aria-label="Username" aria-describedby="fb" value="<?=$details->fb?>">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="input-group social-instagram mb-3">
                                                            <div class="input-group-prepend mr-3">
                                                                <span class="input-group-text" >
                                                                
                                                                </span>
                                                            </div>
                                                            <input type="text" name="whatsapp" class="form-control" placeholder="Whatsapp Link" aria-label="Username" aria-describedby="github" value="<?=$details->whatsapp?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-group social-instagram mb-3">
                                                            <div class="input-group-prepend mr-3">
                                                                <span class="input-group-text" >
                                                                     
                                                                </span>
                                                            </div>
                                                            <input type="text" name="instagram" class="form-control" placeholder="Instagram Link" aria-label="Username" aria-describedby="github" value="<?=$details->instagram?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <div id="about" class="section about">
                                    <div class="info">
                                        <!-- <h5 class="">About</h5> -->
                                        <div class="row">
                                            <div class="col-md-11 mx-auto">
                                                <div class="form-group">
                                                    <label for="aboutBio">Bio</label>
                                                    <textarea name="text" class="form-control" id="aboutBio" placeholder="Tell something interesting about yourself" rows="10"><?=$details->text?></textarea>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <div id="social" class="section social">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include("load_data/alert.php"); ?>
<script>
    $(document).on("submit", "#authorForm", function(e){
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url:"<?=site_url('admin/edit_details');?>",
            type:"POST",
            data: form_data,
            contentType: false,
            processData: false,
            success:function(data){
                alertMassage(data);
            }
        })
    })
</script>
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!--  BEGIN CUSTOM SCRIPTS FILE  -->

    <script src="<?=base_url();?>dashboard/plugins/dropify/dropify.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/blockui/jquery.blockUI.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/users/account-settings.js"></script>
    <!--  END CUSTOM SCRIPTS FILE  -->
    <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>
</body>
</html>