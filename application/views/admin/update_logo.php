<form id="logoForm" enctype="multipart/form-data">
    <!-- <h6 class="text-center"><i class="fas fa-image align-self-center"></i> Logo Update </h6> -->
    <input type="hidden" value="<?= $logo->logo_id?>" name="id">

    <div class="d-flex justify-content-between">
        <div class="d-flex">
            <i class="fas fa-upload btn m-1" id="openInputFile"> Logo </i>
        </div>
        <div class="d-flex">
            <button type="submit" name="" class="btn btn-primary" id="submitImgBtn"> Save </button>
        </div>
    </div>

    <script> $("#submitImgBtn").hide(); </script>
    
    <input type="file" name="logo" id="inputFileImage" accept="image/*" hidden/>
    
    <div class="text-left">
        <img title="Logo Image" style="height: 200px; width: auto;" src="<?= base_url(); ?>images/logo/<?= $logo->logo_img ?>">
    </div>
    
    <div id="preview" class="text-center"></div>
</form>