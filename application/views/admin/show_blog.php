<?php include('header.php'); ?>
<!--  BEGIN CONTENT AREA  -->
<?php echo $this->session->flashdata('suc_msg'); ?>

        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                
                <div class="row layout-top-spacing">
                
                    <div class="col-lg-12 layout-spacing">
                        <div class="statbox widget box box-shadow">
                            <div class="widget-header">
                                <div class="row">
                                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                                        <h4>News Feeds</h4>
                                    </div>           
                                </div>
                            </div>
                            <div class="widget-content widget-content-area">

                                <div class='parent ex-4'>
                                    <div class='row'>
                                        <div class="col-md-12">
                                            <a href="<?=site_url('admin/update_blog/'.$blog->blog_id);?>"> <button class="btn btn-primary">Update</button> </a>
                                            <div id='left-rollbacks' class='dragula'>
                                                <h4><?=$blog->blog_title?></h4>
                                                <div class="card post text-post" style="">
                                                    <div class="card-body">
                                                        <!-- <div class="media user-meta d-sm-flex d-block text-sm-left text-center">
                                                            <img class="" src="<?=base_url();?>dashboard/assets/img/dragp-1.jpg" alt="avatar">
                                                            <div class="media-body">
                                                                <h5 class="">Linda Nelson</h5>
                                                                <p class="meta-time">11 hours ago</p>
                                                            </div>
                                                        </div> -->
                                                        <div class="post-content  text-sm-left text-center">
                                                            <img class="" style="height: auto; width: 700px" src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>" alt="avatar">
                                                        </div>

                                                        <div class="post-content  text-sm-left text-center">
                                                            <p><?=$blog->blog_text?></p>
                                                        </div>
                                                        <!-- <div class="row people-liked-post">
                                                            <div class="col-sm-5 text-sm-left text-center">
                                                                <ul class="list-inline people-liked-img">
                                                                    <li class="list-inline-item chat-online-usr">
                                                                        <img alt="avatar" src="<?=base_url();?>dashboard/assets/img/profile-6.jpg" class="">
                                                                    </li>
                                                                    <li class="list-inline-item chat-online-usr">
                                                                        <img alt="avatar" src="<?=base_url();?>dashboard/assets/img/profile-7.jpg">
                                                                    </li>
                                                                    <li class="list-inline-item chat-online-usr">
                                                                        <img alt="avatar" src="<?=base_url();?>dashboard/assets/img/profile-8.jpg">
                                                                    </li>
                                                                    <li class="list-inline-item chat-online-usr">
                                                                        <img alt="avatar" src="<?=base_url();?>dashboard/assets/img/profile-10.jpg">
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-sm-7 text-sm-right text-center">
                                                                <div class="people-liked-post-name">
                                                                    <span><a href="#">Vincent, Mary</a> and 19 other like this</span>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="footer-wrapper">
                <div class="footer-section f-section-1">
                    <p class="">Copyright © 2020 <a target="_blank" href="https://designreset.com/">DesignReset</a>, All rights reserved.</p>
                </div>
                <div class="footer-section f-section-2">
                    <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg></p>
                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->
    </div>
    <!-- END MAIN CONTAINER -->




    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url();?>dashboard/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/app.js"></script>
    
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="<?=base_url();?>dashboard/plugins/highlight/highlight.pack.js"></script>
    <script src="<?=base_url();?>dashboard/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->
    <script src="<?=base_url();?>dashboard/assets/js/scrollspyNav.js"></script>

     <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?=base_url();?>dashboard/plugins/table/datatable/datatables.js"></script>
    <script>
        $('#zero-config').DataTable({
            "oLanguage": {
                "oPaginate": { "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>', "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>' },
                "sInfo": "Showing page _PAGE_ of _PAGES_",
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
               "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            "lengthMenu": [7, 10, 20, 50],
            "pageLength": 7 
        });
    </script>

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?=base_url();?>dashboard/plugins/drag-and-drop/dragula/dragula.min.js"></script>
    <script src="<?=base_url();?>dashboard/plugins/drag-and-drop/dragula/custom-dragula.js"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
</body>
</html>