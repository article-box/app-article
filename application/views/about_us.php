<!DOCTYPE html>
<html lang="en">
<head>
 <?php include('meta.php'); ?>
 <?php include('links.php'); ?>
</head>
<body class="news-content">
<?php include('nav.php'); ?>
    <section class="section-content single">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-push-1 entry-content m-auto">
                    <div style=" border-radius: 5px;  height: 50px; margin-bottom: 30px; padding: 10px; box-shadow: 0 1px 4px 0 rgba(32,33,36,0.18);"> <p style=" text-align: center; "> <i class="fa fa-info-circle"></i> ABOUT-US</p> </div>
                    <?php foreach($this->wp_connection->select_about() as $about){ ?>
                    <article class="blog-item blog-single card-header">
                        <h1 class="post-title"> <?=$about->title?></h1>
                        <p><?=$about->text?></p>
                    </article>
                    <?php } ?>
                </div>
            </div>
        </div>
        
    </section>
    <?php include('footer.php'); ?>