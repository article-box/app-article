<!DOCTYPE html>
<html>
    <head>
        <title>CodeIgniter Pagination</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body id="content">
        <div class="container">
            <h3 class="title is-3">CodeIgniter Database Pagination ' <?=$caty->category_name?> '</h3>
            <div class="column">
                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Contact Name</th>
                            <th>Contact Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=0; foreach ($data as $blog_d){ $i++; ?>
                            <tr>
                                <td><?= $i?></td>
                                <td><?= $blog_d->blog_title ?></td>
                                <td><?= $blog_d->time ?></td>
                                <td><?= $blog_d->category_id ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <p><?php echo $links; ?></p>
            </div>
        </div>
        
        <a class="w-inline-block social-share-btn fb" href="https://www.facebook.com/sharer/sharer.php?u=http://magnati.co.in&t=demo" title="Share on Facebook" target="_blank">dsfkljdshl</a>


        <!-- Facebook -->
        <a href="http://www.facebook.com/sharer.php?u=http://www.example.com" target="_blank">Share to FaceBook</a>

        <!-- Twitter -->
        <a href="http://twitter.com/share?url=http://magnati.co.in/&text=Simple Share Buttons&hashtags=simplesharebuttons" target="_blank">Twitter</a>

        <!-- Google+ -->
        <a href="https://plus.google.com/share?url=http://www.example.com" target="_blank">Google+</a>

        <!-- Digg -->
        <a href="http://www.digg.com/submit?url=http://www.example.com" target="_blank">Digg</a>

        <!-- Reddit -->
        <a href="http://reddit.com/submit?url=http://www.example.com&title=Simple Share Buttons" target="_blank">Reddit</a>

        <!-- LinkedIn -->
        <a href="http://www.linkedin.com/shareArticle?mini=true&url=http://www.example.com" target="_blank">LinkedIn</a>

        <!-- Pinterest -->
        <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">Pinterest</a>

        <!-- StumbleUpon-->
        <a href="http://www.stumbleupon.com/submit?url=http://www.example.com&title=Simple Share Buttons" target="_blank">StumbleUpon</a>

        <!-- Email -->
        <a href="mailto:?Subject=Simple Share Buttons&Body=I%20saw%20this%20and%20thought%20of%20you!%20 http://www.example.com">Email</a>        
    </body>
</html>