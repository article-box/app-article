	<?php foreach($this->wp_connection->select_meta() as $meta_data); ?>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <title> <?=$meta_data->title?> </title>
    <meta name="keywords" content="<?=$meta_data->keywords?>">
    <meta name="description" content="<?=$meta_data->description?>">
    <meta name="author" content="Themeton">