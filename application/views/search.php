<!DOCTYPE html>
<html lang="en">
<head>
 <?php include('meta.php'); ?>
 <?php include('links.php'); ?>
</head>
<body class="news-content">
<?php include('nav.php'); ?>

<div class="content-area pvt0">
	<div class="container">	
		<div class="form-group">
			<div class="input-group">
				<button class="input-group-addon btn btn-light border rounded-0">Search</button>
				<input type="text" name="search_text" id="search_text" placeholder="Enter Text For Search" class="form-control"  />
			</div>
		</div>
	<br />
	<script>
		$(document).ready(function(){

			load_data();

			function load_data(query)
			{
				$.ajax({
					url:"<?php echo base_url(); ?>welcome/fetch",
					method:"POST",
					data:{query:query},
					success:function(data){
						$('#result').html(data);
					}
				})
			}

			$('#search_text').keyup(function(){
				var search = $(this).val();
				if(search != '')
				{
					load_data(search);
				}
				else
				{
					load_data();
				}
			});
		});
	</script>
	<!-- end .row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="news-block parallax-columns-container">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<div class="parallax-content">
							<div class="row" id="result">
							</div>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="border-line mv3"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>
<?php include('footer.php'); ?>