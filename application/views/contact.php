<!DOCTYPE html>
<html lang="en">
<head>
 <?php include('meta.php'); ?>
 <?php include('links.php'); ?>
</head>
<body class="news-content">
<?php include('nav.php'); ?>
    <?php foreach($this->wp_connection->select_details_info() as $details); ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?=base_url();?>">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page"> Contact Us</li>
                  </ol>
                </nav>
            </div>  
        </div>
    </div>
    <!-- end .section-content -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12 p-3 ">
                <div id="message" class="alert alert-success" role="alert">
                </div>
                <?=form_open_multipart('welcome/insert_inquiry', array('id' => 'contactForm') );?>
                    <div class="col-sm-6 float-left border p-5 shadow-box">
                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputEmail4">Name</label>
                          <input type="text" name="name" class="form-control" id="inputEmail4" placeholder="Full Name"  required>
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputEmail4">Email</label>
                          <input type="email" name="email" class="form-control" id="inputEmail4" placeholder="Email" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputAddress">Phone No.</label>
                        <input type="number" name="phone" class="form-control" id="inputAddress" placeholder="Enter Phone No.">
                      </div>
                      <div class="form-group">
                        <label for="inputAddress2">Subject</label>
                        <input type="text" name="subject" class="form-control" id="inputAddress2" placeholder="Enter subject">
                      </div>
                      <div class="form-group">
                        <label for="inputAddress2">Message</label>
                        <textarea name="message" class="form-control"></textarea>
                      </div>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
                <div class="col-sm-6 float-left">
                    <div class="card">
                      <div class="card-header">
                        Quote
                      </div>
                      <div class="card-body">
                        <blockquote class="blockquote mb-0">
                          <p><?=$details->text?></p>
                          <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
                        </blockquote>
                      </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <script>
        $("#message").hide();
        $(function() {
            $("#contactForm").on('submit', function(e) {
                e.preventDefault();
                var contactForm = $(this);
                $.ajax({
                    url: contactForm.attr('action'),
                    type: 'post',
                    data: contactForm.serialize(),
                    success: function(response){
                        console.log(response);
                        // location.reload();
                        if(response.status == 'success') {
                            $("#message").show();
                            setTimeout(function(){ $("#message").hide(); }, 5000);
                            setTimeout(function(){ $("#contactForm")[0].reset(); }, 1000);
                            $("#message").html(response.message);
                        }

                        // $("#message").html(response.message);

                    }
                });
            });
        });
    </script>
    <?php include('footer.php'); ?>