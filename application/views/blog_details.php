<!DOCTYPE html>
<html lang="en">
<head>
 <?php include('blog_meta.php'); ?>
 <?php include('links.php'); ?>
</head>
<body class="news-content">
<?php include('nav.php'); ?>
<div class="container">
        <div class="row">
            <div class="col-sm-12">
                <nav aria-label="breadcrumb" class="breadcrumb_border">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?=base_url();?>">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="<?=site_url('welcome/search');?>">Article</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        <?php 
                            $blog_title = $blogss->blog_title;
                            if (strlen($blog_title) > 40) {
                                $stringCut = substr($blog_title, 0, 40);
                                $endPoint = strrpos($stringCut, ' ');
                                $blog_title = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                $blog_title .= '<a title="$blogss->blog_title" href="#" > ... </a>';
                            }
                            echo $blog_title;
                        ?>
                    </li>
                  </ol>
                </nav>
            </div>  
        </div>
    </div>
    <section class="section-content single">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 with-sidebar">
                    <article class="blog-item blog-single">
                        <h1 class="post-title"> <?=$blogss->blog_title;?> </h1>
                        <p class="headline_text">
                            <i><?=$blogss->header_text?></i>
                        </p>
                        <p class="update_datetext">
                            UPDATED on : 
                            <i class="fa fa-history p2"></i> 
                            <?=$blogss->date_time?>
                        </p>
                        <div class="post first text-bigger hover">
                            <div class="image">
                                <img class="shadow-box p2" src="<?=base_url();?>images/blog_image/<?=$blogss->blog_img?>" alt="Post image"/>
                            </div>
                            <div class="social_section">
                                <div class="col-md-12 d-flex justify-content-between padding_for_mobile_view">
                                    <div class="entry-date col-md-3 d-flex font_style_">
                                        <h5>
                                            <i class="fa fa-history p2 comment_icon_color"></i> 
                                            <span class="like_font">
                                                <?php echo time_elapsed_string($blogss->time); ?>
                                            </span>
                                        </h5>
                                    </div>
                                    <div class="entry-date col-md-3 d-flex font_style_">
                                        <h5>
                                            <a href="#comment">
                                                <i class="fa fa-comment p2 comment_icon_color "></i> 
                                                <span class="like_font">Comments</span> 
                                            </a>
                                        </h5>
                                    </div>
                                    <div class="entry-author col-md-3 d-flex font_style_">
                                        <div class="like-animaction" id="messages">
                                            <nav aria-label="breadcrumb breadcrumb_border" class="breadcrumb_border">
                                              <ol class="breadcrumb">
                                                <p>Add Like</p>
                                              </ol>
                                            </nav>
                                        </div>
                                        <?=form_open_multipart('welcome/like/'.$blogss->blog_id,array('id' => 'like'));?>
                                            <span class="like_font"><?php echo $blogss->likes;?> Likes</span>
                                            <span><?php $likee=$blogss->likes+1;?></span>
                                            <span>
                                                <input type="hidden" value="<?php echo $likee; ?>" name="like">
                                                <button type="submit" data-title="Like" class="btn like_button font_style_">
                                                    <i class="fa fa-heart" style="color: black; "></i>
                                                </button>
                                            </span>
                                        </form>
                                        <span class="like_font" id="num">
                                            <?php echo $likee=$blogss->likes+1;?> Likes
                                        </span>
                                        <button class="btn like_button font_style_" data-title="Liked" id="likes">
                                            <i class="fa fa-heart heart_color_red"></i>
                                        </button>
                                    </div>
                                    <div class="entry-social col-md-3 d-flex font_style_">
                                        <a href="javascript:;" class="p2">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                        <a href="javascript:;" class="p2">
                                            <i class="fa fa-youtube"></i>
                                        </a>
                                        <a href="javascript:;" class="p2">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                        <a href="javascript:;" class="p2">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                        <a href="javascript:;" class="p2">
                                            <i class="fa fa-share"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <script>
                    $(function() {
                        $("#messages").hide();
                        $("#likes").hide();
                        $("#num").hide();
                        $("#like").on('submit', function(e) {
                            e.preventDefault();
                            var contactForm = $(this);
                            $.ajax({
                                url: contactForm.attr('action'),
                                type: 'post',
                                data: contactForm.serialize(),
                                success: function(response){
                                    console.log(response);
                                    if(response.status == 'success') {
                                        $("#messages").show();
                                        $("#like").hide();
                                        $("#likes").show();
                                        $("#num").show();
                                        setTimeout(function(){ $("#messages").hide(); }, 1000);
                                        setTimeout(function(){ $("#like")[0].reset(); }, 1000);
                                         $("#messages").html(response.message);
                                    }
                                }
                            });
                        });
                    });
                </script>
                <!-- end .col-md-9 -->
                    <div class="col-sm-3 sidebar mt2 mobile_view_hidden breadcrumb_style">
                        <div class="widget">
                            <h5 class="widget-title">
                                <span>
                                    <i class="fa fa-tags right_data_icon" style="color:#aed6f1; "></i>
                                    Tag Cloud
                                </span>
                            </h5>
                            <div class="widget-tags">
                                <?php foreach($this->wp_connection->blog_category() as $cat ){ ?>
                                    <a href="<?=site_url('welcome/category/'.$cat->url_slug);?>"><?=$cat->category_name?></a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="widget no-border article_latestpost_details">
                            <h5 class="widget-title">
                                <span>
                                    Latest Post
                                    <img class="right_data_icon" src="<?=base_url();?>images/icons/mewscar.png"/>
                                </span>
                            </h5>
                            <ul>
                                <?php $i=0; foreach($this->wp_connection->orderbyblog() as $blog){ if($i++ == 10) break; ?>
                                <li>
                                    <div class="post">
                                        <div class="text-muted">
                                            Last updated : <?php echo time_elapsed_string($blog->time); ?>
                                        </div>
                                        <h4><a href="#"><?=$blog->blog_title?></a></h4>
                                    </div>
                                </li>
                                <?php }?>
                            </ul>
                        </div>
                    </div>
                <!-- end .sidebar -->
            </div>
            <!-- end .row -->
        </div>
        <!-- end .container -->
    </div>
    </section>

    <div class="container1">
        <div class="row">
            <div class="container">
                <div class="row d-flex justify-content-between">
                    <!-- end .entry-details -->
                    <div class="col-md-12 entry-content article_font_style">
                        <p> <?=$blogss->blog_text?> </p>
                    </div>
                    <!-- end .entry-details -->
                </div>
            </div>
            <div class="container">
            <div class="row">
                <div class="col-md-12">                
                    <div class="row">
                        <div class="col-md-9">              
                            <div id="comments" class="comments-area">
                                <div class="comments-wrapper">    
                                    <div id="commentd"><?=$blogss->blog_id?></div>
                                    <script>
                                        $(document).ready(function(){
                                            $("#commentd").load("<?=base_url();?>/comment_data/<?=$blogss->blog_id?>");
                                        });
                                    </script>

                                    <!-- <ol class="comment-list" id="comment">
                                        <?php foreach($this->wp_connection->comment() as $comment){ if($comment->blog_id==$blogss->blog_id){ ?>
                                        <li>
                                            <article class="bg-light p-3 m-1">
                                                <div class="comment-avatar">
                                                    <img alt="Image" src="<?=base_url();?>images/profile/profile.png" class="avatar">
                                                </div>
                                                <div class="comment-body">
                                                    <div class="meta-data ">
                                                        <span class="comment-date "> <i class="fa fa-user p2"></i> <?=$comment->name?> </span>
                                                    </div>
                                                    <div class="comment-content alert alert-success p-2">
                                                        <?=$comment->comment?>
                                                    </div>
                                                    <div class="meta-data" style="float: right;">
                                                        <span class="comment-date trend_comment"><i class="fa fa-history p2"></i><?php echo time_elapsed_string($comment->time); ?></span>
                                                        <span class="comment-date trend_comment"><?=$comment->date_time?></span>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <?php } } ?>
                                        <li id="messagess">
                                        </li>
                                    </ol> -->


                                </div>
                                <?php $today = date("j-F-Y, g:i a");?>
                                <div id="respond" class="comment-respond trend_search" style="padding: 10px;">
                                    <div class="alert alert-success" id="alert" role="alert"></div>
                                    <?=form_open_multipart('welcome/insert_comment',array('id' => 'comment_form'));?>
                                        <div >
                                            <div class="row">
                                                <p class="comment-form-author col-sm-6">
                                                    <input class="form-control" id="author" name="name" type="text" value="" size="30" placeholder="Your Name *" required>
                                                </p>
                                                <p class="comment-form-email col-sm-6">
                                                    <input class="form-control" id="email" name="email" type="email" value="" size="30" placeholder="Your E-mail *" required>
                                                </p>
                                            </div>
                                            <input type="hidden" name="blog_id" value="<?=$blogss->blog_id?>">
                                            <input type="hidden" name="time" value="<?php echo $today ?>">
                                            <p class="comment-form-comment">
                                                <textarea class="form-control" id="comment" name="comment" placeholder="Comments" required></textarea>
                                            </p>
                                            <p class="form-submit">
                                                <button type="submit" class="btn btn-light shadow-box">
                                                    <i class="fa fa-comment p2 comment_icon_color"></i>
                                                    Post Comment
                                                </button>
                                            </p>                    
                                        </div>
                                    </form>
                                </div>
                            </div>                                        
                        </div>
                    </div>
                    </div>
                </div>
        </div>
    </div>
    <div class=" scroll-to-top go_to_top_up" onclick="topFunction()" id="myBtn_">
        <div class=" col-md-12 col-sm-12 col-12 shadow-box article_title_top"> 
            <h2><?=$blogss->blog_title?></h2>
        </div>
    </div>
<script>
    $(function() {
        $("#messagess").hide();
        $("#alert").hide();
        $("#comment_form").on('submit', function(e) {
            e.preventDefault();
            var contactForm = $(this);
            $.ajax({
                url: contactForm.attr('action'),
                type: 'post',
                data: contactForm.serialize(),
                success: function(response){
                    console.log(response);
                    if(response.status == 'success') {
                        $("#messagess").show();
                        $("#alert").show();
                        setTimeout(function(){ $("#comment_form")[0].reset(); }, 100);
                        setTimeout(function(){ $("#alert").hide(); }, 2000);
                         $("#messagess").html(response.message);
                         $("#alert").html('Comment Post successfully ');  
                        setTimeout(function(){ $("#commentd").load("<?=base_url();?>/comment_data/<?=$blogss->blog_id?>"); }, 100);
                    }
                }
            });
        });
    });
</script>
<div class="container">
   <?php include('footer_data.php'); ?> 
</div>
