<?php include('loderdata/time_3321313654kjhdsj2ds1231s1ds.php'); ?>
    <div class="tana-loader container1 bg-white">
        <div class="row loader-content">
            <!-- <div class="loader-circle"></div>
            <div class="loader-line-mask">
                <div class="loader-line"></div>
            </div> -->
            <div class="col-12 col-sm-12">
                <box class="shine"></box>
                <div class="div">
                    <lines class="shine"></lines>
                    <lines class="shine"></lines>
                    <lines class="shine"></lines>
                </div>
                <photo class="shine"></photo>
                <photo class="shine"></photo>
                <br>
                <box class="shine"></box>
                <div class="div">
                    <lines class="shine"></lines>
                    <lines class="shine"></lines>
                    <lines class="shine"></lines>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper">
        <header id="header" class="header-news">
            <div class="panel-header col-sm-12 col-12">
                <div class="container">
                    <div class="row">
                        <div class="header_style">
                            <div class="col-sm-12 col-12 col-md-12 d-flex justify-content-between">
                                <div class="col-lg-4 col-3 col-sm-3 d-flex">
                                    <a href="<?=site_url('welcome');?>" rel="home" class="custom-logo-link">
                                        <img class="header_image_ " src="<?=base_url();?>images/logo/<?=$logo->logo_img?>" alt="logo image">
                                    </a>
                                </div>
                                <div class="col-lg-6 col-sm-8 col-8 col-md-6 d-flex justify-content-end">
                                    <div class="col-lg-6 col-sm-6 d-flex padding_0">
                                        <?php foreach($this->wp_connection->select_details() as $details); ?>
                                        <a href="<?=site_url('welcome/profile');?>">
                                            <div class="user-avatar user_avatar d-flex justify-content-between">
                                                <div class="d-flex col-sm-4 padding_0">
                                                    <img class="header_image_ mobile_view_hidden border_r" src="<?=base_url();?>images/profile/<?=$details->profile_img?>" alt="user avatar">
                                                </div>
                                                <div class="d-flex col-sm-8 padding_0">
                                                    <a href="<?=site_url('welcome/profile');?>" class="author_wid">
                                                        <span class="author_in_header_for_color padding_5">
                                                            Author, <?=$details->name?>
                                                        </span>        
                                                    </a>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class=" d-flex padding_0">
                                        <div class=" social-links social_links d-flex">
                                            <a class="d-flex  padding_5" href="<?=$details->fb?>"><i class="fa fa-facebook"></i></a>
                                            <a class="d-flex padding_5" href="<?=$details->twitter?>"><i class="fa fa-twitter"></i></a>
                                            <a class="d-flex padding_5" href="<?=$details->whatsapp?>"><i class="fa fa-whatsapp"></i></a>
                                            <a class="d-flex padding_5" href="<?=$details->instagram?>"><i class="fa fa-instagram"></i></a>
                                            <a class="d-flex padding_5" href="<?=$details->linkedin?>"><i class="fa fa-linkedin"></i></a>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="site-branding col-sm-2 col-md-4 col-lg-4 float-left">
                                <a href="<?=site_url('welcome');?>" rel="home" class="custom-logo-link">
                                    <img src="<?=base_url();?>images/logo/<?=$logo->logo_img?>" alt="logo image">
                                </a>                                    
                            </div>
                            <div class="right-content float-right col-sm-6">
                                <div class="user-profile col-sm-6 float-left">
                                    <?php foreach($this->wp_connection->select_details() as $details); ?>
                                    <a href="<?=site_url('welcome/profile');?>">
                                        <div class="user-avatar user_avatar d-flex justify-content-between">
                                            <img src="<?=base_url();?>images/profile/<?=$details->profile_img?>" alt="user avatar" class="d-flex">
                                            <a href="<?=site_url('welcome/profile');?>" class="d-flex author_wid">
                                                <span class="author_in_header_for_color d-flex">
                                                    Author, <?=$details->name?>
                                                </span>        
                                            </a>
                                        </div>
                                    </a>
                                </div>
                                <div class="social-links col-sm-6 float-right social_links">
                                    <a href="<?=$details->fb?>"><i class="fa fa-facebook"></i></a>
                                    <a href="<?=$details->twitter?>"><i class="fa fa-twitter"></i></a>
                                    <a href="<?=$details->whatsapp?>"><i class="fa fa-whatsapp"></i></a>
                                    <a href="<?=$details->instagram?>"><i class="fa fa-instagram"></i></a>
                                    <a href="<?=$details->linkedin?>"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="panel-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <nav class="main-nav">
                                <ul>
                                    <li class="current-menu-item" title="Home">
                                        <a href="<?=base_url();?>">Home</a>
                                    </li>
                                    <?php foreach($this->wp_connection->blog_category() as $categories ){ ?>
                                            <li class="menu-item-has-children menu-item-mega" title="<?=$categories->category_name?>">
                                                <a href="<?=site_url('welcome/category/'.$categories->url_slug);?>"><?=$categories->category_name?></a>
                                                <?php
                                                 foreach($this->wp_connection->select_sub_categories() as $sub ){
                                                    if( $categories->category_id == $sub->category_id){
                                                ?>
                                                    <ul>
                                                        <li>
                                                            <div class="mega-menu-lists">
                                                                <?php foreach($this->wp_connection->select_sub_categories() as $sub ){ if( $categories->category_id == $sub->category_id){ ?>
                                                                        <a href="<?=site_url('welcome/sub_category/'.$sub->url_slug);?>" title="<?=$sub->sub_categories_name?>" ><i class="fa fa-location-arrow p2"></i> <?=$sub->sub_categories_name?></a>
                                                                <?php } } ?>

                                                            </div>
                                                            <div class="mega-menu-panel">
                                                                <div class="row">
                                                                    <div class="col-sm-4 col-md-3 mega-menu-column">
                                                                        <?php $i=0; foreach($this->wp_connection->select_blog() as $blogs ){ if($categories->category_id==$blogs->category_id){ if($i++ ==3) break; ?>
                                                                            <div class="post ps-medium">
                                                                                <div class="meta">
                                                                                    <span class="date">
                                                                                        <i class="fa fa-history p2"></i>
                                                                                        <?php echo time_elapsed_string($blogs->time); ?>
                                                                                    </span>
                                                                                </div>
                                                                                <h4><a href="<?=site_url('welcome/blog_details/'.$blogs->url_slug);?>"><?=$blogs->blog_title?></a></h4>
                                                                                <!-- <p><?=$blogs->blog_title?></p> -->
                                                                            </div>
                                                                        <?php } }?>
                                                                    </div>
                                                                    <div class="col-sm-4 col-md-6 mega-menu-column">
                                                                        <div class="post ps-large">
                                                                            
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-4 col-md-3 mega-menu-column">
                                                                        <h4 class="mega-title">Radio</h4>
                                                                        <?php $i=0; foreach($this->wp_connection->select_blog() as $blogp ){ if($blogp->trending==1){ if($i++ == 4) break;?>

                                                                        <div class="post ps-small">
                                                                            <p><a href="<?=site_url('welcome/blog_details/'.$blogp->url_slug);?>"><?=$blogp->blog_title?></a></p>
                                                                        </div>
                                                                        <?php } }?>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                <?php } }?>
                                            </li>
                                    <?php }  ?>
                                </ul>
                                <div class="search-panel">
                                    <form method="get">
                                        <input type="text" name="search_text" id="search_text1" placeholder="Enter Text For Search" autocomplete="off" >
                                        <button type="submit"></button>
                                    </form>
                                    <div id="result1" class="search_result_style">
                                    </div>
                                </div>
                                <script>
                                    $(document).ready(function(){
                                        load_data();
                                        function load_data(query)
                                        {
                                            $.ajax({
                                                url:"<?php echo base_url(); ?>welcome/fetch1",
                                                method:"POST",
                                                data:{query:query},
                                                success:function(data){
                                                    $('#result1').html(data);
                                                }
                                            })
                                        }
                                        $('#search_text1').keyup(function(){
                                            var search = $(this).val();
                                            if(search != '')
                                            {
                                                load_data(search);
                                            }
                                            else
                                            {
                                                load_data();
                                            }
                                        });
                                    });
                                </script>
                                <div class="right-content">
                                    <a href="javascript:;" id="search_handler">
                                        <img src="<?=base_url();?>tana/images/search.png" alt="search icon">
                                    </a>
                                    <a href="javascript:;" class="burger-menu pm-right mobile_view_in_hide">
                                        <i class="fa fa-bars fa-1x"></i>
                                    </a>
                                </div>
                            </nav>                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-ticker" >
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-md-8">
                            <div class="tt-el-ticker">
                                <strong class="hide-boxs"> <img src="<?=base_url();?>images/icons/antenna.png"> Latest Blogs: </strong>
                                <span class="entry-arrows">
                                    <a href="javascript:;" class="ticker-arrow-prev"><img src="<?=base_url();?>tana/images/arrow-lr-left.png" alt="arrow"></a>
                                    <a href="javascript:;" class="ticker-arrow-next"><img src="<?=base_url();?>tana/images/arrow-lr-right.png" alt="arrow"></a>
                                </span>
                                <span class="entry-ticker trend_comment">
                                    <?php $i=0; foreach($this->wp_connection->select_blog() as $blog ){if($i++ ==3) break;?>
                                        <a href="<?=site_url('welcome/blog_details/'.$blog->url_slug);?>">
                                            <span><?=$blog->blog_title?></span>
                                        </a>
                                    <?php } ?>
                                </span>
                            </div>
                        </div>
                        <?php
                            $apiKey = "4c670d0cb29b7b80fdc457be82eaf42a";
                            $cityId = "1273293";
                            $googleApiUrl = "http://api.openweathermap.org/data/2.5/weather?id=" . $cityId . "&lang=en&units=metric&APPID=" . $apiKey;

                            $ch = curl_init();

                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_URL, $googleApiUrl);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                            curl_setopt($ch, CURLOPT_VERBOSE, 0);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            $response = curl_exec($ch);

                            curl_close($ch);
                            $datas = json_decode($response);
                            $currentTime = time();
                        ?>
                        <div class="col-sm-4 text-right mobile_view_in_hide">
                            <div class="tt-el-info text-right">
                                <h4 id="currentTime">
                                    <?php date_default_timezone_set('Asia/Kolkata'); ?>
                                </h4>
                                <p><?php echo date("F".", "."l"); ?> </p>
                            </div>                            
                            <div class="tt-el-info text-right">
                                <h4 class="text-time">
                                    <?php echo $datas->main->temp_max; ?>
                                    &deg;C 
                                </h4>
                                <p>Delhi</p>
                            </div>
                            <script >
                                window.onload = function() {
                                  clock();  
                                    function clock() {
                                    var now = new Date();
                                    var TwentyFourHour = now.getHours();
                                    var hour = now.getHours();
                                    var min = now.getMinutes();
                                    var sec = now.getSeconds();
                                    var mid = 'pm';
                                    if (min < 10) {
                                      min = "0" + min;
                                    }
                                    if (hour > 12) {
                                      hour = hour - 12;
                                    }    
                                    if(hour==0){ 
                                      hour=12;
                                    }
                                    if(TwentyFourHour < 12) {
                                       mid = 'am';
                                    }     
                                  document.getElementById('currentTime').innerHTML =     hour+':'+min+':'+sec +' '+mid ;
                                    setTimeout(clock, 1000);
                                    }
                                }
                            </script>
                        </div>
                    </div>
                </div>
            </div>

            <div class="push-menu pm-news">
                <div class="pm-overlay"></div>
                <div class="pm-container">
                    <div class="pm-viewport">
                        <div class="pm-wrap">
                            <a href="javascript:;" class="close-menu"></a>
                            <div class="pm-socials">
                                <a href="<?=$details->fb?>"><i class="fa fa-facebook"></i></a>
                                <a href="<?=$details->twitter?>"><i class="fa fa-twitter"></i></a>
                                <a href="<?=$details->instagram?>"><i class="fa fa-instagram"></i></a>
                                <a href="<?=$details->whatsapp?>"><i class="fa fa-whatsapp"></i></a>
                                <a href="<?=$details->linkedin?>"><i class="fa fa-linkedin"></i></a>
                            </div>

                            <div class="author-info">
                                <img src="<?=base_url();?>images/profile/<?=$details->profile_img?>" alt="<?=$details->name?>">
                                <div class="auth-name">
                                    <h4><?=$details->name?></h4>
                                    <p><a href="<?=site_url('welcome/profile');?>">View Author</a></p>
                                </div>
                            </div>

                            <h4 class="pm-en-title">Menu</h4>

                            <nav class="big-menu">
                                <ul>
                                    <li>
                                        <a href="<?=site_url('welcome/search');?>">Search</a>
                                    </li>
                                    <?php foreach($this->wp_connection->blog_category() as $cats ){ ?>
                                        <li>
                                            <a href="<?=site_url('welcome/category/'.$cats->url_slug);?>"><?=$cats->category_name?></a>
                                        </li>
                                    <?php } ?>
                                    <li>
                                        <a href="<?=site_url('welcome/about_us');?>">About Us</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>