<!DOCTYPE html>
<html lang="en">
<head>
 <?php include('meta.php'); ?>
 <?php include('links.php'); ?>
</head>
<body class="news-content">
<?php include('nav.php'); ?>

	<div class="content-area pvt0">
		<div class="container">	
			<div class="row">
				<div class="col-sm-12">
					<div class="news-block parallax-columns-container">
						<div class="row">
							<div class="col-sm-12 col-md-12">
								<div class="parallax-content">
									<div class="row" id="result">
										<div class="container">
								            <br />
								            <div id="load_data"></div>
								            <div id="load_data_message"></div>
								            <br />
								            <br />
								            <br />
								            <br />
								            <br />
								            <br />
								        </div>
									</div>
								</div>
							</div>
							
							<script>
								  $(document).ready(function(){
								    var limit = 5;
								    var start = 0;
								    var action = 'inactive';

								    function lazzy_loader(limit)
								    {
								      var output = '';
								      for(var count=0; count<limit; count++)
								      {
								        output += '<div class="post_data">';
								        output += '<p><span class="content-placeholder" style="width:100%; height: 30px;">&nbsp;</span></p>';
								        output += '<p><span class="content-placeholder" style="width:100%; height: 100px;">&nbsp;</span></p>';
								        output += '</div>';
								      }
								      $('#load_data_message').html(output);
								    }

								    lazzy_loader(limit);

								    function load_data(limit, start)
								    {
								      $.ajax({
								        url:"<?php echo base_url(); ?>welcome/fetch_data",
								        method:"POST",
								        data:{limit:limit, start:start},
								        cache: false,
								        success:function(data)
								        {
								          if(data == '')
								          {
								            $('#load_data_message').html('<img class="center_not_found_img" src="<?=base_url();?>images/icons/windmillbirds.png" /> <p style="text-align:center;">Oops No more data.. </p> ');
								            action = 'active';
								          }
								          else
								          {
								            $('#load_data').append(data);
								            $('#load_data_message').html("");
								            action = 'inactive';
								          }
								        }
								      })
								    }

								    if(action == 'inactive')
								    {
								      action = 'active';
								      load_data(limit, start);
								    }

								    $(window).scroll(function(){
								      if($(window).scrollTop() + $(window).height() > $("#load_data").height() && action == 'inactive')
								      {
								        lazzy_loader(limit);
								        action = 'active';
								        start = start + limit;
								        setTimeout(function(){
								          load_data(limit, start);
								        }, 1000);
								      }
								    });

								  });
							</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <!-- Include jQuery and Scripts -->
    <script type="text/javascript" src="<?=base_url();?>tana/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/jquery.waypoints.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/typed.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/theia-sticky-sidebar.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/circles.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/jquery.stellar.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/jquery.parallax.columns.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/svg-morpheus.js"></script>

    <!-- Swiper -->
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/swiper/js/swiper.min.js"></script>

    <!-- Magnific-popup -->
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/magnific-popup/jquery.magnific-popup.min.js"></script>
    
    <!-- Master Slider -->
    <script src="<?=base_url();?>tana/vendors/masterslider/jquery.easing.min.js"></script>
    <script src="<?=base_url();?>tana/vendors/masterslider/masterslider.min.js"></script>
    
        
    <script type="text/javascript" src="<?=base_url();?>tana/js/scripts.js"></script>

<link rel="stylesheet" type="text/css" href="<?=base_url();?>tana/preview/preview.css">

</body>
</html>