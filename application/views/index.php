<!DOCTYPE html>
<html lang="en">
<head>
 <?php include('meta.php'); ?>
 <?php include('links.php'); ?>
</head>
<body class="news-content">
<?php include('nav.php'); ?>
<script>
	 $(document).ready(function(){
	 	$("#right_data").load("<?=base_url();?>/right_6sas54asd6asd4as98as3as21asd3");
	 	$("#trending_data").load("<?=base_url();?>/trending_6sas54asd6asd4as98as3as21asd3");
	 });
</script>
<div id="banner_data1"></div>
<div class="">
	<div class="container">		
		<div class="row">
			<div class="col-md-12">
				<div class="news-slider news-block mv5 mvt0 shadow-box news_slider">
					<div class="master-slider ms-skin-default" id="masterslider1">
						<?php $i=0; foreach($this->wp_connection->orderbyblog() as $blog ){ if($i++ == 5) break; ?>
							<div class="ms-slide" data-delay="0">
						        <img src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>" data-src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>" alt="Image"/>
						        <div class="ms-thumb post hover-zoom">
						        	<div class="image" data-src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>"></div>
						        	<div class="thumb-meta">
							        	<div class="meta">
											<?php foreach($this->wp_connection->blog_category() as $category){ if($blog->category_id==$category->category_id){ ?>
												<span class="author category"><?=$category->category_name?></span>
												<span class="date"> <i class="fa fa-history p2"></i> <?php echo time_elapsed_string($blog->time); ?></span>
											<?php } }?>
										</div>
										<?php 
		                                    $blog_title = $blog->blog_title;
		                                    if (strlen($blog_title) > 50) {
		                                        $stringCut = substr($blog_title, 0, 50);
		                                        $endPoint = strrpos($stringCut, ' ');
		                                        $blog_title = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
		                                        $blog_title .= '<a title="SHOW MORE.." href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > ...</a>';
		                                    }
		                                ?>
										<h4 title="<?=$blog->blog_title?>"><?=$blog_title?></h4>
									</div>
						        </div>
						        <div class="ms-layer box " data-delay="0" data-effect="bottom(45)" data-duration="300" data-ease="easeInOut" >
									<h4 class="banner_text_mobile_view" title="<?=$blog->blog_title?>" class="animate-element" data-anim="fadeInUp">
										<a href="<?=site_url('welcome/blog_details/'.$blog->url_slug);?>">
											<?=$blog_title?>
										</a>
									</h4>
									<?php 
	                                    $string = $blog->header_text;
	                                    if (strlen($string) > 100) {
	                                        $stringCut = substr($string, 0, 100);
	                                        $endPoint = strrpos($stringCut, ' ');
	                                        $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
	                                        $string .= '<a title="SHOW MORE.." href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > ...</a>';
	                                    }
	                                ?>
							        <p class="animate-element" data-anim="fadeInUp">
							        	 <span class="banner_text_mobile_view_text"> <?php echo $string;?> </span>
							        </p>
						        </div>
						    </div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	<div class="row">
		<div class="col-sm-12 ">
			<div class="news-block parallax-columns-container">
				<div class="row">
					<div class="col-sm-3 col-md-2 parallax-col-wrap post-border" id="right_data">
					</div>
					<div class="col-sm-6 col-md-8">
						<div class="parallax-content" id="pin">
							<div class="row">
								<?php $i=0; foreach($data as $blog){ if($i++ == 4) break; ?>
									<div class="col-md-6 articles_pin">
										<div class="category-block articles">
											<div class="post first hover-dark shadow-box articles_pin_post">
												<a href="#">
													<div class="image" data-src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>">
														<img src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>" alt="Proportion"/>
													</div>
												</a>
												<div class="meta">
													<?php foreach($this->wp_connection->blog_category() as $category){ if($blog->category_id==$category->category_id){ ?>
														<span class=" category"><?=$category->category_name?></span>
														<span class="date"> <i class="fa fa-history p2"></i><?php echo time_elapsed_string($blog->time); ?></span>
														<script>
															$(document).ready(function(){
																$("#share_data<?=$blog->blog_id;?>").hide();
																$("#share_close<?=$blog->blog_id;?>").hide();
															 	$("#share_more<?=$blog->blog_id;?>").click(function(){
															    	$("#share_data<?=$blog->blog_id;?>").slideToggle('10','linear');
															    	$('#share_more<?=$blog->blog_id;?>').toggleClass("specialroteateClass");
															  	});
															});
														</script>

														<span id="share_more<?=$blog->blog_id;?>" class="date pull-right specialroteateClass_">
															<i class="fa fa-chevron-down fa-1x chevron_down_bold"></i>
														</span>
														<span id="share_close<?=$blog->blog_id;?>" class="date pull-right share_close">
															<i class="fa fa-chevron-up fa-1x chevron_down_bold"></i>
														</span>
														<p id="share_data<?=$blog->blog_id;?>" class="col-sm-12 share_data">
															<span class="date">
																<i class="fa fa-facebook-f social-link color_f"></i>
																<i class="fa fa-twitter social-link color_t"></i>
																<i class="fa fa-instagram social-link color_i"></i>
																<i class="fa fa-whatsapp social-link color_w"></i>
																<i class="fa fa-chevron-circle-down social-link"><span class="p2"> <b> More.. </b> </span></i>
															</span>
														</p>
														
													<?php } }?>
												</div>
												<?php 
				                                    $blog_title = $blog->blog_title;
				                                    if (strlen($blog_title) > 50) {
				                                        $stringCut = substr($blog_title, 0, 50);
				                                        $endPoint = strrpos($stringCut, ' ');
				                                        $blog_title = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
				                                        $blog_title .= '<a title="SHOW MORE.." href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > ...</a>';
				                                    }
				                                ?>
												<h4 title="<?=$blog->blog_title?>"><a href="<?=site_url('welcome/blog_details/'.$blog->url_slug);?>"><?=$blog_title?></a></h4>
												<?php 
				                                    $string = $blog->header_text;
				                                    if (strlen($string) > 100) {
				                                        $stringCut = substr($string, 0, 100);
				                                        $endPoint = strrpos($stringCut, ' ');
				                                        $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
				                                        $string .= '<a data-title="SHOW MORE.." href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > <span class="read_more_"> ...Read More <i class="fa fa-angle-right"></i> </span></a>';
				                                    }
				                                    echo '<p>'.$string.'</p>';
				                                ?>
											</div>
										</div>
									</div>
							    <?php } ?>	
							</div>
							<p> <?php echo $links; ?></p> 
							<div class="row">
								<div class="col-sm-12 mv3 mvt0">
									<div class="read_more_s" title="Read More..">
										<a href="<?=site_url('welcome/search');?>">
											<p class="read_more__text_align">  	
												<b>READ-MORE <i class="fa fa-angle-right"></i></b> 
											</p> 
										</a> 
									</div>
								</div>
							</div>
							<?php foreach($this->wp_connection->select_banner() as $banner){
								if($banner->banner_id==1){
							?>
							<div class="row">
								<div class="col-sm-12 mv3 mvt0">
									<a href="<?=$banner->banner_text?>" target="_blank">
										<img src="<?=base_url();?>images/banner/<?=$banner->banner_img?>" alt="<?=$banner->banner_title?>" class="full-size ">
										<p>Ad</p>
									</a>
								</div>
							</div>
							<?php } } ?>
							<?php $i=0; foreach($this->wp_connection->orderbyblog() as $blog){
								{ if($blog->star==1){ if($i++ == 1) break; ?>
							<div class="row">
								<div class="category-block articles">
									<div class="col-md-12">
										<div class="post first text-bigger hover-dark">
											<div class="image video-frame">
												<img src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>" alt="Proportion"/>
											</div>
											<div class="meta font16">
												<?php foreach($this->wp_connection->blog_category() as $category){ if($blog->category_id==$category->category_id){ ?>
													<span class=" category"><?=$category->category_name?></span>
													<span class="date"> <i class="fa fa-history p2"></i><?php echo time_elapsed_string($blog->time); ?></span>
													<span class="date">
														<i class="fa fa-facebook-f social-link color_f"></i>
														<i class="fa fa-twitter social-link color_t"></i>
														<i class="fa fa-instagram social-link color_i"></i>
														<i class="fa fa-whatsapp social-link color_w"></i>
														<i class="fa fa-chevron-circle-down social-link"><span class="p2"> <b> More.. </b> </span></i>
													</span>
												<?php } }?>
											</div>
											<h4 class="index_h4_font_size">
												<a href="<?=site_url("welcome/blog_details/$blog->url_slug");?>"><?=$blog->blog_title?></a>
											</h4>
											<p><?=$blog->header_text?></p>
										</div>
									</div>
								</div>
							</div>
							<?php }}} ?>
						</div>
					</div>
					<div class="col-sm-3 col-md-2 post-border">
						<div class="parallax-column">
							<h3 class="title-middle title-border text-center">
								<img class="right_data_icon" src="<?=base_url();?>images/icons/firecamp.png"/>
								 Trending
							</h3>
							<div class="category-block articles" id="trending_data"></div>
						</div>
					</div>
					<!-- end .col-4 -->
					<div class="col-sm-12">
						<div class="mv3 divider_border"></div>
					</div>
				</div>
				<!-- end row -->
			</div>
			<!-- end .news-block -->
		</div>
	</div>
	<!-- end .row -->
<?php include('footer_data.php'); ?>