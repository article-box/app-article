<!DOCTYPE html>
<html lang="en">
<head>
 <?php include('category_meta.php'); ?>
 <?php include('links.php'); ?>
</head>
<body class="news-content">
<?php include('nav.php'); ?>
	<div class="container">
        <div class="row">
            <div class="col-sm-12">
                <nav aria-label="breadcrumb breadcrumb_border" class="breadcrumb_border">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?=base_url();?>">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?=site_url('welcome/mypost');?>">Category</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?=$cat->category_name?></li>
                  </ol>
                </nav>
            </div>  
        </div>
    </div>
<div class="">
	<div class="container">
	<br />
	<div class="container">
  		<div class="row d-flex justify-content-between">
  			<?php $i=0; foreach ($data as $blogs){ $i++; ?>
			<div class="col-sm-3 d-flex">
				<div class="category-block articles" style=" padding: 10px; box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);">
    				<div class="post hover-dark">
							<img class="card-img-top pb-2" src="<?=base_url();?>images/blog_image/<?=$blogs->blog_img?>"/>
						<!-- </div> -->
						<div class="meta">
							<?php foreach($this->wp_connection->blog_category() as $category){ if($blogs->category_id==$category->category_id){ ?>
								<span class="author category"><?=$category->category_name?></span>
								<span class="date"> <i class="fa fa-history p2"></i> <?php echo time_elapsed_string($blogs->time); ?></span>
							<?php } }?>
						</div>
						<?php 
                            $blog_title = $blogs->blog_title;
                            if (strlen($blog_title) > 50) {
                                $stringCut = substr($blog_title, 0, 50);
                                $endPoint = strrpos($stringCut, ' ');
                                $blog_title = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                $blog_title .= '<a title="SHOW MORE.." href=" '. site_url("welcome/blog_details/$blogs->url_slug") .'" > ...</a>';
                            }
                        ?>
						<h4 title="<?=$blogs->blog_title?>"><a href="<?=site_url('welcome/blog_details/'.$blogs->url_slug);?>"><?=$blog_title?></a></h4>
						<?php 
                            $string = $blogs->header_text;
                            if (strlen($string) > 80) {
                                $stringCut = substr($string, 0, 80);
                                $endPoint = strrpos($stringCut, ' ');
                                $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                $string .= '<a title="Show More..." href=" '. site_url("welcome/blog_details/$blogs->url_slug") .'" > <span class="read_more_"> ...Read More <i class="fa fa-angle-right"></i> </span> </a>';
                            }
                            echo '<p>'.$string.'</p>';
                        ?>
					</div>
    			</div>
			</div>			
			<?php }?>
		</div>
		<p><?php echo $links; ?></p>
	</div>

<?php include('footer_data.php'); ?>