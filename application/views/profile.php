<!DOCTYPE html>
<html lang="en">
<head>
 <?php include('meta.php'); ?>
 <?php include('links.php'); ?>
 <style type="text/css">
.card1 {
    width: 350px;
    border-radius: 50px;
    background: #eeeeee;
    box-shadow: 20px 20px 60px #cacaca, -20px -20px 60px #ffffff;
    border: none
}

.neo-button {
    width: 40px;
    height: 40px;
    outline: 0 !important;
    cursor: pointer;
    color: #fff;
    font-size: 15px;
    border: none;
    margin-right: 10px;
    border-radius: 50%;
    background: linear-gradient(145deg, #d6d6d6, #ffffff);
    box-shadow: 20px 20px 60px #cacaca, -20px -20px 60px #ffffff
}

.neo-button:active {
    border-radius: 50%;
    background: linear-gradient(145deg, #d6d6d6, #ffffff);
    box-shadow: inset 20px 20px 60px #cacaca, inset -20px -20px 60px #ffffff
}

.fa-facebook {
    color: #3b5998
}

.fa-linkedin {
    color: #0077b5
}

.fa-google {
    color: #dc4e41
}

.fa-youtube {
    color: #cd201f
}

.fa-twitter {
    color: #55acee
}
 </style>
</head>
<body class="news-content">
<?php include('nav.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?=base_url();?>">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Profile</li>
                  </ol>
                </nav>
            </div>  
        </div>
    </div>
    <?php foreach($this->wp_connection->select_details() as $details); ?>
    <section class="section-content single" style="margin-bottom: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-push-1 entry-content" style="">
                    <div class="col-sm-4" style="float: left; margin-bottom: 20px;">
                        <div class="container d-flex justify-content-center">
                            <div class="card1 p-3 py-4">
                                <div class="text-center"> <img src="<?=base_url();?>images/profile/<?=$details->profile_img?>" style=" border-radius: 50px; padding: 5px; width: auto; height: 150px;">
                                    <h3 class="mt-2"><?=$details->name?></h3> <span class="mt-1 clearfix"><?=$details->about_you?></span>
                                    <div class="social-buttons mt-5">
                                        <a href="<?=$details->fb?>" target="_blank">
                                            <button class="neo-button">
                                                <i class="fa fa-facebook fa-1x"></i> 
                                            </button>
                                        </a> 
                                        <a href="<?=$details->linkedin?>" target="_blank">
                                            <button class="neo-button">
                                                <i class="fa fa-linkedin fa-1x"></i>
                                            </button> 
                                        </a>
                                        <a href="<?=$details->instagram?>" target="_blank">
                                            <button class="neo-button">
                                                <i class="fa fa-instagram fa-1x" style="color: red;"></i> 
                                            </button> 
                                        </a>
                                        <a href="<?=$details->whatsapp?>" target="_blank">
                                            <button class="neo-button">
                                                <i class="fa fa-whatsapp fa-1x" style="color: #32D411;"></i> 
                                            </button> 
                                        </a>
                                        <a href="<?=$details->twitter?>" target="_blank">
                                            <button class="neo-button">
                                                <i class="fa fa-twitter fa-1x"></i> 
                                            </button> 
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8" style=" float: left;">
                        <div id="accordion">
                          <div class="card">
                            <div class="card-header" id="headingOne">
                              <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  <i class="fa fa-info-circle"></i> Bio
                                </button>
                              </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                              <div class="card-body">
                                <?=$details->text?>
                              </div>
                            </div>
                          </div>
                          <div class="card">
                            <div class="card-header" id="headingTwo">
                              <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-person-lines-fill" fill="currentColor"><path fill-rule="evenodd" d="M1 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm7 1.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5zm2 9a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5z"/>
                                    </svg>
                                    Contact List
                                </button>
                              </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                              <div class="card-body">
                                <div class="list-group">
                                  <a href="#" class="list-group-item list-group-item-action">
                                   <b><i class="fa fa-envelope"></i> Email: </b> <?=$details->gmail?>
                                  </a>
                                  <a href="tel:<?=$details->phone?>" class="list-group-item list-group-item-action"> <b>   <i class="fa fa-phone"></i> Phone: </b> <?=$details->phone?></a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="card">
                            <div class="card-header" id="headingThree">
                              <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                 <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor">
                                  <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                 </svg>
                                  Location
                                </button>
                              </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                              <div class="card-body">
                                <p>
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-geo-alt" fill="currentColor">
                                      <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                    </svg>
                                    Address: <?=$details->address?>
                                </p>
                                <?=$details->map?>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                <!-- end .col-md-7.entry-content -->
            </div>
            <!-- end .row -->
        </div>
        <!-- end .container -->
        <div class="container">
            
        </div>
    <!-- end .container -->
    </section>
    <!-- end .section-content -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12 p-3 ">
                <div id="message" class="alert alert-success" role="alert">
                </div>
                <?=form_open_multipart('welcome/insert_inquiry', array('id' => 'contactForm') );?>
                    <div class="col-sm-10 float-left border p-5 shadow-box">
                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputEmail4">Name</label>
                          <input type="text" name="name" class="form-control" id="inputEmail4" placeholder="Full Name"  required>
                        </div>
                        <div class="form-group col-md-6">
                          <label for="inputEmail4">Email</label>
                          <input type="email" name="email" class="form-control" id="inputEmail4" placeholder="Email" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputAddress">Phone No.</label>
                        <input type="number" name="phone" class="form-control" id="inputAddress" placeholder="Enter Phone No.">
                      </div>
                      <div class="form-group">
                        <label for="inputAddress2">Subject</label>
                        <input type="text" name="subject" class="form-control" id="inputAddress2" placeholder="Enter subject">
                      </div>
                      <div class="form-group">
                        <label for="inputAddress2">Message</label>
                        <textarea name="message" class="form-control"></textarea>
                      </div>
                      <button type="submit" class="btn btn-primary"><i class="fa fa-paper-plane" ></i> Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $("#message").hide();
        $(function() {
            $("#contactForm").on('submit', function(e) {
                e.preventDefault();
                var contactForm = $(this);
                $.ajax({
                    url: contactForm.attr('action'),
                    type: 'post',
                    data: contactForm.serialize(),
                    success: function(response){
                        console.log(response);
                        // location.reload();
                        if(response.status == 'success') {
                            $("#message").show();
                            setTimeout(function(){ $("#message").hide(); }, 5000);
                            setTimeout(function(){ $("#contactForm")[0].reset(); }, 1000);
                            $("#message").html(response.message);
                        }

                        // $("#message").html(response.message);

                    }
                });
            });
        });
    </script>
    <?php include('footer.php'); ?>