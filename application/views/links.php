<?php foreach($this->wp_connection->logo() as $logo); ?>
    <link rel="shortcut icon" type="image/x-icon" href="<?=base_url();?>images/logo/<?=$logo->logo_img?>"/>

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    
    <!-- Bootstrap -->
    <!-- <link rel="stylesheet" type="text/css" href="<?=base_url();?>tana/vendors/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>tana/vendors/bootstrap/css/bootstrap-theme.min.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Fontawesome -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>tana/vendors/font-awesome/css/font-awesome.min.css">

    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>tana/vendors/animate.css">

    <!-- Swiper -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>tana/vendors/swiper/css/swiper.min.css">

    <!-- Magnific-popup -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>tana/vendors/magnific-popup/magnific-popup.css">

    <!-- Base MasterSlider style sheet -->
    <link rel="stylesheet" href="<?=base_url();?>tana/vendors/masterslider/style/masterslider.css" />
    
    <!-- Master Slider Skin -->
    <link href="<?=base_url();?>tana/vendors/masterslider/skins/default/style.css" rel='stylesheet' type='text/css'>
    
    <!-- owl courcel -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g==" crossorigin="anonymous" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw==" crossorigin="anonymous" />
    <!-- end owl -->
        
    <!-- Stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>tana/style.css">
    <script src="<?=base_url();?>tana/js/3.5.1-jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <div id="fb-root"></div>

    <style type="text/css">
        [data-title] {          
          position: relative;
        }

        [data-title]:hover::before {
          content: attr(data-title);
          position: absolute;
          z-index: 99;
          bottom: -30px;
          display: inline-block;
          padding: 3px 6px;
          margin-left: -7px;
          border-radius: 2px;
          background: #000;
          color: #fff;
          font-size: 12px;
          font-family: sans-serif;
          white-space: nowrap;
        }
        [data-title]:hover::after {
          content: '';
          position: absolute;
          bottom: -10px;
          left: 8px;
          display: inline-block;
          color: #fff;
          border: 8px solid transparent;    
          border-bottom: 8px solid #000;
        }
        .like-animaction{
            width: auto; height: auto; color:green; position: fixed; z-index: 9999;  bottom: 1px; left: 30px;
        }
        #messages{
            animation-name: like;
            animation-duration: 1s;
        }

        @-webkit-keyframes placeHolderShimmer {
          0% {
            background-position: -468px 0;
          }
          100% {
            background-position: 468px 0;
          }
        }

        @keyframes placeHolderShimmer {
          0% {
            background-position: -468px 0;
          }
          100% {
            background-position: 468px 0;
          }
        }

        .content-placeholder {
          display: inline-block;
          -webkit-animation-duration: 1s;
          animation-duration: 1s;
          -webkit-animation-fill-mode: forwards;
          animation-fill-mode: forwards;
          -webkit-animation-iteration-count: infinite;
          animation-iteration-count: infinite;
          -webkit-animation-name: placeHolderShimmer;
          animation-name: placeHolderShimmer;
          -webkit-animation-timing-function: linear;
          animation-timing-function: linear;
          background: #f6f7f8;
          background: -webkit-gradient(linear, left top, right top, color-stop(8%, #eeeeee), color-stop(18%, #dddddd), color-stop(33%, #eeeeee));
          background: -webkit-linear-gradient(left, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
          background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
          -webkit-background-size: 800px 104px;
          background-size: 800px 104px;
          height: inherit;
          position: relative;
        }

        .post_data
        {
          padding:24px;
          border:1px solid #f9f9f9;
          border-radius: 5px;
          margin-bottom: 24px;
          /*box-shadow: 10px 10px 5px #eeeeee;*/
        }

        .demo_1{
          margin: auto;
          /*width: 100%;*/
          height: 600px;
          background-color: white;
          background-image:
          linear-gradient( lightgray 0px, transparent 0 ),
          linear-gradient( 100deg, rgba(255, 255, 255, 0), rgba(255, 255, 255, 0.5) 50%, rgba(255, 255, 255, 0) 80% ),
          linear-gradient( lightgray 20px, transparent 0 ),
          linear-gradient( lightgray 20px, transparent 0 ),
          linear-gradient( lightgray 20px, transparent 0 ),
          linear-gradient( lightgray 20px, transparent 0 );

          background-repeat: repeat-y;

          background-size:
            100px 200px, /* circle */
            50px 200px, /* highlight */
            1200px 200px,
            1000px 200px,
            400px 200px,
            1100px 200px;

          background-position:
            0 0, /* circle */
            0 0, /* highlight */
            10px 0,
            10px 40px,
            10px 80px,
            10px 120px;

          animation: shine 1s infinite;
          }

          @keyframes shine {
            to {
              background-position:
                0 0,
                100% 0, /* move highlight to right */
                10px 0,
                10px 40px,
                10px 80px,
                10px 120px;
            }
          }

          .shine {
          background: #f6f7f8;
          background-image: linear-gradient(to right, #f6f7f8 0%, #edeef1 20%, #f6f7f8 40%, #f6f7f8 100%);
          background-repeat: no-repeat;
          background-size: 800px 104px; 
          display: inline-block;
          position: relative; 
          
          -webkit-animation-duration: 1s;
          -webkit-animation-fill-mode: forwards; 
          -webkit-animation-iteration-count: infinite;
          -webkit-animation-name: placeholderShimmer;
          -webkit-animation-timing-function: linear;
          }

        box {
          height: 104px;
          width: 100%;
        }

        .div {
          display: inline-flex;
          flex-direction: column; 
          margin-left: 0px;
          margin-top: 15px;
          vertical-align: top; 
        }

        lines {
          height: 10px;
          margin-top: 10px;
          width: 1100px; 
        }

        photo {
          display: block!important;
          width: 100%; 
          height: 100px; 
          margin-top: 15px;
        }

        @-webkit-keyframes placeholderShimmer {
          0% {
            background-position: -468px 0;
          }
          
          100% {
            background-position: 468px 0; 
          }
        }

        .wrap {
          height: 100%;
          margin-bottom: 50px;
          display: flex;
          align-items: center;
          justify-content: center;
        }

        .button {
          width: auto;
          height: 45px;
          font-family: 'Roboto', sans-serif;
          font-size: 11px;
          text-transform: uppercase;
          letter-spacing: 2.5px;
          font-weight: 500;
          color: #007bff;
          font-weight: bold;
          background-color: #fff;
          border: none;
          border-radius: 45px;
          box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
          transition: all 0.3s ease 0s;
          cursor: pointer;
          outline: none;
          }

        .button:hover {
          background-color: #2EE59D;
          box-shadow: 0px 15px 20px rgba(46, 229, 157, 0.4);
          color: #fff;
          transform: translateY(-7px);
        }

        .content-area{
          
        }
        .specialroteateClass{
          transform: rotate(180deg);
          transition: 0.3s;
        }
        .specialroteateClass_{
          /*transform: rotate(180deg);*/
          transition: 0.3s;
        }
    </style>