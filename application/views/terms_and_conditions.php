<!DOCTYPE html>
<html lang="en">
<head>
 <?php include('meta.php'); ?>
 <?php include('links.php'); ?>
</head>
<body class="news-content">
<?php include('nav.php'); ?>
    <section class="section-content single">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-push-1 entry-content m-auto">
                    <div style=" border-radius: 5px;  height: 50px; margin-bottom: 30px; padding: 10px; box-shadow: 0 1px 6px 0 rgba(32,33,36,0.28);"> <p style=" text-align: center; "> <i class="fa fa-info-circle"></i> ABOUT-US</p> </div>
                    <?php foreach($this->wp_connection->select_tac() as $tac){ ?>
                    <article class="blog-item blog-single">
                        <h1 class="post-title">Terms And Conditions</h1>
                        <p><?=$tac->text?></p>
                    </article>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="container">
             <div class="col-md-12">
                <div class="border-line mv3"></div>
            </div>
            <div class="row">
            <div class="col-md-12">
                <h2 class="block-title mv5" data-title="for you">
                    Recommended
                    <a href="<?=site_url('welcome/mypost');?>" class="category-more text-right hidden-xs">Continue to the Latest <img src="<?=base_url();?>tana/images/arrow-right.png" alt="Arrow"></a>
                </h2>
                <div class="m-dimension-carousel news-block" data-col="5" data-row="1">
                    <div class="swiper-container carousel-container">
                        <div class="swiper-wrapper" style="">
                            <?php $i=0; foreach($this->wp_connection->orderbyblog() as $blog ){ if($blog->status==1){ if($blog->star==1){ if($i++ == 9) break; ?>
                                <div class="swiper-slide " style="padding:5px;">
                                    <div class="category-block articles">
                                        <div class="post hover-dark">
                                            <div class="image1 video-frame">
                                                <img style="border-radius: 10px 10px 0px 0px;"  src="<?=base_url();?>images/blog_image/<?=$blog->blog_img?>"/>
                                            </div>
                                            <div class="meta">
                                                <?php foreach($this->wp_connection->blog_category() as $category){ if($blog->category_id==$category->category_id){ ?>
                                                    <span class="author category"><?=$category->category_name?></span>
                                                    <span class="date"> <i class="fa fa-history p2"></i> <?php echo time_elapsed_string($blog->time); ?></span>
                                                <?php } }?>
                                            </div>
                                            <?php 
                                                $blog_title = $blog->blog_title;
                                                if (strlen($blog_title) > 50) {
                                                    $stringCut = substr($blog_title, 0, 50);
                                                    $endPoint = strrpos($stringCut, ' ');
                                                    $blog_title = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                                    $blog_title .= '<a title="SHOW MORE.." href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > ...</a>';
                                                }
                                            ?>
                                            <h4 title="<?=$blog->blog_title?>"><a href="<?=site_url('welcome/blog_details/'.$blog->url_slug);?>"><?=$blog_title?></a></h4>
                                            <?php 
                                                $string = $blog->header_text;
                                                if (strlen($string) > 80) {
                                                    $stringCut = substr($string, 0, 80);
                                                    $endPoint = strrpos($stringCut, ' ');
                                                    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                                    $string .= '<a title="Show More..." href=" '. site_url("welcome/blog_details/$blog->url_slug") .'" > ...</a>';
                                                }
                                                echo '<p>'.$string.'</p>';
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            <?php }} } ?>                                   
                        </div>
                        <div class="pagination-next-prev mt3">
                            <a href="javascript:;" class="swiper-button-prev arrow-link" title="Prev"><img src="<?=base_url();?>tana/images/arrow-left.png" alt="Arrow"></a>
                            <a href="javascript:;" class="swiper-button-next arrow-link" title="Next"><img src="<?=base_url();?>tana/images/arrow-right.png" alt="Arrow"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="border-line mv3"></div>
            </div>
        </div>
        </div>
    </section>
    <?php include('footer.php'); ?>