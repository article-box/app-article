
    <footer id="footer" class="footer-news">
        <div class="">
            <div class="col-sm-12 col-md-12 col-lg-12 footer-row d-flex justify-content-between">
                <div class="col-sm-3 d-flex">
                    <img class="shadow-box" style="height: 70px; width: 100px;" src="<?=base_url();?>images/logo/<?=$logo->logo_img?>"/>
                </div>
                <div class="col-sm-3 d-flex">
                    <div class="widget">
                        <div class="">
                            <a href="<?=site_url('welcome/profile');?>">
                                 <button class="btn btn-light shadow-box" > 
                                    <i class="fa fa-envelope-o"></i> 
                                    Contact Us
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row footer-row d-flex justify-content-between footer_p_15">
                <?php foreach($this->wp_connection->blog_category() as $category ){ ?>
                    <div class="col-lg-2 col-sm-4 col-md-4 col-4 d-flex footer_catogery">
                        <div class="widget">
                            <a href="<?=site_url('welcome/category/'.$category->url_slug);?>">
                                <h5 class="widget-title widget_title">
                                    <?=$category->category_name?> 
                                </h5>
                            </a>
                            <ul>
                                <?php foreach($this->wp_connection->select_sub_categories() as $sub){ if($category->category_id==$sub->category_id){ ?>
                                    <li>
                                        <a href="<?=site_url('welcome/sub_category/'.$sub->url_slug);?>"><?=$sub->sub_categories_name?>
                                    </li>
                                <?php } } ?>
                            </ul>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="sub-footer">
            <div class="container">
                <div class="row footer-row mv1">
                    <div class="col-sm-12">
                        <div class="widget">
                            <div class="social-links">
                                <a href="<?=$details->fb?>"><i class="fa fa-facebook"></i></a>
                                <a href="<?=$details->twitter?>"><i class="fa fa-twitter"></i></a>
                                <a href="<?=$details->whatsapp?>"><i class="fa fa-whatsapp"></i></a>
                                <a href="<?=$details->instagram?>"><i class="fa fa-instagram"></i></a>
                                <a href="<?=$details->linkedin?>"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-12">
                        <!-- <div class="border-line"></div> -->
                        <ul class="list-inline d-flex justify-content-between">
                            <li class="d-flex footer_menu">
                                <a href="<?=base_url();?>">Home</a>
                            </li>
                            <li class="d-flex footer_menu pr-3">
                                <a href="<?=site_url('welcome/contact');?>">Contact us</a>
                            </li>
                            <li class="d-flex footer_menu pr-3">
                                <a href="<?=site_url('welcome/search');?>">Search</a>
                            </li>
                            <li class="d-flex footer_menu pr-3">
                                <a href="<?=site_url('welcome/about_us');?>">About Us</a>
                            </li>
                            <li class="d-flex footer_menu pr-3">
                                <a href="<?=site_url('welcome/profile');?>">Author Profile</a>
                            </li>
                        </ul>
                        <div class="copyright-text d-flex justify-content-center">
                            Copyight &copy; 2020
                            <i class="fa fa-chevron-up scroll-to-top "></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=" bg-white shadow-box scroll-to-top go_to_top" onclick="topFunction()" id="myBtn" title="Go to top">
            <i class="fa fa-chevron-up"></i>
        </div>
    </footer>
</div>
<!-- end .wrapper -->
<script>
    //Get the button
    var mybutton = document.getElementById("myBtn");
    var mybutton_ = document.getElementById("myBtn_");
     mybutton.style.display = "none";
     mybutton_.style.display = "none";
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {scrollFunction()};
    function scrollFunction() {
      if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
        mybutton.style.display = "block";
        mybutton_.style.display = "block";
      } else {
        mybutton.style.display = "none";
        mybutton_.style.display = "none";
      }
    }
</script>
    <!-- Include jQuery and Scripts -->
    <script type="text/javascript" src="<?=base_url();?>tana/js/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/jquery.waypoints.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/typed.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/theia-sticky-sidebar.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/circles.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/jquery.stellar.min.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/jquery.parallax.columns.js"></script>
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/svg-morpheus.js"></script>
    <!-- Swiper -->
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/swiper/js/swiper.min.js"></script>
    <!-- Magnific-popup -->
    <script type="text/javascript" src="<?=base_url();?>tana/vendors/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Master Slider -->
    <script src="<?=base_url();?>tana/vendors/masterslider/jquery.easing.min.js"></script>
    <script src="<?=base_url();?>tana/vendors/masterslider/masterslider.min.js"></script>   
    <script type="text/javascript" src="<?=base_url();?>tana/js/scripts.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw==" crossorigin="anonymous"></script>
</body>
</html>