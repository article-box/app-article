<!DOCTYPE html>
<html lang="en">
<head>
 <?php include('sub_category_meta.php'); ?>
 <?php include('links.php'); ?>
</head>
<body class="news-content">
<?php include('nav.php'); ?>

<div class="content-area pvt0">
	<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="m-dimension-carousel news-block" data-col="5" data-row="1">
				<div class="swiper-container carousel-container">
				    <div class="swiper-wrapper">
					    <?php $i=0; foreach ($data as $blogs){ $i++; ?>
							<div class="swiper-slide" style="padding:5px;">
								<div class="category-block articles">
				    				<div class="post hover-dark">
										<div class="image1 video-frame">
											<img style="border-radius: 10px 10px 0px 0px;"  src="<?=base_url();?>images/blog_image/<?=$blogs->blog_img?>"/>
										</div>
										<div class="meta">
											<!-- <span class="author category">
												<?=$blogs->sub_category_id?>
											</span> -->
											<span class="date"> <i class="fa fa-history p2"></i> 
												<?php echo time_elapsed_string($blogs->time); ?>
											</span>
										</div>
										<?php 
		                                    $blog_title = $blogs->blog_title;
		                                    if (strlen($blog_title) > 50) {
		                                        $stringCut = substr($blog_title, 0, 50);
		                                        $endPoint = strrpos($stringCut, ' ');
		                                        $blog_title = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
		                                        $blog_title .= '<a title="SHOW MORE.." href=" '. site_url("welcome/blog_details/$blogs->url_slug") .'" > ...</a>';
		                                    }
		                                ?>
										<h4 title="<?=$blogs->blog_title?>"><a href="<?=site_url('welcome/blog_details/'.$blogs->url_slug);?>"><?=$blog_title?></a></h4>
										<?php 
				                            $string = $blogs->header_text;
				                            if (strlen($string) > 80) {
				                                // truncate string
				                                $stringCut = substr($string, 0, 80);
				                                $endPoint = strrpos($stringCut, ' ');

				                                //if the string doesn't contain any space then it will cut without word basis.
				                                $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
				                                $string .= '<a title="Show More..." href=" '. site_url("welcome/blog_details/$blogs->url_slug") .'" > ...</a>';
				                            }
				                            echo '<p>'.$string.'</p>';
				                        ?>
									</div>
				    			</div>
				    		</div>
						<?php }?>	    							
					</div>
					<p><?php echo $links; ?></p>
				</div>
			</div>
 		</div>
			<div class="col-md-12">
			<div class="border-line mv3"></div>
		</div>
	</div>
	</div>
</div>
<div class="container">
   <?php include('footer_data.php'); ?>
</div>