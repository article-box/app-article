<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model("db_connection");
	}

	public function index()
	{
		$this->load->view('admin/index');
	}
	public function gallery()
	{
		$this->load->view('admin/gallery');
	}
	public function add_gallery()
	{
		$this->load->view('admin/add_gallery');
	}
	public function delete_category()
	{
		if ($this->db_connection->delete_category()) {
			$response = array(
				'status' => 'success',
				'message' => "<h3 style='color:#009688;'>Delete successfully. </h3>"
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => validation_errors()
			);
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}

	public function delete_subcategory()
	{
		if ($this->db_connection->delete_subcategory()) {
			$response = array(
				'status' => 'success',
				'message' => "<h3 style='color:#009688;'>Delete successfully. </h3>"
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => validation_errors()
			);
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}

	public function show_service()
	{
		$this->load->view('admin/show_service');
	}

	public function show_sub_categories()
	{
		$this->load->view('admin/show_sub_categories');
	}

	public function insert_category()
	{
		$url_slug = $this->generate_url_slug($this->input->post('sub_category_name'), 'sub_categories');

		$sub_categories = array(
			"sub_categories_name" => $this->input->post('sub_category_name'),
			"category_id" => $this->input->post('category_id'),
			"meta_title" => $this->input->post('meta_title'),
			"meta_description" => $this->input->post('meta_description'),
			"meta_keyword" => $this->input->post('meta_keyword'),
			"url_slug" => $url_slug
		);
		if ($this->db->insert('sub_categories', $sub_categories) == true) {
			$data = $this->input->post('sub_category_name');
			$response = array(
				'status' => 'success',
				'message' => "+ Add Sub Category, Name is :- " . $data . "<h3 style='color:#009688;'>Insert successfully. </h3>"
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => validation_errors()
			);
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}

	public function add_categories()
	{
		$this->load->view('admin/add_categories');
	}

	public function show_categories()
	{
		$this->load->view('admin/show_categories');
	}

	public function insert_categories()
	{
		$url_slug = $this->generate_url_slug($this->input->post('categories_name'), 'blog_category');
		$ins_categories = array(
			"category_name" => $this->input->post('categories_name'),
			"meta_title" => $this->input->post('meta_title'),
			"meta_description" => $this->input->post('meta_description'),
			"meta_keyword" => $this->input->post('meta_keyword'),
			"url_slug" => $url_slug
		);
		if ($this->db->insert('blog_category', $ins_categories) == true) {
			$data = $this->input->post('categories_name');
			$response = array(
				'status' => 'success',
				'message' => '+Add New Categorie name is :- ' . $data . "<h3 style='color:#009688;'>Insert successfully. </h3>"
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => validation_errors()
			);
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}

	public function categories_status($id)
	{
		$status = array("status" => $this->input->post('status'));
		if ($this->db->where('category_id', $id)->update('blog_category', $status) == true) {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="Click To Remove" role="alert" style="border:1px solid green; background:green;  color:white; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #1EB629; padding:4px 8px; ">✔️</span> Update Status Successfully</div>');
			redirect('admin/show_categories');
		} else {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:red; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Something Error ! </div>');
			redirect('admin/show_categories');
		}
	}

	public function update_categories()
	{
		$id = $this->input->post('id');
		$rew['sub'] = $this->db_connection->get_categories($id);
		$this->load->view('admin/update_categories', $rew);
	}

	public function edit_categories($id)
	{
		$url_slug = $this->generate_url_slug($this->input->post('categories_name'), 'blog_category');
		$edit_categories = array(
			"category_name" => $this->input->post('categories_name'),
			"meta_title" => $this->input->post('meta_title'),
			"meta_description" => $this->input->post('meta_description'),
			"meta_keyword" => $this->input->post('meta_keyword'),
			"url_slug" => $url_slug
		);
		if ($this->db->where('category_id', $id)->update('blog_category', $edit_categories) == true) {
			$data = $this->input->post('categories_name');
			$response = array(
				'status' => 'success',
				'message' => 'Update :- ' . $data . "<h3 style='color:#009688;'>Insert successfully. </h3>"
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => validation_errors()
			);
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}

	function subCategoriesList(){
		$id = $this->input->post('id');
		$data = $this->db_connection->sub_categories_list($id,$count=false);
		// echo '<pre>'
		// print_r($data);
		// $output=."";
		foreach( $data as $subCat ){
			// print_r($subCat->sub_categories_name);
			$output="";
			$output=" <p> ".$subCat->sub_categories_name." ";
			$output.=' <a data-toggle="modal" data-target="#editSubCatModel" data-id="'.$subCat->id.'" href="#" id="editSubCategories"> Edit  </a>
			</p>
			';
			echo $output;
		}

	}

	public function add_service()
	{
		$this->load->view('admin/add_service');
	}

	public function update_sub_categories()
	{
		$id = $this->input->post('id');
		$reqa['cat'] = $this->db_connection->get_sub_categories($id);
		$this->load->view('admin/update_sub_categories', $reqa);
	}
	public function edit_sub_categories($id)
	{
		$url_slug = $this->generate_url_slug($this->input->post('sub_categories_name'), 'sub_categories');
		$edit_sub_categories = array(
			"sub_categories_name" => $this->input->post('sub_categories_name'),
			"meta_title" => $this->input->post('meta_title'),
			"meta_description" => $this->input->post('meta_description'),
			"meta_keyword" => $this->input->post('meta_keyword'),
			"url_slug" => $url_slug
		);
		if ($this->db->where('id', $id)->update('sub_categories', $edit_sub_categories) == true) {
			$data = $this->input->post('sub_categories_name');
			$response = array(
				'status' => 'success',
				'message' => 'Update :- ' . $data . "<h3 style='color:#009688;'>Update successfully. </h3>"
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => validation_errors()
			);
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}

	public function subcategories_status($id)
	{
		$status = array("status" => $this->input->post('status'));
		if ($this->db->where('id', $id)->update('sub_categories', $status) == true) {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="Click To Remove" role="alert" style="border:1px solid green; background:green;  color:white; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #1EB629; padding:4px 8px; ">✔️</span> Update Status Successfully</div>');
			redirect('admin/show_sub_categories');
		} else {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:red; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Something Error ! </div>');
			redirect('admin/show_sub_categories');
		}
	}


	public function add_blog()
	{
		$data['category'] = $this->db_connection->fetch_category();
		$this->load->view('admin/add_blog', $data);
	}

	public function fetch_categories()
	{
		if ($this->input->post('category_id')) {
			echo $this->db_connection->fetch_categories($this->input->post('category_id'));
		}
	}

	function fetch_state()
	{
		// echo $this->db_connection->fetch_state(2);
		echo $this->input->post('category_id');
		// this is get name in ajax  
		if ($this->input->post('category_id')) {
			echo $this->db_connection->fetch_state($this->input->post('category_id'));
		}
	}

	public function show_blog($id)
	{
		$reqst['blog'] = $this->db_connection->get_blog($id);
		$this->load->view('admin/show_blog', $reqst);
	}
	public function show_blogs($id)
	{
		$reqst['blog'] = $this->db_connection->get_blog($id);
		$this->load->view('admin/show_blogs', $reqst);
	}
	public function generate_url_slug($string, $table, $field = 'url_slug', $key = NULL, $value = NULL)
	{
		$t = &get_instance();
		$slug = url_title($string);
		$slug = strtolower($slug);
		$i = 0;
		$params = array();
		$params[$field] = $slug;

		if ($key) $params["$key !="] = $value;

		while ($t->db->where($params)->get($table)->num_rows()) {
			// $string = preg_replace("/[^a-z0-9_\s-ءاأإآؤئبتثجحخدذرزسشصضطظعغفقكلمنهويةى]/u", "", $string);
			// $slug = mb_strtolower($slug, "UTF-8");;

			if (!preg_match('/-{1}[0-9]+$/', $slug))
				$slug .= '-' . ++$i;
			else
				$slug = preg_replace('/[0-9]+$/', ++$i, $slug);

			$params[$field] = $slug;
		}
		return $slug;
	}

	public function insert_blog(){

		date_default_timezone_set("Asia/Calcutta");
		$today = date("Y-m-d");

		$url_slug = $this->generate_url_slug($this->input->post('blog_title'), 'blog');

		$blogimg = rand(1111, 9999) . "_" . time() . "_" . $_FILES['blog_img']['name'];
		move_uploaded_file($_FILES['blog_img']['tmp_name'], "images/blog_image/" . $blogimg);
		$session_id = rand(1111, 9999) . "_" . time();
		$ins_blog = array(
			"blog_title" => $this->input->post('blog_title'),
			"blog_text" => $this->input->post('blog_text'),
			"category_id" => $this->input->post('category_id'),
			"date_time" => $this->input->post('time'),
			"sub_category_id" => $this->input->post('sub_category'),
			"header_text" => $this->input->post('header_text'),
			"meta_keyword" => $this->input->post('meta_keyword'),
			"meta_title" => $this->input->post('meta_title'),
			"meta_description" => $this->input->post('meta_description'),
			"blog_img" => $blogimg,
			"session_id" => $session_id,
			"url_slug" => $url_slug,
			"date" => $today,
			"status" => 2
		);
		if ($this->db->insert('blog', $ins_blog) == true) {
			$data = $this->input->post('blog_title');
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4 alerts" id="hideMe" title="Click To Remove" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span class="icon">✔️</span> Add Successfully</div>');

			$this->session->set_flashdata('suc_data', '<div class="message" id="hideMe">' . '<span class="icon">✔️</span> 	Insert New Blog Title is :- ' . $data . '</div>');
			redirect('admin/update_blog/' . $session_id);
		} else {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:red; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Something Error ! </div>');
			redirect('admin/blogs');
		}
	}

	public function update_blog($id)
	{
		$dbsl = $this->db_connection->get_blog($id);
		if (!empty($dbsl)) {
			if ($id == $dbsl->blog_id) {
				$ree['blog'] = $this->db_connection->get_blog($id);
				$this->load->view('admin/update_blog', $ree);
			} else {
				$ree['blog'] = $this->db_connection->get_blog_session($id);
				$this->load->view('admin/update_blog', $ree);
			}
		} else {
			$ree['blog'] = $this->db_connection->get_blog_session($id);
			$this->load->view('admin/update_blog', $ree);
		}

		// echo $dbsl;
		// print_r($dbsl);
		// $ree['blog']=$this->db_connection->get_blog($id);
		// $this->load->view('admin/update_blog',$ree);
	}

	public function edit_blog($id)
	{
		$imgs = $this->db_connection->get_blog($id);

		if ($_FILES['blog_img']['name'] == "") {
			$blogimg = $imgs->blog_img;
		} else {

			$blogimg = rand(1111, 9999) . "_" . time() . "_" . $_FILES['blog_img']['name'];
			move_uploaded_file($_FILES['blog_img']['tmp_name'], "images/blog_image/" . $blogimg);
		}

		$upd_blog = array(
			"blog_title" => $this->input->post('blog_title'),
			"blog_text" => $this->input->post('blog_text'),
			"header_text" => $this->input->post('header_text'),
			"meta_keyword" => $this->input->post('meta_keyword'),
			"meta_title" => $this->input->post('meta_title'),
			"meta_description" => $this->input->post('meta_description'),
			"status" => $this->input->post('status'),
			"pin" => $this->input->post('pin'),
			"trending" => $this->input->post('trending'),
			"star" => $this->input->post('star'),
			"blog_img" => $blogimg
		);
		if ($this->db->where('blog_id', $id)->update('blog', $upd_blog) == true) {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="Click To Remove" role="alert" style="border:1px solid green; background:green;  color:white; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #1EB629; padding:4px 8px; ">✔️</span> Update Successfully</div>');
			redirect('admin/update_blog/' . $id);
		} else {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:red; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Something Error ! </div>');
			redirect('admin/blogs');
		}
	}

	public function updateStatus(){
		$id = $this->input->post('statusData');
		$status = array("status" => $this->input->post('blogValue'));
		if ($this->db->where('blog_id', $id)->update('blog', $status) == true){
			if($this->input->post('blogValue')==1){
				echo "Article Active Successfull !";
			}else{
				echo 'Article Move to draft Successfully !';
			}
		}else{
			echo "Status not update";
		}
	}

	function updateTrending(){
		$numRow = $this->db_connection->countTrandRow();

		$id = $this->input->post('articleId');
		$trandStatus = array("trending" => $this->input->post('trandStatus'));

		if($this->input->post('trandStatus')==1){
			if($numRow < 10 ){
				if ($this->db->where('blog_id', $id)->update('blog', $trandStatus) == true) {
					echo "Trending Update Successfull ! ";
				}else{
					echo 'Something went wrong !';
				}
			}else{
				echo "Only 10 article will come on trending !";
			}
		}else{
			if ($this->db->where('blog_id', $id)->update('blog', $trandStatus) == true) {
				echo "Remove to trending Successfull ! ";
			}else{
				echo 'Something went wrong !';
			}
		}
	}

	function updatePin(){
		$id = $this->input->post('articleIdPin');
		$pinStatus = array("pin" => $this->input->post('pinStatus'));

		if ($this->db->where('blog_id', $id)->update('blog', $pinStatus) == true) {
			if($this->input->post('pinStatus')==1){
				echo 'Pin Article Successfully !';
			}else{
				echo 'UnPin Article Successfully !';
			}
		}else{
			echo 'Something went wrong !';
		}
	}

	function updateStar(){
		$id = $this->input->post('articleStarId');
		$starStatus = array("star" => $this->input->post('starStatus'));

		if ($this->db->where('blog_id', $id)->update('blog', $starStatus) == true) {
			if($this->input->post('starStatus')==1){
				echo 'Add To Star Article Successfully !';
			}else{
				echo 'Remove Star Successfully !';
			}
		}else{
			echo 'Something went wrong !';
		}
	}

	function deleteArticle(){
		$id = $this->input->post("deleteId");
		if ($this->db->where('blog_id', $id)->delete('blog') == true) {
			echo "Delete Article Successfull !";
		}else{
			echo "error cant delete articles !";
		}
	}

	public function blog_status($id)
	{
		$status = array("status" => $this->input->post('status'));
		if ($this->db->where('blog_id', $id)->update('blog', $status) == true) {
			$this->session->set_flashdata('suc_msg', '<div  class="alert alert-success mb-4 alerts" id="hideMe" title="Click To Remove" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span class="icon" ">✔️</span> Update Status Successfully</div>');
			redirect('admin/blogs');
		} else {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:red; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Something Error ! </div>');
			redirect('admin/blogs');
		}
	}

	public function trending($id)
	{
		$trending = array("trending" => $this->input->post('status'));
		if ($this->db->where('blog_id', $id)->update('blog', $trending) == true) {
			$this->session->set_flashdata('suc_msg', '<div  class="alert alert-success mb-4 alerts" id="hideMe" title="Click To Remove" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span class="icon" ">✔️</span> Update Successfully</div>');
			redirect('admin/blogs');
		} else {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:red; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Something Error ! </div>');
			redirect('admin/blogs');
		}
	}
	public function pin($id)
	{
		$trending = array("pin" => $this->input->post('pin'));
		if ($this->db->where('blog_id', $id)->update('blog', $trending) == true) {
			$this->session->set_flashdata('suc_msg', '<div  class="alert alert-success mb-4 alerts" id="hideMe" title="Click To Remove" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span class="icon" ">✔️</span> Update Successfully</div>');
			redirect('admin/blogs');
		} else {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:red; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Something Error ! </div>');
			redirect('admin/blogs');
		}
	}
	public function star($id)
	{
		$trending = array("star" => $this->input->post('star'));
		if ($this->db->where('blog_id', $id)->update('blog', $trending) == true) {
			$this->session->set_flashdata('suc_msg', '<div  class="alert alert-success mb-4 alerts" id="hideMe" title="Click To Remove" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span class="icon" ">✔️</span> Update Successfully</div>');
			redirect('admin/blogs');
		} else {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:red; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Something Error ! </div>');
			redirect('admin/blogs');
		}
	}

	public function delete_blog($id)
	{
		if ($this->db->where('blog_id', $id)->delete('blog') == true) {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:white; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Delete successfully</div>');
			redirect('admin/blogs');
		} else {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:red; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Something Error ! </div>');
			redirect('admin/blogs');
		}
	}
	public function deleteblog()
	{
		if ($this->db_connection->deleteblog()) {
			$response = array(
				'status' => 'success',
				'message' => "<h3 style='color:#009688;'>Delete successfully. </h3>"
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => validation_errors()
			);
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}

	public function add_banner()
	{
		$this->load->view('admin/add_banner');
	}
	public function show_banner()
	{
		$this->load->view('admin/show_banner');
	}
	public function banner()
	{
		$this->load->view('admin/banner');
	}

	public function ins_banner()
	{
		$banner_img = rand(1111, 9999) . "_" . time() . "_" . $_FILES['banner_image']['name'];
		move_uploaded_file($_FILES['banner_image']['tmp_name'], "images/banner/" . $banner_img);

		$insbanner =	array(
			"banner_title" => $this->input->post('banner_title'),
			"banner_text" => $this->input->post('banner_text'),
			"banner_img" => $banner_img
		);
		if ($this->db->insert('banner', $insbanner) == true) {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="Click To Remove" role="alert" style="border:1px solid green; background:green;  color:white; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #1EB629; padding:4px 8px; ">✔️</span> Add Successfully</div>');
			redirect('admin/show_banner');
		} else {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:red; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Something Error ! </div>');
			redirect('admin/add_banner');
		}
	}

	public function update_banner()
	{
		$id = $this->input->post("id");
		$req['banner'] = $this->db_connection->get_banner($id);
		$this->load->view('admin/update_banner', $req);
	}

	public function edit_banner()
	{
		$id = $this->input->post("id");
		$bimg = $this->db_connection->get_banner($id);

		if ($_FILES['banner_image']['name'] == "") {
			$banner_img = $bimg->banner_img;
		} else {

			$banner_img = rand(1111, 9999) . "_" . time() . "_" . $_FILES['banner_image']['name'];
			move_uploaded_file($_FILES['banner_image']['tmp_name'], "images/banner/" . $banner_img);
		}

		$insbanner =	array(
			"banner_title" => $this->input->post('banner_title'),
			"banner_text" => $this->input->post('banner_text'),
			"banner_img" => $banner_img
		);
		if ($this->db->where('banner_id', $id)->update('banner', $insbanner) == true) {
			echo "Banner update Successfull !";
		} else {
			echo "Error !";
		}
	}
	public function banner_status()
	{
		$id = $this->input->post("id");
		$status = array("status" => $this->input->post('status'));
		if ($this->db->where('banner_id', $id)->update('banner', $status) == true) {
			echo "Banner update Successfull !";
		} else {
			echo "Error !";
		}
	}
	public function delete_banner()
	{
		$id = $this->input->post("id");
		if ($this->db->where('banner_id', $id)->delete('banner') == true) {
			echo "Delete banner Successfull !";
		} else {
			echo "Error !";
		}
	}

	public function ins_signup()
	{
		$ins_signup = array(
			"email" => $this->input->post('email'),
			"password" => $this->input->post('password')
		);
		$this->db->insert('signup', $ins_signup);
		redirect('admin/login');
	}

	public function login()
	{
		$this->load->view('admin/login');
	}

	public function chacke_login()
	{
		$chakesign = array(
			"email" => $this->input->post('email'),
			"password" => $this->input->post('password'),
			"date" => $this->input->post('date'),
			"time" => $this->input->post('time'),
			"ip" => $this->input->post('ip')
		);

		$chake_sign = $this->db_connection->checksignin($chakesign);

		$msg = $this->session->set_flashdata('msg');
		$mail__ = $this->input->post('email');
		$date__ = $this->input->post('date');
		$time__ = $this->input->post('time');

		if ($chake_sign == 1) {
			$this->session->set_userdata("checksesn", "asdf8s7df8dsf");
			$this->session->set_userdata("sesn", $chakesign['email']);
			$this->db->insert('login_history', $chakesign);
			$this->login_alert($mail__, $date__, $time__);
			redirect('admin/index');
		} else {
			echo "<script> alert('somethings wrong'); location='login'  </script>";
		}
	}
	public function login_alert($mail__, $date__, $time__)
	{
		$message = $mail__ . ' ' . ' Login admin panel At' . $date__ . '' . $time__;
		// $user_mail= ;
		$this->load->library('email');
		$this->email->set_mailtype("html");
		$this->email->from("niralanishant331@gmail.com");
		$this->email->to("niralanishant800@gmail.com");
		$this->email->subject("Login Alert !");
		$this->email->message($message);
		$this->email->set_newline("\r\n");
		$this->email->send();
	}

	public function login_history()
	{
		$this->load->view('admin/login_history');
	}
	public function logout()
	{
		$this->session->set_userdata("checksesn", "");
		$this->session->set_userdata("sesn", '');
		redirect('admin/login');
	}

	public function show_logo()
	{
		$this->load->view('admin/show_logo');
	}

	public function update_logo()
	{
		$req['logo'] = $this->db_connection->get_logo(1);
		$this->load->view('admin/update_logo', $req);
	}

	public function updatelogo()
	{
		$id = $this->input->post("id");
		$logoimg = $this->db_connection->get_logo($id);

		if ($_FILES['logo']['name'] == "") {
			$logo = $logoimg->logo_img;
		} else {
			$logo = rand(1111, 9999) . "_" . time() . "_" . $_FILES['logo']['name'];
			move_uploaded_file($_FILES['logo']['tmp_name'], "images/logo/" . $logo);
		}

		$ins_logo = array("logo_img" => $logo);

		if ($this->db->where('logo_id', $id)->update('logo', $ins_logo) == true) {
			echo "Logo Update Successfull !";
		} else {
			echo "Error !";
		}
	}

	public function edit_others($id)
	{
		$image = $this->db_connection->get_others($id);

		if ($_FILES['imgs']['name'] == "") {
			$imgs = $image->img;
		} else {
			$imgs = rand(1111, 9999) . "_" . time() . "_" . $_FILES['imgs']['name'];
			move_uploaded_file($_FILES['imgs']['tmp_name'], "images/others/" . $imgs);
		}
		$ins_img = array("name" => $this->input->post('name'), "img" => $imgs);
		if ($this->db->where('others_id', $id)->update('others', $ins_img) == true) {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="Click To Remove" role="alert" style="border:1px solid green; background:green;  color:white; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #1EB629; padding:4px 8px; ">✔️</span> Update Successfully</div>');
			redirect('admin/gallery');
		} else {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:red; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Something Error ! </div>');
			redirect('admin/gallery');
		}
	}

	public function update_others($id)
	{
		$req['imgs'] = $this->db_connection->get_others($id);
		$this->load->view('admin/update_others', $req);
	}

	public function insert_others($id)
	{
		$imgs = rand(1111, 9999) . "_" . time() . "_" . $_FILES['imgs']['name'];
		move_uploaded_file($_FILES['imgs']['tmp_name'], "images/others/" . $imgs);

		$ins_img = array("name" => $this->input->post('name'), "img" => $imgs);

		if ($this->db->insert('others', $ins_img) == true) {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="Click To Remove" role="alert" style="border:1px solid green; background:green;  color:white; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #1EB629; padding:4px 8px; ">✔️</span> Insert Successfully</div>');
			redirect('admin/gallery');
		} else {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:red; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Something Error ! </div>');
			redirect('admin/gallery');
		}
	}

	public function delete_others($id)
	{
		if ($this->db->where('others_id', $id)->delete('others') == true) {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:white; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Delete successfully</div>');
			redirect('admin/gallery');
		} else {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:red; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Something Error ! </div>');
			redirect('admin/gallery');
		}
	}

	public function blogs()
	{
		$this->load->view('admin/blogs');
	}

	public function articles()
	{
		$this->load->view('admin/articles');
	}

	public function comment()
	{
		$this->load->view('admin/comment');
	}

	public function comments()
	{
		$this->load->view('admin/comments');
	}

	public function varified(){
		$id = $this->input->post("id");
		$status = array("varified" => $this->input->post('status'));
		if ($this->db->where('comment_id', $id)->update('comment', $status) == true) {
			if($status['varified']==1){
				echo "Varified Successfully !";
			}else{
				echo "Unvarified Successfully !";
			}
		} else {
			echo "Error !";
		}
	}

	public function delete_comment(){
		$id = $this->input->post("id");
		if ($this->db->where("comment_id",$id)->delete("comment")) {
			echo "Comment Delete successfully !";
		}else {
			echo "Error !";
		}
	}

	public function notification($id)
	{
		$read = array("notification" => $this->input->post('notification'));
		if ($this->db->where('comment_id', $id)->update('comment', $read) == true) {
			$response = array(
				'status' => 'success',
				'message' => "<h3 style='color:#31D011';>Update successfully. </h3>"
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => validation_errors()
			);
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}

	public function insert_reply()
	{
		$reply = array(
			"comment_id" => $this->input->post('comment_id'),
			"text" => $this->input->post('reply_text'), "status" => '1'
		);
		if ($this->db->insert('reply', $reply) == true) {
			// echo "Reply Comment Successfull !";
			echo $this->input->post('comment_id');
		}else {
			echo "";	
		}
	}

	public function delete_reply(){
		$id = $this->input->post("id");
		$comment_id = $this->input->post("comment_id");

		if( $this->db->where("reply_id ",$id)->delete("reply") ){
			echo $comment_id;
		}else{
			echo "";
		}
	}

	function comment_replyData(){
		$id = $this->input->post("id");
		$data = $this->db_connection->get_reply($id);
		
		echo '<input type="hidden" id="comment_id" name="comment_id" value="'.$id.'">';

		foreach($data as $replyData){
			echo '<div class="on_hover shadow d-flex justify-content-between mb-1 p-2">';
			echo '<div class="d-flex"> <i class="fas fa-comments mr-1 p-1"></i>'. $replyData->text .'</div>';
			echo '<i class="fas fa-trash-alt text-danger" id="deleteReply" data-id="'.$replyData->reply_id.'" style="cursor:pointer;"></i>';
			echo '</div>';
		}
	}

	public function add_about()
	{
		$this->load->view('admin/add_about');
	}
	public function ins_about()
	{
		$ins_about = array(
			"title" => $this->input->post('title'),
			"text" => $this->input->post('text')
		);
		if ($this->db->insert('about', $ins_about) == true) {
			echo "Insert About Successfull !";
		} else {
			echo "Error !";
		}
	}
	public function show_about()
	{
		$data['data'] = $this->db_connection->select_about();
		$this->load->view('admin/show_about',$data);
	}
	public function update_about()
	{
		$id = $this->input->post("id");
		$reqa['about'] = $this->db_connection->get_about($id);
		$this->load->view('admin/update_about', $reqa);
	}
	public function edit_about()
	{
		$id = $this->input->post("id");
		$update_about = array(
			"title" => $this->input->post('title'),
			"text" => $this->input->post('utext')
		);
		if ($this->db->where('about_id', $id)->update('about', $update_about) == true) {
			echo "update About Successfull !";
		} else {
			echo "Error !";
		}
	}
	public function about_status()
	{
		$id = $this->input->post("id");
		$status = array("status" => $this->input->post('status'));
		if ($this->db->where('about_id', $id)->update('about', $status) == true) {
			echo "Status update Successfull !";
		} else {
			echo "Error !";
		}
	}


	public function delete_about()
	{
		$id = $this->input->post("id");
		if ($this->db->where('about_id', $id)->delete('about') == true) {
			echo "Delete About Successfull !";
		} else {
			echo "Error !";
		}
	}
	public function inquiry()
	{
		$this->load->view('admin/inquiry');
	}
	public function delete_inq($id)
	{
		if ($this->db->where('inquiry_id', $id)->delete('inquiry') == true) {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:white; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Delete successfully</div>');
			redirect('admin/inquiry');
		} else {
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:red; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Something Error ! </div>');
			redirect('admin/inquiry');
		}
	}

	public function reply($id)
	{
		$req['inq'] = $this->db_connection->get_inquiry($id);
		$this->load->view('admin/reply', $req);
	}

	public function reply_mail($id)
	{

		$message = $this->input->post('text');
		$user_mail = $this->input->post('mail_id');
		$this->load->library('email');
		$this->email->set_mailtype("html");
		$this->email->from("niralanishant331@gmail.com");
		$this->email->to($user_mail);
		$this->email->subject("Thanks for contacting us!");

		$this->email->message($message);
		$this->email->set_newline("\r\n");
		// $this->email->send();
		if ($this->email->send()) {
			$status = array("status" => '1');
			$this->db->where('inquiry_id', $id)->update('inquiry', $status);
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="Click To Remove" role="alert" style="border:1px solid green; background:green;  color:white; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #1EB629; padding:4px 8px; ">✔️</span> Reply Successfully</div>');
			redirect('admin/inquiry');
		} else {
			$status = array("status" => '2');
			$this->db->where('inquiry_id', $id)->update('inquiry', $status);
			$this->session->set_flashdata('suc_msg', '<div class="alert alert-success mb-4" title="onClick To Remove" role="alert" style="border:1px solid green; background:green;  color:red; position: fixed; top: 80px; right: 50px; z-index:999999999; height:50px; width:auto;"><button type="button" class="close" data-dismiss="alert" aria-label="Close" style="height:50px;"><svg> ... </svg></button> <span style="border-radius: 100%; background: #E65B16; padding:4px 8px; ">❌️</span> Something Error ! </div>');

			redirect('admin/inquiry');
		}
	}

	public function details()
	{
		$req['details'] = $this->db_connection->get_details(1);
		$this->load->view('admin/details', $req);
	}

	public function details_info(){
		$req['details'] = $this->db_connection->get_details_info(1);
		$this->load->view('admin/details_info', $req);
	}

	public function edit_details_info(){
		$id = $this->input->post("id");
		$update_details = array(
			"name" => $this->input->post('name'),
			"phone" => $this->input->post('phone'),
			"gmail" => $this->input->post('gmail'),
			"pdf" => $this->input->post('pdf'),
			"country" => $this->input->post('country'),
			"city" => $this->input->post('city'),
			"address" => $this->input->post('address'),
			"text" => $this->input->post('text'),
			"map" => $this->input->post('map'),
			"about_you" => $this->input->post('about_you'),
			"fb" => $this->input->post('fb'),
			"linkedin" => $this->input->post('linkedin'),
			"twitter" => $this->input->post('twitter'),
			"instagram" => $this->input->post('instagram'),
			"whatsapp" => $this->input->post('whatsapp')
		);
		if ($this->db->where('details', $id)->update('details_info', $update_details) == true) {
			echo "Details Update Successfull !";
		} else {
			echo "Error !";
		}
	}


	public function edit_details()
	{
		$id = $this->input->post("id");
		$imgs = $this->db_connection->get_details($id);

		if ($_FILES['profile_img']['name'] == "") {
			$profile_imgs = $imgs->profile_img;
		} else {

			$profile_imgs = rand(1111, 9999) . "_" . time() . "_" . $_FILES['profile_img']['name'];
			move_uploaded_file($_FILES['profile_img']['tmp_name'], "images/profile/" . $profile_imgs);
		}
		$update_details = array(
			"name" => $this->input->post('name'),
			"phone" => $this->input->post('phone'),
			"gmail" => $this->input->post('gmail'),
			"pdf" => $this->input->post('pdf'),
			"country" => $this->input->post('country'),
			"city" => $this->input->post('city'),
			"address" => $this->input->post('address'),
			"text" => $this->input->post('text'),
			"map" => $this->input->post('map'),
			"about_you" => $this->input->post('about_you'),
			"fb" => $this->input->post('fb'),
			"linkedin" => $this->input->post('linkedin'),
			"twitter" => $this->input->post('twitter'),
			"instagram" => $this->input->post('instagram'),
			"whatsapp" => $this->input->post('whatsapp'),
			"profile_img" => $profile_imgs
		);
		if ($this->db->where('details', $id)->update('details', $update_details) == true) {
			echo "Details Update Successfull !";
		} else {
			echo "Error !";
		}
	}
	public function show_details()
	{
		$this->load->view('admin/show_details');
	}

	public function show_details_info()
	{
		$this->load->view('admin/show_details_info');
	}

	public function meta_data()
	{
		$req['meta'] = $this->db_connection->get_meta(1);
		$this->load->view('admin/meta_data', $req);
	}

	public function edit_meta()
	{
		$id = $this->input->post("id");
		$meta = array(
			"title" => $this->input->post('title'),
			"author" => $this->input->post('author'),
			"description" => $this->input->post('description'),
			"robots" => $this->input->post('robots'),
			"keywords" => $this->input->post('keywords')
		);
		if ($this->db->where('meta_id', $id)->update('meta_data', $meta) == true) {
			echo "Meta Data Update Successfull !";
		} else {
			echo "Error !";
		}
	}
	public function show_meta_data()
	{
		$this->load->view('admin/show_meta_data');
	}

	public function show_admin()
	{
		$this->load->view('admin/show_admin');
	}

	public function add_admin()
	{
		$this->load->view('admin/add_admin');
	}

	public function insert_admin()
	{
		$insert_admin = array(
			"email" => $this->input->post('email'),
			"password" => $this->input->post('password'),
			"name" => $this->input->post('name')
		);
		if ($this->db->insert('signup', $insert_admin) == true) {
			$data = $this->input->post('name');
			$response = array(
				'status' => 'success',
				'message' => '<p>New Admin :</p> ' . $data . "<h3 style='color:#009688;'>Insert successfully. </h3>"
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => validation_errors()
			);
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}
	public function update_admin($id)
	{
		$req['admin'] = $this->db_connection->get_signup($id);
		$this->load->view('admin/update_admin', $req);
	}
	public function edit_admin($id)
	{
		$edit_admin = array(
			"email" => $this->input->post('email'),
			"password" => $this->input->post('password'),
			"name" => $this->input->post('name')
		);
		if ($this->db->where('signup_id', $id)->update('signup', $edit_admin) == true) {
			$data = $this->input->post('name');
			$response = array(
				'status' => 'success',
				'message' => ' Update Admin : ' . $data . "<h3 style='color:#009688;'>Update successfully. </h3>"
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => validation_errors()
			);
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}

	public function filemanager()
	{
		$this->load->view('admin/filemanager');
	}

	public function update_terms_and_conditions()
	{
		$req['tacs'] = $this->db_connection->get_tac(1);
		$this->load->view('admin/update_terms_and_conditions', $req);
	}

	public function edit_tac()
	{
		$id= $this->input->post("id");
		$ins_tac = array(
			"text" => $this->input->post('ttext')
		);
		if ($this->db->where('tac_id', $id)->update('tac', $ins_tac) == true) {
			echo "update Terms and Conditions Successfull !";
		} else {
			echo "Error !";
		}
	}

	public function update_privacy_policy()
	{
		$req['pap'] = $this->db_connection->get_pap(1);
		$this->load->view('admin/update_privacy_policy', $req);
	}

	public function edit_pap()
	{
		$id= $this->input->post("id");
		$ins_pap = array(
			"text" => $this->input->post('ptext')
		);
		if ($this->db->where('pap_id', $id)->update('pap', $ins_pap) == true) {
			echo "update privacy policy Successfull !";
		} else {
			echo "Error !";
		}
	}

	public function notes()
	{
		$this->load->view('admin/notes');
	}

	public function add_notes()
	{
		$notes = array(
			"title" => $this->input->post('title'),
			"description" => $this->input->post('description'),
			"tags" => $this->input->post('tags'),
			"date" => $this->input->post('date')
		);
		if ($this->db->insert('notes', $notes) == true) {
			$response = array(
				'status' => 'success',
				'message' => "<p style='color:#009688;'>Add Notes successfully. </p>"
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => validation_errors()
			);
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}

	public function delete_notes()
	{
		if ($this->db_connection->delete_notes()) {
			$response = array(
				'status' => 'success',
				'message' => "<h3 style='color:#009688;'>Delete successfully. </h3>"
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => validation_errors()
			);
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}

	public function star_notes($id)
	{
		$star = array("star" => $this->input->post('star'));
		if ($this->db->where('notes_id', $id)->update('notes', $star) == true) {
			$response = array(
				'status' => 'success',
				'message' => "<h3 style='color:#31D011';>Update successfully. </h3>"
			);
		} else {
			$response = array(
				'status' => 'error',
				'message' => validation_errors()
			);
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode($response));
	}


	public function calendar()
	{
		$this->load->view('admin/calendar');
	}

	// for search 

	function fetch1()
	{
		$output = '';
		$query = '';
		$this->load->model('db_connection');
		if ($this->input->post('query')) {
			$query = $this->input->post('query');
		}
		$data = $this->db_connection->fetch_data($query);
		$output .= '
		
		';
		if ($data->num_rows() > 0) {
			$i = 0;
			foreach ($data->result() as $row) {
				if ($i++ == 6) break;
				$output .= '
						<p></p>
						<p style="margin:10px;" class="on_hover" title="' . $row->blog_title . '"><a href=" ' . site_url("admin/show_blogs/$row->blog_id") . ' ">' . $row->blog_title . '</a>' . '</p>
				';
			}
		} else {
			$output .= '<p style="margin:10px 35px;"> No Data Found </p> <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="meh" class="svg-inline--fa fa-meh fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 496 512"><path fill="#FFC300" d="M248 8C111 8 0 119 0 256s111 248 248 248 248-111 248-248S385 8 248 8zm-80 168c17.7 0 32 14.3 32 32s-14.3 32-32 32-32-14.3-32-32 14.3-32 32-32zm176 192H152c-21.2 0-21.2-32 0-32h192c21.2 0 21.2 32 0 32zm-16-128c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32z"></path></svg> ';
		}
		$output .= '</table>';
		echo $output;
	}


	// for load data 
	public function blog_control(){
		$this->load->view('admin/load_data/blog_control');
	}
	public function cat_table(){
		$this->load->view('admin/load_data/cat_table');
	}
	public function loginHistory(){
		$limit = $this->input->post('limit');
		$offset = $this->input->post('page');
		$searchData = $this->input->post('searchData');
		$uplodes = $this->input->post('uplodes');

		if(empty($limit)){
			$limit=10;
		}
		
		if(empty($offset)){
			$offset = 1;
		}
		$this->loginHistoryPaginationLink($limit,$searchData,$uplodes);
		
		$start = ($offset - 1) * $limit;
		$data['loginHistory'] = $this->db_connection->login_history($limit,$start, $searchData,$uplodes);
		$data['selectSignUp'] = $this->db_connection->select_signup();
		$this->load->view('admin/load_data/loginhistory',$data);
	}
	function loginHistoryPaginationLink($limit,$searchData){
		// $limit = 10;
		$totalRows = $this->db_connection->countLoginHistryData($searchData);
		$total_page = ceil($totalRows/$limit);
		$output = '';
		$output .= '<div class="paginating-container pagination-default">';
		$output .= '<ul class="pagination">';
		
		for($i=1; $i<= $total_page; $i++){

			if($i==$this->input->post('page')){
				$active = "active";
			}else{
				$active = "";
			}
			$output .= '
			<li class="page-item '.$active.'">
				<button class="page-link" id="loginPageSegment" data-id="'.$i.'" href="#" >'.$i.'</button>
			</li>';
		}
        
        $output .= '</ul>';
        $output .= '</div>';
		echo $output;
	}

	// ------------------ Addicnal pages For Load -----------------

	function blogdata($offset = null){
		if($this->session->userdata("checksesn")!="asdf8s7df8dsf"){
			redirect("admin/login");
		}

		$searchData = $this->input->post("searchData");
		$status = $this->input->post("status");
		$to = $this->input->post("to");
		$from = $this->input->post("from");
		$FilterBy = $this->input->post("FilterBy");
		$uplodes = $this->input->post("uplodes");
		if(empty($limit)){
			$limit=5;
		}else{
			$limit = $this->input->post("limit");
		}

		$this->load->library('pagination');
		// $limit = 5;
		$offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$config['base_url'] = site_url('admin/blogdata/');
		$config['total_rows'] = $this->db_connection->get_article($limit, $offset, $count = true, $searchData, $status, $to, $from, $FilterBy, $uplodes);
		$config['per_page'] = $limit;
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;

		$config["full_tag_open"] = '<ul class="pagination">';
		$config["full_tag_close"] = '</ul>';
		$config["first_tag_open"] = '<li class="page-item">';
		$config["first_tag_close"] = '</li>';
		$config["last_tag_open"] = '<li class="page-item">';
		$config["last_tag_close"] = '</li>';
		$config['next_link'] = '<i class="fas fa-arrow-right" style="font-size:20px;"></i>';
		$config["next_tag_open"] = '<li class="page-item">';
		$config["next_tag_close"] = '</li>';
		$config["prev_link"] = '<i class="fas fa-arrow-left" style="font-size:20px;"></i>';
		$config["prev_tag_open"] = "<li class='page-item'>";
		$config["prev_tag_close"] = "</li>";
		$config["cur_tag_open"] = "<li class='page-item active'><a class='page-link' id='active' href=''>";
		$config["cur_tag_close"] = "</a></li>";
		$config["num_tag_open"] = "<li class='page-item'>";
		$config["num_tag_close"] = "</li>";

		$this->pagination->initialize($config);
		$data['pagelinks'] = $this->pagination->create_links();

		$data['data'] = $this->db_connection->get_article($limit, $offset, $count = false, $searchData, $status, $to, $from, $FilterBy, $uplodes);

		$this->load->view('admin/load_data/blogdata', $data);
	}

	function commentdata($offset = null){
		if($this->session->userdata("checksesn")!="asdf8s7df8dsf"){
			redirect("admin/login");
		}

		$searchData = $this->input->post("searchData");
		$status = $this->input->post("status");
		$to = $this->input->post("to");
		$from = $this->input->post("from");
		$uplodes = $this->input->post("uplodes");
		if(empty($limit)){
			$limit=5;
		}else{
			$limit = $this->input->post("limit");
		}

		$this->load->library('pagination');
		// $limit = 5;
		$offset = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		$config['base_url'] = site_url('admin/commentdata/');
		$config['total_rows'] = $this->db_connection->getComment($limit, $offset, $count = true, $searchData, $status, $to, $from, $uplodes);
		$config['per_page'] = $limit;
		$config['uri_segment'] = 3;
		$config['num_links'] = 3;

		$config["full_tag_open"] = '<ul class="pagination">';
		$config["full_tag_close"] = '</ul>';
		$config["first_tag_open"] = '<li class="page-item">';
		$config["first_tag_close"] = '</li>';
		$config["last_tag_open"] = '<li class="page-item">';
		$config["last_tag_close"] = '</li>';
		$config['next_link'] = '<i class="fas fa-arrow-right" style="font-size:20px;"></i>';
		$config["next_tag_open"] = '<li class="page-item">';
		$config["next_tag_close"] = '</li>';
		$config["prev_link"] = '<i class="fas fa-arrow-left" style="font-size:20px;"></i>';
		$config["prev_tag_open"] = "<li class='page-item'>";
		$config["prev_tag_close"] = "</li>";
		$config["cur_tag_open"] = "<li class='page-item active'><a class='page-link' id='active' href=''>";
		$config["cur_tag_close"] = "</a></li>";
		$config["num_tag_open"] = "<li class='page-item'>";
		$config["num_tag_close"] = "</li>";

		$this->pagination->initialize($config);
		$data['pagelinks'] = $this->pagination->create_links();

		$data['data'] = $this->db_connection->getComment($limit, $offset, $count = false, $searchData, $status, $to, $from, $uplodes);

		$this->load->view('admin/load_data/commentdata', $data);
	}

	function aboutusdata(){
		if($this->session->userdata("checksesn")!="asdf8s7df8dsf"){
			redirect("admin/login");
		}
		$data['data'] = $this->db_connection->select_about();
		$this->load->view("admin/load_data/aboutusdata",$data);
	}

}
