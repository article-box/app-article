<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	
	function __construct()
	{
	     parent::__construct();
	     $this->load->helper('text');
	     $this->load->helper('url');
	     $this->load->helper(array('pagination'));
	     $this->load->model("wp_connection");
	     $this->load->library("pagination");
	}
	public function index()
	{
	    // $this->output->cache(1);
	    // $this->output->delete_cache();

	    $config = array();
        $config["base_url"] = site_url()."index";
        $config["total_rows"] = $this->wp_connection->get_count();
        $config["per_page"] = 4;
        $config["uri_segment"] = 2;


		$config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next <i class="fa fa-angle-right"></i>';
        $config['prev_link']        = 'Prev <i class="fa fa-angle-left"></i>';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;

        $data["links"] =  $this->pagination->create_links();

        $data['data'] = $this->wp_connection->get_blog_p($config["per_page"], $page);
		$this->load->view('index',$data);
	}

	public function about(){
		$this->load->view('about');
	}

	public function contact(){
		$this->load->view('contact');
	}

	public function product($id){
		$req['prod']=$this->wp_connection->get_product($id);
		$this->load->view('product',$req);
	}

	public function insert_inquiry(){
		$inscontact=array(
				"name"=>$this->input->post('name'),
				"email"=>$this->input->post('email'),
				"phone"=>$this->input->post('phone'),
				"subject"=>$this->input->post('subject'),
				"message"=>$this->input->post('message')
			);
		if($this->db->insert('inquiry',$inscontact) == true){
			$response = array(
                'status' => 'success',
                'message' => " Message send successfully. Thanks for contacting us! check Your email. "
            );
           //  $user__name=$this->input->post('name');
           // $message = "
           //           <html>
           //             <head>
           //               <title>Thanks for contacting us!</title>
           //             </head>
           //             <body>
           //              <h1 style='color:#32D411; text-align: center; '>Thanks for contacting us!</h1>
           //              <h3 style='text-align: center; '>We will be in touch with you shortly.</h3>
           //              <p style='text-align: center; '>While we do our best to answer your queries quickly, it may take about 10 hours to receive a response from us during peak hours. Thanks in advance for your patience.</p>
           //              <p style='color:#32D411; text-align: center; '><i><b>Have a great day!</b></i></p>
           //              <p style='color:#32D411; text-align: center; '> <i><b>Contact No :- 7703865824</b></i> </p>
           //              <p> <b><i>JAIGURU PVT. LTD.</i></b> </p>
           //             </body>
           //           </html>";
           //  $this->load->library('email');
           //  $this->email->set_mailtype("html");
           //  $this->email->from("niralanishant331@gmail.com");
           //  $this->email->to(set_value('email'),set_value('name'));
           //  $this->email->subject("Thanks for contacting us!");
        
           //  $this->email->message($message);
           //  $this->email->set_newline("\r\n");
           //  $this->email->send();
            
		}
		else{
			$response = array(
	            'status' => 'error',
	            'message' => validation_errors()
		    );
		    show_error($this->email->print_debugger());
		}

		$this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
	}
	
// 	 if($this->form_validation->run())
//   {
//     $this->load->library('email');
  
//     $this->email->from(set_value('email'),set_value('fname'));
//     $this->email->to("ajay.suneja1993@gmail.com");
//     $this->email->subject("Registratiion Greeting..");

//     $this->email->message("Thank  You for Registratiion");
//     $this->email->set_newline("\r\n");
//     $this->email->send();

//      if (!$this->email->send()) {
//     show_error($this->email->print_debugger()); }
//   else {
//     echo "Your e-mail has been sent!";
//   }
//   }
//   else
//   {
//   $this->load->view('Admin/register');
//   }


	public function blogs(){
		$this->load->view('blogs');
	}
	public function blog_details($id){
		$req['blogss']=$this->wp_connection->get_blog($id);
		$this->count_views($id);
		$this->load->view('blog_details',$req);

	}

	function search(){
		$this->load->view('search');
	}

	function fetch()
	{
		$output = '';
		$query = '';
		$this->load->model('wp_connection');
		if($this->input->post('query'))
		{
			$query = $this->input->post('query');
		}
		$data = $this->wp_connection->fetch_data($query);
		$output .= '
		
		';
		if($data->num_rows() > 0)
		{
			foreach($data->result() as $row)
			{
				$original_string=$row->blog_title;
				$headertext=$row->header_text;
				$header__text = character_limiter($headertext,90);
				$limited_string = character_limiter($original_string,40);
				$rest_of_string = trim(str_replace($limited_string, "", $original_string));
				$output .= '
					<div class="col-md-4" >
						<div class="category-block articles trend_search">
							<div class="post first hover-dark">
								<a href=" '. site_url("welcome/blog_details/$row->url_slug") .' ">
									<div class="image" data-src="<?=base_url();?>images/blog_image/'.$row->blog_img.'">
										<img style="height:200px; width:400px;" src="'.base_url().'images/blog_image/'.$row->blog_img.'" alt="Proportion"/>
									</div>
								</a>
								<div class="meta">
									<span><i class="fa fa-history p2"></i>'.$row->date_time.'</span>
								</div>
								<h4 title="'.$row->blog_title.'">
									<a href=" '. site_url("welcome/blog_details/$row->url_slug") .' ">
										'.$limited_string.'
									</a>
								</h4>
								<p> '.$header__text.'</p>
								<a href=" '. site_url("welcome/blog_details/$row->url_slug") .' ">
								 	<span class="category" title="Read More.." >Read More  <i class="fa fa-angle-right"></i>...</span> 
								</a>
							</div>
						</div>
					</div>
				';
			}
		}
		else
		{
			$output .= '<tr>
							<td colspan="5">No Data Found</td>
						</tr>';
		}
		$output .= '</table>';
		echo $output;
	}
	function fetch1()
	{
		$output = '';
		$query = '';
		$this->load->model('wp_connection');
		if($this->input->post('query'))
		{
			$query = $this->input->post('query');
		}
		$data = $this->wp_connection->fetch_data($query);
		$output .= '
		
		';
		if($data->num_rows() > 0)
		{ $i=0;
			foreach($data->result() as $row)
			{ if($i++ == 6)break;
				$output .= '
						<p></p>
						<p class="search_text_style" title="'.$row->blog_title.'"><a href=" '. site_url("welcome/blog_details/$row->url_slug") .' ">'.$row->blog_title.'<i class="fa fa-external-link search_link_icon"></i></a>'.' </p>
				';
			}
			$output.='
					<div class="mobile_view_hidden" style="margin:10px;">
						<button class="btn btn-light search_tag"> <i class="fa fa-server p2" style="color:#3498DB;"></i> Technology </button>
						<button class="btn btn-light search_tag"> <i class="fa fa-medkit p2" style="color:#58D68D;"></i> HEALTH </button>
						<button class="btn btn-light search_tag"> <i class="fa fa-bus p2" style="color:#F5B7B1;"></i> travel </button>
						<button class="btn btn-light search_tag"> <i class="fa fa-fire p2" style="color:#E74C3C;"></i> Trending </button>
						<button class="btn btn-light search_tag"> <i class="fa fa-thumbs-up p2" style="color:#3498DB;"></i> Most Like </button>
					</div>
					';
		}
		else
		{
			$output .= '<p style="margin:10px;"> No Data Found </p>';
			$output.='
					<div style="margin:10px;">
						<button class="btn btn-light" style=" box-shadow: 0 1px 6px rgba(57,73,76,.20); border-radius: 30px; margin:5px;"> <i class="fa fa-server p2" style="color:#3498DB;"></i> Technology </button>
						<button class="btn btn-light" style=" box-shadow: 0 1px 6px rgba(57,73,76,.20); border-radius: 30px; margin:5px;"> <i class="fa fa-medkit p2" style="color:#58D68D;"></i> HEALTH </button>
						<button class="btn btn-light" style=" box-shadow: 0 1px 6px rgba(57,73,76,.20); border-radius: 30px; margin:5px;"> <i class="fa fa-bus p2" style="color:#F5B7B1;"></i> travel </button>
						<button class="btn btn-light" style=" box-shadow: 0 1px 6px rgba(57,73,76,.20); border-radius: 30px; margin:5px;"> <i class="fa fa-fire p2" style="color:#E74C3C;"></i> Trending </button>
						<button class="btn btn-light" style=" box-shadow: 0 1px 6px rgba(57,73,76,.20); border-radius: 30px; margin:5px;"> <i class="fa fa-thumbs-up p2" style="color:#3498DB;"></i> Most Like </button>
					</div>
					';
		}
		$output .= '</table>';
		echo $output;
	}


	public function insert_comment(){
		$comments=array(
					"blog_id"=>$this->input->post('blog_id'),
					"date_time"=>$this->input->post('time'),
					"name"=>$this->input->post('name'),
					"comment"=>$this->input->post('comment'),
					"notification"=>'0',
					"email"=>$this->input->post('email')
				);
			if($this->db->insert('comment',$comments)==true){
					// $bid=array("comment_id"=>$this->input->post('blog_id'));
					// $this->db->insert('comment_history',$bid);
					$name=$this->input->post('name');
					$comment=$this->input->post('comment');
					$date_time=$this->input->post('time');
					$response = array(
		            'status' => 'success',
		            'message' => ' 
					<article class="category">
                    <div class="comment-avatar">
                        <img alt="Image" src="'.base_url().'images/profile/profile.png" class="avatar">
                    </div>
                    <div class="comment-body">
                        <div class="meta-data">
                            <span class="comment-date "> <i class="fa fa-user p2"></i> '.$name.'</span>
                        </div>
                        <div class="comment-content category">
                            '.$comment.'
                        </div>
                        <div class="meta-data">
                            <span class="comment-date trend_comment">'.$date_time.'</span>
                        </div>
                    </div>
                	</article>
                	'
		        );
			}else{
			$response = array(
	            'status' => 'error',
	            'message' => validation_errors()
	        );
		}
		// redirect('admin/show_product');
		$this->output
  		->set_content_type('application/json; charset=utf-8')
		    ->set_output(json_encode($response));
	}

	public function about_us(){
		$this->load->view('about_us');
	}

	public function terms_and_conditions(){
		$this->load->view('terms_and_conditions');
	}

	public function like($id){
		$likes=array("likes"=>$this->input->post('like'));
		if($this->db->where('blog_id',$id)->update('blog',$likes)){
		$response = array(
		            'status' => 'success',
		            'message' => "<i class='fa fa-heart' style='color: red;'></i>
                                    <i class='fa fa-heart' style='color: red; font-size: 30px;'></i>
                                    <i class='fa fa-heart' style='color: red;'></i>"
		        );
		}else{
			$response = array(
	            'status' => 'error',
	            'message' => validation_errors()
	        );
		}
		// redirect('admin/show_product');
		$this->output
  		->set_content_type('application/json; charset=utf-8')
		    ->set_output(json_encode($response));
	}
	function category($id = null){
		if (is_numeric($this->uri->segment(2))) 
        $id = NULL;
    	$tttt=$this->wp_connection->get_category($id);
		$data['cat']=$this->wp_connection->get_category($id);
       	$config = array();
        $config["base_url"] = site_url()."category";
        $config["total_rows"] = $this->wp_connection->get_count_($tttt->category_id);
        $config["per_page"] = 8;
        $config["uri_segment"] = 2;

        if (!is_null($id)) {
         $config['uri_segment'] = $config['uri_segment'] + 1;
        $config['base_url'] = $config['base_url'].'/'.$id;
    	}


		$config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next <i class="fa fa-angle-right"></i>';
        $config['prev_link']        = '<i class="fa fa-angle-left"></i> Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next </li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $this->pagination->initialize($config);
        
        $page = ($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;

        $data["links"] =  $this->pagination->create_links();
        $data['data'] = $this->wp_connection->get_blog_p_($config["per_page"], $page,$tttt->category_id);

        $this->load->view('category', $data);
	}

	function sub_category($id){
		// $reqs['subs']=$this->wp_connection->get_sub_categories($id);
		// $this->load->view('sub_category',$reqs);
		if (is_numeric($this->uri->segment(2))) 
        $id = NULL;
    	$tttt=$this->wp_connection->get_sub_categories($id);
		$data['subs']=$this->wp_connection->get_sub_categories($id);
       	$config = array();
        $config["base_url"] = site_url()."sub_category";
        $config["total_rows"] = $this->wp_connection->get_count_sub($tttt->id);
        $config["per_page"] = 5;
        $config["uri_segment"] = 2;

        if (!is_null($id)) {
         $config['uri_segment'] = $config['uri_segment'] + 1;
        $config['base_url'] = $config['base_url'].'/'.$id;
    	}


		$config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next <i class="fa fa-angle-right"></i>';
        $config['prev_link']        = 'Prev <i class="fa fa-angle-left"></i>';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $this->pagination->initialize($config);
        
        $page = ($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;

        $data["links"] =  $this->pagination->create_links();
        $data['data'] = $this->wp_connection->get_blog_sub($config["per_page"], $page,$tttt->id);

        $this->load->view('sub_category', $data);
	}

	function mypost(){
		$this->load->view('mypost');
	}
	// Infinite Scroll Pagination
	function fetch_data()
	{
	  $output = '';
	  $this->load->model('wp_connection');
	  $data = $this->wp_connection->fetch_list($this->input->post('limit'), $this->input->post('start'));
	  if($data->num_rows() > 0)
	  {
	   foreach($data->result() as $row)
	   {
	    $output .= '
		<div class="post hover-light clear-left trend_" style="margin-bottom:10px; padding:20px;">
			<h4>
				<a href=" '. site_url("welcome/blog_details/$row->url_slug") .' "> 
					'.$row->blog_title.'
				</a>
			</h4>
			<p>'.$row->header_text.' <a style="color:#3498db;" href=" '. site_url("welcome/blog_details/$row->url_slug") .' "> ... </a> </p>
		</div>
	    ';
	   }
	  }
	  echo $output;
	}

	public function profile(){
		$this->load->view('profile');
	}

	public function pagination($id = null){
		if (is_numeric($this->uri->segment(2))) 
        $id = NULL;
    	$tttt=$this->wp_connection->get_category($id);
		$data['caty']=$this->wp_connection->get_category($id);
       	$config = array();
        $config["base_url"] = site_url()."pagination";
        $config["total_rows"] = $this->wp_connection->get_count_($tttt->category_id);
        $config["per_page"] = 4;
        $config["uri_segment"] = 2;

        if (!is_null($id)) {
         $config['uri_segment'] = $config['uri_segment'] + 1;
        $config['base_url'] = $config['base_url'].'/'.$id;
    	}


		$config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next <i class="fa fa-angle-right"></i>';
        $config['prev_link']        = 'Prev <i class="fa fa-angle-left"></i>';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
        $this->pagination->initialize($config);
        
        $page = ($this->uri->segment($config['uri_segment'])) ? $this->uri->segment($config['uri_segment']) : 0;

        $data["links"] =  $this->pagination->create_links();
        $data['data'] = $this->wp_connection->get_blog_p_($config["per_page"], $page,$tttt->category_id);

        $this->load->view('pagination', $data);
    }

    public function ajax_pagination(){
    	$this->load->view('ajax_pagination');
    }

    function paginations()
	{
	  // $this->load->model("ajax_pagination_model");
	  $this->load->library("pagination");
	  $config = array();
	  $config["base_url"] = "#";
	  $config["total_rows"] = $this->wp_connection->count_all();
	  $config["per_page"] = 10;
	  $config["uri_segment"] = 3;
	  $config["use_page_numbers"] = TRUE;
	  $config["full_tag_open"] = '<ul class="pagination">';
	  $config["full_tag_close"] = '</ul>';
	  $config["first_tag_open"] = '<li>';
	  $config["first_tag_close"] = '</li>';
	  $config["last_tag_open"] = '<li>';
	  $config["last_tag_close"] = '</li>';
	  $config['next_link'] = '&gt;';
	  $config["next_tag_open"] = '<li>';
	  $config["next_tag_close"] = '</li>';
	  $config["prev_link"] = "&lt;";
	  $config["prev_tag_open"] = "<li>";
	  $config["prev_tag_close"] = "</li>";
	  $config["cur_tag_open"] = "<li class='active'><a href='#'>";
	  $config["cur_tag_close"] = "</a></li>";
	  $config["num_tag_open"] = "<li>";
	  $config["num_tag_close"] = "</li>";
	  $config["num_links"] = 1;
	  $this->pagination->initialize($config);
	  $page = $this->uri->segment(3);
	  $start = ($page - 1) * $config["per_page"];

	  $output = array(
	   'pagination_link'  => $this->pagination->create_links(),
	   'country_table'   => $this->wp_connection->fetch_details($config["per_page"], $start)
	  );
	  echo json_encode($output);
	}


	// loder_data

	public function right_6sas54asd6asd4as98as3as21asd3(){
		$this->load->view('loderdata/right_6sas54asd6asd4as98as3as21asd3');
	}
	public function trending_6sas54asd6asd4as98as3as21asd3(){
		$this->load->view('loderdata/trending_6sas54asd6asd4as98as3as21asd3');
	}
	public function opinion(){
		$this->load->view('loderdata/opinion');
	}
	public function recommented(){
		$this->load->view('loderdata/recommented');
	}
	public function recommented_data(){
		$this->load->view('loderdata/recommented_data');
	}
	public function slider_654654d64sf6ds5f4sd(){
		$this->load->view('loderdata/slider_654654d64sf6ds5f4sd');
	}
	public function like_data(){
		$this->load->view('loderdata/like_data');
	}
	public function categoty_data(){
		$this->load->view('loderdata/categoty_data');
	}
	public function subcategoty_data(){
		$this->load->view('loderdata/subcategoty_data');
	}
	public function skeleton_loader(){
		$this->load->view('loderdata/skeleton_loader');
	}
	public function infnite_post(){
		$this->load->view('infnite_post');
	}
	public function comment_data($cm){
		// $reqst['cat']=$this->wp_connection->get_category($id);
		// $this->load->view('category',$reqst);

		$dt['comment']=$this->wp_connection->comment_data($cm);
		$this->load->view('loderdata/comment_data',$dt);
	}

	// For Vews Systems
	public function count_views($id){
		$get_blogid = $this->wp_connection->get_blog($id);
		$blogid=$get_blogid->blog_id;

		$views_data = $this->wp_connection->get_views($blogid);

		$checkview_ = array("blog_id"=>$blogid);
		$check_view=$this->wp_connection->checkview($checkview_);

		if($check_view==1){
			$total_views = $views_data->views + 1;
			$view_update = array(
				"views" => $total_views
			);
		}else{
			$view_data = array(
				"blog_id"=>$blogid,
				"views" => '1'
			);
		}

		if($check_view==1){
			$this->db->where('blog_id',$blogid)->update('view_counter',$view_update);
		}else{
			$this->db->insert('view_counter',$view_data);
		}
	}

}