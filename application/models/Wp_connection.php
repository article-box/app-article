<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wp_connection extends CI_Model {

		protected $table = 'blog';
		protected $blog_category = 'blog_category';
		public function __construct() {
	        parent::__construct();
	    }

	public function select_preloader(){
		$preloader=$this->db->get('preloader');
		return $preloader->result();
	}

	public function select_details(){
		$details=$this->db->get('details');
		return $details->result();
	}

	public function select_details_info(){
		$details=$this->db->get('details_info');
		return $details->result();
	}

	public function select_menu()
	{
		$getmenu=$this->db->get('menu');
		return $getmenu->result();
	}

	public function select_service()
	{
		$getproduct=$this->db->get('service');
		return $getproduct->result();
	}

	public function get_service($id)
	{
		$getproduct=$this->db->where('url_slug',$id)->get('service');
		return $getproduct->row();
	}

	public function select_categories()
	{
		$getcategories=$this->db->get('categories');
		return $getcategories->result();
	}

	public function select_product()
	{
		$getproduct=$this->db->get('product');
		return $getproduct->result();
	}

	public function get_product($id)
	{
		$getproduct=$this->db->where('url_slug',$id)->get('product');	
		return $getproduct->row();
	}

	public function select_product_img(){
		$product_img=$this->db->get('product_img');
		return $product_img->result();
	}

	public function select_blog()
	{
		$getblog=$this->db->where('status',1)->get('blog');
		return $getblog->result();
	}

	public function get_blog($id)
	{
		$getblogs=$this->db->where('url_slug',$id)->get('blog');
		return $getblogs->row();
	}

	public function getblog($id)
	{
		$getblogs=$this->db->where('blog_id',$id)->get('blog');
		return $getblogs->row();
	}

	public function get_link($id)
	{
		$getlink=$this->db->where('getlink_id',$id)->get('getlink');
		return $getlink->row();
	}
	public function getlink()
	{
		$getlink=$this->db->get('getlink');
		return $getlink->result();
	}

	public function prankdata()
	{
		$prankdata=$this->db->get('prank_data');
		return $prankdata->result();
	}

	public function checksignin($chakesign)
	{
		$sgn=$this->db->query("SELECT * FROM `getlink` where `name`='$chakesign[name]' and `password`='$chakesign[password]' ");
		return $sgn->num_rows();
	}

	public function chackeuser(){
	$em=$this->session->userdata("sesn"); //this is a session  value
	$sql=$this->db->where("name",$em)->get("getlink"); //match session value where database and session 
	return $sql->row();
    }


    public function checksignin1($chakesign)
	{
			$sgn=$this->db->query("SELECT * FROM `user_signup` where `user_email`='$chakesign[user_email]' and `user_password`='$chakesign[user_password]' ");
			return $sgn->num_rows();
	}
	public function chackeuser1(){
	$em=$this->session->userdata("sesn"); //this is a session  value
	$sql=$this->db->where("user_email",$em)->get("user_signup"); //match session value where database and session 
	return $sql->row();
    }
    public function get_signup1($id)
    {
    	$getsignup=$this->db->where('url_slug',$id)->get('user_signup');
    	return $getsignup->row();
    }
    public function get_signup($id)
    {
    	$getsignup=$this->db->where('user_id',$id)->get('user_signup');
    	return $getsignup->row();
    }

    public function select_signup()
    {
    	$signup=$this->db->get('user_signup');
    	return $signup->result();
    }

    public function orderbyblog(){
		$ps=$this->db->where('status',1)->order_by('blog_id','desc')->get('blog'); // order_by() function is work for descinding order on post
		return $ps->result();
	}

	public function orderblog(){
		$ps=$this->db->where('status',1)->order_by('blog_id','asc')->get('blog'); // order_by() function is work for descinding order on post
		return $ps->result();
	}

	public function blog_category()
	{
		$blogc=$this->db->where('status',1)->get('blog_category');
		return $blogc->result();
	}

	public function get_category($id)
	{
		$blogb=$this->db->where('url_slug',$id)->get('blog_category');
		return $blogb->row();
	}

	public function logo()
    {
    	$logo=$this->db->get('logo');
    	return $logo->result();
    }

    public function select_banner(){
    	$select_banner=$this->db->get('banner');
    	return $select_banner->result();
    }

  
   public function select_about(){
   	$select_about=$this->db->get('about');
   	return $select_about->result();
   }
   public function select_tac(){
   	$tac=$this->db->get('tac');
   	return $tac->result();
   }


   public function select_sub_categories(){
   	$select_sub_categories=$this->db->where('status',1)->get('sub_categories');
   	return $select_sub_categories->result();
   }

   public function get_sub_categories($id){
   	$get_sub_categories=$this->db->where('url_slug',$id)->get('sub_categories');
   	return $get_sub_categories->row();
   }
   
   public function get_inquiry($id){
		$get_inquiry=$this->db->where('inquiry_id',$id)->get('inquiry');
		return $get_inquiry->row();
    }
    
    public function select_meta(){
		$select_meta=$this->db->get('meta_data');
		return $select_meta->result();
	}


	function fetch_data($query)
	{
		$this->db->select("*");
		$this->db->where("status",1);
		$this->db->from("blog");
		if($query != '')
		{
			$this->db->like('blog_title', $query);
			$this->db->or_like('blog_text', $query);
			$this->db->or_like('header_text', $query);
			// $this->db->or_like('blog_img', $query);
		}
		$this->db->order_by('blog_id', 'DESC');
		return $this->db->get();
	}

	function select_like(){
		$select_like=$this->db->get('likes');
		return $select_like->result();
	}


	function fetch_list($limit, $start)
	{

	  $this->db->select("*");
	  $this->db->from("blog");
	  $this->db->order_by("blog_id", "DESC");
	  $this->db->limit($limit, $start);
	  $query = $this->db->get();
	  return $query;
	}

	public function comment(){
		$comment=$this->db->get('comment');
		return $comment->result();
	}

	public function comment_data($cm){
		if($cm != '')
		{
		// $comment=$this->db->get('comment');
		// $df= $comment->blog_id;
		$comment_=$this->db->where('blog_id',$cm)->get('comment');
		// $this->db->select("*");
		// $this->db->where("status",1);
		// $this->db->from("blog");
		return $comment_->row();
		}
		else{
			echo "No data";
		}
	}


  	public function orderbylike(){
		$ps=$this->db->where('status',1)->order_by('likes','desc')->get('blog'); // order_by() function is work for descinding order on post
		return $ps->result();
	}

	 public function get_count() {
        return $this->db->where('pin',1)->count_all_results($this->table);
    }

    public function get_count_($tttt) {
        return $this->db->where('category_id', $tttt)->count_all_results($this->table);
    }
    public function get_count_sub($tttt) {
        return $this->db->where('sub_category_id', $tttt)->count_all_results($this->table);
    }

    public function get_blog_p($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->where('pin',1)->order_by('blog_id','desc')->get($this->table);
        return $query->result();
    }
    public function get_blog_p_($limit, $start,$tttt) {
        $this->db->limit($limit, $start);
        $query = $this->db->where('category_id',$tttt)->order_by('blog_id','desc')->get($this->table);
        return $query->result();
    }

    public function get_blog_sub($limit, $start,$tttt) {
        $this->db->limit($limit, $start);
        $query = $this->db->where('sub_category_id',$tttt)->order_by('blog_id','desc')->get($this->table);
        return $query->result();
    }


     function count_all()
	 {
	  $query = $this->db->get("blog");
	  return $query->num_rows();
	 }

	 function fetch_details($limit, $start)
	 {
	  $output = '';
	  $this->db->select("*");
	  $this->db->from("blog");
	  $this->db->order_by("blog_id", "ASC");
	  $this->db->limit($limit, $start);
	  $query = $this->db->get();
	  $output .= '
	  <table class="table table-bordered">
	   <tr>
	    <th>Country ID</th>
	    <th>Country Name</th>
	   </tr>
	  ';
	  foreach($query->result() as $row)
	  {
	   $output .= '
	   <tr>
	    <td>'.$row->blog_id.'</td>
	    <td>'.$row->blog_title.'</td>
	   </tr>
	   ';
	  }
	  $output .= '</table>';
	  return $output;
	}

	public function views(){
		$views = $this->db->get("view_counter");
		return $views->result();
	}

	public function get_views($id)
	{
		$get_views=$this->db->where('blog_id',$id)->get('view_counter');
		return $get_views->row();
	}

	public function checkview($checkview_)
	{
			$sgn=$this->db->query("SELECT * FROM `view_counter` where `blog_id`='$checkview_[blog_id]'");
			return $sgn->num_rows();
	}

	// function update_counter($slug) {
		
	// 	// return current article views 
	//     $this->db->where('article_slug', urldecode($slug));
	//     $this->db->select('article_views');
	//     $count = $this->db->get('articles')->row();
		
	// 	// then increase by one 
	//     $this->db->where('article_slug', urldecode($slug));
	//     $this->db->set('article_views', ($count->article_views + 1));
	//     $this->db->update('articles');
	// }

}
