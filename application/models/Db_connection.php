<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Db_connection extends CI_Model {

	public function select_menu()
	{
		$getmenu=$this->db->get('menu');
		return $getmenu->result();
	}

	public function get_menu($id){
		$get_menu=$this->db->where('menu_id',$id)->get('menu');
		return $get_menu->row();
	}

	public function get_tac($id){
		$get_tac=$this->db->where('tac_id',$id)->get('tac');
		return $get_tac->row();
	}
	public function get_pap($id){
		$get_pap=$this->db->where('pap_id',$id)->get('pap');
		return $get_pap->row();
	}
	public function select_banner(){
		$selectbanner=$this->db->get('banner');
		return $selectbanner->result();
	}

	public function get_banner($id){
		$getbanner=$this->db->where('banner_id',$id)->get('banner');
		return $getbanner->row();
	}

	public function select_service()
	{
		$getservice=$this->db->get('service');
		return $getservice->result();
	}

	public function select_product()
	{
		$getproduct=$this->db->get('product');
		return $getproduct->result();
	}

	public function get_product($id)
	{
		$getproduct=$this->db->where('product_id',$id)->get('product');
		return $getproduct->row();
	}

	public function select_product_img(){
		$getproimg=$this->db->get('product_img');
		return $getproimg->result();
	}

	public function get_product_img($id){
		$getproimg=$this->db->where('img_id',$id)->get('product_img');
		return $getproimg->row();
	}


	public function select_categories()
	{
		$this->db->select('*');
		$this->db->from('blog_category');
		// $this->db->join('sub_categories', 'sub_categories.category_id = blog_category.category_id ');
		// $this->db->group_by('blog_category.category_id');
		$getcategories=$this->db->get();
		return $getcategories->result();
	}
	public function get_categories($id)
	{
		$categories=$this->db->where('category_id',$id)->get('blog_category');
		return $categories->row();
	}
	public function get_sub_categories($id)
	{
		$sub_categories=$this->db->where('id',$id)->get('sub_categories');
		return $sub_categories->row();
	}

	function sub_categories_list($id, $count){
		$sub_categories=$this->db->where('category_id',$id)->get('sub_categories');
		if($count){
			return $sub_categories->num_rows();	
		}else{
			return $sub_categories->result();
		}
	}

	public function select_blog()
	{
		$getblog=$this->db->get('blog');
		return $getblog->result();
	}
	public function get_blog($id)
	{
		if($blog=$this->db->where('blog_id',$id)->get('blog')){
			return $blog->row();
		}else{
			return false;
		}
	}
	public function get_blog_session($id)
	{
		if($blog=$this->db->where('session_id',$id)->get('blog')){
			return $blog->row();	
		}else{
			return false;
		}
		
	}
	public function orderbyblog(){
		$ps=$this->db->order_by('blog_id','desc')->get('blog'); // order_by() function is work for descinding order on post
		return $ps->result();
	}

	public function orderblog(){
		$ps=$this->db->order_by('blog_id','asc')->get('blog'); // order_by() function is work for descinding order on post
		return $ps->result();
	}

	public function checksignin($chakesign)
	{
			$sgn=$this->db->query("SELECT * FROM `signup` where `email`='$chakesign[email]' and `password`='$chakesign[password]' ");
			return $sgn->num_rows();
	}
	public function chackeuser(){
	$em=$this->session->userdata("sesn"); //this is a session  value
	$sql=$this->db->where("user_email",$em)->get("signup"); //match session value where database and session 
	return $sql->row();
    }

    public function logo()
    {
    	$logo=$this->db->get('logo');
    	return $logo->result();
    }

    public function get_logo($id)
    {
    	$logo1=$this->db->where('logo_id',$id)->get('logo');
    	return $logo1->row();
    }

    public function others()
    {
    	$others=$this->db->order_by('others_id','desc')->get('others');
    	return $others->result();
    }
    public function get_others($id)
    {
    	$others=$this->db->where('others_id',$id)->get('others');
    	return $others->row();
    }

    public function get_signup($id)
    {
    	$getsignup=$this->db->where('signup_id',$id)->get('signup');
    	return $getsignup->row();
    }

    public function select_signup()
    {
    	$signup=$this->db->get('signup');
    	return $signup->result();
    }
    public function blog_category()
	{
		$blogc=$this->db->get('blog_category');
		return $blogc->result();
	}

	public function select_weare(){
		$weare=$this->db->get('weare');
		return $weare->result();
	}

	public function get_weare($id){
		$getweare=$this->db->where('weare_id',$id)->get('weare');
		return $getweare->row();
	}
	public function select_about(){
		$this->db->order_by("about_id","desc");
		$select_about=$this->db->get('about');
		return $select_about->result();
	}
	public function get_about($id){
		$get_about=$this->db->where('about_id',$id)->get('about');
		return $get_about->row();
	}

	public function select_project(){
		$select_project=$this->db->get('project');
		return $select_project->result();
	}
	public function get_project($id){
		$get_project=$this->db->where('project_id',$id)->get('project');
		return $get_project->row();
	}
	public function select_organisation(){
		$select_organisation=$this->db->get('organisation');
		return $select_organisation->result();
	}

	public function get_organisation($id){
		$get_organisation=$this->db->where('org_id',$id)->get('organisation');
		return $get_organisation->row();
	}
	public function select_owner(){
		$select_owner=$this->db->get('owner');
		return $select_owner->result();
	}

	public function get_owner($id){
		$get_owner=$this->db->where('owner_id',$id)->get('owner');
		return $get_owner->row();
	}

	public function select_inquiry(){
		$select_inquiry=$this->db->order_by('inquiry_id','desc')->get('inquiry');
		return $select_inquiry->result();
	}
	public function get_inquiry($id){
		$get_inquiry=$this->db->where('inquiry_id',$id)->get('inquiry');
		return $get_inquiry->row();
	}
	
	public function select_details(){
		$details=$this->db->get('details');
		return $details->result();
	}

	public function select_details_info(){
		$details=$this->db->get('details_info');
		return $details->result();
	}
	public function get_details($id){
		$get_details=$this->db->where('details',$id)->get('details');
		return $get_details->row();
	}
	public function get_details_info($id){
		$get_details=$this->db->where('details',$id)->get('details_info');
		return $get_details->row();
	}
	
	public function get_meta($id){
		$get_meta=$this->db->where('meta_id',$id)->get('meta_data');
		return $get_meta->row();
	}

	public function select_meta(){
		$select_meta=$this->db->get('meta_data');
		return $select_meta->result();
	}

	public function select_sub(){
		$sub=$this->db->get("sub_categories");
		return $sub->result();
	}
	public function fetch_category(){
	  $this->db->order_by("category_name", "ASC");
	  $query = $this->db->get("blog_category");
	  // print_r($query);
	  return $query->result();
	}

	public function fetch_categories($id){
	  $this->db->where('categories_id', $id);
	  $this->db->order_by('sub_categories_name', 'ASC');
	  $query = $this->db->get('sub_categories');
	  $output = '<option value=""> Select sub categories </option>';
	  foreach($query->result() as $sub)
	  {
	  	// print_r($output);
	   $output .= '<option value="'.$sub->id.'">'.$sub->sub_categories_name.'</option>';
	  }
	  return print_r($output);
	}


	 function fetch_country()
	 {
	  $this->db->order_by("country_name", "ASC");
	  $query = $this->db->get("country");
	 
	  return $query->result();
	 }

	 function fetch_state($category_id)
	 {
	  $this->db->where('category_id', $category_id);
	  $this->db->order_by('sub_categories_name', 'ASC');
	  $query = $this->db->get('sub_categories');
	  $output = '<option value="">Select Sub Category</option>';
	  foreach($query->result() as $row)
	  {
	   $output .= '<option value="'.$row->category_id.'">'.$row->sub_categories_name.'</option>';
	  }
	  return $output;
	 }

	 function deletemenu(){
		$id = $this->input->get('id');
		// print_r($id);
		$this->db->where('menu_id', $id);
		$this->db->delete('menu');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function deleteblog(){
		$id = $this->input->get('id');
		// print_r($id);
		$this->db->where('blog_id', $id);
		$this->db->delete('blog');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	function delete_category(){
		$id=$this->input->get('id');
		$this->db->where('category_id', $id);
		$this->db->delete('blog_category');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}

	}
	function delete_subcategory(){
		$id=$this->input->get('id');
		$this->db->where('id', $id);
		$this->db->delete('sub_categories');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}

	}

	public function comment(){
		$comment=$this->db->order_by('comment_id','desc')->get('comment');
		return $comment->result();
	}
	public function comment_n(){
		$comment=$this->db->where('notification',0)->order_by('comment_id','desc')->get('comment');
		return $comment->result();
	}
	

	public function get_comment($id){
		$get_comment=$this->db->where('comment_id',$id)->get('comment');
		return $get_comment->row();
	}

	public function notification() {
        return $this->db->where('notification',0)->count_all_results('comment');
    }

	// not in use
	function delete_comment(){
		$id=$this->input->get('id');
		$this->db->where('comment_id', $id);
		$this->db->delete('comment');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}

	}
	function delete_reply(){
		$id=$this->input->get('id');
		$this->db->where('reply_id', $id);
		$this->db->delete('reply');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function select_notes(){
		$notes=$this->db->order_by('notes_id','desc')->get('notes');
		return $notes->result();
	}

	function delete_notes(){
		$id=$this->input->get('id');
		$this->db->where('notes_id', $id);
		$this->db->delete('notes');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function login_history($limit, $offset, $searchData='',$uplodes=''){
		$this->db->limit($limit, $offset);

		if(!empty($uplodes)){
			if($uplodes=='desc'){
				$this->db->order_by('id', 'DESC');
			}else{
				$this->db->order_by('id', 'ASC');
			}
		}else{
			 $this->db->order_by('id', 'DESC');
		}

		if(!empty($searchData)){
			$this->db->like('email', $searchData);
			$this->db->or_like('time', $searchData);
			$this->db->or_like('date', $searchData);
			$this->db->or_like('ip', $searchData);
		}

		$history = $this->db->get('login_history');
		return $history->result();
	}
	function countLoginHistryData($searchData=''){
		
		if(!empty($searchData)){
			$this->db->like('email', $searchData);
			$this->db->or_like('time', $searchData);
			$this->db->or_like('date', $searchData);
			$this->db->or_like('ip', $searchData);
		}

		$history = $this->db->get('login_history');
		return $history->num_rows();
	}

	function fetch_data($query)
	{
		$this->db->select("*");
		$this->db->where("status",1);
		$this->db->from("blog");
		if($query != '')
		{
			$this->db->like('blog_title', $query);
			$this->db->or_like('blog_text', $query);
			$this->db->or_like('header_text', $query);
			// $this->db->or_like('blog_img', $query);
		}
		$this->db->order_by('blog_id', 'DESC');
		return $this->db->get();
	}

	function countTrandRow(){
		$query =  $this->db->where("trending",1)->get('blog');
		return $query->num_rows();
	}


	public function get_article($limit, $offset, $count, $searchData, $status, $to, $from,$FilterBy,$uplodes)
	{
		$this->db->select('*');
		$this->db->from('blog');
		// $this->db->order_by("blog_id", "DESC");

		if (!empty($status)) {
			if ($status == 1) {
				$this->db->where("status", 1);
			} else {
				$this->db->where("status", 2);
			}
		}

		if (!empty($to) && !empty($from)) {
			$this->db->where("date BETWEEN '$to' AND '$from'");
		}

		if (!empty($searchData)) {
			$this->db->like('blog_title', $searchData);
			$this->db->or_like('header_text', $searchData);
			$this->db->or_like('blog_text', $searchData);
			// $this->db->or_like('date', $searchData);
			$this->db->or_like('blog_id ', $searchData);
			$this->db->or_like('time', $searchData);
		}

		if(!empty($FilterBy)){
			if($FilterBy=='pin'){
				$this->db->where("pin",1);
			}elseif($FilterBy=='tranding'){
				$this->db->where("trending",1);
			}else{
				$this->db->where("star",1);
			}
		}

		if(!empty($uplodes)){
			if($uplodes=='desc'){
				$this->db->order_by('blog_id', 'DESC');
			}else{
				$this->db->order_by('blog_id', 'ASC');
			}
		}else{
			 $this->db->order_by('blog_id', 'DESC');
		}



		if ($count) {
			return $this->db->count_all_results();
		} else {
			$this->db->limit($limit, $offset);
			$query = $this->db->get();

			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				echo " <h1> No recod found ! </h1> ";
			}
		}

		return array();
	}

	public function getComment($limit, $offset, $count, $searchData, $status, $to, $from, $uplodes)
	{
		$this->db->select('*');
		$this->db->from('comment');

		$this->db->join('blog', 'blog.blog_id = comment.blog_id');

		if (!empty($status)) {
			if ($status == 1) {
				$this->db->where("varified", 1);
			} else {
				$this->db->where("status", 2);
			}
		}

		if (!empty($to) && !empty($from)) {
			$this->db->where("date BETWEEN '$to' AND '$from'");
		}

		if (!empty($searchData)) {
			$this->db->like('comment_id ', $searchData);
			$this->db->or_like('comment', $searchData);
			$this->db->or_like('name', $searchData);
			$this->db->or_like('date_time', $searchData);
			$this->db->or_like('email ', $searchData);
			$this->db->or_like('time', $searchData);
		}

		if(!empty($uplodes)){
			if($uplodes=='desc'){
				$this->db->order_by('comment_id ', 'DESC');
			}else{
				$this->db->order_by('comment_id ', 'ASC');
			}
		}else{
			 $this->db->order_by('comment_id ', 'DESC');
		}



		if ($count) {
			return $this->db->count_all_results();
		} else {
			$this->db->limit($limit, $offset);
			$query = $this->db->get();

			if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				echo " <h2> No recod found ! </h2> ";
			}
		}

		return array();
	}

	public function get_reply($id){
		$this->db->select('*');
		$this->db->from('reply');
		$this->db->order_by("reply_id","desc");
		$this->db->where("comment_id",$id);		
		$query = $this->db->get();
		return $query->result();
	}

	public function reply(){
		$reply=$this->db->order_by('comment_id','desc')->get('reply');
		return $reply->result();
	}

}
