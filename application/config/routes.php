<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['admin-dasbord'] = 'admin/index';
$route["blogdata/(:any)"] = "admin/blogdata/$1";
$route["commentdata/(:any)"] = "admin/commentdata/$1";

$route['articles'] = 'admin/articles';
$route['articles/(:num)'] = 'admin/articles/$1';


$route['index'] = 'welcome/index';

$route['recommented'] = 'welcome/recommented';
$route['comment_data/(:any)'] = 'welcome/comment_data/$1';
$route['like_data'] = 'welcome/like_data';
$route['categoty_data'] = 'welcome/categoty_data';
$route['subcategoty_data'] = 'welcome/subcategoty_data';
$route['infnite_post'] = 'welcome/infnite_post';
$route['opinion'] = 'welcome/opinion';
$route['right_6sas54asd6asd4as98as3as21asd3'] = 'welcome/right_6sas54asd6asd4as98as3as21asd3';
$route['trending_6sas54asd6asd4as98as3as21asd3'] = 'welcome/trending_6sas54asd6asd4as98as3as21asd3';
$route['slider_654654d64sf6ds5f4sd'] = 'welcome/slider_654654d64sf6ds5f4sd';

$route['index/(:num)'] = 'welcome/index/$1';

// $route['welcome/category/(:any)/(:num)'] = 'welcome/category/$1/$2';
// $route['category/(:any)'] = 'welcome/category/$1/$2';
// $route['about-us'] = 'welcome/about';
// $route['latest-project'] = 'welcome/latest_project';
// $route['we-are'] = 'welcome/we_ares';
// $route['blogs'] = 'welcome/blogs';
// $route['contact-us'] = 'welcome/contact';

// $route["product/(:any)"] = "welcome/product/$1";
// $route["(:any)"] = "welcome/blog_details/$1";
// $route['we-are/(:any)'] = "welcome/weare/$1";

// $route['latest-project/(:any)'] = "welcome/latest_projects/$1";

// $route['contact-us'] = 'welcome/contact';
$route['pagination'] = 'welcome/pagination';
$route['pagination/(:any)'] = 'welcome/pagination/$1';
$route['pagination/(:any)/(:num)'] = 'welcome/pagination/$1/$2';

$route['category'] = 'welcome/category';
$route['category/(:any)'] = 'welcome/category/$1';
$route['category/(:any)/(:num)'] = 'welcome/category/$1/$2';

$route['sub_category'] = 'welcome/sub_category';
$route['sub_category/(:any)'] = 'welcome/sub_category/$1';
$route['sub_category/(:any)/(:num)'] = 'welcome/sub_category/$1/$2';


$route['default_controller'] = 'welcome';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['success-flash'] = 'MyFlashController/success';
$route['error-flash'] = 'MyFlashController/error';
$route['warning-flash'] = 'MyFlashController/warning';
$route['info-flash'] = 'MyFlashController/info';

$route['flash_index'] = 'session controller';
$route['flash_message'] = 'session controller/flash_message';
