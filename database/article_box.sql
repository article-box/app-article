-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 06, 2021 at 10:56 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `news_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `about_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `status` int(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`about_id`, `title`, `text`, `status`, `time`) VALUES
(1, 'HISTORY', '<p>HISTORY &amp; HERITAGE&nbsp;On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire consectetur adipiscing On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire consectetur adipiscing On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire consectetur adipiscing On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire consectetur adipiscing</p>\r\n', 1, '2020-09-18 16:28:18'),
(2, 'TODAY', '<p>&nbsp;TODAY&nbsp;On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire consectetur adipiscing On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire consectetur adipiscing On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire consectetur adipiscing On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire consectetur adipiscing On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire consectetur adipiscing On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire consectetur adipiscing On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire consectetur adipiscing</p>\r\n', 1, '2020-08-24 10:34:41'),
(3, 'MISSION STATEMENT', '<p>MISSION STATEMENT&nbsp;On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire consectetur adipiscing</p>\r\n\r\n<ul>\r\n	<li>To be a world-class formwork enterprise committed to delivering quality solutions, customer satisfaction, on-time projects, continuous innovation and enhancement of stakeholders&rsquo; value.</li>\r\n	<li>To encourage innovation, integrity, teamwork and a safe working environment.</li>\r\n	<li>To build a strong future ensuring increased returns to shareholders and enhancing stakeholder value.</li>\r\n	<li>To be a responsible corporate citizen committed to the social and environmental causes.</li>\r\n</ul>\r\n', 1, '2020-07-05 05:54:02');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `banner_id` int(11) NOT NULL,
  `banner_title` varchar(255) NOT NULL,
  `banner_text` varchar(255) DEFAULT NULL,
  `banner_url` varchar(255) DEFAULT NULL,
  `banner_img` varchar(255) NOT NULL,
  `status` int(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`banner_id`, `banner_title`, `banner_text`, `banner_url`, `banner_img`, `status`, `time`) VALUES
(1, 'anuj', 'https://wa.me/916205472057', '', '6540_1624632978_962.jpg', 1, '2021-06-25 15:01:46'),
(2, 'demo', 'example.com', '', '5119_1593002689_2.jpg', 1, '2021-06-25 15:01:18'),
(3, 'Server Installation', 'example.com', NULL, '2129_1598327282_telegram_video__1597645548023.jpg', 1, '2020-08-25 03:48:02'),
(4, 'Web Development ', 'example.com', NULL, '8128_1598326522_2210_1587420081_IoT-in-web-design-and-development-process.jpg', 1, '2020-08-25 03:38:40');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `blog_id` int(255) NOT NULL,
  `blog_title` varchar(255) NOT NULL,
  `url_slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `header_text` varchar(10000) CHARACTER SET utf8mb4 NOT NULL,
  `blog_text` text CHARACTER SET utf8 NOT NULL,
  `blog_img` varchar(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `category_id` varchar(255) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `status` int(255) DEFAULT NULL,
  `trending` int(255) DEFAULT NULL,
  `pin` int(255) DEFAULT NULL,
  `star` int(255) DEFAULT NULL,
  `likes` int(255) DEFAULT '0',
  `sub_category_id` varchar(255) DEFAULT NULL,
  `meta_title` text,
  `meta_keyword` text,
  `meta_description` text,
  `date_time` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blog_id`, `blog_title`, `url_slug`, `header_text`, `blog_text`, `blog_img`, `user_id`, `category_id`, `session_id`, `status`, `trending`, `pin`, `star`, `likes`, `sub_category_id`, `meta_title`, `meta_keyword`, `meta_description`, `date_time`, `time`) VALUES
(9, 'When and How to Clean Your Phone During the COVID-19 Outbreak', 'When-and-How-to-Clean-Your-Phone-During-the-COVID-19-Outbreak', 'Your cellphone can be a carrier of microbial life forms, including bacteria, viruses, and fungi. Getty Images', '<p>Your cellphone can be a carrier of microbial life forms, including bacteria, viruses, and fungi. Getty Images</p>\r\n\r\n<ul>\r\n	<li><strong>During the COVID-19 pandemic, it&rsquo;s important to clean commonly touched surfaces.</strong></li>\r\n	<li><strong>One type of surface that we touch frequently is our phone touch screen</strong>.</li>\r\n	<li><strong>Phone touch screens can usually be cleaned using disinfecting wipes, but check with the maker of your phone first.</strong></li>\r\n	<li><strong>Frequency of cleaning will vary depending on your habits and the likelihood of exposure.</strong></li>\r\n</ul>\r\n\r\n<p><em>All data and statistics are based on publicly available data at the time of publication. Some information may be out of date. Visit our&nbsp;coronavirus hub&nbsp;and follow our&nbsp;live updates page&nbsp;for the most recent information on the COVID-19 outbreak.</em></p>\r\n\r\n<p>With the recent outbreak of COVID-19, there has been much attention placed on&nbsp;preventive actionsTrusted Source, such as handwashing, staying home when we are sick, and cleaning commonly touched surfaces.</p>\r\n\r\n<p>Regarding the latter, however, there is one type of surface that we may be neglecting: the touch screens and carrying cases for our phones.</p>\r\n\r\n<p>&ldquo;Touch screens on our devices are an often overlooked source of microbes that can be brought into our personal space,&rdquo; noted&nbsp;Dr. David Westenberg, associate professor of biological sciences at Missouri University of Science and Technology.</p>\r\n\r\n<p>In fact,&nbsp;numerous studiesTrusted Source&nbsp;have found that our cellphones can be carriers of microbial life forms, including bacteria, viruses, and fungi.</p>\r\n\r\n<p>While many of these are harmless, according to Westenberg, there are also disease-causing organisms like the SARS-CoV-2 virus that can survive on surfaces long enough to be transmitted to you or another person.</p>\r\n', '3474_1588370427_Cleaning_iPhone.jpg', '2', '1', '', 1, 1, NULL, 1, 220, '2', NULL, NULL, NULL, '', '2020-08-04 11:45:05'),
(10, '7 Emotion Focused Coping Techniques for Uncertain Times', '7-Emotion-Focused-Coping-Techniques-for-Uncertain-Times', 'When a challenge comes up for you, you probably have a handful of go-to strategies to help you deal with it. Even if your approach varies slightly from problem to problem, you probably manage most difficulties in similar ways.', '<div>When a challenge comes up for you, you probably have a handful of go-to strategies to help you deal with it. Even if your approach varies slightly from problem to problem, you probably manage most difficulties in similar ways.</div>\r\n\r\n<div>You might, for example, be a problem solver. When navigating a challenge or stressful event, you go straight to the source and work at it until you&rsquo;ve either fixed what&rsquo;s wrong or brought your stress down to a more manageable level.</div>\r\n\r\n<div>What if taking immediate action isn&rsquo;t your strong point? Maybe you try to hack your emotions by considering the situation from a different perspective or leaning on loved ones for support.</div>\r\n\r\n<div>These two approaches represent two distinct coping strategies:</div>\r\n\r\n<ul>\r\n	<li>\r\n	<div><strong>Problem-focused coping</strong>&nbsp;involves handling&nbsp;stress&nbsp;by facing it head-on and taking action to resolve the underlying cause.</div>\r\n	</li>\r\n	<li>\r\n	<div><strong>Emotion-focused coping</strong>&nbsp;involves regulating your feelings and emotional response to the problem instead of addressing the problem.</div>\r\n	</li>\r\n</ul>\r\n\r\n<div>Both strategies can have benefits, but emotion-focused coping may be particularly useful in certain situations.</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>First, a look at what this coping style is good for</div>\r\n\r\n<div>Emotion-focused coping skills help you process and work through unwanted or painful&nbsp;emotions&nbsp;and reactions. In other words, this approach helps you manage your emotions rather than outside circumstances.</div>\r\n\r\n<div>This approach won&rsquo;t help you solve a problem directly, but it&rsquo;s a great tool to have for dealing with stressful situations you can&rsquo;t change or control.</div>\r\n\r\n<blockquote>\r\n<div>When you can manage your emotional response to a given situation more effectively, you may feel somewhat better about what&rsquo;s happening &mdash; or at least more equipped to handle it.</div>\r\n</blockquote>\r\n\r\n<div>Research from 2015Trusted Source&nbsp;suggests people who tend to use emotion-focused coping strategies may be more resilient to stress and enjoy greater overall wellness.</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>1. Meditation</div>\r\n\r\n<div>Meditation helps you learn to acknowledge and sit with all of your thoughts and experiences, even the difficult ones.</div>\r\n\r\n<div>The key goal of meditation? Mindfulness: to recognize thoughts as they come up, accept them, and let them go without stewing over them or judging yourself for having them.</div>\r\n\r\n<div>You can practice mindfulness anytime, anywhere, and it won&rsquo;t cost you anything. It may feel a little awkward, even unhelpful, at first, and it can take some time before it feels natural. If you stick with it, you&rsquo;ll generally begin seeing some benefits before long.</div>\r\n\r\n<div>If you&rsquo;re new to meditation, get started by learning more about&nbsp;different types&nbsp;or trying&nbsp;this easy body scan exercise.</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>2. Journaling</div>\r\n\r\n<div>Journaling is a great way to sort through and come to terms with challenging emotions.</div>\r\n\r\n<div>When something goes wrong, you might experience a lot of complicated, conflicting feelings. They might feel jumbled up inside you, making the thought of sorting them out exhausting. Or, maybe you&rsquo;re not even sure how to name what you&rsquo;re feeling with words.</div>\r\n\r\n<div>Exhaustion and confusion are valid feelings and can be a good starting point for putting pen to paper.</div>\r\n\r\n<div>Sometimes,&nbsp;writing down your feelings&nbsp;&mdash; no matter how messy or complex they are &mdash; is the first step in working through them. You might eventually find that journaling offers a type of emotional catharsis, as you purge them from your mind and into your journal.</div>\r\n\r\n<div>To get the most out of journaling, try:</div>\r\n\r\n<ul>\r\n	<li>\r\n	<div>writing every day, even if you only have 5 minutes</div>\r\n	</li>\r\n	<li>\r\n	<div>writing whatever comes to mind &mdash; don&rsquo;t worry about editing or censoring yourself</div>\r\n	</li>\r\n	<li>\r\n	<div>keeping track of any mood or emotional changes you experience and any factors that might be contributing to the pattern, whether that&rsquo;s your exercise routine, certain foods, or particular relationships</div>\r\n	</li>\r\n</ul>\r\n\r\n<div>3. Positive thinking</div>\r\n\r\n<div>Optimism&nbsp;won&rsquo;t solve problems alone, but it can certainly boost your emotional wellness.</div>\r\n\r\n<blockquote>\r\n<div>It&rsquo;s important to understand that optimistic or positive thinking does&nbsp;<em>not</em>&nbsp;involve ignoring your problems. It&rsquo;s about giving challenges a positive spin and finding pockets of joy to help you get through them.</div>\r\n</blockquote>\r\n\r\n<div>To add more positive thinking to your life, try:</div>\r\n\r\n<ul>\r\n	<li>\r\n	<div>building yourself up with&nbsp;positive self-talk&nbsp;instead of&nbsp;talking down to yourself</div>\r\n	</li>\r\n	<li>\r\n	<div>recognizing your successes instead of focusing on &ldquo;failures&rdquo;</div>\r\n	</li>\r\n	<li>\r\n	<div>laughing off mistakes</div>\r\n	</li>\r\n	<li>\r\n	<div>reminding yourself you can always try again</div>\r\n	</li>\r\n</ul>\r\n\r\n<div>All these things are easier said than done, but with a bit of practice, they&rsquo;ll start to feel more natural.</div>\r\n\r\n<div>4. Forgiveness</div>\r\n\r\n<div>It&rsquo;s easy to focus on feelings of injustice or unfairness when someone wrongs you or does something unkind.</div>\r\n\r\n<div>Most of the time, though, you can&rsquo;t do anything to change the hurt you&rsquo;ve sustained. In other words, the damage is done, and there&rsquo;s nothing to do but&nbsp;<a href=\"https://www.healthline.com/health/how-to-let-go#1\">let go</a>&nbsp;and move forward.</div>\r\n\r\n<div>Forgiveness can help you let go of hurt and begin healing from it. Of course, forgiveness doesn&rsquo;t always happen easily. It can take some time to come to terms with your pain before you feel able to forgive.</div>\r\n\r\n<div>Practicing forgiveness can benefit your emotional wellness in a number of ways. You might notice:</div>\r\n\r\n<ul>\r\n	<li>\r\n	<div>reduced stress and&nbsp;anger</div>\r\n	</li>\r\n	<li>\r\n	<div>increased&nbsp;compassion</div>\r\n	</li>\r\n	<li>\r\n	<div>greater empathy</div>\r\n	</li>\r\n	<li>\r\n	<div>stronger&nbsp;interpersonal relationships</div>\r\n	</li>\r\n</ul>\r\n\r\n<div>Looking for tips on practicing forgiveness? Check out our guide to letting go of the past.</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>5. Reframing</div>\r\n\r\n<div>When you reframe a situation, you look at it from another perspective. This can help you consider the bigger picture instead of getting stuck on little details, as difficult or unpleasant as those details sometimes are.</div>\r\n\r\n<div>Say, for example, your relationship has been struggling over the past few months, primarily because you and your partner haven&rsquo;t had much time to do things together or communicate about problems.</div>\r\n\r\n<div>Suddenly, you lose your job and find that you&rsquo;re now spending&nbsp;<em>plenty&nbsp;</em>of time at home.</div>\r\n\r\n<div>Not working isn&rsquo;t ideal, of course, but for the moment there&rsquo;s nothing you can do to change that situation. Instead of letting frustration and boredom build up, you can look at the bright side of the situation: You now have plenty of time to reconnect with your partner and&nbsp;strengthen your relationship.</div>\r\n', '1486_1588370726_home-woman-meditation-eyes-closed.jpg', '1', '1', '', 1, 1, 1, 1, 8, '3', NULL, NULL, NULL, '', '2020-08-10 16:42:26'),
(11, 'The No BS Guide to Organizing Your Feelings', 'The-No-BS-Guide-to-Organizing-Your-Feelings', 'How our emotions affect our behavior\r\nIf we don’t take stock of our emotions or why we’re feeling them, they’ll likely continue to stuff our minds  even when they’re not necessary.', '<div>Take stock of your emotions to thrive</div>\r\n\r\n<div>Rarely do our feelings hang neatly on fancy, perfectly spaced hangers. Instead &mdash; like our closets &mdash; we often hold a jumble of both new and outdated emotions.</div>\r\n\r\n<div>But you can organize your feelings and deal with or discard ones that aren&rsquo;t serving you, a la Marie Kondo. Sift through your emotions regularly to slay anxiety, stress, or frustration.</div>\r\n\r\n<div>Here&rsquo;s how to optimize your feelings to start winning at life.</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>How our emotions affect our behavior</div>\r\n\r\n<div>If we don&rsquo;t take stock of our emotions or why we&rsquo;re feeling them, they&rsquo;ll likely continue to stuff our minds &mdash; even when they&rsquo;re not necessary. That could have negative consequences for our success, health, and relationships.</div>\r\n\r\n<div>If you&rsquo;ve ever ran a red light while thinking about that fight you had with your significant other, you&rsquo;re not alone.&nbsp;Studies&nbsp;show that our emotions can affect our logic and our ability to perform tasks.</div>\r\n\r\n<div>When we&rsquo;re anxious or stressed, we&rsquo;re also more likely to&nbsp;self-medicate&nbsp;with alcohol, drugs, or junk food. These all can make us feel like crap when the numbing effects wear off.</div>\r\n\r\n<div>Plus,&nbsp;studies&nbsp;show that the more emotionally intelligent we are, the better our romantic relationships will be &mdash; and that can likely be said for friendships and connections with family, too. And we know how important that&nbsp;inner circle&nbsp;or tribe is to our well-being.</div>\r\n\r\n<div>Organizing your feelings involves a light version of&nbsp;cognitive behavioral therapy (CBT)&nbsp;that you can do on your own or with the help of a therapist. It can really help you grow as a person.</div>\r\n\r\n<div>&ldquo;Skipping over the nuts and bolts of CBT, the basic premise is that our thoughts influence our feelings, which then influence our actions,&rdquo; says&nbsp;Carolyn Robistow, a licensed professional counselor and founder of The Joy Effect counseling in The Woodlands, Texas.</div>\r\n\r\n<div>&ldquo;An unhealthy thought, or being stuck in an unhealthy thought pattern, can lead to actions that just make the problem worse or keep us stuck in the same types of situations, basically spinning our wheels.&rdquo;</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>Step one: Figure out what you&rsquo;re feeling</div>\r\n\r\n<div>The first step to organizing your feelings is to list your problems or worries.</div>\r\n\r\n<div>That might sound like a negative thing to do, but sometimes just writing them down will ease anxiety, says a University of Chicago&nbsp;study.</div>\r\n\r\n<div>&ldquo;Identifying the underlying thought or belief, evaluating it for its helpfulness and truth, and then changing it if it&rsquo;s not serving us well can be incredibly powerful,&rdquo; Robistow explains.</div>\r\n\r\n<div>How to identify the core emotion that&rsquo;s upsetting you</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>List your concerns or problems and assign the emotions, thoughts, and beliefs attached. If you&rsquo;re unsure what those thoughts are, Robistow recommends a &ldquo;So what does that mean?&rdquo; exercise.</div>\r\n\r\n<div><strong>The &ldquo;So what&rdquo; exercise example:</strong></div>\r\n\r\n<div><em>Problem:</em>&nbsp;Everyone expects me to rearrange my schedule to fit theirs.</div>\r\n\r\n<div><em>Feelings or emotions:</em>&nbsp;anger, resentment, hurt</div>\r\n\r\n<table id=\"t1530110376129\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<div>Ask:</div>\r\n			</td>\r\n			<td>\r\n			<div>Answer (to find your underlying belief):</div>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<div>So what?</div>\r\n			</td>\r\n			<td>\r\n			<div>So they think what they have going on is more important than what I have going on.</div>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<div>So what?</div>\r\n			</td>\r\n			<td>\r\n			<div>So that&rsquo;s selfish of them to not even think about how this inconveniences me.</div>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<div>So what?</div>\r\n			</td>\r\n			<td>\r\n			<div>So if I want to see them or be part of the event, I just have to suck it up.</div>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<div>So what does that mean?</div>\r\n			</td>\r\n			<td>\r\n			<div>It means that if I don&rsquo;t make the effort, I&rsquo;ll never get to spend time with them&hellip;</div>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<div><strong>Possible conclusion:&nbsp;</strong>&hellip;<em>which means that I&rsquo;ll be all alone, and they&rsquo;ll eventually forget about me. I&rsquo;m afraid I&rsquo;m forgettable, or they don&rsquo;t care about me.</em></div>\r\n\r\n<div>The meaning we uncover in the exercise might feel brutal. But that&rsquo;s when the true work of CBT, or organizing your feelings, comes into play.</div>\r\n\r\n<div>&ldquo;Look for exceptions,&rdquo; Robistow says. &ldquo;Ask yourself, &lsquo;Is that really true? Or can I find evidence that contradicts that belief?&rsquo;&rdquo;</div>\r\n\r\n<div>In the example provided, the person might think of times when others have gone out of their way to see them or expressed having a blast after hanging out. They&rsquo;ll know that the conclusion they arrived at is false.</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>Step two: Find out if this is a pattern</div>\r\n\r\n<div>Sometimes you have to decide if a feeling is necessary or if it&rsquo;s just operating a gaming controller in your brain.</div>\r\n\r\n<div>Remember, our emotions drive our behavior. We should check in with our emotions often because they can quickly become exaggerated. This eventually creates barriers to the goals we want to achieve and the people we want to be close to.</div>\r\n\r\n<div><strong>If you&rsquo;re feeling negative, you could be experiencing a cognitive distortion.</strong>&nbsp;In short, that&rsquo;s your brain telling you a lie based on old thought patterns.</div>\r\n\r\n<div><strong>IS YOUR MIND LYING TO YOU?</strong>If you&rsquo;re nervous about the date you&rsquo;re on, for example, you might drink too much. But maybe you&rsquo;re basing your nerves off a previous bad date. This could cause a chain reaction of anxiety-filled dates, leading you to think that you need to be tipsy to be a good date (or that nobody is interested in you sober).</div>\r\n\r\n<div>If we&rsquo;re aware of the reasons behind our actions &mdash; and have a better understanding of our emotions &mdash; we can change our patterns. We can stop stress, worry, or frustration from taking over and making us behave in a way we&rsquo;d like to avoid.</div>\r\n', '7311_1588371038_8008-The_No_BS_Guide_to_Organizing_Your_Feelings.jpg', '1', '1', '', 1, 1, NULL, NULL, 2, '3', NULL, NULL, NULL, '', '2020-08-01 12:37:17'),
(12, 'Tips and Strategies for Starting a Running Routine', 'Tips-and-Strategies-for-Starting-a-Running-Routine', '  If you buy something through a link on this page, we may earn a small commission.?How this works.\r\nSo, you?ve caught the running bug and want to get into a regular running routine. But where do you start, and how do you pace yourself ? ...  ', '<div><span style=\"font-family:Arial,Helvetica,sans-serif\">If you buy something through a link on this page, we may earn a small commission.&nbsp;How this works.</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">So, you&rsquo;ve caught the running bug and want to get into a regular running routine. But where do you start, and how do you pace yourself?</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">Not to worry. We&rsquo;ve got the tips, strategies, and training plans you need to get started and stay motivated. And if you think you&rsquo;re ready to tackle a 5K, we have training advice for that, too.</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">What do you need to get started?</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">Running is simple, right? All you need is a pair of shoes and out the door you go. Well, not so fast.</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">Yes, you do need a good pair of&nbsp;running shoes, but other essential items can help make your training more successful and more enjoyable, too. And, let&rsquo;s face it, if you enjoy an activity, you&rsquo;re more likely to stick with it.</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">Invest in a good pair of running shoes</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">Hitting the pavement requires more than a pair of Vans or Converse. To reduce injuries and increase comfort, you need shoes that are designed specifically for running.</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">Ideally, you should get fitted for a pair of shoes at a running specialty store or with a podiatrist. If that&rsquo;s not possible, do some research and look for a pair of running shoes that fits your needs.</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">Opt for comfortable, sweat-wicking clothing</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">When it comes to clothes, comfort is key. Stick with lightweight pants, shorts, and shirts designed for fitness activities.</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">Look for sweat-wicking material and also consider the weather. Wearing layers in the winter helps keep you warm and allows you to remove clothing as necessary once you start warming up.</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">Cushioned running socks are also essential. Again, look for labels that say &ldquo;sweat-wicking,&rdquo; and consider wool running socks in the winter. And finally, don&rsquo;t forget a supportive&nbsp;sports bra.</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">Use technology to track your progress</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">Activity and fitness trackers like Fitbit, Garmin, and others can help keep you motivated and on track with your running goals. Many of these wearable gadgets can keep track of:</span></div>\r\n\r\n<ul>\r\n	<li>\r\n	<div><span style=\"font-family:Arial,Helvetica,sans-serif\">the distance you&rsquo;ve run</span></div>\r\n	</li>\r\n	<li>\r\n	<div><span style=\"font-family:Arial,Helvetica,sans-serif\">how many steps you&rsquo;ve run</span></div>\r\n	</li>\r\n	<li>\r\n	<div><span style=\"font-family:Arial,Helvetica,sans-serif\">how many calories you&rsquo;ve burned</span></div>\r\n	</li>\r\n	<li>\r\n	<div><span style=\"font-family:Arial,Helvetica,sans-serif\">your running pace</span></div>\r\n	</li>\r\n	<li>\r\n	<div><span style=\"font-family:Arial,Helvetica,sans-serif\">your heart rate</span></div>\r\n	</li>\r\n</ul>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">Shop for&nbsp;Fitbit,&nbsp;Garmin, and&nbsp;other fitness trackers&nbsp;online.</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">Create a running playlist</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">A great way to stay motivated is to listen to your favorite tunes while you&rsquo;re running. Create a playlist with the music that&rsquo;s most likely to keep you moving. You can also select your favorite tunes from music apps like Pandora, Spotify, or Apple Music.</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">That said, be sure to use your headphones wisely. You may want to use just one earbud, which allows you to stay alert and aware of what&rsquo;s going on around you.</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">A beginner&rsquo;s guide to running</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">The first priority when starting a running routine is to keep it simple. Don&rsquo;t worry about following a complicated program.</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">Your initial goal is to build confidence and&nbsp;stamina. To do this, Steve Stonehouse, NASM CPT, USATF run coach, director of education for&nbsp;STRIDE, suggests aiming for two to three runs each week at an easy to moderate pace.</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">&ldquo;You can always add techniques like speed work and tempo runs later, but right now it&rsquo;s just about getting your body used to the work,&rdquo; he said.</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">For example, a beginner&rsquo;s week-at-a-glance running routine might look like this:</span></div>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">Beginner&rsquo;s training routine</span></div>\r\n\r\n<ul>\r\n	<li>\r\n	<div><span style=\"font-family:Arial,Helvetica,sans-serif\"><strong>Monday:</strong>&nbsp;Run 2 miles at a moderate pace with a walk/run technique. For the first mile, run for 1 minute, walk for 1 minute. For the second mile, run for 90 seconds, walk for 1 minute.</span></div>\r\n	</li>\r\n	<li>\r\n	<div><span style=\"font-family:Arial,Helvetica,sans-serif\"><strong>Tuesday:</strong>&nbsp;Focus on full-body&nbsp;strength training.</span></div>\r\n	</li>\r\n	<li>\r\n	<div><span style=\"font-family:Arial,Helvetica,sans-serif\"><strong>Wednesday:</strong>&nbsp;Make this an active rest day. Take a walk, or do some&nbsp;light yoga&nbsp;and&nbsp;stretching.</span></div>\r\n	</li>\r\n	<li>\r\n	<div><span style=\"font-family:Arial,Helvetica,sans-serif\"><strong>Thursday:</strong>&nbsp;Run 2 miles at a moderate pace with a walk/run technique. Try to&nbsp;increase your pace&nbsp;slightly from your previous run. For the first mile, run for 1 minute, walk for 1 minute. For the second mile, run for 90 seconds, walk for 1 minute.</span></div>\r\n	</li>\r\n	<li>\r\n	<div><span style=\"font-family:Arial,Helvetica,sans-serif\"><strong>Friday:</strong>&nbsp;Focus on full-body strength training.</span></div>\r\n	</li>\r\n	<li>\r\n	<div><span style=\"font-family:Arial,Helvetica,sans-serif\"><strong>Saturday:</strong>&nbsp;Do 30 to 60 minutes of cardio such as&nbsp;walking,&nbsp;cycling, or&nbsp;swimming.</span></div>\r\n	</li>\r\n	<li>\r\n	<div><span style=\"font-family:Arial,Helvetica,sans-serif\"><strong>Sunday:&nbsp;</strong>Make this an active rest day. Take a walk, or do some light yoga and stretching.</span></div>\r\n	</li>\r\n</ul>\r\n\r\n<div><span style=\"font-family:Arial,Helvetica,sans-serif\">As you gain strength and stamina, you can gradually start increasing the distance you run, or you can add an extra day of running to your weekly routine. Decide what works best for you, but do it slowly.</span></div>\r\n', '4219_1588456251_man-athlete-running-stretching.jpg', '1', '1', '', 1, NULL, 1, 1, 1, '4', NULL, NULL, NULL, '', '2020-08-01 12:37:20'),
(14, 'Coronavirus live updates: 30 Border Security Force jawans tested Covid-19 positive', 'Coronavirus-live-updates-30-Border-Security-Force-jawans-tested-Covid-19-positive', 'The coronavirus case count in India came within touching distance of the half-lakh mark at 49,391. Over 1,600 people have died due to the virus across the country. The lockdown in India has been extended till May 17. Stay with TOI for live updates ...', '<div>The coronavirus case count in India came within touching distance of the half-lakh mark at 49,391. Over 1,600 people have died due to the virus across the country. The lockdown in India has been extended till May 17. Stay with TOI for live updates</div>\r\n', '4250_1588755214_sakldjasklda.jpg', '', '1', '', 1, 1, NULL, NULL, 1, '3', NULL, NULL, NULL, '', '2020-08-10 17:11:16'),
(15, 'Here\'s a guide to receiving benefits every day, even in the time of Coronavirus', 'heres-a-guide-to-receiving-benefits-every-day-even-in-the-time-of-coronavirus', 'Working from home, getting groceries delivered, losing track of which day and avoiding going out at all costs - that’s been the agenda for most of us during these last 40 days, thanks to the Coronavirus outbreak in the country.', '<p>Working from home, getting groceries delivered, losing track of which day and avoiding going out at all costs - that&rsquo;s been the agenda for most of us during these last 40 days, thanks to the&nbsp;Coronavirus&nbsp;outbreak in the country.<br />\r\nWhile the lockdown might have eased a little, we do need to take measured steps to ensure our safety and be vigilant with our new found freedom. Let&nbsp;Times Prime&nbsp;be your guide on how you can make most of the lockdown and the days beyond it while staying safe from Coronavirus.<br />\r\n<em><strong>CONTINUE TO CHERISH THE FAMILY HOUR</strong></em><br />\r\nWe&rsquo;ve all become accustomed to a work culture that involves us continuing to work from our homes, for the foreseeable future. We&rsquo;ve all worked hard to create a balanced work-home environment, but now is really the chance for us to cherish more of that family time.<br />\r\nFrom doing chores together to playing family games, from streaming shows online, to making our favorite playlists, now is probably the time to do it all. What&rsquo;s better than to sit with your family watching a movie on your Zee5&nbsp;subscription, which you received complimentary on Times Prime? Or what can be more fun than breaking into an impromptu dance routine with your siblings, while enjoying uninterrupted streaming from&nbsp;Gaana&nbsp;Plus on your Times Prime membership?<br />\r\n&nbsp;</p>\r\n', '6066_1588755526_75534610.jpg', '', '1', '', 1, NULL, NULL, NULL, 1, '12', NULL, NULL, NULL, '', '2020-07-24 05:03:39'),
(28, 'How to remove and discard a surgical mask', 'how-to-remove-and-discard-a-surgical-mask', 'It\'s important to remove the face mask correctly to ensure you don?t transfer any germs to your hands or face. You also want to make sure you discard the mask safely.', '<h3>Steps to taking off a face mask</h3>\r\n\r\n<ol>\r\n	<li>Before you take off the mask, wash your hands well or use hand sanitizer.</li>\r\n	<li>Avoid touching the mask itself, as it could be contaminated. Hold it by the loops, ties, or bands only.</li>\r\n	<li>Carefully remove the mask from your face once you:\r\n	<ul>\r\n		<li>unhook both ear loops, or</li>\r\n		<li>untie the bottom bow first, followed by the top one, or</li>\r\n		<li>remove the bottom band first by lifting it over your head, then do the same with the top band</li>\r\n	</ul>\r\n	</li>\r\n	<li>Holding the mask loops, ties, or bands, discard the mask by placing it in a covered trash bin.</li>\r\n	<li>After removing the mask, wash your hands thoroughly or use hand sanitizer.</li>\r\n</ol>\r\n', '2367_1589568175_mom-woman-kid-face-mask-hospital-1296x728-header.jpg', '', '1', '', 1, 1, NULL, NULL, NULL, '12', NULL, NULL, NULL, '', '2020-07-27 04:44:19'),
(94, '7 Emotion Focused Coping Techniques for Uncertain Times', '7-emotion-focused-coping-techniques-for-uncertain-times-1', 'aklnm,s', '', '1387_1597052324_2176_1588972031_deepak_1507896942.jpg', NULL, '2', '', 1, 1, 1, NULL, 2, '2', '', '', '', '', '2020-08-11 09:38:21'),
(137, '7 Emotion Focused Coping Techniques for Uncertain Times', '7-emotion-focused-coping-techniques-for-uncertain-times-2', '7 Emotion Focused Coping Techniques for Uncertain Times 7 Emotion Focused Coping Techniques for Uncertain Times 7 Emotion Focused Coping Techniques for Uncertain Times', '<p>7 Emotion Focused Coping Techniques for Uncertain Times&nbsp;7 Emotion Focused Coping Techniques for Uncertain Times&nbsp;7 Emotion Focused Coping Techniques for Uncertain Times&nbsp;7 Emotion Focused Coping Techniques for Uncertain Times</p>\r\n', '4871_1597052296_9058_1588336695_love.jpg', NULL, '2', '', 1, NULL, NULL, 1, 1, '2', NULL, NULL, NULL, '', '2020-08-10 09:38:16'),
(138, 'The Group Plugins lets people join your Facebook group from a link in an email message or a web page.', 'the-group-plugins-lets-people-join-your-facebook-group-from-a-link-in-an-email-message-or-a-web-page', 'The Group Plugin for Email allows email recipients to join your group from an email message. You generate the code to add to the message to the email service provider which then sends a message with a button to your recipients.\r\n', '<p>The Group Plugin for Email allows email recipients to join your group from an email message. You generate the code to add to the message to the email service provider which then sends a message with a button to your recipients.</p>\r\n\r\n<p>The Group Plugin for Email allows email recipients to join your group from an email message. You generate the code to add to the message to the email service provider which then sends a message with a button to your recipients.</p>\r\n\r\n<ul>\r\n</ul>\r\n', '3679_1597479069_facebook.png', NULL, '2', '', 1, NULL, 1, 1, 1, '2', NULL, NULL, NULL, 'Submit', '2020-08-15 08:11:09'),
(139, 'Showing number of comments in blog (while listing all posts)', 'showing-number-of-comments-in-blog-while-listing-all-posts-showing-number-of-comments-in-blog-while-listing-all-posts', 'while working on my little CI blog project I got stuck at the point where I want my post metadata (post author, post date, post time and number of comments) to show throughout the foreach loop in the view file. I managed to retrieve the post author, the p', '<p>Dear community..</p>\r\n\r\n<p>while working on my little CI blog project I got stuck at the point where I want my post metadata (post author, post date, post time and number of comments) to show throughout the foreach loop in the view file. I managed to retrieve the post author, the post date and post time from the posts table in my database. However, I can&rsquo;t think of a valid structure to retrieve the number of comments (linked via a post_id) from the comments table in my database, and meanwhile keeping a clean MVC approach.</p>\r\n\r\n<p>So my blog controller is structured like this:</p>\r\n\r\n<pre>\r\n<code>function blog() {\r\n$data[&#39;posts&#39;] = $this-&gt;Posts_model-&gt;get_all_posts();\r\n$this-&gt;load-&gt;view(&#39;blog_view&#39;, $data);\r\n}</code></pre>\r\n\r\n<p>Model:</p>\r\n\r\n<pre>\r\n<code>function get_all_posts() {\r\n$q = $this-&gt;db-&gt;get(&#39;posts&#39;);\r\nreturn $q-&gt;result();\r\n}</code></pre>\r\n\r\n<p>And my view file looks like this:</p>\r\n\r\n<pre>\r\n<code><!--?php if (count($posts)): ?-->\r\n<!--?php foreach ($posts as $post): ?-->\r\n</code></pre>\r\n\r\n<div class=\"metadata\"><code><!--?php echo $post[\'post_author\']; ?--> | <!--?php echo $post[\'post_date\']; ?--> at <!--?php echo $post[\'post_time\']; ?--> | NUMBER OF COMMENTS HERE</code></div>\r\n\r\n<p><code><!--?php endforeach; ?--> <!--?php endif; ?--></code></p>\r\n\r\n<p>Now I want to show <!--?php echo $...[‘number_of_comments’]; ?--> in the metadata</p>\r\n\r\n<div>too, keeping a clean MVC structure. I know how to count comments in a model. All I gotta do is something like this:\r\n<p>&nbsp;</p>\r\n\r\n<pre>\r\n<code>function count_all_comments($post_id) {\r\n$this-&gt;db-&gt;where(&#39;post_id&#39;, $post_id);\r\n$q = $this-&gt;db-&gt;count_results(&#39;comments&#39;);\r\nreturn $q-&gt;result();\r\n}</code></pre>\r\n\r\n<p>But where do I get the $post_id from while running an foreach loop in the view? I know I can directly call this model action from the view file, but that is not respecting the MVC principles. Also.. how do I incorporate this into my controller?</p>\r\n\r\n<p>All help is welcome, if you guys suggest a totally different structure I am also willing to learn from that. And the next step is doing a structure like:</p>\r\n\r\n<p>if 0 -&gt; 0 comments<br />\r\nif 1 -&gt; 1 comment<br />\r\nif &gt;1 -&gt; x comments</p>\r\n\r\n<p>Thank you for reading!</p>\r\n</div>\r\n', '7234_1597481486_4631_1595345175_509938.jpg', NULL, '5', '', 1, NULL, NULL, NULL, 1, '5', NULL, NULL, NULL, '15-August-2020, 2:19 pm', '2020-08-15 08:51:26'),
(140, 'Apple, Google remove Fortnite from app stores: 5 key points to know ', 'apple-google-remove-fortnite-from-app-stores-5-key-points-to-know', 'Apple and Google have removed gaming app Fortnite by Epic Games from App Store and Google Play Store for allegedly violating their in-app payments policies.  n an unprecedented move, Apple and Google have removed gaming app Fortnite by Epic Games from App', '<p>n an unprecedented move, Apple and Google have removed gaming app Fortnite by Epic Games from App Store and Google Play Store for allegedly violating their&nbsp;in-app payments policies.&nbsp;The removal led to a brawl between the tech giants and the developer of Fortnite, Epic Games, which then filed an anti-trust lawsuit against Apple and Google calling their payment policies &ldquo;anti-competitive&rdquo;.</p>\r\n\r\n<p>Fortnite has over 350 million players across the globe as per a Sensor Tower report. Removing the app from both the Play Store and App Store would result in huge losses for the game developer. So here is what happened that compelled the tech giants to get rid of Fortnite and what followed soon after, five key points to know</p>\r\n\r\n<p>&mdash; It all started when Epic Games rolled out a new update for Fortnite that allowed users to pay directly to Epic for the in-app purchases related to Fortnite. However, this goes against the payment policies of both Apple and Google who demand a 30 share from the revenues generated through in-app purchases.</p>\r\n\r\n<p>&mdash; Apple was quick to take action against Epic Games and removed its popular game Fortnite from the App Store over violating the App Store guidelines. Google soon followed the footsteps of Apple and removed Fortnite from Play Store for violating Google&rsquo;s billing policies.</p>\r\n\r\n<p>&mdash;Apple in its statement said that despite being on the App Store for many years, the direct payment plan announced by Epic Games violated the policies of the App Store. &ldquo;Epic enabled a feature in its app which was not reviewed or approved by Apple, and they did so with the express intent of violating the App Store guidelines regarding in-app payments that apply to every developer who sells digital goods or services.&rdquo;</p>\r\n\r\n<p>&mdash;Google too stressed upon violation of its policies but also ensured that it is welcome to the idea of having a discussion with Epic to bring the gaming app back. The search giant also confirmed that the app has only been banned on Play Store but the users will be able to download it from other Android platforms including the Epic Games app and Samsung Galaxy Store for Samsung users.</p>\r\n\r\n<p>&ldquo;The open Android ecosystem lets developers distribute apps through multiple app stores. For game developers who choose to use the Play Store, we have consistent policies that are fair to developers and keep the store safe for users. While Fortnite remains available on Android, we can no longer make it available on Play because it violates our policies. However, we welcome the opportunity to continue our discussions with Epic and bring Fortnite back to Google Play,&rdquo; the statement issued by Google read.</p>\r\n\r\n<p>&mdash; In its defense, Epic Games filed a lawsuit against the tech giants Apple and Google calling their practices anti-competitive. It said in its lawsuit that it doesn&rsquo;t want monetary help from Google but an order enjoining Google from continuing to impose its anti-competitive conduct on the Android ecosystem. &ldquo;Epic does not seek monetary compensation from this Court for the injuries it has suffered. Epic likewise does not seek a side deal or favorable treatment from Google for itself. Instead, Epic seeks injunctive relief that would deliver Google&rsquo;s broken promise: an open, competitive Android ecosystem for all users and industry participants. Such injunctive relief is sorely needed,&rdquo; Epic&rsquo;s lawsuit against Google read.</p>\r\n', '3710_1597641133_Screenshot_2020-08-14_at_10.04_0.jpg', NULL, '2', '', 1, NULL, NULL, NULL, 2, '2', NULL, NULL, NULL, '17-August-2020, 9:57 am', '2020-08-17 05:12:13'),
(141, 'Fortnite kicked out of Google Play Store, Apple App Store after Epic fights 2 companies', 'fortnite-kicked-out-of-google-play-store-apple-app-store-after-epic-fights-2-companies', 'Apple and Google on Thursday removed Epic Games’ Fortnite from the App Store and Google Play Store for launching a direct payment plan that bypasses Google and Apple.', '<h2>HIGHLIGHTS</h2>\r\n\r\n<ul>\r\n	<li>Apple and Google on Thursday removed Epic Games&rsquo; Fortnite from the App Store and Google Play Store.</li>\r\n	<li>Fortnite has been removed from app stores for launching a direct payment plan that bypasses Google and Apple.</li>\r\n	<li>Epic Games recently launched a direct payment plan that gives the option to users to pay directly to Epic for Fornite&rsquo;s in-app purchases</li>\r\n</ul>\r\n\r\n<p>Apple and Google on Thursday removed Epic Games&rsquo; Fortnite from the App Store and Google Play Store for launching a direct payment plan that bypasses Google and Apple. For the unversed, the Epic Games recently launched a direct payment plan that gives the option to users to pay directly to Epic for Fornite&rsquo;s in-app purchases whereas Apple and Google both take a 30 percent share from the in-app revenues in games.</p>\r\n\r\n<p>Epic Games on Thursday rolled out a new update for Fortnite that would let users say directly to the developer for Fortnite&rsquo;s in-app purchases. However, this violated Apple&rsquo;s longstanding policy that apps must make billing possible only through Apple and pay the company 30 percent of the total revenue earned. Apple was the first to kick the game out of the App Store followed by Google.</p>\r\n\r\n<p>Reacting to the same, Apple said in a statement, &ldquo;Epic enabled a feature in its app which was not reviewed or approved by Apple, and they did so with the express intent of violating the App Store guidelines regarding in-app payments that apply to every developer who sells digital goods or services.&rdquo;</p>\r\n\r\n<p>Whereas Google said that although Fortnite has violated its policies, it has not been banned from Android and can be downloaded from various other Android platforms.</p>\r\n\r\n<p>&ldquo;The open Android ecosystem lets developers distribute apps through multiple app stores. For game developers who choose to use the Play Store, we have consistent policies that are fair to developers and keep the store safe for users. While Fortnite remains available on Android, we can no longer make it available on Play because it violates our policies. However, we welcome the opportunity to continue our discussions with Epic and bring Fortnite back to Google Play,&rdquo; Google said in a statement.</p>\r\n\r\n<p>The game would no longer be available on Google Play Store until the companies reach a settlement but it can be downloaded from other platforms such as the Epic Games app that is available on the Play Store and Samsung Galaxy Store, exclusively for Samsung users.</p>\r\n\r\n<p>However, the move didn&rsquo;t go down well with Epic Games and it filed a suit against Apple and Google alleging that Apple&rsquo;s and Google&rsquo;s policies regarding in-app payments are anti-competitive. Epic in its lawsuit against Google said, &ldquo;Epic does not seek monetary compensation from this Court for the injuries it has suffered. Epic likewise does not seek a side deal or favorable treatment from Google for itself. Instead, Epic seeks injunctive relief that would deliver Google&rsquo;s broken promise: an open, competitive Android ecosystem for all users and industry participants. Such injunctive relief is sorely needed.&rdquo;</p>\r\n', '5618_1597641342_Screenshot_2020-08-14_at_9.20._0.jpg', NULL, '5', '', 1, NULL, NULL, 1, 1, '5', NULL, NULL, NULL, '17-August-2020, 10:44 am', '2020-08-17 05:15:42'),
(142, 'Telegram rolls out video calling feature for beta version v7.0.0', 'telegram-rolls-out-video-calling-feature-for-beta-version-v700', 'As per a report, Telegram is testing the video calling feature in beta version 7.0', '<h2>HIGHLIGHTS</h2>\r\n\r\n<ul>\r\n	<li>Telegram has rolled out video calling feature for beta version 7.0.0</li>\r\n	<li>The beta version can be accessed through App Centre.</li>\r\n	<li>The beta version does not have the option of group video calls.</li>\r\n</ul>\r\n\r\n<p>Telegram, which is a popular cloud-based messaging app, could jump into the fray with other video calling apps while there is still a huge demand for them. Telegram that goes big on privacy introduced its voice calling feature called Telegram Calls in the year 2017 with an end-to-end encryption. Now, the app is testing an option of video calling to add more features for its users.</p>\r\n\r\n<p>Telegram has rolled out a video calling feature for its v7.0.0 beta version. Downloading the beta version is a tad bit complicated as it is not accessible through PlayStore directly. Users wanting to try the version can download it from the Telegram&rsquo;s App Centre page. Telegram releases standalone beta APKs with a different signature. The beta version is then installed alongside Telegram or Telegram X apps that users may already have and can be used once the authentication process is complete, a report by Android Police stated.</p>\r\n\r\n<p>Users must note that the video calling function will work only if both devices have the beta version installed. The report noted that Telegram&rsquo;s video calling interface is similar to that of other apps. The user interface has options to flip between front and rear cameras, turn off the video, mute, and hang up. Tapping the window in the bottom right switches between the two participants.</p>\r\n\r\n<p>The beta version, however, does not support group video calls. A Picture-in-Picture feature was also visible in the beta version. This could be accessed by tapping the back arrow on the top left while in a call. Users would get a permission pop-up which they had to confirm.</p>\r\n\r\n<p>Similar to WhatsApp and other video call apps, a caller&rsquo;s small window could instantly be swapped with the larger window with a single tap. Alternatively, users can press the back button or home button which automatically triggered the PiP mode.</p>\r\n\r\n<p>As per the report, the option to group video call is not available in the beta version. Also, the sound was only being transported through the earpiece with no accessibility to the loudspeaker. This could be fixed by turning off video then turning it back on, on both the devices that forced the loudspeaker on.</p>\r\n\r\n<p>The testing is still in the works and may take a few weeks before Telegram officially releases it. However, it indicates that Telegram might soon release the feature and join the league of its competitors where video calling is concerned.</p>\r\n', '6765_1597641531_Screenshot_2020-08-13_at_4.20._0.jpg', NULL, '5', '', 1, NULL, NULL, NULL, 1, '5', NULL, NULL, NULL, '17-August-2020, 10:45 am', '2020-08-17 05:18:51'),
(143, 'ByteDance is in talks with Reliance to back TikTok in India, says report', 'bytedance-is-in-talks-with-reliance-to-back-tiktok-in-india-says-report', 'As per a report, ByteDance and Reliance are in the early stages of discussion.', '<h2>HIGHLIGHTS</h2>\r\n\r\n<ul>\r\n	<li>ByteDance and Reliance Industries Limited reportedly began talks last month.</li>\r\n	<li>TikTok&rsquo;s business in India is reportedly valued at $3 billion.</li>\r\n	<li>No official word has been given from the two companies.</li>\r\n</ul>\r\n\r\n<p>Chinese tech company ByteDance is in talks with Reliance Industries Limited to back TikTok&rsquo;s business in India, sources familiar with the matter told TechCrunch. The development comes weeks after the&nbsp;Indian government banned TikTok with 58 other Chinese apps. The report by TechCrunch stated that Reliance and ByteDance are still in the early stages of discussions and yet to finalize a deal.</p>\r\n\r\n<p>The two companies reportedly began talks last month. TikTok&rsquo;s business in India is reportedly valued at $3 billion.</p>\r\n\r\n<p>However, there is no official word from Reliance or ByteDance yet.&nbsp;ByteDance has around 2000 employees in India and has currently stopped hiring, a report by ET stated. The company has ensured its employees that there will be no layoffs and that it is in contact with the Indian government.</p>\r\n\r\n<p>Earlier, this week, there was a speculation that TikTok may return to India if Microsoft decides to buy it as a result of global expansion. Microsoft is in talks with ByteDance to purchase the TikTok business in the US, Australia, Canada, and New Zealand. However, things with the deal seemed unclear after US President Donald Trump signed an executive order effectively banning TikTok from the US after a 45-day deadline.</p>\r\n\r\n<p>The Indian government banned TikTok along with 58 other Chinese apps citing security concerns. It further banned 47 apps as a follow-up. India has also prepared a list of over 250 Chinese apps, including those linked to Alibaba and Tencent that it will examine for any user privacy or national security violations.</p>\r\n\r\n<p>Tech giants like Facebook and Google have partnered with Reliance on recent projects. At the first ever virtual Reliance AGM meeting, Reliance Industries chairman Mukesh Ambani announced that Google is investing Rs 33,737 crore in Jio Platforms.</p>\r\n\r\n<p>Google CEO Pichai said the investment was made to launch a customized version of its Android mobile operating system to power low-cost Android smartphones. The investment will get Google a 7.7 per cent stake in Jio. Facebook invested $5.7 billion in Jio to acquire close to 10 per cent stake. Mukesh Ambani launched JioMart in collaboration with WhatsApp.</p>\r\n', '8742_1597642238_Screenshot_2020-08-12_at_12.54_2_0.jpg', NULL, '5', '', 1, NULL, NULL, NULL, 1, '5', NULL, NULL, NULL, '17-August-2020, 10:57 am', '2020-08-17 05:30:38');
INSERT INTO `blog` (`blog_id`, `blog_title`, `url_slug`, `header_text`, `blog_text`, `blog_img`, `user_id`, `category_id`, `session_id`, `status`, `trending`, `pin`, `star`, `likes`, `sub_category_id`, `meta_title`, `meta_keyword`, `meta_description`, `date_time`, `time`) VALUES
(144, 'Telegram Rolls Out One-on-One Video Calls For Android and iOS Users', 'telegram-rolls-out-one-on-one-video-calls-for-android-and-ios-users', 'Telegram will also be launching group video calls in the coming months.\r\nTelegram now allows users to conduct one-on-one video calls on both Android and iOS platforms. The company announced the feature on its seventh anniversary on August 14, saying that t', '<p>Telegram now allows users to conduct one-on-one video calls on both Android and iOS platforms. The company announced the feature on its seventh anniversary on August 14, saying that this year had highlighted the need for face-to-face communication. Available in its alpha version, the video calls are protected with end-to-end encryption that users can confirm by checking if emojis shown on their partner&#39;s screen and their own are matching. Telegram also confirmed that it would be launching group video calls in the coming months.</p>\r\n\r\n<p>Telegram&nbsp;said in a&nbsp;blog post&nbsp;that the video calls would support picture-in-picture mode, that means users can scroll through chats on Telegram and multitask while on call. They can also switch off the video and switch to normal call whenever they wish.</p>\r\n\r\n<p>App users can start a video call by clicking on the call button on the contact&#39;s profile page. To confirm end-to-end encryption, users can check if the four emojis visible on-screen for them and their chat partner&#39;s match. If yes, the call is fully secured, according to Telegram.</p>\r\n\r\n<ul>\r\n	<li>Telegram Gets Profile Videos, 2GB File Sharing Among Other New Features</li>\r\n</ul>\r\n\r\n<p>Telegram also unveiled a batch of animated versions of commonly used emojis for all users to mark its seven-year anniversary. A larger animated version of the&nbsp;emoji&nbsp;will appear when used in a chat.</p>\r\n\r\n<p>In its blog post, Telegram said that although it had begun as a small app focused on secure messaging in 2013, it had now grown into a platform that has over 400 million users, and is now one of the top 10 most-downloaded apps. Introducing the video call feature puts Telegram a step closer to its competitors like&nbsp;WhatsApp&nbsp;and&nbsp;Viber, and the upcoming group video call feature will enhance the same.</p>\r\n\r\n<p>The company said that their Android and iOS apps have reproducible builds. Anyone could&nbsp;verify encryption&nbsp;and confirm that Telegram uses the same open source code that is published with each update. Besides group video calls that are expected to be rolled out soon, Telegram also said that there would be more features and improvements in future versions of the app.</p>\r\n\r\n<ul>\r\n	<li>Telegram to Pay Fine, Return Investor Money to Settle US SEC Charges</li>\r\n</ul>\r\n\r\n<p>Telegram had earlier indicated in April that it would be&nbsp;introducing&nbsp;secure video call features for the app soon, and had started&nbsp;rolling out&nbsp;the feature in beta mode a few days ago, as per reports.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '1370_1597647449_telegram_video__1597645548023.jpg', NULL, '3', '', 1, NULL, NULL, NULL, 2, '3', NULL, NULL, NULL, '17-August-2020, 11:00 am', '2020-08-17 06:57:29'),
(145, 'Aarogya Setu iOS Version Gets Open Sourced Over Two Months After Promise', 'aarogya-setu-ios-version-gets-open-sourced-over-two-months-after-promise', 'The government originally promised to open source the iOS version of Aarogya Setu within two weeks after its Android release in May.\r\n', '<p>Aarogya Setu iOS code has been released over two and a half months after NITI Aayog open sourced its Android version. The source code has been provided on OpenForge, the government&#39;s equivalent to GitHub. However, the Aarogya Setu app&#39;s Android version&#39;s source code is available through GitHub. The repository of the Aarogya Setu iOS version doesn&#39;t provide access to its server-side code. This is identical to its Android client of the contact tracing app where too the server-side code is not available to the public.</p>\r\n\r\n<p>The team behind the&nbsp;Aarogya Setu&nbsp;app&nbsp;announced&nbsp;the release of its&nbsp;iOS&nbsp;source code through a tweet posted on Thursday. The code is&nbsp;available for access&nbsp;through OpenForge. It includes the files enabling the app experience on iOS, though there are no details around the app&#39;s server-side code that helps developers understand the operations of the app.</p>\r\n\r\n<p>The government&nbsp;initially open sourced&nbsp;the&nbsp;Android&nbsp;version of the Aarogya Setu app in May. At that time,&nbsp;NITI Aayog&nbsp;promised to release the source code of the app&#39;s iOS version in the next two weeks. National Informatics Centre (NIC) during the launch also announced a bug bounty programme to incentivise researchers finding flaws in the app.</p>\r\n\r\n<p>This open sourcing of Aarogya Setu&#39;s iOS version comes just after threat intelligence firm ShadowMap allegedly found the log-in credentials used by its developers. The&nbsp;blog post&nbsp;by the company was pulled shortly after it came into the limelight on social media. However, its&nbsp;cached version&nbsp;was still available at the time of filing this story.</p>\r\n\r\n<p>&ldquo;We noticed that one of the Aarogya Setu servers had been recently updated and one of its developers had accidentally published their Git folder into the public webroot, along with the plain-text user name and password details for the official Aarogya Setu GitHub account,&rdquo; the blog post said.</p>\r\n\r\n<p>This wasn&#39;t the first security issue with the Aarogya Setu app as security experts&nbsp;raised privacy concerns&nbsp;and&nbsp;urged the government to open source&nbsp;its code following its debut in April.</p>\r\n\r\n<p>Aarogya Setu has over 15 crore users in India. The app is available for download on devices running on Android, iOS, and&nbsp;KaiOS. It was originally managed by the NITI Aayog, though the NIC team took its charge recently.</p>\r\n', '5605_1597647593_arogya_setue_reuters_full_1590040591641.jpg', NULL, '3', '', 1, NULL, NULL, NULL, 0, '3', NULL, NULL, NULL, '17-August-2020, 12:27 pm', '2020-08-17 06:59:53'),
(146, 'Dropbox Plus Subscribers Get New Password Manager, Vault, and Computer Backup Features', 'dropbox-plus-subscribers-get-new-password-manager-vault-and-computer-backup-features', 'Dropbox Passwords and automatic computer backup features have rolled out for Dropbox Professional subscribers as well.\r\nDropbox has rolled out new password manager, Vault, and automatic computer backup features for Dropbox Plus plan users. These features w', '<p>Dropbox has rolled out new password manager, Vault, and automatic computer backup features for Dropbox Plus plan users. These features were announced in June and back then they were rolled out for testing in private beta. Now, the company has removed them from beta and is making them available for all Dropbox Plus plan users. Additionally, Dropbox Professional subscribers also get Dropbox Passwords and automatic computer backup features. However, the new Dropbox Vault feature is exclusive to Plus plan subscribers.</p>\r\n\r\n<h2>Dropbox Passwords</h2>\r\n\r\n<p>Dropbox&nbsp;Passwords works just like any other password managers out there &ndash; it saves your passwords and auto-fills them for instant sign-in to frequently used websites and apps. This Dropbox Password feature comes as a separate app, which means users can sync passwords on different devices - Windows, Mac, iOS, and Android. Dropbox says passwords from the desktop will automatically sync to your mobile device and vice versa. Dropbox Password comes with zero-knowledge encryption and is also available on Dropbox Professional, apart from rolling out for Dropbox Plus plan subscribers.</p>\r\n\r\n<h2>Dropbox Vault</h2>\r\n\r\n<p>As the name suggests, Dropbox Vault provides an extra layer of security for storing important personal files like bank documents, pay stubs, medical ID cards, passports, and tax documents. This feature uses a PIN for access and is available now for Dropbox Plus users exclusively.</p>\r\n\r\n<ul>\r\n	<li>Dropbox&rsquo;s Latest Feature Will Allow You to Easily Sign Digitally</li>\r\n</ul>\r\n\r\n<h3>Automatic computer backup</h3>\r\n\r\n<p>The last feature that has now&nbsp;come out of beta&nbsp;is the computer backup feature. Once turned on, it automatically syncs folders on the PC or Mac, directly into the Dropbox folder. It essentially removes the process of manual backup, and will work only if enabled. This feature is available on Dropbox Professional and Dropbox Basic plan subscribers as well.</p>\r\n\r\n<p>Dropbox Plus subscription offers 2TB of storage and up to 2GB of Dropbox transfer. This subscription is priced at $9.99 (roughly Rs. 750) per month. It also offers file recovery and history of up to 30 days. Dropbox Professional, on the other hand, is priced at $16.58 (roughly Rs. 1,300) per month. This subscription plan offers 3TB of storage, up to 180 days of file recovery and history, and up to 100GB of Dropbox transfer with customisation options as well.</p>\r\n', '8570_1597647698_dropbox_passwords_vault_1592391834246.jpg', NULL, '3', '', 1, NULL, 1, NULL, 0, '3', NULL, NULL, NULL, '17-August-2020, 12:29 pm', '2020-08-17 07:01:38'),
(147, 'WhatsApp to Enable Syncing of Chat History Across Platforms: Report', 'whatsapp-to-enable-syncing-of-chat-history-across-platforms-report', 'The new addition on WhatsApp would help users seamlessly shift from one device to another.\r\nWhatsApp is working to enable syncing of chat history across platforms, according to a report. The new change would particularly be in line with multiple device sup', '<p>WhatsApp is working to enable syncing of chat history across platforms, according to a report. The new change would particularly be in line with multiple device support that is speculated to debut as a Linked Devices feature. This feature is rumoured to let you use your WhatsApp account on a total of four devices simultaneously. Also, it is unlike the current scenario in which WhatsApp doesn&#39;t sync the chat history once you move your account from one device to another.</p>\r\n\r\n<p>WABetaInfo&nbsp;reports&nbsp;that&nbsp;WhatsApp&nbsp;would start syncing chat history across platforms to enable multiple device support on its platform. It would help users seamlessly shift from one device to another.</p>\r\n\r\n<p>WhatsApp already allows syncing of chat history when using its Web client and desktop app. However, the WhatsApp beta tracker claims that the messaging app would go further and enable syncing in case of mobile devices as well.</p>\r\n\r\n<ul>\r\n	<li>WhatsApp Adds Picture-in-Picture Mode for Watching ShareChat Videos in Beta</li>\r\n</ul>\r\n\r\n<p>It is also important to point out that WhatsApp would require a Wi-Fi connection to begin the multiple device support process. This would enable sharing of data from one device to another.</p>\r\n\r\n<p>However, once the data has been shared, WABetaInfo says that Wi-Fi or Internet won&#39;t be needed on the primary device. This is something new as you currently need an active Internet connection on your mobile device if you&#39;re using WhatsApp Web or its desktop app.</p>\r\n\r\n<p><img src=\"https://img.youtube.com/vi/JT1myNbdp1E/sddefault.jpg\" /></p>\r\n\r\n<p>If a user has removed or changed their devices when using multiple device support on WhatsApp, active chats would be notified as encryption key changes. This would be similar to how WhatsApp notifies users when a contact changes their number.</p>\r\n\r\n<ul>\r\n	<li>WhatsApp Advanced Search Feature Rolling Out for Android Beta Users</li>\r\n</ul>\r\n\r\n<p>WhatsApp is said to have cross-platform syncing of chat history in the works for&nbsp;iPad&nbsp;users as well. This means that it would enable you to use WhatsApp on both your&nbsp;iPhone&nbsp;and iPad at the same time. An iPad version of WhatsApp&nbsp;has also been in plans&nbsp;for some time.</p>\r\n\r\n<p>Similarly, the cross-platform syncing functionality is said to work between desktop and mobile devices. There is, however, no clarity on whether WhatsApp would enable syncing between Android and iPhone devices. This is, of course, quite important in case you have both Android and iPhone and want your WhatsApp data to be synced.</p>\r\n\r\n<p>WABetaInfo mentions that WhatsApp was previously working on converting the&nbsp;iOS&nbsp;database to the one compatible with its&nbsp;Android&nbsp;client. Thus, it is safe to expect syncing across Android and iPhone devices as well.</p>\r\n\r\n<ul>\r\n	<li>WhatsApp May Get 138 New Emojis on Android</li>\r\n	<li>WhatsApp May Soon Let You Mute Annoying Chats Permanently</li>\r\n</ul>\r\n\r\n<p>WhatsApp hasn&#39;t made any confirmation around the arrival of its multiple device support that is a&nbsp;part of the rumour mill&nbsp;at least since July last year. Nevertheless, its references&nbsp;surfaced in the recent beta releases&nbsp;- with the latest one showing&nbsp;Linked Devices&nbsp;as its official name.</p>\r\n', '1581_1597647865_whatsapp_reuters_1572763148535.jpg', NULL, '3', '', 1, NULL, 1, NULL, 0, '3', NULL, NULL, NULL, '17-August-2020, 12:31 pm', '2020-08-17 07:04:25'),
(148, 'Netflix Is Finally Available in Hindi', 'netflix-is-finally-available-in-hindi', 'It took just four years.\r\nNetflix has finally introduced the first localised version of its user interface: in Hindi, over four and a half years after its launch in India. Netflix says that every part of it — including sign-up, title names, search, and pay', '<p>Netflix has finally introduced the first localised version of its user interface: in Hindi, over four and a half years after its launch in India. Netflix says that every part of it &mdash; including sign-up, title names, search, and payment &mdash; is now available in the Hindi language across all apps and devices, be it mobile, computers, or on TV. You can switch to Hindi under&nbsp;<strong>Manage Profiles</strong>&nbsp;&gt;&nbsp;<strong>Language</strong>&nbsp;on the Netflix website. Language settings are profile-specific, so you don&#39;t have to worry about it affecting other members on the same Netflix account.</p>\r\n\r\n<p>&ldquo;Delivering a great&nbsp;Netflix&nbsp;experience is as important to us as creating great content,&rdquo;&nbsp;Netflix India&#39;s&nbsp;VP of content, Monika Shergill, said in a mailed statement. &ldquo;We believe the new user interface will make Netflix even more accessible and better suit members who prefer Hindi.&rdquo;</p>\r\n\r\n<p>Netflix&#39;s Hindi-language interface won&#39;t be limited to members in India but will be available across the world, the streaming service noted, as is the case for the other 26 languages: Bahasa Indonesia, Chinese, Czech, Danish, Dutch, English, Finnish, French, German, Greek, Hebrew, Hungarian, Italian, Japanese, Korean, Norwegian, Polish, Portuguese, Romanian, Spanish, Swahili, Swedish, Thai, Turkish, and Vietnamese.</p>\r\n\r\n<ul>\r\n	<li>Why Netflix Didn&rsquo;t Have a Hindi Interface Until Now</li>\r\n</ul>\r\n\r\n<p>It makes sense that Hindi is the first Indian language of choice given its prominence in India, and the fact that most local-language Netflix originals have been in Hindi, including the likes of&nbsp;Sacred Games,&nbsp;Delhi Crime, and&nbsp;Choked.</p>\r\n\r\n<p>Of course, this is entirely separate from subtitles and dubbing, wherein Hindi, Tamil, and Telugu are the preferred choices. Not all originals and licensed fare is available with Indian-language subtitles or dubs, but Netflix has been pushing to increase that, which can have accidental &mdash; though avoidable &mdash; side-effects such as censorship of&nbsp;Hollywood movies&nbsp;and&nbsp;TV shows.</p>\r\n', '4175_1597650644_netflix_hindi_interface_akhil_arora_1596786600253.jpg', NULL, '3', '', 1, 1, 1, NULL, 27, '3', NULL, NULL, NULL, '17-August-2020, 12:34 pm', '2020-08-17 07:50:44'),
(149, 'Google Docs, Sheets, Slides Apps Getting New Features to Improve Content Creation, Collaboration on Mobile', 'google-docs-sheets-slides-apps-getting-new-features-to-improve-content-creation-collaboration-on-mobile', 'G Suite apps such as Google Docs, Slides, and Sheets get handy updates for both Android and iOS users.\r\n', '<p>Google has started rolling out several new features for its G Suite apps on mobile platforms. These are designed to help make working on Google Docs, Sheets, and Slides much easier on the go. The freshly added features include Smart Compose, link previews, vertical navigation, dark theme, comments interface, and more. All G Suite customers will be able to access these features on Android or iOS devices, as the updates are gradually rolled out. Google said in a blog post that the new features aim to help mobile users &ldquo;efficiently create, collaborate, and communicate.&rdquo;</p>\r\n\r\n<p>Google&nbsp;announced the new features in a&nbsp;blog post&nbsp;on Wednesday. The tech giant said that it was announcing enhancements to Google&nbsp;Docs,&nbsp;Sheets, and&nbsp;Slide&nbsp;mobile apps. You can download the apps to avail these features for Docs (Android/iOS), Sheets (Android/iOS), and Slides (Android/iOS).</p>\r\n\r\n<h2>Here&#39;s a look at the new features introduced in Google Docs, Sheets, and Slides</h2>\r\n\r\n<p><strong>Smart Compose:</strong>&nbsp;This&nbsp;AI-powered tool will help reduce spelling and grammatical errors, and write faster. This feature was&nbsp;made available&nbsp;on the Web earlier this year and is now launching for mobile phones as well. Google said that the Smart Compose will be available on&nbsp;Android&nbsp;and&nbsp;iOS&nbsp;users within a few weeks.</p>\r\n\r\n<ul>\r\n	<li>Here&rsquo;s How to Enable Dark Mode on Google Docs</li>\r\n</ul>\r\n\r\n<p><strong>Link previews:&nbsp;</strong>Clicking on a link in Google Docs will now show a dynamic card with information about the content, such as thumbnails, owner details, latest activity of&nbsp;Drive&nbsp;file, titles, and more. You can do all of this without having to leave the app, thus not disrupting your reading flow. This feature is available on iOS and will be rolling out to Android within a few weeks.</p>\r\n\r\n<p><strong>Comments response:</strong>&nbsp;You will now see an up-to-date comment threads around documents in&nbsp;Gmail&nbsp;that you can resolve or reply directly, though the message. This feature was introduced on the Web last year by Google and will now be available for Android and iOS mobile apps as well.</p>\r\n\r\n<p><strong>Comments interface:</strong>&nbsp;Google has improved the interface for comments, making it easier for team members to collaborate. The new interface makes it easier to scroll through, respond to comments, and @mention others. This is available on Android and will be rolled out to iOS within a few months.</p>\r\n\r\n<ul>\r\n	<li>Google Redesigns Drive, Docs, Sheets Files Sharing Interface</li>\r\n</ul>\r\n\r\n<p><strong>Vertical navigation:</strong>&nbsp;You can now look at slideshows in a vertical stream in slides, using pinch-to-zoom feature. This will help make reviewing presentations faster and make it easier to switch to editing or presenting content. This will be available on Android within the next few weeks, and on iOS in the next few months.</p>\r\n\r\n<p><strong>Dark theme:</strong>&nbsp;Dark theme is supported in Docs, Sheets, and Slides on Android, and will be available on iOS in the coming months, according to Google. This feature rolled out in July.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '1345_1597650938_google_office_1596720989467.jpg', NULL, '5', '', 1, 1, 1, NULL, 5, '5', NULL, NULL, NULL, '17-August-2020, 1:20 pm', '2020-08-17 07:55:38'),
(150, 'PUBG Mobile Beta Version Gets Erangel 2.0 Map With 1.0 Update', 'pubg-mobile-beta-version-gets-erangel-20-map-with-10-update', 'PUBG Mobile beta has gone from version 0.19.0 to 1.0.0 which marks a major update in the games update cycle.\r\n', '<p>PUBG Mobile beta version has finally got Erangel 2.0 map with the 1.0 update. The development was shared by the PUBG Mobile team on its Discord server where it also posted a brief introduction to Erangel 2.0. The 1.0 update is currently available only for Android users with an iOS version coming soon. There are several changes in this version but the most prominent is the highly anticipated Erangel 2.0 map. It should be noted that since it is a beta update, there may be several changes when it finally comes out for the stable version of the game.</p>\r\n\r\n<h2>PUBG Mobile beta 1.0 update</h2>\r\n\r\n<p>The beta 1.0 update for&nbsp;PUBG Mobile&nbsp;is a major step in the games update cycle as it has jumped from 0.19.0 to 1.0.0, without continuing with 0.20.0. It brings the new Erangel 2.0 map that has some aesthetic changes to bring it at par with the&nbsp;latest Livik map&nbsp;and the previously&nbsp;revamped Mad Miramar map. There are some changes to the buildings in terms of design including areas like Mylta Power, Quarry, Prison, and others. Players will see trenches, abandoned tanks, barricades and other additions to the map.</p>\r\n\r\n<p>There is also a new weapon - M1014 &ndash; along with some balancing changes and some bug fixes as well. The Cheer Park has also been revamped with showdowns and more. The updated beta version can be downloaded for&nbsp;Android&nbsp;devices from&nbsp;here. iOS users will have to wait a bit.</p>\r\n\r\n<p><img src=\"https://img.youtube.com/vi/JjuTlPC7Lok/sddefault.jpg\" /></p>\r\n\r\n<p>Notably, according to a moderator on the PUBG Mobile Discord&nbsp;server, there is a possibility of the link not being updated. If that happens, it is recommended that you try downloading it again in a few hours.</p>\r\n\r\n<p>Players who try out the beta 1.0 update can share feedback with the team on the Discord server. The updated game is 1.52GB in size. It is unclear when Erangel 2.0 will make its way to the stable version of the game but seeing as it has reached the beta, PUBG Mobile fans have got an update on its progress.</p>\r\n', '4341_1597651068_PUBG_Mobile_beta_Erangel_2_1596706744055.jpg', NULL, '1', '', 1, 1, NULL, NULL, 4, '12', 'PUBG Mobile Beta Version Gets Erangel 2.0 Map With 1.0 Update', 'PUBG , Game', 'PUBG Mobile beta has gone from version 0.19.0 to 1.0.0 which marks a major update in the games update cycle.\r\n', '17-August-2020, 1:25 pm', '2020-08-17 07:57:48'),
(152, 'HOW TO PLAN A SUCCESSFUL RV TRIPHOW TO PLAN A SUCCESSFUL RV TRIP', 'how-to-plan-a-successful-rv-triphow-to-plan-a-successful-rv-trip', 'Since international travel on pause, people have turned to exploring their own backyards. From the U.S. to Canada to England, Europe, and New Zealand, people are getting in cars, campervans, and RVs and heading out on road trips. After all, it allows you ', '<p><em>Since international travel on pause, people have turned to exploring their own backyards. From the U.S. to Canada to England, Europe, and New Zealand, people are getting in cars, campervans, and RVs and heading out on road trips. After all, it allows you to social distance while still getting outside!</em></p>\r\n\r\n<p><em>Today, I&rsquo;ve invited my friends Mike and Anne from&nbsp;<a href=\"https://www.honeytrek.com/\" rel=\"noopener noreferrer\" target=\"_blank\">HoneyTrek</a>&nbsp;to share their RV tips and advice. They&rsquo;re full-time RVers and will help you get your next RV adventure started easily and on a budget!</em></p>\r\n\r\n<p>A couple of years back, the van life craze had everyone curious about rubber-tramping across North America. Maybe you thought, nah, I prefer my city apartment or jet-setting abroad.</p>\r\n\r\n<p>Then COVID-19 hit. All of a sudden, getting out of Dodge with a house on wheels started to sound really good, didn&rsquo;t it?</p>\r\n\r\n<p>There is no doubt that RVing is one of the easiest and safest ways to travel right now. No crowded planes or questionable hotel rooms required &mdash; an RV gives you the freedom to explore and the peace of mind of having your own space.</p>\r\n\r\n<p>Over the course of&nbsp;<a href=\"https://www.honeytrek.com/\" rel=\"noopener noreferrer\" target=\"_blank\">our eight-year &ldquo;HoneyTrek&rdquo;</a>&nbsp;we&rsquo;ve tried virtually every style of travel &mdash; backpacking,&nbsp;<a href=\"https://www.nomadicmatt.com/travel-blogs/house-sitting/\" rel=\"noopener noreferrer\" target=\"_blank\">house-sitting</a>, small-ship cruising, backcountry camping, five-star honeymooning, etc. &mdash; but the day we rented a campervan in&nbsp;<a href=\"https://www.nomadicmatt.com/travel-guides/new-zealand-travel-tips/\" rel=\"noopener noreferrer\" target=\"_blank\">New Zealand</a>, we knew this was our preferred mode of travel.</p>\r\n\r\n<p>For the past three years, we&rsquo;ve been traveling full-time in our 1985 Toyota Sunrader &ldquo;Buddy the Camper,&rdquo; from the Baja Peninsula to the Arctic Circle and 47 states in between.</p>\r\n\r\n<p>We&rsquo;ve learned a lot along the way and are excited to share what we think are the most important things to know before setting out on your RV journey.</p>\r\n\r\n<p>Here&rsquo;s a video we just filmed which covers all the basics (or read the post below):<br />\r\n<iframe frameborder=\"0\" height=\"395\" id=\"widget2\" scrolling=\"yes\" src=\"https://www.youtube.com/embed/aQnJm6HNCuc\" width=\"675\"></iframe><br />\r\n&nbsp;</p>\r\n\r\n<h3>How to Pick the Right Size RV</h3>\r\n\r\n<p>For maximum adventure and comfort, we&rsquo;d recommend a camper around 21 feet long. We know those big RVs tricked out like a penthouse apartment look tempting, but remember that every foot in length costs mobility. A shorter rig allows you to:</p>\r\n\r\n<ul>\r\n	<li>Access rugged terrain</li>\r\n	<li>Fit in a normal parking space, even parallel park</li>\r\n	<li>Avoid length restrictions on some of America&rsquo;s most beautiful winding roads and ferry rides</li>\r\n	<li>Get better gas mileage (Most rigs get 6&ndash;10 MPG. Ours gets 19.)</li>\r\n	<li>Have less stuff to break, which means more time exploring and having fun!</li>\r\n</ul>\r\n\r\n<p>And, while even shorter 16- to 19-foot-long campervans do have the ultimate mobility, there are a few things you should know before you fall for that adorable Westfalia or stealthy Sprinter.</p>\r\n\r\n<p>First, life ain&rsquo;t so pretty without your own indoor shower and bathroom. And, while we respect the vanlifers who make do with public restrooms, bucket toilets, and catholes (digging a hole outside when you need to go to the bathroom), let us tell you the virtues of having a flushing loo: privacy, cleanliness, and autonomy.</p>\r\n\r\n<p>We can be in a city center or a protected conservation area and conveniently and responsibly stay the night. In these unprecedented times, it&rsquo;s more important than ever to be self-sufficient and not rely on shared facilities.</p>\r\n\r\n<p>Besides a bathroom, a 19- to 22-foot long RV is big enough to also give you a proper bed and ample storage while still being small enough to explore with wild abandon.<br />\r\n&nbsp;</p>\r\n\r\n<h3>How to Get Power (A.K.A. the Virtues of Solar)</h3>\r\n\r\n<p><img alt=\"Mike and Anne from HoneyTrek boondocking in Pariah Canyon, USA\" src=\"https://media.nomadicmatt.com/2020/rvhoneytrek3.jpg\" style=\"height:395px; width:675px\" /><br />\r\nRVs and campers have a house battery to run the lights, water pump, fans, and power electronics. Here are the various way to keep it charged:</p>\r\n\r\n<ul>\r\n	<li>Drive a few hours per day</li>\r\n	<li>Pay to plug in at a campground</li>\r\n	<li>Run a generator</li>\r\n	<li>Have solar panels</li>\r\n</ul>\r\n\r\n<p>Your average road trip will likely give you enough charge from driving, but if you really need power, an RV park is never far away. If you are looking to slow-cruise the wilderness and lower your environmental impact, solar panels are a must. The simplest and most affordable option ($70&ndash;150 USD) is to get a portable panel and use it whenever you&rsquo;re stopped in order to charge up the house battery of your RV. This obviously isn&rsquo;t as convenient or powerful as an integrated system, but it should be enough to keep your phone and laptop charged.</p>\r\n\r\n<p>If you are in this for the long haul, though, you&rsquo;re going to want to install a solar system. We bought 300 watts of flexible monocrystalline solar panels, installed them to the roof, and wired them all together with a charge controller, lead-acid battery, and power inverter in about 20 hours &mdash; all for $1,200 USD.</p>\r\n\r\n<p>If you want the best efficiency and lifespan, spring for a lithium-ion deep cycle battery, like the Relion RB100. If a DIY electrical project sounds too scary, you can have it professionally installed for $1,000&ndash;2,000 USD. We know that&rsquo;s is a chunk of change, but investing in solar has allowed us to spend the last three years without having to ever pay for electricity, worry about running out of power, or generating any greenhouse gases.<br />\r\n&nbsp;</p>\r\n\r\n<h3>How to Get Internet</h3>\r\n\r\n<p><img alt=\"Anne from HoneyTrek working on a laptop in her RV\" src=\"https://media.nomadicmatt.com/2020/rvhoneytrek4.jpg\" style=\"height:395px; width:675px\" /><br />\r\nYour smartphone is your on-the-go router. It&rsquo;s important to use a carrier with an extensive national network (AT&amp;T or Verizon) so as to get reception in remote areas (the dream is to be using your laptop from a secluded beach, right?).</p>\r\n\r\n<p>We use our Verizon phone as a hotspot for our two laptops, getting 50GB unthrottled per month, plus unlimited calls and texts, for $109 USD.</p>\r\n\r\n<p>While that&rsquo;s a decent amount of data, it&rsquo;s not a home internet plan through which you can be streaming all day. If you&rsquo;ll be on the road for more than a couple weeks, monitor your usage with the&nbsp;<a href=\"https://www.glasswire.com/glasswire-for-android/\" rel=\"noopener noreferrer\" target=\"_blank\">GlassWire app</a>&nbsp;and install&nbsp;<a href=\"https://www.netlimiter.com/\" rel=\"noopener noreferrer\" target=\"_blank\">NetLimiter</a>&nbsp;on your laptop to help ration your data. Save your big downloads and uploads for free Wi-Fi zones.</p>\r\n\r\n<p>We love working at libraries, not just for the internet but for their inspiring spaces, peace and quiet, community offerings, and open invitation to stay all day.</p>\r\n\r\n<p>And, when all else fails, McDonald&rsquo;s and Starbucks have wifi that&rsquo;s usually strong enough to tap from the comforts of your camper.<br />\r\n&nbsp;</p>\r\n\r\n<h3>How to Find Places to Camp</h3>\r\n\r\n<p>Your basic campground typically offers a flat parking spot with a picnic table, fire pit, and shared bathroom for $10&ndash;30 USD per night. If you bump up to $35&ndash;80 USD a night, you&rsquo;re in RV park territory and will likely get power, water, sewer, and shared amenities like a clubhouse and a pool.</p>\r\n\r\n<p>But did you know there are tens of thousands of free campsites scattered around the wilds of the USA? The federal government has reserved 640 million acres of public lands (national forests, BLM [Bureau of Land Management] land, national conservation areas, etc.) for your enjoyment. These sites are pretty bare-bones (sometimes it&rsquo;s just a clearing in the forest) but, since we have a self-contained camper with our own drinking water and bathroom, all we really want is a peaceful spot with a good view.</p>\r\n\r\n<p>This style of independent camping has many names: dispersed camping, wild camping, dry camping, freedom camping, and most commonly &ldquo;boondocking.&rdquo; We find our favorite boondocking spots via the&nbsp;<a href=\"http://www.ultimatecampgrounds.com/\" rel=\"noopener noreferrer\" target=\"_blank\">Ultimate Campgrounds</a>&nbsp;app, which we use to see what sites are nearby.</p>\r\n\r\n<p>If we&rsquo;re striking out on that app, we turn to&nbsp;<a href=\"http://ioverlander.com/\" rel=\"noopener noreferrer\" target=\"_blank\">iOverlander</a>&nbsp;and&nbsp;<a href=\"http://www.freecampsites.net/\" rel=\"noopener noreferrer\" target=\"_blank\">FreeCampsites.net</a>.</p>\r\n\r\n<p>With these apps, we&rsquo;re able to find great camping on the fly and rarely pay a dime.</p>\r\n\r\n<p>That said, there is a time and place for more traditional campgrounds. They can be a great way to meet other campers, enjoy a few extra services, or stay in the heart of a national park.&nbsp;<a href=\"https://www.reserveamerica.com/\" rel=\"noopener noreferrer\" target=\"_blank\">ReserveAmerica.com</a>&nbsp;is the main campground portal (290,000 listings!) for public (national and state parks) and private campgrounds.&nbsp;<a href=\"https://www.hipcamp.com/\" rel=\"noopener noreferrer\" target=\"_blank\">HipCamp.com</a>&nbsp;also has extensive offerings and is our favorite for unique sites on private land &mdash; it&rsquo;s like the&nbsp;<a href=\"http://airbnb.7eer.net/c/214481/264339/4273\" rel=\"noopener noreferrer\" target=\"_blank\">Airbnb</a>&nbsp;of camping.&nbsp;<a href=\"https://koa.com/\" rel=\"noopener noreferrer\" target=\"_blank\">KOA</a>&nbsp;has tons of options too.</p>\r\n\r\n<p>If you know there is a certain place you want to be on a specific night, you can book in advance. But also just don&rsquo;t be afraid to go with the flow &mdash; there is always a beautiful boondocking spot somewhere!<br />\r\n&nbsp;</p>\r\n\r\n<h3>Urban Boondocking</h3>\r\n\r\n<p><img alt=\"Mike and Anne from HoneyTrek boondocking in Seattle, USA\" src=\"https://media.nomadicmatt.com/2020/rvhoneytrek5.jpg\" style=\"height:395px; width:675px\" /><br />\r\nSpeaking of boondocking, it&rsquo;s not just for the woods. We have spent countless nights &ldquo;camping&rdquo; in the heart of cities, and if you adhere to a few simple rules, you can feel confident doing the same:</p>\r\n\r\n<ul>\r\n	<li>Obey all street signs and curb markings and keep the meter fed. If it says &ldquo;no overnight parking,&rdquo; take heed. If there is any ambiguity in the signage (street cleaning conflicts, permit parking, etc.), find another spot.</li>\r\n	<li>Don&rsquo;t overstay your welcome. We usually limit our time in the same parking spot to two nights.</li>\r\n	<li>Don&rsquo;t draw attention to yourself with excessive lights, music, noise, etc. Even though our 1980s RV is far from a stealth camper, we have slept in over 50 cities and never been asked to &ldquo;move along.&rdquo;</li>\r\n</ul>\r\n\r\n<p>Be smart, be respectful, and the world is your campground.<br />\r\n&nbsp;</p>\r\n\r\n<h3>How to Save Money on Gas</h3>\r\n\r\n<p><img alt=\"Mike and Anne from HoneyTrek parked at a small general store\" src=\"https://media.nomadicmatt.com/2020/rvhoneytrek6.jpg\" style=\"height:395px; width:675px\" /><br />\r\nWe know gas is only around $2 USD/gallon at the moment, but when it comes to your long-term travel budget, every bit counts. Here are some tips to save at the pump:</p>\r\n\r\n<ul>\r\n	<li>Get the&nbsp;<a href=\"https://www.gasbuddy.com/app\" rel=\"noopener noreferrer\" target=\"_blank\">GasBuddy app</a>. It allows you to see the gas prices along your route, often saving upwards of 50 cents per gallon, particularly if you can wait to cross a state line or get farther off the highway.</li>\r\n	<li>Get yourself a Discover card and/or Chase Freedom Unlimited card; certain months of the year, they offer 5% off your fill-up.</li>\r\n	<li>Sign up for gas station rewards programs, especially Shell and Pilot, which give 3&ndash;5 cents off per gallon.</li>\r\n	<li>Keep your tires inflated at the recommended PSI, and drive under 55mph. In addition to the gas savings, it&rsquo;s safer and prolongs the life of your rig.</li>\r\n</ul>\r\n\r\n<h3>How to Find the Back Roads</h3>\r\n\r\n<p><img alt=\"Mike and Anne from HoneyTrek in the Black Hills\" src=\"https://media.nomadicmatt.com/2020/rvhoneytrek7.jpg\" style=\"height:395px; width:675px\" /><br />\r\nSet your GPS to &ldquo;avoid highways&rdquo; and you&rsquo;ll discover just how beautiful this country can be. Interstates have blazed straight lines across the nation but the old network of roads, working with the contours of the land and connecting historic towns, still exists.</p>\r\n\r\n<p>The best routes are&nbsp;<a href=\"https://www.fhwa.dot.gov/byways\" rel=\"noopener noreferrer\" target=\"_blank\">America&rsquo;s Byways</a>, a collection of 150 distinct and diverse roads protected by the Department of Transportation for their natural or cultural value.</p>\r\n\r\n<p>Even better than that website (because you can&rsquo;t rely on back roads&rsquo; cell reception) is a hard copy of the&nbsp;<em><a href=\"https://amzn.to/39ehGBi\" rel=\"noopener noreferrer\" target=\"_blank\">National Geographic Guide to Scenic Highways and Byways</a></em>. It maps out the prettiest drives in every state, with something to marvel at even in &ldquo;the flyover states.&rdquo; We refer to it every time we start a big drive and discover interesting landmarks, quirky museums, scenic viewpoints, quintessential eateries, and short hikes, which always improves the ride.<br />\r\n&nbsp;</p>\r\n\r\n<h3>Take Glamping Breaks</h3>\r\n\r\n<p><img alt=\"Mike and Anne from HoneyTrek glamping in the desert\" src=\"https://media.nomadicmatt.com/2020/rvhoneytrek2.jpg\" style=\"height:395px; width:675px\" /><br />\r\nTo make sure you don&rsquo;t burn out on small-space, off-grid living, treat yourself to the occasional glamping getaway. Creative outdoor accommodations with a plush bed, hot shower, and friendly host always remind us how much we love the woods.</p>\r\n\r\n<p>When we get to a glamp camp, we can walk away from our normal responsibilities (setting up camp, cooking for ourselves, and DIY everything) and truly relax. A gorgeous treehouse, dome, yurt, or safari tent has been designed with your enjoyment in mind, and if you need anything, your host is at the ready.</p>\r\n\r\n<p>A little pampering and fresh take on the outdoors will give you the energy to keep on truckin&rsquo;.</p>\r\n\r\n<p>To find fabulous getaways along your route, check out our glamping book,&nbsp;<em><a href=\"https://www.honeytrek.com/glampingbook\" rel=\"noopener noreferrer\" target=\"_blank\">Comfortably Wild: The Best Glamping Destinations in North America</a></em>.<br />\r\n&nbsp;</p>\r\n\r\n<h3>How to Protect Yourself and Your Ride</h3>\r\n\r\n<p>You&rsquo;ll be exploring remote areas, going down rough roads, and having wild adventures (get excited!). Consider these three forms of protection and you&rsquo;ll be ready for whatever comes your way:</p>\r\n\r\n<ul>\r\n	<li><strong>RV insurance</strong>&nbsp;&ndash; While this is specialty car insurance, the good news is it can be cheaper than insuring a sedan (we pay $375 USD a year for our Progressive plan).</li>\r\n	<li><strong>Travel insurance</strong>&nbsp;&ndash; While most people think of travel insurance for big international trips, it usually kicks in 100 miles from your house, covering health emergencies, trip delays, canceled reservations (from campgrounds to river rafting excursions), and a variety of other snafus. Rather than getting insurance every time we hit the road, we use the&nbsp;<a href=\"https://www.allianztravelinsurance.com/find-a-plan/alltrips-premier.htm\" rel=\"noopener noreferrer\" target=\"_blank\">Allianz All Trips Premier Plan</a>&nbsp;so we&rsquo;re automatically covered wherever we go throughout the year.</li>\r\n	<li><strong>Roadside assistance</strong>&nbsp;&ndash; Good ol&rsquo;&nbsp;<a href=\"https://www.aaa.com/International/\" rel=\"noopener noreferrer\" target=\"_blank\">AAA</a>&nbsp;does have RV plans, but we like that&nbsp;<a href=\"https://goodsamrvinsurance.com/\" rel=\"noopener noreferrer\" target=\"_blank\">Good Sam</a>&nbsp;is designed specifically for RVers and doesn&rsquo;t charge a premium for it. An annual membership covers towing RVs of all sizes, tire blowouts, running out of gas, locking your keys in your vehicle, plus lots of other benefits and travel discounts.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>***</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>As full-timers, we&rsquo;re incredibly passionate about RVing and lot to share&nbsp;<a href=\"https://www.honeytrek.com/?s=road+trip\" rel=\"noopener noreferrer\" target=\"_blank\">road trip itineraries</a>, advice about buying a vintage camper, and lessons learned from three years on the road. While there is a lot to know about RV travel, renting a camper is a safe and easy way to get started. And there is a wonderful RV and #vanlife community online that will be happy to help too.</p>\r\n', '8926_1600078524_rvhoneytrek1.jpg', NULL, '8', '', 1, NULL, NULL, NULL, 1, '12', 'HOW TO PLAN A SUCCESSFUL RV TRIPHOW TO PLAN A SUCCESSFUL RV TRIP', 'trip', 'HOW TO PLAN A SUCCESSFUL RV TRIPHOW TO PLAN A SUCCESSFUL RV TRIP', '14-September-2020, 3:37 pm', '2020-09-14 10:14:49'),
(153, 'MY CURRENT LIST OF FAVORITE TRAVEL BLOGS', 'my-current-list-of-favorite-travel-blogs', 'Tucked away up in the northeast corner of the United States, Maine evokes images of endless shorelines, wild forests, Stephen King, iconic lighthouses, and lots and lots of lobster dinners.Tucked away up in the northeast corner of the United States, Maine', '<p>Tucked away up in the northeast corner of the&nbsp;United States, Maine evokes images of endless shorelines, wild forests, Stephen King, iconic lighthouses, and lots and lots of lobster dinners.</p>\r\n\r\n<p>Despite growing up only 90 minutes from the state, I only visited once in my life. I was in college and my friend George was from there, so one weekend, we drove up to his hometown of Gorham.</p>\r\n\r\n<p>Maine was always one of those places that I felt I could visit anytime so was never a rush to do so. There was always a flight to some distant land to get on instead. Maine could wait.</p>\r\n\r\n<p>People tend to put off traveling their &ldquo;backyard&rdquo; until the end and I was no different.</p>\r\n\r\n<p>But then COVID struck and there were no more flights to distant lands.</p>\r\n\r\n<p><img alt=\"A busy harbour on the coast of Maine, USA\" src=\"https://media.nomadicmatt.com/2020/maine7.jpg\" style=\"height:395px; width:675px\" /></p>\r\n\r\n<p>There was just my backyard to see.</p>\r\n\r\n<p>So, while I was back in&nbsp;Boston&nbsp;longing for nature, I decided to finally visit Maine. My original plan was to spend a roughly ten days there before heading to Vermont then Upstate&nbsp;<a href=\"https://www.nomadicmatt.com/travel-guides/united-states-travel-guide/new-york-city/\" rel=\"noopener noreferrer\" target=\"_blank\">New York</a>&nbsp;and then back to Boston.</p>\r\n\r\n<p>But as the days ticked by, 10 turned to 12, which turned to 14, which turned to 21.</p>\r\n\r\n<p>I just couldn&rsquo;t quit Maine.</p>\r\n\r\n<p>I loved the quiet, slow pace of the state.</p>\r\n\r\n<p>I loved the small-town feel to the cities, and the fact you were never far from nature. Every city had access to it, and there was always someplace to go hike. Even tiny Bangor had parks and greenways galore.</p>\r\n', '2955_1600079100_maine6.jpg', NULL, '8', '', 1, NULL, NULL, NULL, 1, '12', 'MY CURRENT LIST OF FAVORITE TRAVEL BLOGS', 'travel', 'MY CURRENT LIST OF FAVORITE TRAVEL BLOGS', '14-September-2020, 3:54 pm', '2020-09-14 10:25:00');
INSERT INTO `blog` (`blog_id`, `blog_title`, `url_slug`, `header_text`, `blog_text`, `blog_img`, `user_id`, `category_id`, `session_id`, `status`, `trending`, `pin`, `star`, `likes`, `sub_category_id`, `meta_title`, `meta_keyword`, `meta_description`, `date_time`, `time`) VALUES
(154, '5 THINGS I’D TELL ANY NEW TRAVELER', '5-things-id-tell-any-new-traveler', 'When I left to travel the world on my first round-the-world trip, I didn’t know what to expect.', '<p>Hope. Fear. Excitement. Traveling for the first time produced a wave of emotions.</p>\r\n\r\n<p>When I left to travel the world on my first&nbsp;<a href=\"https://www.nomadicmatt.com/travel-tips/buying-a-rtw-ticket/\" rel=\"noopener noreferrer\" target=\"_blank\">round-the-world trip</a>, I didn&rsquo;t know what to expect.</p>\r\n\r\n<p>Now,&nbsp;<a href=\"https://www.nomadicmatt.com/ten-years-a-nomad/\" rel=\"noopener noreferrer\" target=\"_blank\">with fifteen&nbsp;years of travel experience under my belt</a>, I know better. Traveling is second nature to me now. I land in an airport and I just go on autopilot.</p>\r\n\r\n<p>But, back then, I was as green as they come.</p>\r\n\r\n<p>To compensate for my lack of experience, I followed my guidebooks and wet my feet by going on organized tours. I was young&nbsp;and&nbsp;inexperienced and I made a lot of&nbsp;<a href=\"https://www.nomadicmatt.com/travel-blogs/not-to-do/\" rel=\"noopener noreferrer\" target=\"_blank\">rookie travel mistakes</a>.</p>\r\n\r\n<p>I know what it&rsquo;s like to just be starting out and have a mind filled with questions, anxieties, and concerns.</p>\r\n\r\n<p>So, if you&rsquo;re new to travel and looking for advice to help you prepare, here are 12 tips that I&rsquo;d tell a new traveler to help them avoid some of my early mistakes:<br />\r\n&nbsp;</p>\r\n\r\n<h3>1. Don&rsquo;t Be Scared</h3>\r\n\r\n<p><a href=\"https://www.nomadicmatt.com/travel-blogs/how-to-overcome-your-fears/\" rel=\"noopener noreferrer\" target=\"_blank\">Fear is a powerful deterrent</a>. Taking the leap into the unknown is scary, but remember: you aren&rsquo;t the first person to travel the world. You aren&rsquo;t discovering new continents or exploring uncharted territories.</p>\r\n\r\n<p>There is a well-worn travel trail out&nbsp;<a href=\"https://www.nomadicmatt.com/travel-blogs/introducing-the-nomadic-network/\" rel=\"noopener noreferrer\" target=\"_blank\">there and people to help guide you along the way</a>. If millions of people can make their way around the world each year, you can too.</p>\r\n\r\n<p>You&rsquo;re just as capable as anyone else. After all, you did the hardest part: deciding to go. Having the courage to make that decision is the hardest part.</p>\r\n\r\n<p>You&rsquo;ll make mistakes. Everyone does. But that&rsquo;s part of the experience.</p>\r\n\r\n<p>There will be lots of people out there to help you. You&rsquo;ll be shocked at just how helpful and kind people are. You&rsquo;ll make friends, you&rsquo;ll survive, and you&rsquo;ll be better for it.<br />\r\n&nbsp;</p>\r\n\r\n<h3>2. Don&rsquo;t Live by Your Guidebook</h3>\r\n\r\n<p>Guidebooks are useful for a general overview of a destination. They&rsquo;re a great way to learn the basics and get introduced to the cities and countries you plan to visit. But you&rsquo;ll never find the latest off-the-beaten-path attractions, bars, or restaurants in them.</p>\r\n\r\n<p>For the latest info (as well as insider tips), connect with locals. Use websites like&nbsp;<a href=\"http://meetup.com/\" rel=\"noopener noreferrer\" target=\"_blank\">Meetup.com</a>&nbsp;or&nbsp;<a href=\"https://www.nomadicmatt.com/travel-blogs/how-to-crush-it-on-couchsurfing/\" rel=\"noopener noreferrer\" target=\"_blank\">Couchsurfing</a>&nbsp;to connect directly with local and expats so you can get suggesitons, advice, and tips to make the most of your trip.</p>\r\n\r\n<p>Additionally, ask other travelers you meet or the staff at your hotel/hostel. Visit the local tourist board as well. It&rsquo;s a wealth of information that often gets overlooked.</p>\r\n\r\n<p>In short, use a guidebook for the foundation of your plans but fill in the details with up-to-date info from locals.</p>\r\n\r\n<p><a href=\"https://www.nomadicmatt.com/travel-blogs/my-current-list-of-favorite-blogs/\" rel=\"noopener noreferrer\" target=\"_blank\">You can also use travel blogs for planning tips</a>&nbsp;since they are updated more often than guidebooks.<br />\r\n&nbsp;</p>\r\n\r\n<h3>3. Travel Slow</h3>\r\n\r\n<p>This is something most new long-term travelers learn the hard way (myself included).</p>\r\n\r\n<p>I know it can be tempting to pack in as many cities and activities as possible. (This is especially true if you only have a few weeks of vacation.)</p>\r\n\r\n<p>But rushing from city to city every other day is just going to leave you exhausted and stressed out. You&rsquo;ll experience a whirlwind of activity, most of which will remain a blur when you look back on it. Sure, you&rsquo;ll have some great pictures for Instagram but is that really why you&rsquo;re traveling?</p>\r\n\r\n<p>Travel is about quality, not quantity. Don&rsquo;t worry about how much you see. Don&rsquo;t worry about trying to impress people with the number of countries you&rsquo;ve visited. Slow down and soak up your destinations. You&rsquo;ll learn more, enjoy it more, and have a much more memorable experience.</p>\r\n\r\n<p>When it comes to travel, less is more. (Plus, traveling slow helps reduce your transportation costs. It&rsquo;s cheaper to go slow!)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>4. Pack Light</h3>\r\n\r\n<p>When I went to&nbsp;<a href=\"https://www.nomadicmatt.com/travel-guides/costa-rica-travel-tips/\" rel=\"noopener noreferrer\" target=\"_blank\">Costa Rica</a>&nbsp;in 2003, I brought a bag filled with tons of stuff:&nbsp;hiking boots and pants, a fleece jacket, too much clothing, and my bodyweight in toiletries. And it all sat in my bag, mostly unused.</p>\r\n\r\n<p>I was packing for &ldquo;just in case&rdquo; and &ldquo;what if&rdquo; instead of the reality of my trip.</p>\r\n\r\n<p>While it can be tempting to bring more than you need &ldquo;just in case,&rdquo; remember this: you can buy things on the road. Socks, shampoo, jackets, new shoes &mdash; you can find it all aborad. There&rsquo;s no need to bring everything and the kitchen sink.</p>\r\n\r\n<p>So,&nbsp;<a href=\"https://www.nomadicmatt.com/travel-blogs/packing/\" rel=\"noopener noreferrer\" target=\"_blank\">pack light</a>. You&rsquo;ll have less to carry, saving you the hassle and stress of lugging a huge backpack around for weeks (or months) on end.</p>\r\n\r\n<p>Unless you are going somewhere cold, a bag around 40 liters will suffice. Bags around this size are easier to carry, don&rsquo;t get too unwieldy, and can fit on your flight as carry-on only if need be (a huge perk if you want to save yourself some headaches).</p>\r\n\r\n<p><a href=\"https://www.nomadicmatt.com/travel-tips/choosing-the-right-backpack/\" rel=\"noopener noreferrer\" target=\"_blank\">Here&rsquo;s everything you need to know to help you find the perfect bag for your budget and your trip</a>.<br />\r\n&nbsp;</p>\r\n\r\n<h3>5. Get Travel Insurance</h3>\r\n\r\n<p>Whether you&rsquo;re a travel veteran or a brand new backpacker, don&rsquo;t leave home without making sure you&rsquo;re protected in case something goes wrong. As we learned during the COVID-19 pandemic, sudden emergencies can come out of nowhere.</p>\r\n\r\n<p>I&rsquo;ve had my luggage lost. I popped an eardrum in Thailand.&nbsp;<a href=\"https://www.nomadicmatt.com/travel-blogs/getting-stabbed-in-colombia/\" rel=\"noopener noreferrer\" target=\"_blank\">I was knifed in Colombia</a>.</p>\r\n\r\n<p>I&rsquo;ve had a friend break bones, need to be helicoptered out of the Amazon, or fly back due to a sudden death.</p>\r\n\r\n<p>Stuff happens.</p>\r\n\r\n<p>To ensure you&rsquo;re protected,&nbsp;<a href=\"https://www.nomadicmatt.com/travel-blogs/travel-insurance/\" rel=\"noopener noreferrer\" target=\"_blank\">buy travel insurance</a>.</p>\r\n\r\n<p>I never leave home without it because I know just how quickly things can go sideways.</p>\r\n\r\n<p>You never know what might happen. The road is filled with uncertainty. Make sure you&rsquo;re protected. It will also give you peace of mind and help you travel with confidence.</p>\r\n\r\n<p>Here are a few posts worth reading. I know it&rsquo;s not a fun or sexy topic, but it&rsquo;s an important one!</p>\r\n\r\n<ul>\r\n	<li><a href=\"https://www.nomadicmatt.com/travel-blogs/is-travel-insurance-worth-it/\" rel=\"noopener noreferrer\" target=\"_blank\">Is Travel Insurance Worth it?</a></li>\r\n	<li><a href=\"https://www.nomadicmatt.com/travel-blogs/best-travel-insurance-companies/\" rel=\"noopener noreferrer\" target=\"_blank\">The 7 Best Travel Insurance Companies</a></li>\r\n	<li><a href=\"https://www.nomadicmatt.com/travel-blogs/world-nomads-review/\" rel=\"noopener noreferrer\" target=\"_blank\">World Nomads Insurance Review</a></li>\r\n</ul>\r\n', '6133_1600079305_newtraveler3.jpg', NULL, '8', '', 1, NULL, NULL, NULL, 0, '12', '5 THINGS I’D TELL ANY NEW TRAVELER', 'travel', '5 THINGS I’D TELL ANY NEW TRAVELER', '14-September-2020, 3:56 pm', '2020-09-14 10:28:25'),
(155, 'THE 5 BEST HOSTELS IN SAN FRANCISCO', 'the-5-best-hostels-in-san-francisco', 'San Francisco is a beautiful, eclectic city. Balancing its hippie roots with its modern, techie scene, it’s a cool, lively, energetic place bursting with history and tons of amazing food. It’s home to hippies, students, artists, entrepreneurs, and sizable', '<p><a href=\"https://www.nomadicmatt.com/travel-guides/united-states-travel-guide/san-francisco/\" rel=\"noopener noreferrer\" target=\"_blank\">San Francisco</a>&nbsp;is a beautiful, eclectic city. Balancing its hippie roots with its modern, techie scene, it&rsquo;s a cool, lively, energetic place bursting with history and tons of amazing food. It&rsquo;s home to hippies, students, artists, entrepreneurs, and sizable immigrant communities. All of that combines to create one of the best metropolises in the country.</p>\r\n\r\n<p>However, it&rsquo;s also one of the most expensive too!</p>\r\n\r\n<p>I&rsquo;ve been visiting San Francisco for a decade and have stayed in dozens of hostels, hotels, and Airbnbs. Since SF is so expensive, hostels are your best bet here. Fortunately, it has a growing hostel scene, so you&nbsp;<em>can</em>&nbsp;find affordable accommodation.</p>\r\n\r\n<p>There are a lot of things to consider when selecting a hostel. The top four in San Francisco are:</p>\r\n\r\n<ol>\r\n	<li><strong>Location</strong>&nbsp;&ndash; San Francisco is huge, and it can take some time to get around. Pick a place that is central to the sites and nightlife you want to experience. (All the hostels listed here are in central locations.)</li>\r\n	<li><strong>Price</strong>&nbsp;&ndash; In San Francisco, you really get what you pay for, so if you go with a really cheap place, you&rsquo;re probably going to get a hostel that is small and cramped and doesn&rsquo;t offer great service.</li>\r\n	<li><strong>Amenities</strong>&nbsp;&ndash; Every hostel in the city offers free Wi-Fi, and most have a free breakfast, but if you want more than that, be sure to do your research to find the one that best meets your needs!</li>\r\n	<li><strong>Staff</strong>&nbsp;&ndash; All the hostels listed here have amazing staff who are super friendly and knowledgeable. Even if you don&rsquo;t end up staying at one of the places listed below, be sure to look up reviews to ensure that you end up somewhere where the staff is helpful and friendly, as they can make or break a hostel!</li>\r\n</ol>\r\n\r\n<p>To help you plan your trip, here is my list of the hostels in San Francisco that I like the most. If you don&rsquo;t want to read the longer list below, the following are the best in each category:</p>\r\n\r\n<p><strong>Best Hostel for Budget Travelers</strong>:&nbsp;<a href=\"https://prf.hn/click/camref:1101lHtj/destination:https%3A%2F%2Fwww.hostelworld.com%2Fpwa%2Fhosteldetails.php%2FOrange-Village-Hostel%2FSan-Francisco%2F39781\" rel=\"noopener nofollow noreferrer\" target=\"_blank\">Orange Village</a><br />\r\n<strong>Best Hostel for Families</strong>:&nbsp;<a href=\"https://prf.hn/click/camref:1101lHtj/destination:https%3A%2F%2Fwww.hostelworld.com%2Fpwa%2Fhosteldetails.php%2FAdelaide-Hostel%2FSan-Francisco%2F300815\" rel=\"noopener nofollow noreferrer\" target=\"_blank\">Adelaide Hostel</a><br />\r\n<strong>Best Hostel for Solo Female Travelers</strong>:&nbsp;<a href=\"https://prf.hn/click/camref:1101lHtj/destination:https%3A%2F%2Fwww.hostelworld.com%2Fpwa%2Fhosteldetails.php%2FHI-San-Francisco-Fisherman-s-Wharf%2FSan-Francisco%2F4551\" rel=\"noopener nofollow noreferrer\" target=\"_blank\">HI Fisherman&rsquo;s Wharf</a><br />\r\n<strong>Best Hostel for Partying</strong>:&nbsp;<a href=\"https://prf.hn/click/camref:1101lHtj/destination:https%3A%2F%2Fwww.hostelworld.com%2Fhosteldetails.php%2FGreen-Tortoise-Hostel%2FSan-Francisco%2F1821\" rel=\"noopener nofollow noreferrer\" target=\"_blank\">Green Tortoise Hostel</a><br />\r\n<strong>Best Hostel for Digital Nomads</strong>:&nbsp;<a href=\"https://prf.hn/click/camref:1101lHtj/destination:https%3A%2F%2Fwww.hostelworld.com%2Fpwa%2Fhosteldetails.php%2FHI-San-Francisco-Downtown%2FSan-Francisco%2F4549\" rel=\"noopener nofollow noreferrer\" target=\"_blank\">HI Downtown</a><br />\r\n<strong>Best Overall Hostel</strong>:&nbsp;<a href=\"https://prf.hn/click/camref:1101lHtj/destination:https%3A%2F%2Fwww.hostelworld.com%2Fhosteldetails.php%2FGreen-Tortoise-Hostel%2FSan-Francisco%2F1821\" rel=\"noopener nofollow noreferrer\" target=\"_blank\">Green Tortoise Hostel</a></p>\r\n\r\n<p>Want the specifics of each hostel? Here&rsquo;s my comprehensive list of the best hostels in San Francisco:</p>\r\n\r\n<p><strong>Price Legend (per night)</strong></p>\r\n\r\n<ul>\r\n	<li>$ = Under $40 USD</li>\r\n	<li>$$ = $40-50 USD</li>\r\n	<li>$$$ = Over $50 USD</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>1. HI San Francisco &ndash; Downtown</h3>\r\n\r\n<p><img alt=\"The interior of a spacious private room at HI San Francisco Downtown\" src=\"https://media.nomadicmatt.com/2020/sfhostel5.jpg\" style=\"height:375px; width:675px\" /><br />\r\nHI Downtown has some standard perks, like free breakfast and free towels, but it also organizes a lot of events, including pub crawls, trips to Muir Woods and Yosemite, and bike tours across the Golden Gate Bridge. It&rsquo;s a fun, social hostel with decent beds (but no privacy curtains) and lots of storage space so you can lock your stuff up and keep it safe. They also have a fully-equipped kitchen, lots of board games, and are wheelchair accessible too.</p>\r\n\r\n<p><strong>HI Downtown at a glance</strong>:</p>\r\n\r\n<ul>\r\n	<li>$$$</li>\r\n	<li>Organizes lots of events?</li>\r\n	<li>Free breakfast and free towels</li>\r\n	<li>Lots of common areas</li>\r\n</ul>\r\n\r\n<p><em>Beds from $54 USD a night, rooms from $159 USD.</em></p>\r\n\r\n<p><a href=\"https://prf.hn/click/camref:1101lHtj/destination:https%3A%2F%2Fwww.hostelworld.com%2Fpwa%2Fhosteldetails.php%2FHI-San-Francisco-Downtown%2FSan-Francisco%2F4549\" rel=\"noopener nofollow noreferrer\">&mdash;&gt; Book your stay at HI Downtown!</a><br />\r\n&nbsp;</p>\r\n\r\n<h3>2. Green Tortoise Hostel</h3>\r\n\r\n<p><img alt=\"The huge ballroom dining area at the Green Tortoise Hostel in San Francisco, USA\" src=\"https://media.nomadicmatt.com/2020/sfhostel11.jpg\" style=\"height:375px; width:675px\" /><br />\r\nThis hostel is an institution in San Francisco, one of the oldest in town. It offers free breakfast, free dinners multiple times per week, and even a free sauna! It has a huge common room so it&rsquo;s easy to meet people and has a very fun, social atmosphere. There&rsquo;s a pool, tons of games (like giant jenga and foosball), and musical instruments in case you want to jam. The wooden bunks are basic (thick mattresses, no curtains) but are comfy. It&rsquo;s my favorite hostel in SF (and one of my favorites in the entire country).</p>\r\n\r\n<p><strong>Green Tortoise at a glance</strong>:</p>\r\n\r\n<ul>\r\n	<li>$$</li>\r\n	<li>Lots of free perks (free breakfast, free dinners, free sauna)</li>\r\n	<li>Lively party atmosphere</li>\r\n	<li>Great for solo travelers</li>\r\n</ul>\r\n\r\n<p><em>Beds from $47 USD a night.</em></p>\r\n\r\n<p><a href=\"https://prf.hn/click/camref:1101lHtj/destination:https%3A%2F%2Fwww.hostelworld.com%2Fhosteldetails.php%2FGreen-Tortoise-Hostel%2FSan-Francisco%2F1821\" rel=\"noopener nofollow noreferrer\">&mdash;&gt; Book your stay at Green Tortoise!</a><br />\r\n&nbsp;</p>\r\n\r\n<h3>3. HI San Francisco &ndash; Fisherman&rsquo;s Wharf</h3>\r\n\r\n<p><img alt=\"The front desk and lobby of the HI hostel at Fisherman\'s Wharf in San Francisco, USA\" src=\"https://media.nomadicmatt.com/2020/sfhostel6.jpg\" style=\"height:375px; width:675px\" /><br />\r\nThis is another great HI hostel. It has lots of common areas (so it&rsquo;s easy to relax and meet people), the staff organizes many events (like bike tours, museum tours, and pub crawls), and there&rsquo;s even have a small theater for watching movies. The beds aren&rsquo;t anything special (the mattresses are thin and there are no curtains) but there&rsquo;s a caf&eacute; on-site (with reasonable prices), female-only dorms, and the hostel is kept clean and tidy.</p>\r\n\r\n<p><strong>HI Fisherman&rsquo;s Wharf at a glance</strong>:</p>\r\n\r\n<ul>\r\n	<li>$$</li>\r\n	<li>Female-only dorms for extra security</li>\r\n	<li>Organizes lots of events</li>\r\n	<li>Huge kitchen</li>\r\n</ul>\r\n\r\n<p><em>Beds from $41.50 USD a night.</em></p>\r\n\r\n<p><a href=\"https://prf.hn/click/camref:1101lHtj/destination:https%3A%2F%2Fwww.hostelworld.com%2Fpwa%2Fhosteldetails.php%2FHI-San-Francisco-Fisherman-s-Wharf%2FSan-Francisco%2F4551\" rel=\"noopener nofollow noreferrer\">&mdash;&gt; Book your stay at HI Fisherman&rsquo;s Wharf!</a><br />\r\n&nbsp;</p>\r\n\r\n<h3>4. Found San Francisco &ndash; Union Square</h3>\r\n\r\n<p><img alt=\"Bunk beds in a dorm room at FOUND hostel in San Francisco, USA\" src=\"https://media.nomadicmatt.com/2020/sfhostel8.jpg\" style=\"height:375px; width:675px\" /><br />\r\nLocated right near Union Square in the heart of the city, Found has spacious private rooms as well as smaller dorm rooms. It&rsquo;s a good choice for anyone looking for a bit more privacy and some peace and quiet. The beds are super comfy, and the rooms are clean and a bit more stylish than most hostels. The kitchen isn&rsquo;t huge, though.</p>\r\n\r\n<p><strong>Found Union Square at a glance</strong>:</p>\r\n\r\n<ul>\r\n	<li>$$</li>\r\n	<li>Convenient location</li>\r\n	<li>Modern amenities</li>\r\n</ul>\r\n\r\n<p><em>Beds from $40.50 USD a night, rooms from $269 USD.</em></p>\r\n\r\n<p><a href=\"https://prf.hn/click/camref:1101lHtj/destination:https%3A%2F%2Fwww.hostelworld.com%2Fpwa%2Fhosteldetails.php%2FFound-San-Francisco-Union-Square%2FSan-Francisco%2F299085\" rel=\"noopener nofollow noreferrer\">&mdash;&gt; Book your stay at Found!</a><br />\r\n&nbsp;</p>\r\n\r\n<h3>5. HI San Francisco &ndash; City Center</h3>\r\n\r\n<p><img alt=\"A comfy bed in a spacious private room at the HI Hostel City Center hostel in San Francisco, USA\" src=\"https://media.nomadicmatt.com/2020/sfhostel7.jpg\" style=\"height:375px; width:675px\" /><br />\r\nThis cozy hostel is located in a boutique hotel from the 1920s. It does an excellent job of balancing historic charm with a modern atmosphere. There are lots of beautiful paintings and murals, and they even have a speakeasy-style caf&eacute;. But it&rsquo;s the atmosphere here that makes your stay worthwhile. There are lots of common areas to chill out and meet people in; there are also female-only dorms too. It&rsquo;s just a 10-minute walk from the main shopping area, and there are lots of cheap restaurants nearby as well. The beds are comfy but not all of them have curtains. There&rsquo;s a fully-equipped kitchen too!</p>\r\n\r\n<p><strong>HI City Center at a glance</strong>:</p>\r\n\r\n<ul>\r\n	<li>$$</li>\r\n	<li>Easy to meet people</li>\r\n	<li>Free breakfast and free towels</li>\r\n	<li>Comfy beds</li>\r\n</ul>\r\n\r\n<p><em>Beds from $49 USD a night, rooms from $125 USD.</em></p>\r\n', '3938_1600079482_sfhostel4.jpg', NULL, '8', '', 1, 2, 2, 2, 7, '12', 'THE 5 BEST HOSTELS IN SAN FRANCISCO', 'travel', 'THE 5 BEST HOSTELS IN SAN FRANCISCO', '14-September-2020, 3:58 pm', '2020-09-14 10:31:22');

-- --------------------------------------------------------

--
-- Table structure for table `blog_category`
--

CREATE TABLE `blog_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `url_slug` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keyword` text NOT NULL,
  `status` int(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_category`
--

INSERT INTO `blog_category` (`category_id`, `category_name`, `url_slug`, `meta_title`, `meta_description`, `meta_keyword`, `status`, `time`) VALUES
(1, 'Health', 'health-1', 'Health Title And More', 'Health Title And More .', 'Health Title And More', 1, '2021-04-22 10:59:10'),
(2, 'Beauty', 'beauty', 'Beauty', 'Beauty', 'Beauty', 1, '2021-04-22 09:51:07'),
(3, 'Technology', 'technology', 'Technology', 'Technology', 'Technology', 1, '2020-08-25 10:01:06'),
(5, 'Creativity', 'creativity-1', 'Creativity', 'Creativity', 'Creativity', 1, '2020-08-25 08:15:57'),
(8, 'Travel', 'travel-1', 'Travel', 'Travel', 'Travel', 1, '2021-04-22 09:38:26');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `comment_id` int(11) NOT NULL,
  `blog_id` int(255) NOT NULL,
  `comment` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `varified` int(255) DEFAULT NULL,
  `notification` int(255) DEFAULT NULL,
  `date_time` varchar(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`comment_id`, `blog_id`, `comment`, `name`, `email`, `varified`, `notification`, `date_time`, `time`) VALUES
(24, 9, 'sdsdsds', 'NISHANT KUMAR NIRALA', 'hello@gmai.com', NULL, 1, '14-August-2020, 11:01 pm', '2020-09-15 09:18:30'),
(44, 148, 'demo', 'php', 'niralanishant@123gmail.com', NULL, 1, '17-August-2020, 2:39 pm', '2020-09-15 09:18:33'),
(45, 143, 'tik tok is very good app \r\n', 'Shindhu Kumari', 'hello@gmai.com', 1, 1, '18-August-2020, 8:25 pm', '2020-09-15 09:18:35'),
(57, 149, 'Here\'s a look at the new features introduced in Google Docs, Sheets, and Slides\r\n', 'Shindhu Kumari', 'niralanishant331@gmail.com', 1, 1, '20-August-2020, 10:16 pm', '2020-09-15 09:18:37'),
(59, 142, 'demo', 'NISHANT KUMAR NIRALA', 'niralanishant331@gmail.com', NULL, 1, '28-August-2020, 11:13 am', '2020-09-15 09:18:40'),
(60, 142, 'sdsd', 'NISHANT KUMAR NIRALA', 'niralanishant331@gmail.com', 1, 1, '28-August-2020, 11:14 am', '2020-09-15 09:18:42'),
(61, 142, 'sds', 'php', 'niralanishant331@gmail.com', NULL, 1, '28-August-2020, 11:14 am', '2020-09-15 09:18:45'),
(62, 142, 'dsds', 'nishant nirala', 'niralanishant331@gmail.com', 2, 1, '28-August-2020, 11:24 am', '2020-09-15 09:18:49'),
(63, 142, 'asa', 'NISHANT KUMAR NIRALA', 'niralanishant331@gmail.com', 1, 1, '28-August-2020, 11:25 am', '2020-09-15 09:18:51'),
(66, 150, 'demo', 'php', 'hello123@gmail.com', 1, 1, '28-August-2020, 12:54 pm', '2020-09-15 07:44:37'),
(68, 150, 'demo', 'nishant nirala', 'niralanishant331@gmail.com', 2, 1, '29-August-2020, 9:19 am', '2020-09-15 10:17:12'),
(69, 149, 'demo', 'Manish Kumar', 'manish123@gmail.com', 1, 1, '30-August-2020, 8:24 am', '2020-09-15 07:38:18'),
(70, 149, 'demo', 'NISHANT KUMAR NIRALA', 'niralanishant331@gmail.com', 1, 1, '30-August-2020, 8:27 am', '2020-09-15 10:17:10'),
(71, 148, 'ddsds', 'nishant', 'hello123@gmail.com', NULL, 1, '7-September-2020, 8:59 pm', '2020-09-15 06:55:00'),
(72, 148, 'ddsds', 'nishant', 'hello123@gmail.com', NULL, 1, '7-September-2020, 8:59 pm', '2020-09-15 10:17:07'),
(74, 140, 'waas', 'NISHANT KUMAR NIRALA', 'niralanishant331@gmail.com', 1, 1, '7-September-2020, 9:01 pm', '2020-09-15 06:54:56'),
(75, 140, 'ASA', 'NISHANT KUMAR NIRALA', 'niralanishant@123gmail.com', NULL, 1, '7-September-2020, 9:02 pm', '2020-09-15 08:56:14'),
(76, 146, 'askjsaj', 'NISHANT KUMAR NIRALA', 'niralanishant331@gmail.com', 1, 1, '8-September-2020, 7:21 pm', '2020-09-15 10:17:04'),
(77, 149, 'asda', 'nishant nirala', 'hello123@gmail.com', NULL, 1, '9-September-2020, 3:11 pm', '2020-09-15 08:19:23'),
(78, 149, 'TEST', 'TEST', 'niralanishant@123gmail.com', NULL, 1, '9-September-2020, 3:18 pm', '2020-09-15 10:17:01'),
(79, 149, 'JKJKKJH', 'php', 'hello@gmai.com', NULL, 1, '9-September-2020, 3:19 pm', '2020-09-15 07:37:18'),
(80, 149, 'VNVNBVNBV', 'nishant nirala', 'niralanishant@123gmail.com', 1, 1, '9-September-2020, 3:20 pm', '2020-09-15 10:38:25'),
(81, 149, 'JKJKH', 'nishant nirala', 'niralanishant331@gmail.com', NULL, 1, '9-September-2020, 3:21 pm', '2020-09-15 10:38:22'),
(82, 140, 'i love FORTNITE game , and nice blog , thank you so much ', 'Rajnandan kumar', 'rajnandan123@gmail.com', 0, 1, '12-September-2020, 5:32 pm', '2020-09-15 10:38:20'),
(83, 9, 'im monu , i love blog', 'monu ', 'monu123@gmail.com', NULL, 1, '15-September-2020, 2:47 pm', '2020-09-15 13:02:44'),
(84, 9, 'sSaasa', 'sonu', 'sonu12@sdj.lskdj', NULL, 1, '15-September-2020, 3:27 pm', '2020-09-15 10:27:32'),
(85, 9, 'i love blog', 'shweta kumari', 'shweta123@gmail.com', NULL, 1, '15-September-2020, 6:33 pm', '2020-09-18 16:08:11'),
(86, 155, 'sdfdsfsd', 'NISHANT KUMAR NIRALA', 'hello@gmai.com', NULL, 1, '18-September-2020, 8:15 pm', '2020-09-18 16:08:18'),
(87, 150, 'im anuj', 'anuj ', 'hello123@gmail.com', NULL, 1, '18-September-2020, 9:37 pm', '2020-09-18 16:14:42'),
(88, 150, 'sadas', 'hy', 'hello@gmai.com', NULL, 1, '18-September-2020, 9:37 pm', '2020-09-18 16:16:05'),
(89, 12, 'zwsxfcb', 'php', 'hello@gmai.com', 1, 1, '18-September-2020, 9:58 pm', '2020-09-18 16:32:41'),
(90, 11, 'dcvdsfdsfds', 'anuj', 'anuj@gmai.com', NULL, 1, '23-September-2020, 12:52 pm', '2020-11-18 07:31:23'),
(91, 152, 'try', 'NISHANT KUMAR NIRALA', 'hello@gmai.com', 2, 1, '23-November-2020, 12:51 pm', '2020-11-23 07:23:48'),
(92, 148, 'lsak', 'dm', 'dm12@gmail.com', NULL, 0, '19-March-2021, 8:02 am', '2021-03-19 02:34:30'),
(93, 149, 'ksjdfhksj', 'NISHANT KUMAR NIRALA', 'niralanishant@123gmail.com', NULL, 0, '19-March-2021, 10:45 am', '2021-03-19 05:15:40'),
(94, 149, 'asldjal', 'NISHANT KUMAR NIRALA', 'niralanishant@123gmail.com', NULL, 0, '19-March-2021, 10:50 am', '2021-03-19 05:20:47'),
(95, 148, 'd;lls', 'honey', 'hello123@gmail.com', NULL, 0, '19-March-2021, 11:08 am', '2021-03-19 05:38:59'),
(96, 156, 'nice blog', 'NISHANT KUMAR NIRALA', 'hello@gmai.com', NULL, 0, '19-March-2021, 11:51 am', '2021-03-19 06:21:44'),
(97, 156, 'dksjh', 'php', 'hello@gmai.com', NULL, 0, '19-March-2021, 11:51 am', '2021-03-19 06:21:59'),
(98, 156, 'awsm blog nice ', 'sonu', 'sonu123@gmail.com', NULL, 0, '19-March-2021, 11:54 am', '2021-03-19 06:24:53'),
(99, 10, 'sadas', 'nishant nirala', 'hello@gmai.com', NULL, 0, '31-March-2021, 12:02 pm', '2021-03-31 06:32:46'),
(100, 153, 'hello world !', 'Sonu Kumar', 'sonukumarbgp3498@gmail.com', NULL, 0, '7-May-2021, 8:23 pm', '2021-05-07 14:55:52');

-- --------------------------------------------------------

--
-- Table structure for table `comment_history`
--

CREATE TABLE `comment_history` (
  `history_id` int(11) NOT NULL,
  `comment_id` int(255) NOT NULL,
  `status` int(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment_history`
--

INSERT INTO `comment_history` (`history_id`, `comment_id`, `status`, `time`) VALUES
(1, 149, 0, '2020-08-31 03:29:55');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`) VALUES
(1, 'USA'),
(2, 'Canada'),
(3, 'Australia'),
(4, 'India');

-- --------------------------------------------------------

--
-- Table structure for table `details`
--

CREATE TABLE `details` (
  `details` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `gmail` varchar(255) DEFAULT NULL,
  `profile_img` varchar(255) DEFAULT NULL,
  `pdf` varchar(255) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `map` text,
  `text` text,
  `about_you` varchar(255) DEFAULT NULL,
  `fb` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `whatsapp` varchar(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `details`
--

INSERT INTO `details` (`details`, `name`, `phone`, `gmail`, `profile_img`, `pdf`, `address`, `country`, `city`, `map`, `text`, `about_you`, `fb`, `linkedin`, `twitter`, `instagram`, `whatsapp`, `time`) VALUES
(1, 'Admin', '01234567801', 'admin1234@gmail.com', '3096_1600446223_profileimgs.jpg', 'https://drive.google.com/file/d/0B_Bf56Yp8VAQeUNqWkUyYVVvMTA/view?usp=sharing', 'xxx xxxxxxxx yyyyyyyyyyyyyy ZZZZZZZZZZZZZZZ 000 ,723627 ', 'India', 'Delhi', '                      <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d66599.51351325722!2d77.25944207117419!3d28.683460661162044!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cfd06212b4c29%3A0xc9d99da2f09b9325!2sKashmiri%20Gate!5e0!3m2!1sen!2sin!4v1597828641474!5m2!1sen!2sin\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>                       ', 'Make the word count for the online version of a given topic about half the word count used when writing for print: Users find it painful to read too much text on screens, and they read about 25 percent more slowly from screens than from paper. Users don\'t like to scroll through masses of text, so put the most important information at the top. Start the page with the conclusion as well as a short summary of the remaining contents (\"inverted pyramid\" style).\r\n\r\n', '      I\'m Admin', 'https://www.facebook.com', 'https://www.linkedin.com/in/', 'https://twitter.com', 'https://www.instagram.com', 'https://wa.me', '2021-09-06 20:52:51');

-- --------------------------------------------------------

--
-- Table structure for table `details_info`
--

CREATE TABLE `details_info` (
  `details` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `gmail` varchar(255) NOT NULL,
  `pdf` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `map` text NOT NULL,
  `text` text NOT NULL,
  `about_you` varchar(255) NOT NULL,
  `fb` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `whatsapp` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `details_info`
--

INSERT INTO `details_info` (`details`, `name`, `phone`, `gmail`, `pdf`, `address`, `country`, `city`, `map`, `text`, `about_you`, `fb`, `linkedin`, `twitter`, `instagram`, `whatsapp`, `time`) VALUES
(1, 'NISHANT KUMAR NIRALA', '462873346728', 'demo1234@gmail.com', 'demo', 'demo', 'India', 'Delhi', '    sdhfghjg              ', 'Make the word count for the online version of a given topic about half the word count used when writing for print: Users find it painful to read too much text on screens, and they read about 25 percent more slowly from screens than from paper. Users don\'t like to scroll through masses of text, so put the most important information at the top. Start the page with the conclusion as well as a short summary of the remaining contents (\"inverted pyramid\" style).\r\n', '    demo    ', 'demo', 'demo', 'demo', 'demo', 'demo', '2021-04-25 15:33:29');

-- --------------------------------------------------------

--
-- Table structure for table `inquiry`
--

CREATE TABLE `inquiry` (
  `inquiry_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` varchar(500) NOT NULL,
  `status` int(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inquiry`
--

INSERT INTO `inquiry` (`inquiry_id`, `name`, `email`, `phone`, `subject`, `message`, `status`, `time`) VALUES
(35, 'php', 'hello@gmai.com', '7703865824', 'php development', 'demo', 2, '2020-08-20 03:39:18'),
(38, 'NISHANT KUMAR NIRALA', 'hello@gmai.com', '7703865824', 'php development', 'demo', 2, '2020-08-31 16:33:50');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `like_id` int(11) NOT NULL,
  `blog_id` int(255) NOT NULL,
  `likes` int(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login_history`
--

CREATE TABLE `login_history` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_history`
--

INSERT INTO `login_history` (`id`, `email`, `time`, `date`, `ip`, `date_time`, `password`) VALUES
(1, 'niralanishant331@gmail.com', '1:27 pm', '31-March-2021', '::1', '2021-03-31 11:27:50', '1234'),
(2, 'niralanishant331@gmail.com', '1:44 pm', '31-March-2021', '::1', '2021-03-31 11:44:56', '1234'),
(3, 'niralanishant331@gmail.com', '1:46 pm', '31-March-2021', '::1', '2021-03-31 11:46:25', '1234'),
(4, 'niralanishant331@gmail.com', '1:48 pm', '31-March-2021', '::1', '2021-03-31 11:58:02', '1234'),
(5, 'niralanishant331@gmail.com', '1:59 pm', '31-March-2021', '::1', '2021-03-31 11:59:48', '1234'),
(6, 'niralanishant331@gmail.com', '2:02 pm', '31-March-2021', '::1', '2021-03-31 12:03:06', '1234'),
(7, 'niralanishant800@gmail.com', '2:49 pm', '31-March-2021', '::1', '2021-03-31 12:49:30', '1234'),
(8, 'niralanishant331@gmail.com', '2:50 pm', '31-March-2021', '::1', '2021-03-31 12:50:43', '1234'),
(9, 'niralanishant331@gmail.com', '2:51 pm', '31-March-2021', '::1', '2021-03-31 12:51:30', '1234'),
(10, 'niralanishant800@gmail.com', '2:53 pm', '31-March-2021', '::1', '2021-03-31 12:53:59', '1234'),
(11, 'niralanishant331@gmail.com', '3:00 pm', '31-March-2021', '::1', '2021-03-31 13:01:02', '1234'),
(12, 'niralanishant331@gmail.com', '3:01 pm', '31-March-2021', '::1', '2021-03-31 13:01:47', '1234'),
(13, 'sonu123@gmail.com', '3:02 pm', '31-March-2021', '::1', '2021-03-31 13:02:10', '1234'),
(14, 'niralanishant331@gmail.com', '3:02 pm', '31-March-2021', '::1', '2021-03-31 13:03:10', '1234'),
(15, 'sonu123@gmail.com', '3:04 pm', '31-March-2021', '::1', '2021-03-31 13:04:24', '1234'),
(16, 'niralanishant331@gmail.com', '6:45 pm', '31-March-2021', '::1', '2021-03-31 16:46:41', '1234'),
(17, 'niralanishant331@gmail.com', '7:15 pm', '31-March-2021', '::1', '2021-03-31 17:15:33', '1234'),
(18, 'niralanishant331@gmail.com', '5:36 pm', '2-April-2021', '::1', '2021-04-02 15:36:55', '1234'),
(19, 'niralanishant331@gmail.com', '12:34 pm', '3-April-2021', '::1', '2021-04-03 10:34:19', '1234'),
(20, 'niralanishant331@gmail.com', '12:40 pm', '3-April-2021', '127.0.0.1', '2021-04-03 10:40:21', '1234'),
(21, 'niralanishant331@gmail.com', '9:32 am', '5-April-2021', '::1', '2021-04-05 07:33:30', '1234'),
(22, 'niralanishant331@gmail.com', '11:26 am', '10-April-2021', '::1', '2021-04-10 09:27:25', '1234'),
(23, 'niralanishant331@gmail.com', '11:32 am', '10-April-2021', '127.0.0.1', '2021-04-10 09:32:31', '1234'),
(24, 'niralanishant331@gmail.com', '6:22 am', '12-April-2021', '::1', '2021-04-12 04:24:03', '1234'),
(25, 'niralanishant331@gmail.com', '7:08 am', '13-April-2021', '::1', '2021-04-13 05:09:36', '1234'),
(26, 'niralanishant331@gmail.com', '10:22 am', '13-April-2021', '::1', '2021-04-13 08:22:29', '1234'),
(27, 'niralanishant331@gmail.com', '7:55 am', '14-April-2021', '::1', '2021-04-14 05:56:06', '1234'),
(28, 'niralanishant331@gmail.com', '8:34 am', '14-April-2021', '::1', '2021-04-14 06:34:40', '1234'),
(29, 'niralanishant331@gmail.com', '5:17 pm', '20-April-2021', '::1', '2021-04-20 15:17:58', '1234'),
(30, 'niralanishant331@gmail.com', '4:23 pm', '21-April-2021', '::1', '2021-04-21 14:24:12', '1234'),
(31, 'niralanishant331@gmail.com', '9:58 am', '22-April-2021', '::1', '2021-04-22 07:58:38', '1234'),
(32, 'niralanishant331@gmail.com', '5:44 am', '25-April-2021', '::1', '2021-04-25 03:45:11', '1234'),
(33, 'niralanishant331@gmail.com', '10:52 am', '25-April-2021', '::1', '2021-04-25 08:52:16', '1234'),
(34, 'niralanishant331@gmail.com', '5:14 pm', '25-April-2021', '::1', '2021-04-25 15:15:12', '1234'),
(35, 'niralanishant331@gmail.com', '6:30 pm', '25-April-2021', '::1', '2021-04-25 16:30:24', '1234'),
(36, 'niralanishant331@gmail.com', '4:45 pm', '7-May-2021', '::1', '2021-05-07 14:46:00', '1234'),
(37, 'niralanishant331@gmail.com', '6:40 pm', '11-May-2021', '::1', '2021-05-11 16:40:52', '1234'),
(38, 'niralanishant331@gmail.com', '1:05 pm', '12-May-2021', '::1', '2021-05-12 11:05:48', '1234'),
(39, 'niralanishant331@gmail.com', '10:51 am', '18-May-2021', '::1', '2021-05-18 08:56:53', '1234'),
(40, 'niralanishant331@gmail.com', '10:12 am', '19-May-2021', '::1', '2021-05-19 08:13:00', '1234'),
(41, 'niralanishant331@gmail.com', '2:51 pm', '19-May-2021', '::1', '2021-05-19 09:21:51', '1234'),
(42, 'niralanishant331@gmail.com', '3:29 pm', '19-May-2021', '::1', '2021-05-19 09:59:15', '1234'),
(43, 'niralanishant331@gmail.com', '8:40 pm', '24-June-2021', '127.0.0.1', '2021-06-24 15:10:37', '1234'),
(44, 'niralanishant331@gmail.com', '8:24 pm', '25-June-2021', '127.0.0.1', '2021-06-25 14:54:49', '1234'),
(45, 'niralanishant331@gmail.com', '11:54 pm', '11-July-2021', '::1', '2021-07-11 18:25:37', '1234'),
(46, 'niralanishant331@gmail.com', '4:09 pm', '5-September-2021', '::1', '2021-09-05 10:39:27', '1234'),
(47, 'niralanishant331@gmail.com', '1:39 am', '7-September-2021', '::1', '2021-09-06 20:09:21', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `logo`
--

CREATE TABLE `logo` (
  `logo_id` int(11) NOT NULL,
  `logo_img` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logo`
--

INSERT INTO `logo` (`logo_id`, `logo_img`, `time`) VALUES
(1, '4429_1624547793_logo1.png', '2021-06-24 15:16:33');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `menu_name` varchar(255) NOT NULL,
  `menu_url` varchar(255) NOT NULL,
  `status` int(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_url`, `status`, `time`) VALUES
(181, 'Home', 'welcome/product', 1, '2020-07-25 11:24:17'),
(212, 'About Us', 'welcome/to', 1, '2020-07-25 10:19:56'),
(213, 'About Us', 'admin/index', NULL, '2020-08-18 07:55:14');

-- --------------------------------------------------------

--
-- Table structure for table `meta_data`
--

CREATE TABLE `meta_data` (
  `meta_id` int(11) NOT NULL,
  `title` varchar(5000) NOT NULL,
  `description` text NOT NULL,
  `keywords` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `robots` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meta_data`
--

INSERT INTO `meta_data` (`meta_id`, `title`, `description`, `keywords`, `author`, `robots`, `time`) VALUES
(1, 'blogpost.com', ' health blog, health blog commenting sites, health blog india', ' health blog, health blog commenting sites, health blog india', 'Nishant', 'Index,Follow', '2021-03-25 15:32:48');

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `notes_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(555) NOT NULL,
  `star` int(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`notes_id`, `title`, `description`, `star`, `tags`, `date`, `time`) VALUES
(78, 'my computer', 'demo', 1, 'note-social', '2-September-2020, 3:45 pm', '2020-09-06 04:57:16'),
(83, 'my name', 'my name is rajnandan kumar', NULL, 'note-important', '5-September-2020, 9:31 pm', '2020-09-05 16:01:50'),
(85, 'new project ', 'i will create new project in machine learning and ai development ', 2, 'note-important', '13-September-2020, 9:11 am', '2020-09-18 16:12:13');

-- --------------------------------------------------------

--
-- Table structure for table `others`
--

CREATE TABLE `others` (
  `others_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `others`
--

INSERT INTO `others` (`others_id`, `name`, `img`, `time`) VALUES
(6, 'NISHANT KUMAR NIRALA1', '3847_1598613066_login-bg.png', '2020-08-28 12:48:54'),
(8, 'jhds', '3041_1598618904_4206_1588369460_COVID_Scientist.jpg', '2020-08-28 12:48:24'),
(9, 'NISHANT KUMAR NIRALA', '4842_1598619616_1387_1597052324_2176_1588972031_deepak_1507896942.jpg', '2020-08-28 13:01:43'),
(10, 'nishant nirala', '1175_1598619624_1370_1597647449_telegram_video__1597645548023.jpg', '2020-08-28 13:01:51'),
(11, 'jhds', '3867_1598619636_3827_1588370006_Father_Daughter_Project.jpg', '2020-08-28 13:01:59'),
(12, 'nishant nirala', '8332_1598619787_7234_1597481486_4631_1595345175_509938.jpg', '2020-08-28 13:03:15'),
(13, 'jhds', '1121_1598670941_1486_1588370726_home-woman-meditation-eyes-closed.jpg', '2020-08-29 03:15:41'),
(14, 'nishant nirala', '4995_1598670956_5605_1597647593_arogya_setue_reuters_full_1590040591641.jpg', '2020-08-29 03:15:56'),
(15, 'jhds', '3431_1598671116_xl-2018-ai-brain-1.jpg', '2020-08-29 03:18:36'),
(16, 'fb images', '6763_1599321593_3679_1597479069_facebook.png', '2020-09-05 15:59:53'),
(18, 'demo', '8343_1600076439_4509_1593343917_oneplus-8-pro.jpg', '2020-09-14 09:40:39'),
(19, 'nishant', '3380_1600445572_2955_1600079100_maine6.jpg', '2020-09-18 16:12:52');

-- --------------------------------------------------------

--
-- Table structure for table `pap`
--

CREATE TABLE `pap` (
  `pap_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pap`
--

INSERT INTO `pap` (`pap_id`, `text`, `time`) VALUES
(1, '<p>demo21</p>\r\n', '2020-08-27 11:53:08');

-- --------------------------------------------------------

--
-- Table structure for table `preloader`
--

CREATE TABLE `preloader` (
  `preloader_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `preloader`
--

INSERT INTO `preloader` (`preloader_id`, `image`, `time`) VALUES
(1, 'loading.gif', '2020-06-20 10:46:05');

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE `reply` (
  `reply_id` int(11) NOT NULL,
  `comment_id` int(255) DEFAULT NULL,
  `text` text,
  `status` int(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reply`
--

INSERT INTO `reply` (`reply_id`, `comment_id`, `text`, `status`, `time`) VALUES
(39, 72, 'nishant', NULL, '2020-08-31 06:58:20'),
(44, 71, 'ssadasa', 1, '2020-08-31 08:20:22'),
(45, 72, 'ssadasa', 1, '2020-08-31 08:20:37'),
(46, 70, 'nishant', 1, '2020-08-31 08:20:49'),
(49, 66, 'demo', 1, '2020-08-31 08:21:28'),
(59, 73, 'ssadasa', 1, '2020-08-31 08:54:34'),
(61, 70, 'ssadasa', 1, '2020-09-02 11:19:28'),
(62, 69, 'nishant', 1, '2020-09-06 04:57:51'),
(65, 70, 'ssada', 1, '2020-09-07 15:35:37'),
(67, 82, 'try', 1, '2020-09-14 09:35:18'),
(68, 85, 'thank you so much ', 1, '2020-09-15 13:04:19'),
(69, 86, 'asdas', 1, '2020-09-18 16:08:22'),
(70, 86, 'ssada', 1, '2021-02-18 11:46:48');

-- --------------------------------------------------------

--
-- Table structure for table `signup`
--

CREATE TABLE `signup` (
  `signup_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `signup`
--

INSERT INTO `signup` (`signup_id`, `name`, `email`, `password`, `time`) VALUES
(1, 'Admin', 'admin1234@gmail.com', '1234', '2021-09-06 20:49:47'),
(2, 'Test', 'Test1234@gmail.com', '1234', '2021-09-06 20:50:41'),
(3, 'Hello', 'hello123@gmail.com', '123456', '2021-03-31 12:43:55'),
(4, 'Demo', 'demo123@gmail.com', '1234', '2021-09-06 20:50:13');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_name` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `country_id`, `state_name`) VALUES
(1, 1, 'New York'),
(2, 1, 'Alabama'),
(3, 1, 'California'),
(4, 2, 'Ontario'),
(5, 2, 'British Columbia'),
(6, 3, 'New South Wales'),
(7, 3, 'Queensland'),
(8, 4, 'Karnataka'),
(9, 4, 'Telangana');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL,
  `category_id` int(255) NOT NULL,
  `sub_categories_name` varchar(255) NOT NULL,
  `url_slug` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keyword` text NOT NULL,
  `status` int(255) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category_id`, `sub_categories_name`, `url_slug`, `meta_title`, `meta_description`, `meta_keyword`, `status`, `time`) VALUES
(2, 3, 'Mobile', 'mobile', 'news', 'news', 'news', 1, '2021-09-06 20:12:41'),
(3, 5, 'Maths', 'maths', 'Maths', 'sdf', 'sdf', 1, '2021-09-06 20:14:11'),
(4, 5, 'emotional creativity', 'emotional-creativity', 'emotional creativity', 'emotional creativity', 'emotional creativity', 1, '2021-09-06 20:15:58'),
(5, 3, 'Spontaneous ', 'spontaneous', 'Spontaneous ', 'Spontaneous ', 'Spontaneous ', 1, '2021-09-06 20:16:18'),
(7, 3, 'Tech Movies ', 'tech-movies', 'Tech Movies ', 'Tech Movies ', 'Tech Movies ', 1, '2021-09-06 20:16:52'),
(8, 3, 'Tech Books', 'tech-books', 'Tech Books', 'Tech Books', 'Tech Books', 1, '2021-09-06 20:17:10'),
(9, 2, 'Lakme Makeup', 'lakme-makeup', 'Lakme', 'Lakme', 'Lakme', 1, '2021-09-06 20:18:29'),
(10, 2, 'Hair care', 'hair-care', 'Hair care', 'Hair care', 'Hair care', 1, '2021-09-06 20:18:58'),
(11, 3, 'Java Tools', 'java-tools', 'Tools', 'Tools', 'Tools', 1, '2021-09-06 20:19:27'),
(12, 1, 'Body care', 'body-care', 'Body care', 'Body care', 'Body care', 1, '2021-09-06 20:19:52'),
(13, 1, 'Body Hair ', 'body-hair', 'Body Hair ', 'Body Hair ', 'Body Hair ', 1, '2021-09-06 20:20:25');

-- --------------------------------------------------------

--
-- Table structure for table `tac`
--

CREATE TABLE `tac` (
  `tac_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tac`
--

INSERT INTO `tac` (`tac_id`, `text`, `time`) VALUES
(1, '<p>rdssffffsasay</p>\r\n', '2020-08-27 13:04:30');

-- --------------------------------------------------------

--
-- Table structure for table `view_counter`
--

CREATE TABLE `view_counter` (
  `id` int(11) NOT NULL,
  `blog_id` int(255) NOT NULL,
  `views` int(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `view_counter`
--

INSERT INTO `view_counter` (`id`, `blog_id`, `views`, `time`) VALUES
(69, 149, 45, '2021-09-06 15:01:22'),
(70, 155, 26, '2021-06-27 09:04:06'),
(71, 141, 34, '2021-09-06 14:56:47'),
(72, 11, 1, '2021-06-25 11:54:10'),
(73, 10, 2, '2021-06-26 13:28:24'),
(74, 156, 74, '2021-06-26 17:31:57'),
(75, 137, 8, '2021-06-26 17:17:31'),
(76, 150, 14, '2021-09-06 08:29:03'),
(77, 153, 8, '2021-06-26 17:58:30'),
(78, 147, 17, '2021-09-06 14:42:09'),
(79, 148, 115, '2021-09-05 18:04:26'),
(80, 154, 13, '2021-09-06 20:10:37'),
(81, 12, 1, '2021-09-05 15:29:18'),
(82, 15, 10, '2021-09-05 18:13:31'),
(83, 146, 74, '2021-09-06 09:59:04'),
(84, 140, 8, '2021-09-06 14:43:08'),
(85, 151, 1, '2021-09-06 16:07:43'),
(86, 9, 1, '2021-09-06 20:08:06'),
(87, 145, 7, '2021-09-06 20:35:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`about_id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `comment_history`
--
ALTER TABLE `comment_history`
  ADD PRIMARY KEY (`history_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `details`
--
ALTER TABLE `details`
  ADD PRIMARY KEY (`details`);

--
-- Indexes for table `details_info`
--
ALTER TABLE `details_info`
  ADD PRIMARY KEY (`details`);

--
-- Indexes for table `inquiry`
--
ALTER TABLE `inquiry`
  ADD PRIMARY KEY (`inquiry_id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`like_id`);

--
-- Indexes for table `login_history`
--
ALTER TABLE `login_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`logo_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `meta_data`
--
ALTER TABLE `meta_data`
  ADD PRIMARY KEY (`meta_id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`notes_id`);

--
-- Indexes for table `others`
--
ALTER TABLE `others`
  ADD PRIMARY KEY (`others_id`);

--
-- Indexes for table `pap`
--
ALTER TABLE `pap`
  ADD PRIMARY KEY (`pap_id`);

--
-- Indexes for table `preloader`
--
ALTER TABLE `preloader`
  ADD PRIMARY KEY (`preloader_id`);

--
-- Indexes for table `reply`
--
ALTER TABLE `reply`
  ADD PRIMARY KEY (`reply_id`);

--
-- Indexes for table `signup`
--
ALTER TABLE `signup`
  ADD PRIMARY KEY (`signup_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tac`
--
ALTER TABLE `tac`
  ADD PRIMARY KEY (`tac_id`);

--
-- Indexes for table `view_counter`
--
ALTER TABLE `view_counter`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `about_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `blog_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;
--
-- AUTO_INCREMENT for table `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `comment_history`
--
ALTER TABLE `comment_history`
  MODIFY `history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `details`
--
ALTER TABLE `details`
  MODIFY `details` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `details_info`
--
ALTER TABLE `details_info`
  MODIFY `details` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `inquiry`
--
ALTER TABLE `inquiry`
  MODIFY `inquiry_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `like_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `login_history`
--
ALTER TABLE `login_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `logo`
--
ALTER TABLE `logo`
  MODIFY `logo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;
--
-- AUTO_INCREMENT for table `meta_data`
--
ALTER TABLE `meta_data`
  MODIFY `meta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `notes_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `others`
--
ALTER TABLE `others`
  MODIFY `others_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `pap`
--
ALTER TABLE `pap`
  MODIFY `pap_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `preloader`
--
ALTER TABLE `preloader`
  MODIFY `preloader_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `reply`
--
ALTER TABLE `reply`
  MODIFY `reply_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `signup`
--
ALTER TABLE `signup`
  MODIFY `signup_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tac`
--
ALTER TABLE `tac`
  MODIFY `tac_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `view_counter`
--
ALTER TABLE `view_counter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
